import { isMedicineCovered } from "./medicine-covered";
import { medicineNotCovered } from "./medicine-not-covered";
import { someMedicinesNotCovered } from "./some-of-hrt-medicines-are-not-covered";
import { dateOfBirth } from "./date-of-birth";
import { ageEntitledToFreePrescriptions } from "./age-entitled-to-free-prescriptions";
import { name } from "./name";
import { prescriptionCountryName } from "./where-you-collect-your-prescriptions";
import { outsideEngland } from "./outside-england";
import { doYouStillWantToBuyHrtPpc } from "./do-you-want-to-continue";
import { postcode } from "./address/postcode";
import { selectAddress } from "./address/select-address";
import { address } from "./address/manual-address";
import { existingExemptionFound } from "./existing-exemption-found";
import { doYouKnowYourNhsNumber } from "./do-you-know-your-nhs-number";
import { certificateStartDate } from "./certificate-start-date";
import { emailAddress } from "./email-address";
import { registerCheckAnswersRoutes } from "./check-your-answers/check-your-answers";
import { registerProcessPaymentReturnRoute } from "./process-payment/process-on-payment-return";
import { registerApplicationCompleteRoute } from "./decision/application-complete";
import { cookies } from "./cookies";
import { cookieDeclaration } from "./cookie-declaration";
import { cookieConfirmation } from "./cookie-confirmation";
import { registerPaymentCancelled } from "./payment-cancelled/payment-cancelled";
import { registerPaymentDeclined } from "./payment-declined/payment-declined";
import { registerSessionTimeout } from "./session-timeout/session-timeout";
import { registerDownloadCertificateRoute } from "./decision/application-complete/download-certificate-route";

export {
  isMedicineCovered,
  medicineNotCovered,
  someMedicinesNotCovered,
  dateOfBirth,
  ageEntitledToFreePrescriptions,
  name,
  prescriptionCountryName,
  outsideEngland,
  doYouStillWantToBuyHrtPpc,
  postcode,
  selectAddress,
  address,
  existingExemptionFound,
  doYouKnowYourNhsNumber,
  certificateStartDate,
  emailAddress,
  registerPaymentCancelled,
  registerCheckAnswersRoutes,
  registerProcessPaymentReturnRoute,
  registerApplicationCompleteRoute,
  cookies,
  cookieDeclaration,
  cookieConfirmation,
  registerPaymentDeclined,
  registerSessionTimeout,
  registerDownloadCertificateRoute,
};
