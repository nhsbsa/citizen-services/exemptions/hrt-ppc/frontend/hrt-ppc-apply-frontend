import {
  isDateSixteenOrMoreYearsInThePast,
  isDateSixtyOrMoreYearsInThePast,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import { path } from "ramda";
import { behaviourForGet } from "./behaviour-for-get";

const pageContent = ({ translate }) => ({
  title: translate("ageEntitledToFreePrescriptions.title"),
  heading: translate("ageEntitledToFreePrescriptions.heading"),
  paragraphOne: translate("ageEntitledToFreePrescriptions.paragraphOne"),
  paragraphTwo: translate("ageEntitledToFreePrescriptions.paragraphTwo"),
  paragraphThree: translate("ageEntitledToFreePrescriptions.paragraphThree"),
  listItemOne: translate("ageEntitledToFreePrescriptions.listItemOne"),
  listItemTwo: translate("ageEntitledToFreePrescriptions.listItemTwo"),
  previous: false,
});

const isNavigable = (req, session) =>
  !isDateSixteenOrMoreYearsInThePast(path(["claim", "dateOfBirth"], session)) ==
    true ||
  isDateSixtyOrMoreYearsInThePast(path(["claim", "dateOfBirth"], session)) ==
    true;

const ageEntitledToFreePrescriptions = {
  path: "/age-entitled-to-free-prescriptions",
  template: "age-entitled-to-free-prescriptions",
  isNavigable,
  pageContent,
  behaviourForGet,
};

export { ageEntitledToFreePrescriptions, pageContent, isNavigable };
