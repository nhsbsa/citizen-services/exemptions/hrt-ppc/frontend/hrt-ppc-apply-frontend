import expect from "expect";
import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";
import { stateMachine } from "../../flow-control/state-machine";
import { states } from "../../flow-control";
import { behaviourForGet } from "./behaviour-for-get";
import { REQUEST_ID_HEADER } from "../../../../server/headers";

const journey = {
  name: "apply",
};

test("behaviourForGet() should set the journey STATE TO COMPLETED", () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: "apply",
        state: states.IN_PROGRESS,
        nextAllowedPath: "/next-path",
      }),
    },
    headers: {
      [REQUEST_ID_HEADER]: "123456",
    },
  };

  const config = {};
  const res = {};
  const next = jest.fn;

  behaviourForGet(config, journey)(req, res, next);

  expect(stateMachine.getState(req, journey)).toEqual(states.COMPLETED);
});
