import { dateAsString } from "@nhsbsa/health-charge-exemption-common-frontend";
import expect from "expect";
import { isNavigable, pageContent } from "./age-entitled-to-free-prescriptions";

test(`isNavigable() returns true if age is more than sixty years`, () => {
  const session = {
    claim: {
      dateOfBirth: "1955-05-30",
    },
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(true);
});

test(`isNavigable() returns true if age equals to sixty years`, () => {
  const sixtyYearsAgo = dateAsString({
    yearAdjustment: -60,
  });

  const session = {
    claim: {
      dateOfBirth: sixtyYearsAgo,
    },
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(true);
});

test(`isNavigable() returns true if age is less than sixteen years`, () => {
  const fifteenYearsElevenMonthsAgo = dateAsString({
    yearAdjustment: -15,
    monthAdjustment: -11,
  });

  const session = {
    claim: {
      dateOfBirth: fifteenYearsElevenMonthsAgo,
    },
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(true);
});

test(`isNavigable() returns false if age is equal to sixteen years`, () => {
  const sixteenYearsAgo = dateAsString({
    yearAdjustment: -16,
  });

  const session = {
    claim: {
      dateOfBirth: sixteenYearsAgo,
    },
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(false);
});

test(`isNavigable() returns false if age is more than or equals to sixteen years and less than sixty years`, () => {
  const moreThenSixteenAndLessThenSixtyYearsAgo = dateAsString({
    yearAdjustment: -40,
    monthAdjustment: -11,
  });

  const session = {
    claim: {
      dateOfBirth: moreThenSixteenAndLessThenSixtyYearsAgo,
    },
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(false);
});

test("pageContent() should match expected translated values", () => {
  const translate = (string, object?) => `${string}${object}`;
  const result = pageContent({ translate });

  const expected = {
    title: translate("ageEntitledToFreePrescriptions.title"),
    heading: translate("ageEntitledToFreePrescriptions.heading"),
    paragraphOne: translate("ageEntitledToFreePrescriptions.paragraphOne"),
    paragraphTwo: translate("ageEntitledToFreePrescriptions.paragraphTwo"),
    paragraphThree: translate("ageEntitledToFreePrescriptions.paragraphThree"),
    listItemOne: translate("ageEntitledToFreePrescriptions.listItemOne"),
    listItemTwo: translate("ageEntitledToFreePrescriptions.listItemTwo"),
    previous: false,
  };

  expect(result).toEqual(expected);
});
