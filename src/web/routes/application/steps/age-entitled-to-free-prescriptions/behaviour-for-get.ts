import { stateMachine } from "../../flow-control/state-machine";
import { COMPLETED } from "../../flow-control/states";

const behaviourForGet = (config, journey) => (req, res, next) => {
  stateMachine.setState(COMPLETED, req, journey);
  next();
};

export { behaviourForGet };
