import * as validator from "validator";
import { check } from "express-validator";
import { isEmpty } from "ramda";
import {
  pastPermittedStartDateRange,
  futurePermittedStartDateRange,
  translateValidationMessage,
  toDateString,
  isDateRealDate,
  isValidString,
  datePeriodCheck,
  validateCertificateStartDateIsEmpty,
  validateCertificateStartDate as validateCertificateStartDateCommon,
  validateCertificateStartDateDay,
  validateCertificateStartDateMonth,
  validateCertificateStartDateYear,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import {
  CERT_START_DATE_DAY,
  CERT_START_DATE_MONTH,
  CERT_START_DATE_YEAR,
  CERT_BACK_DATE_DAY,
  CERT_BACK_DATE_MONTH,
  CERT_BACK_DATE_YEAR,
  CERT_START_OPTION,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";
import { POSITIVE_INTEGER_PATTERN } from "@nhsbsa/health-charge-exemption-common-frontend";
import { YES, NO } from "../common/constants";

function addFutureDateToBody(req) {
  req.body.certificateStartDate = toDateString(
    req.body[CERT_START_DATE_DAY],
    req.body[CERT_START_DATE_MONTH],
    req.body[CERT_START_DATE_YEAR],
  );
}

function cleanPastDateFields(req) {
  delete req.body[CERT_BACK_DATE_DAY];
  delete req.body[CERT_BACK_DATE_MONTH];
  delete req.body[CERT_BACK_DATE_YEAR];
}

function addPastDateToBody(req) {
  req.body.certificateBackDate = toDateString(
    req.body[CERT_BACK_DATE_DAY],
    req.body[CERT_BACK_DATE_MONTH],
    req.body[CERT_BACK_DATE_YEAR],
  );
}

function cleanFutureDateFields(req) {
  delete req.body[CERT_START_DATE_DAY];
  delete req.body[CERT_START_DATE_MONTH];
  delete req.body[CERT_START_DATE_YEAR];
}

const addDateToBody = (req, res, next) => {
  if (req.body[CERT_START_OPTION] === YES) {
    addFutureDateToBody(req);
    cleanPastDateFields(req);
  } else if (req.body[CERT_START_OPTION] === NO) {
    addPastDateToBody(req);
    cleanFutureDateFields(req);
  }
  next();
};

const validateCertificateStartDate = (certificateStartDate, { req }) => {
  const permittedStartDateRange = futurePermittedStartDateRange(new Date());
  return validateCertificateStartDateCommon(
    certificateStartDate,
    permittedStartDateRange,
    { req },
  );
};

const validateCertificateBackDate = (certBackDate, { req }) => {
  if (
    !isEmpty(req.body[CERT_BACK_DATE_DAY]) &&
    !isEmpty(req.body[CERT_BACK_DATE_MONTH]) &&
    !isEmpty(req.body[CERT_BACK_DATE_YEAR])
  ) {
    if (!isDateRealDate(certBackDate)) {
      throw new Error(req.t("validation:startDateNotReal"));
    }
    if (!isValidString(certBackDate)) {
      throw new Error(req.t("validation:startDateNotReal"));
    }
    if (
      !validator.matches(
        req.body[CERT_BACK_DATE_DAY],
        POSITIVE_INTEGER_PATTERN,
      ) ||
      !validator.matches(
        req.body[CERT_BACK_DATE_MONTH],
        POSITIVE_INTEGER_PATTERN,
      ) ||
      !validator.matches(
        req.body[CERT_BACK_DATE_YEAR],
        POSITIVE_INTEGER_PATTERN,
      )
    ) {
      throw new Error(req.t("validation:startDateNotReal"));
    }
    datePeriodCheck(
      certBackDate,
      pastPermittedStartDateRange(new Date()),
      req,
      CERT_BACK_DATE_DAY,
      CERT_BACK_DATE_MONTH,
      CERT_BACK_DATE_YEAR,
    );
  }
  return true;
};

const validateCertificateBackDateDay = (certStartDate, { req }) => {
  if (
    isEmpty(req.body[CERT_BACK_DATE_DAY]) &&
    (!isEmpty(req.body[CERT_BACK_DATE_MONTH]) ||
      !isEmpty(req.body[CERT_BACK_DATE_YEAR]))
  ) {
    throw new Error(req.t("validation:missingStartDateDay"));
  }
  return true;
};

const validateCertificateBackDateMonth = (certStartDate, { req }) => {
  if (
    isEmpty(req.body[CERT_BACK_DATE_MONTH]) &&
    (!isEmpty(req.body[CERT_BACK_DATE_DAY]) ||
      !isEmpty(req.body[CERT_BACK_DATE_YEAR]))
  ) {
    throw new Error(req.t("validation:missingStartDateMonth"));
  }
  return true;
};

const validateCertificateBackDateYear = (certStartDate, { req }) => {
  if (
    isEmpty(req.body[CERT_BACK_DATE_YEAR]) &&
    (!isEmpty(req.body[CERT_BACK_DATE_DAY]) ||
      !isEmpty(req.body[CERT_BACK_DATE_MONTH]))
  ) {
    throw new Error(req.t("validation:missingStartDateYear"));
  }
  return true;
};

const validate = () => [
  addDateToBody,
  check(CERT_START_OPTION)
    .exists({ checkFalsy: true, checkNull: true })
    .withMessage(translateValidationMessage("validation:selectYourStartDate")),
  check("certificateStartDate")
    .custom(validateCertificateStartDateIsEmpty)
    .bail()
    .custom(validateCertificateStartDate)
    .optional(),
  check("certificateBackDate")
    .custom(validateCertificateStartDateIsEmpty)
    .bail()
    .custom(validateCertificateBackDate)
    .bail()
    .optional(),
  check(CERT_START_DATE_DAY).custom(validateCertificateStartDateDay),
  check(CERT_BACK_DATE_DAY).custom(validateCertificateBackDateDay),
  check(CERT_START_DATE_MONTH).custom(validateCertificateStartDateMonth),
  check(CERT_BACK_DATE_MONTH).custom(validateCertificateBackDateMonth),
  check(CERT_START_DATE_YEAR).custom(validateCertificateStartDateYear),
  check(CERT_BACK_DATE_YEAR).custom(validateCertificateBackDateYear),
];

export {
  addDateToBody,
  validateCertificateStartDate,
  validateCertificateBackDate,
  validateCertificateBackDateDay,
  validateCertificateBackDateMonth,
  validateCertificateBackDateYear,
  validateCertificateStartDateIsEmpty,
  validate,
};
