import {
  sanitizeStartDate,
  trimLeadingZerosAndReplaceSpaces,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import {
  CERT_BACK_DATE_DAY,
  CERT_BACK_DATE_MONTH,
  CERT_BACK_DATE_YEAR,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";

const sanitize = () => (req, res, next) => {
  trimLeadingZerosAndReplaceSpaces(req, CERT_BACK_DATE_DAY);
  trimLeadingZerosAndReplaceSpaces(req, CERT_BACK_DATE_MONTH);
  trimLeadingZerosAndReplaceSpaces(req, CERT_BACK_DATE_YEAR);
  sanitizeStartDate(req);
  next();
};

export { sanitize };
