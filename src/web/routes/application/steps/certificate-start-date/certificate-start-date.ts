import { validate } from "./validate";
import { sanitize } from "./sanitize";
import { CERTIFICATE_LIST } from "../check-your-answers/constants";
import { behaviourForPost } from "./behaviour-for-post";
import {
  DATE_FORMAT_FULL,
  entirePermittedStartDateRange,
  EXAMPLE_DATE_FORMAT,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import { YES } from "../common/constants";
import {
  CERT_BACK_DATE_DAY,
  CERT_BACK_DATE_MONTH,
  CERT_BACK_DATE_YEAR,
  CERT_START_DATE_DAY,
  CERT_START_DATE_MONTH,
  CERT_START_DATE_YEAR,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";
import {
  formatDateForDisplay,
  formatDateForDisplayFromDate,
} from "../common/formatters/dates";

const pageContent = ({ translate }) => {
  const { earliestPermittedStartDate, farthestPermittedStartDate } =
    entirePermittedStartDateRange(new Date());
  return {
    title: translate("certificateStartDate.title"),
    heading: translate("certificateStartDate.heading"),
    paragraphOne: translate("certificateStartDate.paragraphOne", {
      earliestDate: earliestPermittedStartDate.format(DATE_FORMAT_FULL),
      farthestDate: farthestPermittedStartDate.format(DATE_FORMAT_FULL),
    }),
    dateHintText: translate("certificateStartDate.dateHintText", {
      sampleDate: farthestPermittedStartDate.format(EXAMPLE_DATE_FORMAT),
    }),
    backDateHintText: translate("certificateStartDate.dateHintText", {
      sampleDate: earliestPermittedStartDate.format(EXAMPLE_DATE_FORMAT),
    }),
    dayLabel: translate("certificateStartDate.dayLabel"),
    monthLabel: translate("certificateStartDate.monthLabel"),
    yearLabel: translate("certificateStartDate.yearLabel"),
    optionStartTodayText: translate(
      "certificateStartDate.optionStartTodayText",
    ),
    optionStartTodayParagraphOne: translate(
      "certificateStartDate.optionStartTodayParagraphOne",
    ),
    optionStartTodayParagraphTwo: translate(
      "certificateStartDate.optionStartTodayParagraphTwo",
    ),
    optionStartTodayParagraphTwoLinkText: translate(
      "certificateStartDate.optionStartTodayParagraphTwoLinkText",
    ),
    optionBackDayText: translate("certificateStartDate.optionBackDayText"),
    optionBackDayParagraphOne: translate(
      "certificateStartDate.optionBackDayParagraphOne",
    ),
    optionBackDayParagraphTwo: translate(
      "certificateStartDate.optionBackDayParagraphTwo",
    ),
    optionBackDayParagraphThree: translate(
      "certificateStartDate.optionBackDayParagraphThree",
    ),
    buttonText: translate("buttons:continue"),
    cancel: translate("cancel"),
  };
};

const startDateContentSummaryId = "#certificate-start-date-day";
const endDateContentSummaryId = "#certificate-end-date";

const contentSummary = (req) => [
  {
    id: startDateContentSummaryId,
    key: req.t("certificateStartDate.summaryKey"),
    section: CERTIFICATE_LIST,
    value:
      req.session.claim["prescriptionStartFromToday"] == YES
        ? formatDateForDisplay(
            req.session.claim[CERT_START_DATE_DAY],
            req.session.claim[CERT_START_DATE_MONTH],
            req.session.claim[CERT_START_DATE_YEAR],
          )
        : formatDateForDisplay(
            req.session.claim[CERT_BACK_DATE_DAY],
            req.session.claim[CERT_BACK_DATE_MONTH],
            req.session.claim[CERT_BACK_DATE_YEAR],
          ),
    visuallyHidden: req.t("certificateStartDate.hiddenChangeText"),
  },
  {
    id: endDateContentSummaryId,
    key: req.t("certificateEndDate.summaryKey"),
    section: CERTIFICATE_LIST,
    value: formatDateForDisplayFromDate(
      new Date(req.session.claim["certificateEndDate"]),
    ),
    visuallyHidden: req.t("certificateEndDate.hiddenChangeText"),
  },
];

const requestBody = (session) => ({
  startDate:
    session.claim.certificateStartDate || session.claim.certificateBackDate,
  endDate: session.claim.certificateEndDate,
});

const certificateStartDate = {
  path: "/your-start-date",
  template: "certificate-start-date",
  pageContent,
  sanitize,
  validate,
  requestBody,
  behaviourForPost,
  contentSummary,
};

export {
  certificateStartDate,
  contentSummary,
  requestBody,
  pageContent,
  validate,
  behaviourForPost,
  sanitize,
};
