import { productClient } from "../../../../client/product-client";
import { validationResult } from "express-validator";
import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { NO, YES } from "../common/constants";
import {
  CERT_START_OPTION,
  CERT_START_DATE_DAY,
  CERT_START_DATE_MONTH,
  CERT_START_DATE_YEAR,
  CERT_BACK_DATE_DAY,
  CERT_BACK_DATE_MONTH,
  CERT_BACK_DATE_YEAR,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";

const behaviourForPost = (config) => async (req, res, next) => {
  try {
    if (!validationResult(req).isEmpty()) {
      return next();
    }

    cleanDateFromSession(req);

    const certificateStartDate =
      req.body.certificateStartDate || req.body.certificateBackDate;
    const response = await new productClient(config).makeRequest({
      method: "GET",
      url: `v1/products/HRT_12m/end-date?startDate=${certificateStartDate}`,
    });
    req.session.claim["certificateEndDate"] = response.data.endDate;
    next();
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      }),
    );
  }
};

const cleanDateFromSession = (req) => {
  if (req.body[CERT_START_OPTION] === YES) {
    cleanPastDateInSession(req);
  } else if (req.body[CERT_START_OPTION] === NO) {
    cleanFutureDateInSession(req);
  }
};

function cleanFutureDateInSession(req) {
  delete req.session.claim[CERT_START_DATE_DAY];
  delete req.session.claim[CERT_START_DATE_MONTH];
  delete req.session.claim[CERT_START_DATE_YEAR];
  delete req.session.claim.certificateStartDate;
}

function cleanPastDateInSession(req) {
  delete req.session.claim[CERT_BACK_DATE_DAY];
  delete req.session.claim[CERT_BACK_DATE_MONTH];
  delete req.session.claim[CERT_BACK_DATE_YEAR];
  delete req.session.claim.certificateBackDate;
}

export { behaviourForPost };
