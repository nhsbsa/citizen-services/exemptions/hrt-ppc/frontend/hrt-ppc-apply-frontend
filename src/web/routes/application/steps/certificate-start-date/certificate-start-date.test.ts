import expect from "expect";
import {
  behaviourForPost,
  certificateStartDate,
  contentSummary,
  pageContent,
  requestBody,
  sanitize,
  validate,
} from "./certificate-start-date";

let req;

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date());

  req = {
    t: (string) => string,
    session: {
      claim: {
        prescriptionStartFromToday: "yes",
        "certificateStartDate-day": "10",
        "certificateStartDate-month": "04",
        "certificateStartDate-year": "2023",
        certificateStartDate: "2023-04-10",
        certificateEndDate: "2024-04-10",
      },
    },
  };
});

test("Certificate start date contentSummary() should return content summary in correct format when prescription starts from today is YES", () => {
  const result = contentSummary(req);

  const expected = [
    {
      id: "#certificate-start-date-day",
      key: "certificateStartDate.summaryKey",
      section: "aboutCertificate",
      value: "10 April 2023",
      visuallyHidden: "certificateStartDate.hiddenChangeText",
    },
    {
      id: "#certificate-end-date",
      key: "certificateEndDate.summaryKey",
      section: "aboutCertificate",
      value: "10 April 2024",
      visuallyHidden: "certificateEndDate.hiddenChangeText",
    },
  ];

  expect(result).toEqual(expected);
});

test("Certificate start date contentSummary() should return content summary in correct format when prescription starts from today is NO", () => {
  req.session.claim.prescriptionStartFromToday = "no";
  req.session.claim["certificateBackDate-day"] = "06";
  req.session.claim["certificateBackDate-month"] = "03";
  req.session.claim["certificateBackDate-year"] = "2022";
  const result = contentSummary(req);

  const expected = [
    {
      id: "#certificate-start-date-day",
      key: "certificateStartDate.summaryKey",
      section: "aboutCertificate",
      value: "6 March 2022", // using different day/ month/ year to check code is not picking from 'certificateStartDate-*' fields
      visuallyHidden: "certificateStartDate.hiddenChangeText",
    },
    {
      id: "#certificate-end-date",
      key: "certificateEndDate.summaryKey",
      section: "aboutCertificate",
      value: "10 April 2024",
      visuallyHidden: "certificateEndDate.hiddenChangeText",
    },
  ];

  expect(result).toEqual(expected);
});

describe("Certificate start date requestBody() should return request body in correct format", () => {
  test("using certificateStartDate when provided", () => {
    const result = requestBody({
      claim: {
        certificateStartDate: "2023-04-10",
        certificateEndDate: "2024-04-10",
      },
    });

    const expected = {
      startDate: "2023-04-10",
      endDate: "2024-04-10",
    };

    expect(result).toEqual(expected);
  });

  test("using certificateBackDate when certificateStartDate not provided", () => {
    const result = requestBody({
      claim: {
        certificateBackDate: "2023-05-10",
        certificateEndDate: "2024-05-10",
      },
    });

    const expected = {
      startDate: "2023-05-10",
      endDate: "2024-05-10",
    };

    expect(result).toEqual(expected);
  });
});

test("pageContent() should match expected translated values", () => {
  jest.useFakeTimers().setSystemTime(new Date("2023-12-03"));

  const translate = (string, object?) => `${string}${JSON.stringify(object)}`;
  const result = pageContent({ translate });

  const expected = {
    title: translate("certificateStartDate.title"),
    heading: translate("certificateStartDate.heading"),
    paragraphOne: translate("certificateStartDate.paragraphOne", {
      earliestDate: "3 November 2023",
      farthestDate: "3 January 2024",
    }),
    dateHintText: translate("certificateStartDate.dateHintText", {
      sampleDate: "3 1 2024",
    }),
    backDateHintText: translate("certificateStartDate.dateHintText", {
      sampleDate: "3 11 2023",
    }),
    dayLabel: translate("certificateStartDate.dayLabel"),
    monthLabel: translate("certificateStartDate.monthLabel"),
    yearLabel: translate("certificateStartDate.yearLabel"),
    optionStartTodayText: translate(
      "certificateStartDate.optionStartTodayText",
    ),
    optionStartTodayParagraphOne: translate(
      "certificateStartDate.optionStartTodayParagraphOne",
    ),
    optionStartTodayParagraphTwo: translate(
      "certificateStartDate.optionStartTodayParagraphTwo",
    ),
    optionStartTodayParagraphTwoLinkText: translate(
      "certificateStartDate.optionStartTodayParagraphTwoLinkText",
    ),
    optionBackDayText: translate("certificateStartDate.optionBackDayText"),
    optionBackDayParagraphOne: translate(
      "certificateStartDate.optionBackDayParagraphOne",
    ),
    optionBackDayParagraphTwo: translate(
      "certificateStartDate.optionBackDayParagraphTwo",
    ),
    optionBackDayParagraphThree: translate(
      "certificateStartDate.optionBackDayParagraphThree",
    ),
    buttonText: translate("buttons:continue"),
    cancel: translate("cancel"),
  };

  expect(result).toEqual(expected);
});

test(`certificateStartDate should match expected outcomes`, () => {
  const expectedResults = {
    path: "/your-start-date",
    template: "certificate-start-date",
    pageContent,
    sanitize,
    contentSummary,
    requestBody,
    validate,
    behaviourForPost,
  };
  expect(certificateStartDate).toEqual(expectedResults);
});
