import expect from "expect";
import { behaviourForPost } from "./behaviour-for-post";
import { productClient } from "../../../../client/product-client";
import { wrapError } from "../../errors";
import * as httpStatus from "http-status-codes";
import {
  CERT_START_DATE_DAY,
  CERT_START_DATE_MONTH,
  CERT_START_DATE_YEAR,
  CERT_BACK_DATE_DAY,
  CERT_BACK_DATE_MONTH,
  CERT_BACK_DATE_YEAR,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";

const isEmpty = jest.fn();
const config = {
  environment: {
    OUTBOUND_API_TIMEOUT: 20000,
    PRODUCT_API_URI: "baseURI",
    PRODUCT_API_KEY: "test-key",
  },
};
const res = jest.fn();
const next = jest.fn();

jest.mock("../../../../client/product-client");
jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty,
  }),
}));

const resetStubs = () => {
  isEmpty.mockReset();
};

beforeEach(() => {
  jest.clearAllMocks();
});

const assertFutureDatesCleaned = (session) => {
  expect(session.claim["certificateStartDate-day"]).not.toBeDefined();
  expect(session.claim["certificateStartDate-month"]).not.toBeDefined();
  expect(session.claim["certificateStartDate-year"]).not.toBeDefined();
  expect(session.claim.certificateStartDate).not.toBeDefined();
};

const assertBackDatesCleaned = (session) => {
  expect(session.claim["certificateBackDate-day"]).not.toBeDefined();
  expect(session.claim["certificateBackDate-month"]).not.toBeDefined();
  expect(session.claim["certificateBackDate-year"]).not.toBeDefined();
  expect(session.claim.certificateBackDate).not.toBeDefined();
};

const sessionStartAndBackDates = {
  [CERT_START_DATE_DAY]: "01",
  [CERT_START_DATE_MONTH]: "04",
  [CERT_START_DATE_YEAR]: "2023",
  [CERT_BACK_DATE_DAY]: "02",
  [CERT_BACK_DATE_MONTH]: "03",
  [CERT_BACK_DATE_YEAR]: "2022",
  certificateStartDate: "2023-04-01",
  certificateBackDate: "2022-03-02",
};

describe("behaviourForPost()", () => {
  describe("should set certificateEndDate in session", () => {
    test.each([
      ["future", "certificateStartDate", assertBackDatesCleaned],
      ["past", "certificateBackDate", assertFutureDatesCleaned],
    ])(
      "using a %s date",
      async (period, submittedDatum, assertSessionDatesCleaned) => {
        const req = {
          body: {
            prescriptionStartFromToday: period === "future" ? "yes" : "no",
            [submittedDatum]: "2023-04-05",
          },
          session: {
            claim: {
              ...sessionStartAndBackDates,
            },
          },
        };
        isEmpty.mockReturnValue(true);
        const makeRequestMock = jest.fn();
        productClient.prototype.makeRequest = makeRequestMock;
        makeRequestMock.mockReturnValue(
          Promise.resolve({ data: { endDate: "2024-04-05" } }),
        );

        await behaviourForPost(config)(req, res, next);

        expect(productClient.prototype.makeRequest).toBeCalledWith({
          method: "GET",
          url: "v1/products/HRT_12m/end-date?startDate=2023-04-05",
        });
        expect(req.session.claim["certificateEndDate"]).toEqual("2024-04-05");
        assertSessionDatesCleaned(req.session);
        expect(next).toBeCalledTimes(1);
        resetStubs();
      },
    );
  });

  test("should not call productClient when there are validation errors ", async () => {
    // Set return values for stubs
    isEmpty.mockReturnValue(false);
    const makeRequestMock = jest.fn();
    productClient.prototype.makeRequest = makeRequestMock;
    makeRequestMock.mockReturnValue(
      Promise.resolve({ data: { endDate: "2024-04-05" } }),
    );

    const req = {
      path: "/test",
      body: {
        certificateStartDate: "2023-04-05",
      },
      session: {
        claim: {},
      },
    };
    const res = jest.fn();
    const next = jest.fn();

    await behaviourForPost(config)(req, res, next);

    expect(productClient).not.toBeCalled();
    expect(req.session.claim["certificateEndDate"]).not.toBeDefined();
    expect(next).toBeCalledTimes(1);
    resetStubs();
  });

  test("calls next with wrapped error when error is thrown", async () => {
    const req = {
      path: "/test",
      body: {
        certificateStartDate: "2023-04-05",
      },
      session: {
        claim: {
          ...sessionStartAndBackDates,
        },
      },
    };
    const res = jest.fn();
    const next = jest.fn();

    const expectedError = wrapError({
      cause: new Error("Axios error"),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
    });
    isEmpty.mockReturnValue(true);
    const makeRequestMock = jest.fn();
    productClient.prototype.makeRequest = makeRequestMock;
    makeRequestMock.mockReturnValue(Promise.reject(new Error("Axios error")));

    await behaviourForPost(config)(req, res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(expectedError);
    expect(req.session.claim["certificateEndDate"]).not.toBeDefined();
    expect(req.session.claim).toEqual(sessionStartAndBackDates);
    resetStubs();
  });

  test("calls next with wrapped error when response status is 200 but body is empty", async () => {
    const req = {
      path: "/test",
      body: {
        certificateStartDate: "2023-04-05",
      },
      session: {
        claim: {},
      },
    };
    const res = jest.fn();
    const next = jest.fn();

    const expectedError = wrapError({
      cause: "",
      message: `Error posting /test. TypeError: Cannot read properties of undefined (reading 'endDate')`,
      statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
    });
    isEmpty.mockReturnValue(true);
    const makeRequestMock = jest.fn();
    productClient.prototype.makeRequest = makeRequestMock;
    makeRequestMock.mockReturnValue({});

    await behaviourForPost(config)(req, res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(expectedError);
    expect(req.session.claim["certificateEndDate"]).not.toBeDefined();
    resetStubs();
  });
});
