import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend";
import expect from "expect";
import {
  validate,
  validateCertificateStartDate,
  validateCertificateBackDate,
  validateCertificateBackDateDay,
  validateCertificateBackDateMonth,
  validateCertificateBackDateYear,
  validateCertificateStartDateIsEmpty,
  addDateToBody,
} from "./validate";
import { FieldValidationError } from "express-validator";

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date());
});

test("validateCertificateStartDate()", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "yes",
      "certificateStartDate-day": "10",
      "certificateStartDate-month": "04",
      "certificateStartDate-year": "2023",
      "certificateBackDate-day": "03",
      "certificateBackDate-month": "01",
      "certificateBackDate-year": "2021",
    },
  };

  jest.useFakeTimers().setSystemTime(new Date("2023-03-22"));
  expect(validateCertificateStartDate("2023-04-10", { req })).toBe(true);
  expect(validateCertificateStartDate.bind(null, "", { req })).toThrowError(
    /validation:startDateNotReal/,
  );
  expect(
    validateCertificateStartDate.bind(null, "2023-02-29", { req }),
  ).toThrowError(/validation:startDateNotReal/);
  expect(
    validateCertificateStartDate.bind(null, "2023-02-299", { req }),
  ).toThrowError(/validation:startDateNotReal/);
  expect(
    validateCertificateStartDate.bind(null, "EEEE-EE-2E", { req }),
  ).toThrowError(/validation:startDateNotReal/);
  expect(
    validateCertificateStartDate.bind(null, "EE/10/2023", { req }),
  ).toThrowError(/validation:startDateNotReal/);
  expect(
    validateCertificateStartDate.bind(null, "10/EE/2023", { req }),
  ).toThrowError(/validation:startDateNotReal/);
  expect(
    validateCertificateStartDate.bind(null, "10/10/EEEE", { req }),
  ).toThrowError(/validation:startDateNotReal/);
});

test("validateCertificateBackDate()", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "no",
      "certificateStartDate-day": "10",
      "certificateStartDate-month": "04",
      "certificateStartDate-year": "2023",
      "certificateBackDate-day": "03",
      "certificateBackDate-month": "01",
      "certificateBackDate-year": "2021",
    },
  };
  expect(validateCertificateBackDate.bind(null, "", { req })).toThrowError(
    /validation:startDateNotReal/,
  );
  expect(
    validateCertificateBackDate.bind(null, "2023-02-29", { req }),
  ).toThrowError(/validation:startDateNotReal/);
  expect(
    validateCertificateBackDate.bind(null, "2023-02-299", { req }),
  ).toThrowError(/validation:startDateNotReal/);
  expect(
    validateCertificateBackDate.bind(null, "EEEE-EE-2E", { req }),
  ).toThrowError(/validation:startDateNotReal/);
  expect(
    validateCertificateBackDate.bind(null, "EE/10/2023", { req }),
  ).toThrowError(/validation:startDateNotReal/);
  expect(
    validateCertificateBackDate.bind(null, "10/EE/2023", { req }),
  ).toThrowError(/validation:startDateNotReal/);
  expect(
    validateCertificateBackDate.bind(null, "10/10/EEEE", { req }),
  ).toThrowError(/validation:startDateNotReal/);
});

describe("validateCertificateBackDate()", () => {
  test("throws an error for an invalid back date day which does not match pattern 0-9", () => {
    const req = {
      t: (string) => string,
      body: {
        "certificateBackDate-day": "-24",
        "certificateBackDate-month": "03",
        "certificateBackDate-year": "1997",
      },
    };

    expect(() =>
      validateCertificateBackDate("1997-03--24", {
        req,
      }),
    ).toThrowError(/validation:startDateNotReal/);
  });

  test("throws an error for an invalid back date month which does not match pattern 0-9", () => {
    const req = {
      t: (string) => string,
      body: {
        "certificateBackDate-day": "24",
        "certificateBackDate-month": "-03",
        "certificateBackDate-year": "1997",
      },
    };

    expect(() =>
      validateCertificateBackDate("1997--03-24", {
        req,
      }),
    ).toThrowError(/validation:startDateNotReal/);
  });

  test("throws an error for an invalid back date year which does not match pattern 0-9", () => {
    const req = {
      t: (string) => string,
      body: {
        "certificateBackDate-day": "24",
        "certificateBackDate-month": "-03",
        "certificateBackDate-year": "19 97-",
      },
    };

    expect(() =>
      validateCertificateBackDate("19 97--03-24", {
        req,
      }),
    ).toThrowError(/validation:startDateNotReal/);
  });
});

test("validateCertificateStartDateIsEmpty()", () => {
  const req = {
    t: (string) => string,
  };
  expect(
    validateCertificateStartDateIsEmpty.bind(null, "", { req }),
  ).toThrowError(/validation:missingStartDate/);
});

test("addDateToBody() should add date to certificateStartDate with 'yes' value for radio button and clear back date part", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "yes",
      "certificateStartDate-day": "10",
      "certificateStartDate-month": "04",
      "certificateStartDate-year": "2023",
      "certificateBackDate-day": "03",
      "certificateBackDate-month": "01",
      "certificateBackDate-year": "2021",
    },
  };
  const res = {};
  const next = jest.fn();

  addDateToBody(req, res, next);

  expect(req.body["certificateStartDate"]).toEqual("2023-04-10");
  expect(req.body["certificateBackDate-day"]).toBeUndefined();
  expect(req.body["certificateBackDate-month"]).toBeUndefined();
  expect(req.body["certificateBackDate-year"]).toBeUndefined();
  expect(next).toBeCalled();
  expect(next).toBeCalledWith();
});

test("addDateToBody() should add date to certificateBackDate with 'no' value for radio button and clear start date part", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "no",
      "certificateBackDate-day": "10",
      "certificateBackDate-month": "04",
      "certificateBackDate-year": "2023",
      "certificateStartDate-day": "03",
      "certificateStartDate-month": "03",
      "certificateStartDate-year": "2024",
    },
  };
  const res = {};
  const next = jest.fn();

  addDateToBody(req, res, next);

  expect(req.body["certificateBackDate"]).toEqual("2023-04-10");
  expect(req.body["certificateStartDate-day"]).toBeUndefined();
  expect(req.body["certificateStartDate-month"]).toBeUndefined();
  expect(req.body["certificateStartDate-year"]).toBeUndefined();
  expect(next).toBeCalled();
  expect(next).toBeCalledWith();
});

describe("validateCertificateStartDate() throw errors for an invalid date range", () => {
  test("when date is less than 1st April 2023", () => {
    const req = {
      t: (string) => string,
      body: {
        prescriptionStartFromToday: "yes",
        "certificateStartDate-day": "31",
        "certificateStartDate-month": "03",
        "certificateStartDate-year": "2023",
      },
    };

    expect(
      validateCertificateStartDate.bind(null, "2023-03-31", { req }),
    ).toThrowError(/validation:startDateInvalidRange/);
  });

  test("when date is greater than 1st May 2023", () => {
    const req = {
      t: (string) => string,
      body: {
        prescriptionStartFromToday: "yes",
        "certificateStartDate-day": "02",
        "certificateStartDate-month": "05",
        "certificateStartDate-year": "2023",
      },
    };

    jest.useFakeTimers().setSystemTime(new Date("2023-03-22"));

    expect(
      validateCertificateStartDate.bind(null, "2023-05-02", { req }),
    ).toThrowError(/validation:startDateInvalidRange/);
  });
});

test("validateCertificateBackDateDay() throws an error for an empty day when input into back date part", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "no",
      "certificateBackDate-day": "",
      "certificateBackDate-month": "04",
      "certificateBackDate-year": "2023",
    },
  };

  expect(
    validateCertificateBackDateDay.bind(null, "2023-04-", { req }),
  ).toThrowError(/validation:missingStartDateDay/);
});

test("validateCertificateBackDateMonth() throws an error for an empty month when input into back date part", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "no",
      "certificateBackDate-day": "10",
      "certificateBackDate-month": "",
      "certificateBackDate-year": "2023",
    },
  };

  expect(
    validateCertificateBackDateMonth.bind(null, "2023--10", { req }),
  ).toThrowError(/validation:missingStartDateMonth/);
});

test("validateCertificateBackDateYear() throws an error for an empty year when input into back date part", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "no",
      "certificateBackDate-day": "10",
      "certificateBackDate-month": "04",
      "certificateBackDate-year": "",
    },
  };

  expect(
    validateCertificateBackDateYear.bind(null, "-04-10", { req }),
  ).toThrowError(/validation:missingStartDateYear/);
});

test("validateCertificateBackDateDay() and validateCertificateStartDateMonth() throw errors for an empty day and month when input into back date part", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "no",
      "certificateBackDate-day": "",
      "certificateBackDate-month": "",
      "certificateBackDate-year": "2023",
    },
  };

  expect(
    validateCertificateBackDateDay.bind(null, "2023--", { req }),
  ).toThrowError(/validation:missingStartDateDay/);
  expect(
    validateCertificateBackDateMonth.bind(null, "2023--", { req }),
  ).toThrowError(/validation:missingStartDateMonth/);
});

test("validateCertificateBackDateDay() and validateCertificateStartDateYear() throw errors for an empty day and year when input into back date part", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "no",
      "certificateBackDate-day": "",
      "certificateBackDate-month": "04",
      "certificateBackDate-year": "",
    },
  };

  expect(
    validateCertificateBackDateDay.bind(null, "-04-", { req }),
  ).toThrowError(/validation:missingStartDateDay/);
  expect(
    validateCertificateBackDateYear.bind(null, "-04-", { req }),
  ).toThrowError(/validation:missingStartDateYear/);
});

test("validateCertificateBackDateMonth() and validateCertificateStartDateYear() throw errors for an empty month and year when input into back date part", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "no",
      "certificateBackDate-day": "10",
      "certificateBackDate-month": "",
      "certificateBackDate-year": "",
    },
  };

  expect(
    validateCertificateBackDateMonth.bind(null, "--10", { req }),
  ).toThrowError(/validation:missingStartDateMonth/);
  expect(
    validateCertificateBackDateYear.bind(null, "--10", { req }),
  ).toThrowError(/validation:missingStartDateYear/);
});

test("verify that custom check for validateCertificateStartDate() is being called", async () => {
  const req = {
    body: {
      prescriptionStartFromToday: "yes",
      "certificateStartDate-day": "10",
      "certificateStartDate-month": "13",
      "certificateStartDate-year": "2023",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const errorOne = result.array()[0] as FieldValidationError;

  expect(result.array()).toHaveLength(1);
  expect(errorOne.path).toBe("certificateStartDate");
  expect(errorOne.msg).toBe("validation:startDateNotReal");
});

test("verify that custom check for validateCertificateBackDate() is being called", async () => {
  const req = {
    body: {
      prescriptionStartFromToday: "no",
      "certificateBackDate-day": "08",
      "certificateBackDate-month": "13",
      "certificateBackDate-year": "2023",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const errorOne = result.array()[0] as FieldValidationError;

  expect(result.array().length).toBe(1);
  expect(errorOne.path).toBe("certificateBackDate");
  expect(errorOne.msg).toBe("validation:startDateNotReal");
});

test("validateCertificateStartDate() should throw an error for an invalid day", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "yes",
    },
  };

  expect(
    validateCertificateStartDate.bind(null, "2023-04-EE", { req }),
  ).toThrowError(/validation:startDateNotReal/);
});

test("validateCertificateStartDate() should throw an error for an invalid month", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "yes",
    },
  };

  expect(
    validateCertificateStartDate.bind(null, "2023-EE-10", { req }),
  ).toThrowError(/validation:startDateNotReal/);
});

test("validateCertificateStartDate() should throw an error for an invalid year", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "yes",
    },
  };

  expect(
    validateCertificateStartDate.bind(null, "EEEE-04-10", { req }),
  ).toThrowError(/validation:startDateNotReal/);
});

test("validateCertificateStartDate() throw errors for an invalid day and month", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "yes",
    },
  };

  expect(
    validateCertificateStartDate.bind(null, "2023-EE-EE", { req }),
  ).toThrowError(/validation:startDateNotReal/);
});

test("validateCertificateStartDate() throw errors for an invalid day and year", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "yes",
    },
  };

  expect(
    validateCertificateStartDate.bind(null, "EEEE-04-EE", { req }),
  ).toThrowError(/validation:startDateNotReal/);
});

test("validateCertificateStartDate() throw errors for an invalid month and year", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "yes",
    },
  };

  expect(
    validateCertificateStartDate.bind(null, "EEEE-EE-10", { req }),
  ).toThrowError(/validation:startDateNotReal/);
});

test("validateCertificateStartDate() throw errors for an invalid day, month and year", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "yes",
    },
  };

  expect(
    validateCertificateStartDate.bind(null, "EEEE-EE-EE", { req }),
  ).toThrowError(/validation:startDateNotReal/);
});

describe("boundary cases for start from today", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "yes",
      "certificateStartDate-day": "10",
      "certificateStartDate-month": "04",
      "certificateStartDate-year": "2023",
      "certificateBackDate-day": "03",
      "certificateBackDate-month": "01",
      "certificateBackDate-year": "2021",
    },
  };

  test.each([
    ["2023-10-31", "2023-11-30"],
    ["2024-02-29", "2024-03-29"],
    ["2024-01-29", "2024-02-29"],
  ])(
    "on %s, a certificate start date (from start date) of %s is valid",
    (currentDate: string, certificateDate: string) => {
      jest.useFakeTimers().setSystemTime(new Date(currentDate));
      expect(validateCertificateStartDate(certificateDate, { req })).toBe(true);
    },
  );

  test.each([
    ["2023-04-19", "2023-04-01"],
    ["2023-05-10", "2023-04-11"],
    ["2024-03-30", "2024-02-29"],
    ["2024-03-29", "2024-02-29"],
    ["2024-02-29", "2024-01-29"],
    ["2023-10-31", "2023-09-30"],
    ["2023-10-31", "2023-09-29"],
    ["2023-10-31", "2023-12-01"],
    ["2024-02-29", "2024-01-28"],
    ["2024-02-29", "2024-03-30"],
    ["2024-01-28", "2024-02-29"],
    ["2023-04-01", "2023-03-31"],
    ["2023-05-01", "2023-06-02"],
  ])(
    "on %s, a certificate start date (from start date) of %s is invalid",
    (currentDate: string, certificateDate: string) => {
      jest.useFakeTimers().setSystemTime(new Date(currentDate));
      expect(() =>
        validateCertificateStartDate(certificateDate, { req }),
      ).toThrowError(/validation:startDateInvalidRange/);
    },
  );
});

describe("boundary cases for back date", () => {
  const req = {
    t: (string) => string,
    body: {
      prescriptionStartFromToday: "no",
      "certificateStartDate-day": "10",
      "certificateStartDate-month": "04",
      "certificateStartDate-year": "2023",
      "certificateBackDate-day": "03",
      "certificateBackDate-month": "01",
      "certificateBackDate-year": "2021",
    },
  };

  test.each([
    ["2023-04-19", "2023-04-01"],
    ["2023-05-10", "2023-04-11"],
    ["2024-03-30", "2024-02-29"],
    ["2024-03-29", "2024-02-29"],
    ["2024-02-29", "2024-01-29"],
    ["2023-10-31", "2023-09-30"],
  ])(
    "on %s, a certificate back date (end start date -1) of %s is valid",
    (currentDate: string, certificateDate: string) => {
      jest.useFakeTimers().setSystemTime(new Date(currentDate));
      expect(validateCertificateBackDate(certificateDate, { req })).toBe(true);
    },
  );

  test.each([
    ["2023-10-31", "2023-11-30"],
    ["2024-02-29", "2024-03-29"],
    ["2024-01-29", "2024-02-29"],
    ["2023-10-31", "2023-09-29"],
    ["2023-10-31", "2023-12-01"],
    ["2024-02-29", "2024-01-28"],
    ["2024-02-29", "2024-03-30"],
    ["2024-01-28", "2024-02-29"],
    ["2023-04-01", "2023-03-31"],
    ["2023-05-01", "2023-06-02"],
  ])(
    "on %s, a certificate back date (end start date -1) of %s is invalid",
    (currentDate: string, certificateDate: string) => {
      jest.useFakeTimers().setSystemTime(new Date(currentDate));
      expect(() =>
        validateCertificateBackDate(certificateDate, { req }),
      ).toThrowError(/validation:startDateInvalidRange/);
    },
  );
});

describe("options cases", () => {
  const req = {
    t: (string) => string,
    body: {},
  };

  test("verify error when no option selected", async () => {
    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0];
    expect(errorOne.msg).toBe("validation:selectYourStartDate");
  });

  test.each([
    ["yes", "validation:startDateInvalidRange"],
    ["no", "validation:startDateInvalidRange"],
    ["", "validation:selectYourStartDate"],
  ])(
    "the '%s' option will have error %s",
    async (currentValue: string, expectation: string) => {
      const reqCurr = {
        t: (string) => string,
        body: {
          prescriptionStartFromToday: currentValue,
          "certificateStartDate-day": "10",
          "certificateStartDate-month": "04",
          "certificateStartDate-year": "2023",
          "certificateBackDate-day": "03",
          "certificateBackDate-month": "01",
          "certificateBackDate-year": "2021",
        },
      };
      const result = await applyExpressValidation(reqCurr, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(1);
      const errorOne = result.array()[0];
      expect(errorOne.msg).toBe(expectation);
    },
  );
});
