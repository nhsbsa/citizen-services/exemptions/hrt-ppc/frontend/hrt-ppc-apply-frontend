import expect from "expect";

const mockSanitizeStartDate = jest.fn();
const mockTrimLeadingZerosAndReplaceSpaces = jest.fn();

jest.mock("@nhsbsa/health-charge-exemption-common-frontend", () => ({
  sanitizeStartDate: mockSanitizeStartDate,
  trimLeadingZerosAndReplaceSpaces: mockTrimLeadingZerosAndReplaceSpaces,
}));

import { sanitize } from "./sanitize";

const req = {
  body: {
    "certificateBackDate-day": "11",
    "certificateBackDate-month": "2",
    "certificateBackDate-year": "2013",
  },
};
const next = jest.fn();

test("sanitize() should call trimLeadingZerosAndReplaceSpaces and sanitizeStartDate", () => {
  const expectedReq = {
    body: {
      "certificateBackDate-day": "11",
      "certificateBackDate-month": "2",
      "certificateBackDate-year": "2013",
    },
  };

  sanitize()(req, {}, next);

  expect(req).toEqual(expectedReq);
  expect(mockTrimLeadingZerosAndReplaceSpaces).toHaveBeenCalledTimes(3);
  expect(mockTrimLeadingZerosAndReplaceSpaces).toHaveBeenNthCalledWith(
    1,
    req,
    "certificateBackDate-day",
  );
  expect(mockTrimLeadingZerosAndReplaceSpaces).toHaveBeenNthCalledWith(
    2,
    req,
    "certificateBackDate-month",
  );
  expect(mockTrimLeadingZerosAndReplaceSpaces).toHaveBeenNthCalledWith(
    3,
    req,
    "certificateBackDate-year",
  );
  expect(mockSanitizeStartDate).toHaveBeenCalledTimes(1);
  expect(mockSanitizeStartDate).toHaveBeenCalledWith(req);
  expect(next).toHaveBeenCalled();
  expect(next).toHaveBeenCalledWith();
});
