import expect from "expect";
import { behaviourForGet } from "./cookie-confirmation";

test("behaviourForGet() on cookie confirmation", () => {
  const req = {
    session: {
      measureAnalytics: true,
    },
  };

  const res = {
    locals: { measureAnalytics: undefined },
  };

  const next = jest.fn();

  behaviourForGet()(req, res, next);

  expect(res.locals.measureAnalytics).toEqual(true);
  expect(next).toHaveBeenCalled();
});
