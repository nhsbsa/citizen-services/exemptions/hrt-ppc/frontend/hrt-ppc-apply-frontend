const pageContent = ({ translate }) => ({
  title: translate("cookiesConfirmation.title"),
  heading: translate("cookiesConfirmation.heading"),
  paragraphOne: translate("cookiesConfirmation.paragraphOne"),
  paragraphTwo: translate("cookiesConfirmation.paragraphTwo"),
  listItemOne: translate("cookiesConfirmation.listItemOne"),
  listItemTwo: translate("cookiesConfirmation.listItemTwo"),
  paragraphThreeFirstHalf: translate(
    "cookiesConfirmation.paragraphThreeFirstHalf",
  ),
  linkContent: translate("cookiesConfirmation.linkContent"),
  paragraphThreeSecondHalf: translate(
    "cookiesConfirmation.paragraphThreeSecondHalf",
  ),
  paragraphFour: translate("cookiesConfirmation.paragraphFour"),
  buttonText: translate("cookiesConfirmation.buttonText"),
  hideCookieBanner: true,
});

const behaviourForGet = () => (req, res, next) => {
  res.locals.measureAnalytics = req.session.measureAnalytics;
  next();
};

const cookieConfirmation = {
  path: "/cookies-saved",
  template: "cookies-confirmation",
  pageContent,
  behaviourForGet,
};

export { behaviourForGet, cookieConfirmation };
