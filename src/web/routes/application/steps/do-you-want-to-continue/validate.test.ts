import expect from "expect";
import { assocPath } from "ramda";
import { validate } from "./validate";
import { NO, YES } from "../common/constants";
import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend";
import { FieldValidationError } from "express-validator";

const translate = (string) => string;
const req = {
  t: translate,
  body: {
    doYouStillWantToBuyCountry: YES,
  },
};

test("validation middleware passes with valid option YES body", async () => {
  const testReq = { ...req };

  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(true);
});

test("validation middleware passes with valid option NO body", async () => {
  const testReq = assocPath(["body", "doYouStillWantToBuyCountry"], NO, req);
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(true);
});

test("validation middleware fails with invalid option Test body", async () => {
  const testReq = assocPath(
    ["body", "doYouStillWantToBuyCountry"],
    "Test",
    req,
  );
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(false);
});

test("validation middleware errors for do you still want to buy hrt ppc field", async () => {
  const testReq = assocPath(
    ["body", "doYouStillWantToBuyCountry"],
    "maybe",
    req,
  );
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0] as FieldValidationError;
  expect(result.array().length).toBe(1);
  expect(error.path).toBe("doYouStillWantToBuyCountry");
  expect(error.msg).toBe("validation:selectPrescriptionsEligibility");
});

test("validation middleware errors for do you still want to buy hrt ppc field is empty", async () => {
  const testReq = assocPath(["body", "doYouStillWantToBuyCountry"], null, req);
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0] as FieldValidationError;
  expect(result.array().length).toBe(1);
  expect(error.path).toBe("doYouStillWantToBuyCountry");
  expect(error.msg).toBe("validation:selectPrescriptionsEligibility");
});
