import expect from "expect";
import { isNavigable } from "./do-you-want-to-continue";

test(`isNavigable() returns true if prescription country name is selected as WALES`, () => {
  const session = {
    claim: {
      prescriptionCountryName: "wales",
    },
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(true);
});

test(`isNavigable() returns false if prescription country name is selected as ENGLAND`, () => {
  const session = {
    claim: {
      prescriptionCountryName: "england",
    },
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(false);
});
