import { translateValidationMessage } from "@nhsbsa/health-charge-exemption-common-frontend";
import { check } from "express-validator";
import { YES, NO } from "../common/constants";

const validate = () => [
  check("doYouStillWantToBuyCountry")
    .isIn([YES, NO])
    .withMessage(
      translateValidationMessage("validation:selectPrescriptionsEligibility"),
    ),
];

export { validate };
