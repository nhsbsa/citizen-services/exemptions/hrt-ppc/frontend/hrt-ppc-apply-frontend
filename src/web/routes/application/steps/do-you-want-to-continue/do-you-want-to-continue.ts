import { validate } from "./validate";
import { Countries } from "../common/enums";
import { path } from "ramda";

const pageContent = ({ translate, req }) => ({
  title: translate("doYouStillWantToBuyHrtPpc.title"),
  heading: translate("doYouStillWantToBuyHrtPpc.heading"),
  paragraphOne: translate("doYouStillWantToBuyHrtPpc.paragraphOne"),
  paragraphTwo: translate("doYouStillWantToBuyHrtPpc.paragraphTwo"),
  paragraphTwoLinkText: translate(
    "doYouStillWantToBuyHrtPpc.paragraphTwoLinkText",
  ),
  radioQuestion: translate("doYouStillWantToBuyHrtPpc.radioQuestion"),
  buttonText: translate("buttons:continue"),
  yes: req.t("yes"),
  no: req.t("no"),
  cancel: req.t("cancel"),
});

const isNavigable = (req, session) =>
  path(["claim", "prescriptionCountryName"], session) !== Countries.ENGLAND;

const doYouStillWantToBuyHrtPpc = {
  path: "/do-you-want-to-continue",
  template: "collect-their-prescriptions-outside-of-england",
  pageContent,
  validate,
  isNavigable,
};

export { doYouStillWantToBuyHrtPpc, pageContent, isNavigable };
