import expect from "expect";
import {
  registerApplicationCompleteRoute,
  pageContent,
  formatReference,
} from "./application-complete";
import { configureSessionDetails } from "../../../flow-control/middleware/session-details";
import { handleRequestForPath } from "../../../flow-control/middleware/handle-path-request";

jest.mock("../../../flow-control/middleware/session-details");
jest.mock("../../../flow-control/middleware/handle-path-request");

const APPLICATION_COMPLETE_URL = "/test-context/application-complete";

test(`registerApplicationCompleteRoute() should register route ${APPLICATION_COMPLETE_URL} with additional state-machine functionality `, () => {
  const app = {
    ...jest.requireActual("express"),
    get: jest.fn(),
    all: jest.fn(),
  };

  const journey = {
    name: "apply",
    pathPrefix: "/test-context",
    steps: [{ path: "/first", next: () => "/second" }, { path: "/second" }],
  };

  registerApplicationCompleteRoute(journey, app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toHaveBeenNthCalledWith(
    1,
    APPLICATION_COMPLETE_URL,
    undefined,
    undefined,
    pageContent,
  );
  expect(configureSessionDetails).toBeCalledTimes(1);
  expect(handleRequestForPath).toBeCalledTimes(1);
});

test(`pageContent() should render the page with translated values`, () => {
  const req = {
    session: {
      issuedCertificateDetails: {
        certificateReference: "HRT8660A013",
        certificateStartDate: "2022-01-01",
        certificateEndDate: "2022-12-31",
      },
      claim: {
        firstName: "fName",
        lastName: "lName",
      },
    },
    t: (string, object?) => `${string}${JSON.stringify(object)}`,
  };

  const res = {
    redirect: jest.fn(),
    status: jest.fn().mockReturnThis(),
    render: jest.fn(),
  };

  const expectedApplicationCompleteTranslation = {
    title: req.t("applicationComplete.title"),
    phaseBannerHtml: req.t("phaseBanner.htmlWithoutSurveyLink"),
    greenPanelHeading: req.t("applicationComplete.greenPanelHeading"),
    greenPanelHTML: req.t("applicationComplete.greenPanelHTML"),
    summaryHeading: req.t("applicationComplete.summaryHeading"),
    emailCertificateInformation: req.t(
      "applicationComplete.emailCertificateInformation",
    ),
    summaryList: req.t("applicationComplete.summaryList", {
      fullName: "fName lName",
      certificateReference: "HRT 8660 A013 ",
      certificateStartDate: "1 January 2022",
      certificateEndDate: "31 December 2022",
    }),
    summaryContentHTML: req.t("applicationComplete.summaryContentHTML", {
      contactEmail: "nhsbsa.hrtppc-support@nhs.net",
    }),
    saveYourCertificateHeading: req.t(
      "applicationComplete.saveYourCertificateHeading",
    ),
    saveYourCertificateParagraphOne: req.t(
      "applicationComplete.saveYourCertificateParagraphOne",
    ),
    saveYourCertificateParagraphTwo: req.t(
      "applicationComplete.saveYourCertificateParagraphTwo",
    ),
    saveYourCertificateParagraphTwoEmail: req.t(
      "applicationComplete.saveYourCertificateParagraphTwoEmail",
    ),
    saveYourCertificateButtonText: req.t(
      "applicationComplete.saveYourCertificateButtonText",
    ),
    printYourCertificateButtonText: req.t(
      "applicationComplete.printYourCertificateButtonText",
    ),
    useYourCertificateHeading: req.t(
      "applicationComplete.useYourCertificateHeading",
    ),
    useYourCertificateParagraphOne: req.t(
      "applicationComplete.useYourCertificateParagraphOne",
    ),
    useYourCertificateParagraphTwo: req.t(
      "applicationComplete.useYourCertificateParagraphTwo",
    ),
    useYourCertificateParagraphThree: req.t(
      "applicationComplete.useYourCertificateParagraphThree",
    ),
    useYourCertificateParagraphFour: req.t(
      "applicationComplete.useYourCertificateParagraphFour",
    ),
    useYourCertificateParagraphFive: req.t(
      "applicationComplete.useYourCertificateParagraphFive",
    ),
    feedbackHeading: req.t("applicationComplete.feedbackHeading"),
    feedbackLinkText: req.t("applicationComplete.feedbackLinkText"),
    contactUsHeading: req.t("applicationComplete.contactUsHeading"),
    contactUsParagraphOne: req.t("applicationComplete.contactUsParagraphOne"),
  };

  pageContent(req, res);
  expect(res.render).toBeCalledWith(
    "application-complete",
    expectedApplicationCompleteTranslation,
  );
});

test(`formatReference() formats reference when exactly 11 characters`, () => {
  const reference = "HRT8660A013";

  const result = formatReference(reference);

  expect(result).toBe("HRT 8660 A013 ");
});

test(`formatReference() formats reference when longer than 11 characters`, () => {
  const reference = "HRT8660A0134567890";

  const result = formatReference(reference);

  expect(result).toBe("HRT 8660 A013 4567890");
});

test(`formatReference() does not format reference when shorter than 11 characters`, () => {
  const reference = "HRT8660";

  const result = formatReference(reference);

  expect(result).toBe(reference);
});
