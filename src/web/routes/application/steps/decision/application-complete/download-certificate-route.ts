import { prefixPath } from "../../../paths/prefix-path";
import { DOWNLOAD_PDF_URL, SESSION_TIMEOUT_URL } from "../../../paths/paths";
import { pdfClient } from "../../../../../client/common-pdf-client";
import { config } from "../../../../../../config";
import * as httpStatus from "http-status-codes";
import { wrapError } from "../../../errors";
import {
  CONTENT_TYPE_HEADER,
  CONTENT_TYPE_PDF,
  RESPONSE_TYPE_STREAM,
} from "../../../../../client/constants";
import { completedJourneyExistsInSession } from "../../../flow-control/middleware/handle-path-request/predicates";
import { contextPath } from "../../../paths/context-path";
import { formatDateForDisplayFromDate } from "../../common/formatters/dates";
import { DownloadCertificateRequestBody } from "../../common/types"; 

const createRequestBody = (req): DownloadCertificateRequestBody => {
  return {
    personalisation: {
      firstName: req.session.claim.firstName,
      lastName: req.session.claim.lastName,
      reference: req.session.issuedCertificateDetails.certificateReference,
      startDate: formatDateForDisplayFromDate(
        new Date(req.session.issuedCertificateDetails.certificateStartDate),
      ),
      endDate: formatDateForDisplayFromDate(
        new Date(req.session.issuedCertificateDetails.certificateEndDate),
      ),
    },
    service: "HRT_PPC",
    template: "hrt-exemption-issue",
    accessibilitySwitch: true,
  };
};

const downloadCertificate = async (req, res, next) => {
  try {
    const fileName = `HRTPPC-${req.session.claim.firstName}${req.session.claim.lastName}.pdf`;

    const client = new pdfClient(config);
    const pdfResponse: any = await client.makeRequest({
      method: "POST",
      url: "/v1/pdf",
      data: createRequestBody(req),
      responseType: RESPONSE_TYPE_STREAM,
    });

    const contentType = pdfResponse.headers[CONTENT_TYPE_HEADER];
    if (contentType && contentType.includes(CONTENT_TYPE_PDF)) {
      res.setHeader(
        "Content-Disposition",
        `attachment; filename="${fileName}"`,
      );
      pdfResponse.data.pipe(res, "binary");
    } else {
      const chunks: Buffer[] = [];
      pdfResponse.data.on("data", (chunk: Buffer) => {
        chunks.push(chunk);
      });

      const endEventPromise = new Promise<void>((resolve) => {
        pdfResponse.data.on("end", () => {
          resolve();
        });
      });

      await endEventPromise;

      throw new Error(
        `The content returned is not of pdf type: [status: ${
          pdfResponse.status
        }, responseUrl: ${pdfResponse.data.responseUrl}, data: ${Buffer.concat(
          chunks,
        )}]`,
      );
    }
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      }),
    );
  }
};

const registerDownloadCertificateRoute = (journey, app) => {
  app.get(
    prefixPath(journey.pathPrefix, DOWNLOAD_PDF_URL),
    checkStateIsComplete(journey),
    downloadCertificate,
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const checkStateIsComplete = (journey) => (req, res, next) => {
  if (!completedJourneyExistsInSession(req)) {
    return res.redirect(contextPath(SESSION_TIMEOUT_URL));
  }

  next();
};

export {
  registerDownloadCertificateRoute,
  downloadCertificate,
  createRequestBody,
  checkStateIsComplete,
};
