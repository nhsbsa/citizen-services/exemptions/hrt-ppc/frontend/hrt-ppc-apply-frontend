import { prefixPath } from "../../../paths/prefix-path";
import { configureSessionDetails } from "../../../flow-control/middleware/session-details";
import { handleRequestForPath } from "../../../flow-control/middleware/handle-path-request";
import { APPLICATION_COMPLETE_URL } from "../../../paths/paths";
import { formatDateForDisplayFromDate } from "../../common/formatters/dates";

const CONTACT_EMAIL = process.env.CONTACT_EMAIL;

const pageContent = (req, res) => {
  return res.render("application-complete", {
    title: req.t("applicationComplete.title"),
    phaseBannerHtml: req.t("phaseBanner.htmlWithoutSurveyLink"),
    greenPanelHeading: req.t("applicationComplete.greenPanelHeading"),
    greenPanelHTML: req.t("applicationComplete.greenPanelHTML"),
    summaryHeading: req.t("applicationComplete.summaryHeading"),
    emailCertificateInformation: req.t(
      "applicationComplete.emailCertificateInformation",
    ),
    summaryList: req.t("applicationComplete.summaryList", {
      fullName: `${req.session.claim.firstName} ${req.session.claim.lastName}`,
      certificateReference: formatReference(
        req.session.issuedCertificateDetails.certificateReference,
      ),
      certificateStartDate: formatDateForDisplayFromDate(
        new Date(req.session.issuedCertificateDetails.certificateStartDate),
      ),
      certificateEndDate: formatDateForDisplayFromDate(
        new Date(req.session.issuedCertificateDetails.certificateEndDate),
      ),
    }),
    summaryContentHTML: req.t("applicationComplete.summaryContentHTML", {
      contactEmail: CONTACT_EMAIL,
    }),
    saveYourCertificateHeading: req.t(
      "applicationComplete.saveYourCertificateHeading",
    ),
    saveYourCertificateParagraphOne: req.t(
      "applicationComplete.saveYourCertificateParagraphOne",
    ),
    saveYourCertificateParagraphTwo: req.t(
      "applicationComplete.saveYourCertificateParagraphTwo",
    ),
    saveYourCertificateParagraphTwoEmail: req.t(
      "applicationComplete.saveYourCertificateParagraphTwoEmail",
    ),
    saveYourCertificateButtonText: req.t(
      "applicationComplete.saveYourCertificateButtonText",
    ),
    printYourCertificateButtonText: req.t(
      "applicationComplete.printYourCertificateButtonText",
    ),
    useYourCertificateHeading: req.t(
      "applicationComplete.useYourCertificateHeading",
    ),
    useYourCertificateParagraphOne: req.t(
      "applicationComplete.useYourCertificateParagraphOne",
    ),
    useYourCertificateParagraphTwo: req.t(
      "applicationComplete.useYourCertificateParagraphTwo",
    ),
    useYourCertificateParagraphThree: req.t(
      "applicationComplete.useYourCertificateParagraphThree",
    ),
    useYourCertificateParagraphFour: req.t(
      "applicationComplete.useYourCertificateParagraphFour",
    ),
    useYourCertificateParagraphFive: req.t(
      "applicationComplete.useYourCertificateParagraphFive",
    ),
    feedbackHeading: req.t("applicationComplete.feedbackHeading"),
    feedbackLinkText: req.t("applicationComplete.feedbackLinkText"),
    contactUsHeading: req.t("applicationComplete.contactUsHeading"),
    contactUsParagraphOne: req.t("applicationComplete.contactUsParagraphOne"),
  });
};

const formatReference = (reference) => {
  if (reference.length < 11) {
    return reference;
  }
  const first = reference.substring(0, 3);
  const second = reference.substring(3, 7);
  const third = reference.substring(7, 11);
  const fourth = reference.substring(11);

  return `${first} ${second} ${third} ${fourth}`;
};

const registerApplicationCompleteRoute = (journey, app) =>
  app.get(
    prefixPath(journey.pathPrefix, APPLICATION_COMPLETE_URL),
    configureSessionDetails(journey),
    handleRequestForPath(journey),
    pageContent,
  );

export { registerApplicationCompleteRoute, pageContent, formatReference };
