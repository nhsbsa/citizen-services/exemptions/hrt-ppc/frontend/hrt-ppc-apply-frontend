const mockMakeRequest = jest.fn();

import { config } from "../../../../../../config";
import { pdfClient } from "../../../../../client/common-pdf-client";
import * as httpStatus from "http-status-codes";
import { wrapError } from "../../../errors";
import { CONTENT_TYPE_PDF } from "../../../../../client/constants";

jest.mock("../../../../../client/common-pdf-client");

const journey = {};
const expectedRequestBody = {
  personalisation: {
    firstName: "John",
    lastName: "Doe",
    reference: "12345",
    startDate: "1 January 2023",
    endDate: "31 December 2023",
  },
  service: "HRT_PPC",
  template: "hrt-exemption-issue",
  accessibilitySwitch: true,
};

const expectedMockRequestCall = {
  method: "POST",
  url: "/v1/pdf",
  data: expectedRequestBody,
  responseType: "stream",
};

const req = {
  session: {
    claim: {
      firstName: "John",
      lastName: "Doe",
    },
    issuedCertificateDetails: {
      certificateReference: "12345",
      certificateStartDate: "2023-01-01",
      certificateEndDate: "2023-12-31",
    },
  },
  path: "/download-pdf",
};

const res = {
  setHeader: jest.fn(),
  status: jest.fn(),
  end: jest.fn(),
  redirect: jest.fn(),
};

const next = jest.fn();

const mockCompletedJourneyExistsInSession = jest.fn().mockReturnValue(true);
jest.mock(
  "../../../flow-control/middleware/handle-path-request/predicates",
  () => ({
    completedJourneyExistsInSession: mockCompletedJourneyExistsInSession,
  }),
);

const mockPipe = jest.fn((response) => {
  response.write("PDF content");
  response.end();
});

import {
  downloadCertificate,
  createRequestBody,
  registerDownloadCertificateRoute,
  checkStateIsComplete,
} from "./download-certificate-route";

beforeEach(() => {
  jest.clearAllMocks();
  pdfClient.prototype.makeRequest = mockMakeRequest;
});

describe("createRequestBody", () => {
  test("should create the request body", () => {
    const requestBody = createRequestBody(req);
    expect(requestBody).toEqual(expectedRequestBody);
  });
});

describe("downloadCertificate", () => {
  test("should handle a successful download when the response header includes application/pdf", async () => {
    mockMakeRequest.mockResolvedValueOnce({
      headers: {
        "content-type": CONTENT_TYPE_PDF,
      },
      data: {
        pipe: mockPipe,
      },
    });

    await downloadCertificate(req, res, next);

    expect(pdfClient).toBeCalledTimes(1);
    expect(pdfClient).toBeCalledWith(config);
    expect(pdfClient.prototype.makeRequest).toBeCalledWith(
      expectedMockRequestCall,
    );
    expect(res.setHeader).toHaveBeenCalledWith(
      "Content-Disposition",
      'attachment; filename="HRTPPC-JohnDoe.pdf"',
    );
    expect(mockPipe).toHaveBeenCalledTimes(1);
  });

  test("should handle a download request with an invalid content type", async () => {
    mockMakeRequest.mockResolvedValueOnce({
      headers: {
        "content-type": "text/json",
      },
      data: {
        pipe: mockPipe,
        errorInfo: "errorMessage",
        req: {
          method: "POST",
        },
        responseUrl: "/mock-url",
        on: jest.fn().mockImplementation((event, callback) => {
          callback(Buffer.from("Non-PDF content"));
        }),
      },
      status: 200,
      config: {
        data: '{"service": "mockService", "template": "mockTemplate"}',
      },
    });

    await downloadCertificate(req, res, next);

    expect(mockPipe).not.toHaveBeenCalled();
    expect(next).toBeCalledWith(
      wrapError({
        cause: new Error(
          "The content returned is not of pdf type: [status: 200, responseUrl: /mock-url, data: Non-PDF content]",
        ),
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      }),
    );
  });

  test("should handle an error during download", async () => {
    mockMakeRequest.mockRejectedValueOnce(new Error("Download error"));

    await downloadCertificate(req, res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(
      wrapError({
        cause: new Error("Download error"),
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      }),
    );
  });
});

describe("registerDownloadCertificateRoute", () => {
  test("should register download certificate route with correct middleware", () => {
    const journey = {
      pathPrefix: "/your-path-prefix",
    };

    const app = {
      get: jest.fn().mockReturnThis(),
    };

    registerDownloadCertificateRoute(journey, app);

    expect(app.get).toHaveBeenCalledTimes(1);
    expect(app.get).toHaveBeenCalledWith(
      "/your-path-prefix/download-pdf",
      expect.any(Function),
      downloadCertificate,
    );
  });
});

describe("checkStateIsComplete()", () => {
  test("should call next() if completed journey exists in session", () => {
    mockCompletedJourneyExistsInSession.mockReturnValue(true);
    const req = {};

    checkStateIsComplete(journey)(req, res, next);

    expect(res.redirect).not.toHaveBeenCalled();
    expect(next).toHaveBeenCalled();
  });

  test("should redirect to SESSION_TIMEOUT_URL if completed journey does not exist in session", () => {
    mockCompletedJourneyExistsInSession.mockReturnValue(false);
    const req = {};

    checkStateIsComplete(journey)(req, res, next);

    expect(res.redirect).toHaveBeenCalledWith("/test-context/session-timeout");
    expect(next).not.toHaveBeenCalled();
  });
});
