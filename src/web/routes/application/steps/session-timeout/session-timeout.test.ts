import { handleStateDestroySession } from "../../flow-control/middleware/handle-get";
import { CONTACT_EMAIL } from "../common/constants";
import { registerSessionTimeout, renderContent } from "./session-timeout";

jest.mock("../../flow-control/middleware/handle-get");

test(`registerSessionTimeout() should route and get page correctly`, () => {
  const app = {
    ...jest.requireActual("express"),
    get: jest.fn(),
  };

  const journey = {
    name: "apply",
    pathPrefix: "/test-context",
    steps: [{ path: "/first", next: () => "/second" }, { path: "/second" }],
  };

  registerSessionTimeout(journey, app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toHaveBeenNthCalledWith(
    1,
    "/test-context/session-timeout",
    undefined,
    renderContent,
  );
  expect(handleStateDestroySession).toBeCalledTimes(1);
  expect(handleStateDestroySession).toHaveBeenNthCalledWith(1, journey);
});

test(`renderContent() should render the page with page translation values`, () => {
  const req = { t: (string, object?) => `${string}${JSON.stringify(object)}` };
  const res = { render: jest.fn() };
  const translate = req.t;
  const expectedSessionTimeoutTransalation = {
    title: translate("sessionTimeout.title"),
    heading: translate("sessionTimeout.heading"),
    paragraphOne: translate("sessionTimeout.paragraphOne"),
    startAgainLink: translate("sessionTimeout.startAgainLink"),
    contactText: translate("sessionTimeout.contactText", {
      contactEmailAddress: CONTACT_EMAIL,
    }),
    previous: false,
  };

  renderContent(req, res);
  expect(res.render).toBeCalledTimes(1);
  expect(res.render).toBeCalledWith(
    "session-timeout",
    expectedSessionTimeoutTransalation,
  );
});
