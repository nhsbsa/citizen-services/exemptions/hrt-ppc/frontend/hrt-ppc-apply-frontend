import { handleStateDestroySession } from "../../flow-control/middleware/handle-get";
import { SESSION_TIMEOUT_URL } from "../../paths/paths";
import { prefixPath } from "../../paths/prefix-path";
import { CONTACT_EMAIL } from "../common/constants";

const pageContent = (translate) => ({
  title: translate("sessionTimeout.title"),
  heading: translate("sessionTimeout.heading"),
  paragraphOne: translate("sessionTimeout.paragraphOne"),
  startAgainLink: translate("sessionTimeout.startAgainLink"),
  contactText: translate("sessionTimeout.contactText", {
    contactEmailAddress: CONTACT_EMAIL,
  }),
  previous: false,
});

const renderContent = (req, res) => {
  return res.render("session-timeout", {
    ...pageContent(req.t),
  });
};

const registerSessionTimeout = (journey, app) => {
  app.get(
    prefixPath(journey.pathPrefix, SESSION_TIMEOUT_URL),
    handleStateDestroySession(journey),
    renderContent,
  );
};

export { registerSessionTimeout, renderContent };
