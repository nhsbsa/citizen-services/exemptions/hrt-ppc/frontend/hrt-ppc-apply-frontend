import {
  checkPostcodeIsValid,
  translateValidationMessage,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import { check } from "express-validator";

const validate = () => [
  check("postcode")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingPostcode"))
    .custom(checkPostcodeIsValid)
    .withMessage(translateValidationMessage("validation:invalidPostcode")),
];

export { validate };
