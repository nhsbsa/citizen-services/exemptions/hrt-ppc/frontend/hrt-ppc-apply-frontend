/* eslint-disable @typescript-eslint/no-var-requires */
import expect from "expect";
import { validate } from "./validate";
import { sanitize } from "../sanitize";
import { partial } from "ramda";
import { states, testUtils } from "../../../flow-control";
import { REQUEST_ID_HEADER } from "../../../../../server/headers";
import { wrapError } from "../../../errors";
import { logger } from "../../../../../logger/logger";

import * as TEST_FIXTURES from "./text-fixtures.json";
import * as TEST_FIXTURES_NO_RESULTS from "./text-fixtures-no-results.json";

const { IN_PROGRESS, IN_REVIEW } = states;
const isEmpty = jest.fn();
const addressResults = jest.fn();
const errorSpy = jest.spyOn(logger, "error");
const mockSearchAndReturnKickoutIfExemptionFound = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

jest.mock("./os-places", () => ({
  getAddressLookupResults: addressResults,
}));

jest.mock("../search", () => ({
  searchAndReturnKickoutIfExemptionFound:
    mockSearchAndReturnKickoutIfExemptionFound,
}));

const config = {
  environment: {
    OS_PLACES_API_KEY: "123",
    GA_TRACKING_ID: "UA-133839203-1",
    GOOGLE_ANALYTICS_URI: "http://localhost:8150/collect",
  },
};

const resetStubs = () => {
  isEmpty.mockReset();
  errorSpy.mockReset();
};

beforeEach(() => {
  mockSearchAndReturnKickoutIfExemptionFound.mockResolvedValue(null);
  jest.clearAllMocks();
});

const {
  buildSessionForJourney,
  getNextAllowedPathForJourney,
  getStateForJourney,
} = testUtils;

const APPLY = "apply";
const JOURNEY = { name: APPLY };

const getNextAllowedPathForApplyJourney = partial(
  getNextAllowedPathForJourney,
  [APPLY],
);

const expectedNoErrorsFound = (body) => ({
  claim: { postcode: body.postcode },
  errorTitleText: "validation:errorTitleText",
  errors: [{ msg: "postcode.noAddressFoundError", path: "postcode" }],
});

test("requestBody() returns request body in correct format for AA1 1AA", () => {
  const { requestBody } = require("./postcode");

  const session = {
    claim: {
      postcode: "AA1 1AA",
    },
  };
  const result = requestBody(session);

  const expected = {
    postcode: "AA1 1AA",
  };

  expect(result).toEqual(expected);
});

test(`postcode constant should be set`, () => {
  const {
    behaviourForGet,
    behaviourForPost,
    requestBody,
    postcode,
  } = require("./postcode");

  const expected = {
    path: "/postcode",
    template: "postcode",
    pageContent: expect.anything(),
    behaviourForPost: behaviourForPost,
    behaviourForGet: behaviourForGet,
    sanitize,
    validate,
    requestBody,
    toggle: "ADDRESS_LOOKUP_ENABLED",
  };

  expect(postcode).toEqual(expected);
});

test(`behaviourForGet() sets state to ${IN_PROGRESS} and resets postcodeLookupError`, () => {
  const { behaviourForGet } = require("./postcode");

  const config = {};
  const journey = { name: "apply" };

  const req = {
    session: {
      fromManual: undefined,
      postcodeLookupError: true,
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    },
    headers: {
      [REQUEST_ID_HEADER]: "123456",
    },
  };

  const res = {};
  const next = jest.fn();
  const expectedState = states.IN_PROGRESS;

  behaviourForGet(config, journey)(req, res, next);

  expect(getNextAllowedPathForApplyJourney(req)).toBe(
    "/test-context/manual-address",
  );
  expect(next).toHaveBeenCalled();
  expect(getStateForJourney(JOURNEY.name, req)).toBe(expectedState);
  expect(req.session.postcodeLookupError).toBe(undefined);
  expect(req.session.fromManual).toBe(true);
});

test("behaviourForPost() handles when async call returns redirect result", async () => {
  // Set return values for stubs
  isEmpty.mockReturnValue(true);
  const res = {};
  const resAfter = { redirect: "test.com" };
  mockSearchAndReturnKickoutIfExemptionFound.mockResolvedValue(resAfter);
  const { behaviourForPost } = require("./postcode");

  const config = {};
  const req = {
    headers: { "x-forwarded-for": "100.200.0.45" },
    body: { postcode: "BS7 8EE" },
    session: {
      id: "skdjfhs-sdfnks-sdfhbsd",
      postcodeLookupResults: undefined,
      postcodeLookupError: undefined,
      fromManual: undefined,
    },
  };
  const next = jest.fn();

  const result = await behaviourForPost(config)(req, res, next);

  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledTimes(1);
  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledWith(
    req,
    res,
    undefined,
    next,
  );
  expect(next).toBeCalledTimes(0);
  expect(result).toEqual(resAfter);
});

test("behaviourForPost() handles successful address lookup", async () => {
  // Set return values for stubs
  isEmpty.mockReturnValue(true);
  addressResults.mockImplementation(() =>
    Promise.resolve({ data: TEST_FIXTURES }),
  );
  const { behaviourForPost } = require("./postcode");

  const req = {
    headers: { "x-forwarded-for": "100.200.0.45" },
    body: { postcode: "BS7 8EE" },
    session: {
      id: "skdjfhs-sdfnks-sdfhbsd",
      postcodeLookupResults: undefined,
      postcodeLookupError: undefined,
      fromManual: undefined,
    },
  };
  const res = {};
  const next = jest.fn();

  const expectedAddresses = [
    {
      ADDRESS: "Alan Jeffery Engineering, 1, Valley Road, Plymouth",
      ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
      BUILDING_NUMBER: "1",
      DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
      THOROUGHFARE_NAME: "VALLEY ROAD",
      POST_TOWN: "PLYMOUTH",
      POSTCODE: "PL7 1RF",
      LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
      UDPRN: "50265368",
    },
    {
      ADDRESS: "Dulux Decorator Centre, 2, Valley Road, Plymouth",
      ORGANISATION_NAME: "DULUX DECORATOR CENTRE",
      BUILDING_NUMBER: "2",
      DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
      THOROUGHFARE_NAME: "VALLEY ROAD",
      POST_TOWN: "PLYMOUTH",
      POSTCODE: "PL7 1RF",
      LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
      UDPRN: "19000955",
    },
    {
      ADDRESS: "Mill Autos, 3, Valley Road, Plymouth",
      ORGANISATION_NAME: "MILL AUTOS",
      BUILDING_NUMBER: "3",
      DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
      THOROUGHFARE_NAME: "VALLEY ROAD",
      POST_TOWN: "PLYMOUTH",
      POSTCODE: "PL7 1RF",
      LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
      UDPRN: "19000927",
    },
    {
      ADDRESS:
        "Goat Hill Farm, 2, Troll Bridge, Goat Hill, Slaithwaite, Slaith, Huddersfield",
      ORGANISATION_NAME: "GOAT HILL FARM",
      BUILDING_NUMBER: "2",
      DEPENDENT_THOROUGHFARE_NAME: "TROLL BRIDGE",
      THOROUGHFARE_NAME: "GOAT HILL",
      DOUBLE_DEPENDENT_LOCALITY: "SLAITHWAITE",
      DEPENDENT_LOCALITY: "SLAITH",
      POST_TOWN: "HUDDERSFIELD",
      POSTCODE: "HD7 5UZ",
      LOCAL_CUSTODIAN_CODE_DESCRIPTION: "KIRKLEES",
      UDPRN: "10668197",
    },
    {
      ADDRESS: "10a, Mayfield Avenue, Weston-Super-Mare",
      BUILDING_NAME: "10A",
      THOROUGHFARE_NAME: "MAYFIELD AVENUE",
      POST_TOWN: "WESTON-SUPER-MARE",
      POSTCODE: "BS22 6AA",
      LOCAL_CUSTODIAN_CODE_DESCRIPTION: "NORTH SOMERSET",
      UDPRN: "2916679",
    },
  ];

  await behaviourForPost(config)(req, res, next);

  expect(req.session.postcodeLookupResults).toEqual(expectedAddresses);
  expect(req.session.postcodeLookupError).toBe(undefined);
  expect(req.session.fromManual).toBe(false);
  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledTimes(1);
  expect(next).toBeCalled();

  resetStubs();
});

test("behaviourForPost() handles when no addresses found", async () => {
  // Set return values for stubs
  isEmpty.mockReturnValue(true);
  addressResults.mockImplementation(() =>
    Promise.resolve({ data: TEST_FIXTURES_NO_RESULTS }),
  );
  const { behaviourForPost } = require("./postcode");

  const req = {
    t: (string) => string,
    headers: { "x-forwarded-for": "100.200.0.45" },
    body: { postcode: "BS7 8EE" },
    session: {
      id: "skdjfhs-sdfnks-sdfhbsd",
      postcodeLookupResults: undefined,
      postcodeLookupError: undefined,
      fromManual: undefined,
    },
  };
  const res = { locals: {} };
  const next = jest.fn();

  const expectedAddresses = [];

  await behaviourForPost(config)(req, res, next);

  expect(req.session.postcodeLookupResults).toEqual(expectedAddresses);
  expect(req.session.postcodeLookupError).toBe(undefined);
  expect(req.session.fromManual).toBe(true);
  expect(res.locals).toEqual(expectedNoErrorsFound(req.body));
  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledTimes(1);
  expect(next).toBeCalled();

  resetStubs();
});

test("behaviourForPost() handles address lookup error", async () => {
  // Set return values for stubs
  isEmpty.mockReturnValue(true);
  addressResults.mockImplementation(() => Promise.reject(new Error("error")));
  const { behaviourForPost } = require("./postcode");

  const req = {
    t: (string) => string,
    headers: { "x-forwarded-for": "100.200.0.45" },
    body: { postcode: "BS7 8EE" },
    session: {
      id: "skdjfhs-sdfnks-sdfhbsd",
      postcodeLookupResults: undefined,
      postcodeLookupError: undefined,
      fromManual: undefined,
    },
  };
  const res = { locals: {} };
  const next = jest.fn();

  await behaviourForPost(config)(req, res, next);

  expect(req.session.postcodeLookupResults).toEqual([]);
  expect(req.session.postcodeLookupError).toBe(true);
  expect(req.session.fromManual).toBe(true);
  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledTimes(1);
  expect(next).toBeCalled();
  expect(errorSpy).toBeCalledWith(
    "Error looking up address for postcode: Error: error",
    req,
  );
  expect(res.locals).toEqual(expectedNoErrorsFound(req.body));

  resetStubs();
});

test("behaviourForPost() does not call os places when there are validation errors", async () => {
  // Set return values for stubs
  isEmpty.mockReturnValue(false);
  addressResults.mockImplementation(() =>
    Promise.resolve({ data: TEST_FIXTURES }),
  );
  const { behaviourForPost } = require("./postcode");

  const req = {};
  const res = {};
  const next = jest.fn();

  await behaviourForPost(config)(req, res, next);
  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledTimes(0);
  expect(addressResults).toBeCalledTimes(0);
  resetStubs();
});

test("behaviourForPost() handles 400 response from OS places API", async () => {
  // Set return values for stubs
  const cause = {
    toString: () => "mocking Error.osPlaces()",
    stack: "mock cause error stack",
  };
  const osPlacesError = wrapError({
    cause: cause,
    message: "Os places error",
    statusCode: 400,
  });
  isEmpty.mockReturnValue(true);
  addressResults.mockImplementation(() => Promise.reject(osPlacesError));
  const { behaviourForPost } = require("./postcode");

  const req = {
    t: (string) => string,
    headers: { "x-forwarded-for": "100.200.0.45" },
    body: { postcode: "BS14TM" },
    session: {
      id: "skdjfhs-sdfnks-sdfhbsd",
      postcodeLookupResults: undefined,
      postcodeLookupError: undefined,
      fromManual: undefined,
    },
  };
  const res = { locals: {} };
  const next = jest.fn();

  await behaviourForPost(config)(req, res, next);

  expect(req.session.postcodeLookupResults).toEqual([]);
  expect(req.session.postcodeLookupError).toBe(true);
  expect(req.session.fromManual).toBe(true);
  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledTimes(1);
  expect(next).toBeCalled();
  expect(errorSpy).toBeCalledWith(
    "Error looking up address for postcode: Error: Os places error. mocking Error.osPlaces()",
    req,
  );
  expect(res.locals).toEqual(expectedNoErrorsFound(req.body));

  resetStubs();
});

test("behaviourForPost() resets address in session", () => {
  // Set return values for stubs
  isEmpty.mockReturnValue(true);
  addressResults.mockImplementation(() =>
    Promise.resolve({ data: TEST_FIXTURES }),
  );
  const { behaviourForPost } = require("./postcode");

  const req = {
    t: (string) => string,
    session: {
      claim: {
        firstName: "Eric",
        addressLine1: "29 Acacia Road",
        addressLine2: "Downtown",
        townOrCity: "Nuttytown",
        postcode: "BN11NA",
      },
    },
  };

  const res = { locals: {} };
  const next = jest.fn();

  const expectedClaim = {
    firstName: "Eric",
    addressLine1: "",
    addressLine2: "",
    townOrCity: "",
    postcode: "",
  };

  behaviourForPost(config)(req, res, next);

  expect(req.session.claim).toEqual(expectedClaim);
  resetStubs();
});

test("behaviourForPost() does not reset address in session if validation errors exist", () => {
  // Set return values for stubs
  isEmpty.mockReturnValue(false);
  addressResults.mockImplementation(() =>
    Promise.resolve({ data: TEST_FIXTURES }),
  );
  const { behaviourForPost } = require("./postcode");

  const claim = {
    firstName: "Eric",
    addressLine1: "29 Acacia Road",
    addressLine2: "Downtown",
    townOrCity: "Nuttytown",
    postcode: "BN11NA",
  };

  // Create copies of claim object to ensure they are compared by value
  const req = { session: { claim: { ...claim } } };
  const res = {};
  const next = jest.fn();
  const expectedClaim = { ...claim };

  behaviourForPost(config)(req, res, next);

  expect(req.session.claim).toEqual(expectedClaim);
  resetStubs();
});

test("previous() returns name url when previous query param undefined", () => {
  const req = {
    query: {
      previous: undefined,
    },
  };

  const { previous } = require("./postcode");

  const result = previous(req);

  expect(result).toEqual("/test-context/name");
});

test("previous() returns select address url when previous query param is set", () => {
  const req = {
    query: {
      previous: "something",
    },
  };

  const { previous } = require("./postcode");

  const result = previous(req);

  expect(result).toEqual("/test-context/what-is-your-address");
});
