import { compose, map, pick, prop, propOr } from "ramda";
import { OS_PLACES_ADDRESS_KEYS } from "../constants";
import { toTitleCase } from "../formats";

const RESULTS_PROP = "results";
const DELIVERY_POINT_ADDRESS_PROP = "DPA";

// Transform single line address field (ADDRESS) for use in UI:
// - Convert to title case
// - Remove postcode
const transformAddressField = (address) => ({
  ...address,
  ADDRESS: toTitleCase(address.ADDRESS.replace(`, ${address.POSTCODE}`, "")),
});

const transformAddress = compose(
  transformAddressField,
  pick(OS_PLACES_ADDRESS_KEYS),
  prop(DELIVERY_POINT_ADDRESS_PROP),
);

const transformOsPlacesApiResponse = compose(
  map(transformAddress),
  propOr([], RESULTS_PROP),
);

export { transformAddress, transformOsPlacesApiResponse };
