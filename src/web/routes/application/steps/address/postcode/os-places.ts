import axios from "axios";
import moment from "moment";
import { logger } from "../../../../../logger/logger";

const REQUEST_TIMEOUT = 5000;
const OS_PLACES_API_PATH = "/search/places/v1/postcode";

const standardisePostcode = (postcode) =>
  postcode.toUpperCase().replace(/\s/g, "");

const getAddressLookupResults = async (config, postcode) => {
  const { OS_PLACES_URI, OS_PLACES_API_KEY } = config.environment;
  const standardisedPostcode = standardisePostcode(postcode);
  const requestStartTime = moment();

  const uri = `${OS_PLACES_URI}${OS_PLACES_API_PATH}?postcode=${standardisedPostcode}&key=${OS_PLACES_API_KEY}&lr=en`;
  const response = await axios.get(uri, {
    timeout: REQUEST_TIMEOUT,
  });

  const requestEndTime = moment();
  const requestTimeInSeconds = requestEndTime.diff(
    requestStartTime,
    "seconds",
    true,
  );
  logger.debug(
    `OS Places API request time in seconds: ${requestTimeInSeconds}`,
  );

  return response;
};

export { getAddressLookupResults, standardisePostcode };
