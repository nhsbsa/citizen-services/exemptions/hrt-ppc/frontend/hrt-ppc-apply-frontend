import { validationResult } from "express-validator";
import { transformOsPlacesApiResponse } from "./adapters";
import { validate } from "./validate";
import { sanitize } from "../sanitize";
import { contextPath } from "../../../paths/context-path";
import { stateMachine } from "../../../flow-control/state-machine";
import { logger } from "../../../../../logger/logger";
import { getAddressLookupResults } from "./os-places";
import * as states from "../../../flow-control/states";
import * as actions from "../../../flow-control/state-machine/actions";
import * as constants from "../constants";
import { isUndefined } from "../../../../../../common/predicates";
import { searchAndReturnKickoutIfExemptionFound } from "../search";
const { SET_NEXT_ALLOWED_PATH } = actions;
const { ADDRESS_KEYS } = constants;

const resetAddressKey = (address, key) => ({ ...address, [key]: "" });

const resetAddressOnClaim = (claim) => ({
  ...claim,
  ...ADDRESS_KEYS.reduce(resetAddressKey, {}),
});

const behaviourForGet = (config, journey) => (req, res, next) => {
  req.session.fromManual = true;
  resetPostcodeLookupError(req);
  stateMachine.setState(states.IN_PROGRESS, req, journey);
  // There is a hyperlink on this page that directly takes you to the manual-address (next step)
  // therefore this line is needed to prevent the state machine from redirecting the user back to postcode.
  stateMachine.dispatch(
    SET_NEXT_ALLOWED_PATH,
    req,
    journey,
    contextPath("/manual-address"),
  );
  next();
};

const behaviourForPost = (config, journey) => async (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  req.session.claim = resetAddressOnClaim(req.session.claim);

  const result = await searchAndReturnKickoutIfExemptionFound(
    req,
    res,
    journey,
    next,
  );
  if (result !== null) return result;

  try {
    const addressLookupResults = await getAddressLookupResults(
      config,
      req.body.postcode,
    );

    const results = transformOsPlacesApiResponse(addressLookupResults.data);

    req.session.postcodeLookupResults = results;
    req.session.fromManual = false;

    if (Array.isArray(results) && results.length === 0) {
      setNoAddressFoundError(req, res);
    }

    return next();
  } catch (error) {
    logger.error(`Error looking up address for postcode: ${error}`, req);
    req.session.postcodeLookupResults = [];
    req.session.postcodeLookupError = true;
    setNoAddressFoundError(req, res);
    return next();
  }
};

const setNoAddressFoundError = (req, res) => {
  req.session.fromManual = true;
  res.locals.errorTitleText = req.t("validation:errorTitleText");
  res.locals.errors = [
    {
      path: "postcode",
      msg: req.t("postcode.noAddressFoundError"),
    },
  ];
  res.locals.claim = {
    ...res.locals.claim,
    ...req.body,
  };
};

const resetPostcodeLookupError = (req) => {
  if (req.session.postcodeLookupError) {
    delete req.session.postcodeLookupError;
  }
};

const previous = (req) => {
  const previousQueryParam = req.query.previous;
  // TODO check IN_REVIEW when implementing check-your-answers page jira Ref link https://bsa2468.atlassian.net/browse/HPPC-38
  if (isUndefined(previousQueryParam)) {
    return contextPath(`/name`);
  } else {
    return contextPath(`/what-is-your-address`);
  }
};

const pageContent = ({ translate, req }) => ({
  title: translate("postcode.title"),
  heading: translate("postcode.heading"),
  paragraphOne: translate("postcode.paragraphOne"),
  postcodeLabel: translate("postcode.postcodeLabel"),
  linkName: translate("postcode.linkName"),
  buttonText: translate("postcode.buttonText"),
  cancel: req.t("cancel"),
  previous: previous(req),
});

const requestBody = (session) => ({
  postcode: session.claim.postcode,
});

const postcode = {
  path: "/postcode",
  template: "postcode",
  pageContent,
  behaviourForPost,
  behaviourForGet,
  requestBody,
  toggle: "ADDRESS_LOOKUP_ENABLED",
  sanitize,
  validate,
};

export { requestBody, postcode, behaviourForGet, behaviourForPost, previous };
