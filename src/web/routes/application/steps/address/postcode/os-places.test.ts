import axios from "axios";

jest.mock("axios");

import { standardisePostcode, getAddressLookupResults } from "./os-places";

const config = {
  environment: {
    OS_PLACES_URI: "localhost:8150",
    OS_PLACES_API_KEY: "123",
    GA_TRACKING_ID: "UA-1330000-1",
    GOOGLE_ANALYTICS_URI: "http://localhost:8150/collect",
  },
};

test("standardisePostcode() standardises the postcode", () => {
  expect(standardisePostcode("AB1 1AB")).toBe("AB11AB");
  expect(standardisePostcode("AB1      1AB")).toBe("AB11AB");
  expect(standardisePostcode("   AB1 1AB ")).toBe("AB11AB");
  expect(standardisePostcode("ab11ab")).toBe("AB11AB");
});

test("getAddressLookupResults() calls os places with the correct arguments", async () => {
  const mock = jest.fn();
  axios.get = mock.mockResolvedValue("{}");
  await getAddressLookupResults(config, "AB1 1AB");
  const uri =
    "localhost:8150/search/places/v1/postcode?postcode=AB11AB&key=123&lr=en";
  const expectedOSPlacesRequestArgs = {
    timeout: 5000,
  };

  expect(mock).toBeCalledTimes(1);
  expect(mock).nthCalledWith(1, uri, expectedOSPlacesRequestArgs);
});
