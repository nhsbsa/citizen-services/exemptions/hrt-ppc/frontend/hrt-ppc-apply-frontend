import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend";
import expect from "expect";
import { validate } from "./validate";
import { FieldValidationError } from "express-validator";

test("validation middleware errors for postcode field is empty", async () => {
  const req = {
    body: {
      postcode: "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const errorOne = result.array()[0] as FieldValidationError;

  expect(result.array().length).toBe(1);
  expect(errorOne.path).toBe("postcode");
  expect(errorOne.msg).toBe("validation:missingPostcode");
});

test("validation middleware errors for postcode field is invalid", async () => {
  const req = {
    body: {
      postcode: "D@£ AB%",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const errorOne = result.array()[0] as FieldValidationError;

  expect(result.array().length).toBe(1);
  expect(errorOne.path).toBe("postcode");
  expect(errorOne.msg).toBe("validation:invalidPostcode");
});

test("validation middleware errors for postcode field when invalid sanitisation", async () => {
  const req = {
    body: {
      postcode: "aa11 1aa",
      sanitizedPostcode: " ",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const errorOne = result.array()[0] as FieldValidationError;

  expect(result.array().length).toBe(1);
  expect(errorOne.path).toBe("postcode");
  expect(errorOne.msg).toBe("validation:invalidPostcode");
});

test("No validation error when postcode field is valid", async () => {
  const req = {
    body: {
      postcode: "AA1 1AA",
      sanitizedPostcode: "AA1 1AA",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.array().length).toBe(0);
});

test("no validation error when postcode is lowercase but valid", async () => {
  const req = {
    body: {
      postcode: "aa1 1aa",
      sanitizedPostcode: "AA1 1AA",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.array().length).toBe(0);
});

test("no validation error when postcode is lowercase with two spaces but valid", async () => {
  const req = {
    body: {
      postcode: "aa1  1aa",
      sanitizedPostcode: "AA1 1AA",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.array().length).toBe(0);
});

test("no validation error when postcode is lowercase with more than two spaces", async () => {
  const req = {
    body: {
      postcode: "aa11     1aa",
      sanitizedPostcode: "AA1 1AA",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.array().length).toBe(0);
});

test("no validation error when postcode is valid in different format", async () => {
  const req = {
    body: {
      postcode: "sw1a   1aa",
      sanitizedPostcode: "SW1A 1AA",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.array().length).toBe(0);
});

test("no validation error when postcode is mix of lower & upper-case but valid", async () => {
  const req = {
    body: {
      postcode: "aA1 1Aa",
      sanitizedPostcode: "AA1 1AA",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.array().length).toBe(0);
});

test("no validation error when postcode is lowercase with no space but valid", async () => {
  const req = {
    body: {
      postcode: "aa11aa",
      sanitizedPostcode: "AA1 1AA",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.array().length).toBe(0);
});
