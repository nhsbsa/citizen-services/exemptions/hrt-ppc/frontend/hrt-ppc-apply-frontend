import expect from "expect";
import { sanitize } from "./sanitize";

const next = jest.fn();

test("sanitize replaces multiple whitespace with one, converts to uppercase and saves to a new variable", () => {
  const req = {
    body: {
      postcode: "bs1     4tb",
    },
  };
  const expectedSanitizedPostcode = "BS1 4TB";
  const expectedPostcode = "bs1     4tb";

  sanitize()(req, {}, next);
  expect(req.body["sanitizedPostcode"]).toEqual(expectedSanitizedPostcode);
  expect(req.body.postcode).toEqual(expectedPostcode);
  expect(next).toHaveBeenCalled();
});

test("sanitize adds a single space before the last 3 ending characters, and saves to a new variable for postcode CB30QB", () => {
  const req = {
    body: {
      postcode: "cb3  0qb",
    },
  };
  const expectedSanitizedPostcode = "CB3 0QB";
  const expectedPostcode = "cb3  0qb";

  sanitize()(req, {}, next);

  expect(req.body["sanitizedPostcode"]).toEqual(expectedSanitizedPostcode);
  expect(req.body.postcode).toEqual(expectedPostcode);
  expect(next).toHaveBeenCalled();
});

test("sanitize adds a single space before the last 3 ending characters, and saves to a new variable for postcode N12NL", () => {
  const req = {
    body: {
      postcode: "n1  2nl",
    },
  };
  const expectedSanitizedPostcode = "N1 2NL";
  const expectedPostcode = "n1  2nl";

  sanitize()(req, {}, next);

  expect(req.body["sanitizedPostcode"]).toEqual(expectedSanitizedPostcode);
  expect(req.body.postcode).toEqual(expectedPostcode);
  expect(next).toHaveBeenCalled();
});

test("sanitize adds a single space before the last 3 ending characters, and saves to a new variable for postcode CB249LQ", () => {
  const req = {
    body: {
      postcode: "cb24  9lq",
    },
  };
  const expectedSanitizedPostcode = "CB24 9LQ";
  const expectedPostcode = "cb24  9lq";

  sanitize()(req, {}, next);

  expect(req.body["sanitizedPostcode"]).toEqual(expectedSanitizedPostcode);
  expect(req.body.postcode).toEqual(expectedPostcode);
  expect(next).toHaveBeenCalled();
});

test("sanitize adds a single space before the last 3 ending characters, and saves to a new variable for postcode OX145FB", () => {
  const req = {
    body: {
      postcode: "ox14  5fb",
    },
  };
  const expectedSanitizedPostcode = "OX14 5FB";
  const expectedPostcode = "ox14  5fb";

  sanitize()(req, {}, next);

  expect(req.body["sanitizedPostcode"]).toEqual(expectedSanitizedPostcode);
  expect(req.body.postcode).toEqual(expectedPostcode);
  expect(next).toHaveBeenCalled();
});

test("sanitize adds a single space before the last 3 ending characters, and saves to a new variable for postcode OX145FC", () => {
  const req = {
    body: {
      postcode: "ox145fc",
    },
  };
  const expectedSanitizedPostcode = "OX14 5FC";
  const expectedPostcode = "ox145fc";
  const next = jest.fn();

  sanitize()(req, {}, next);

  expect(req.body["sanitizedPostcode"]).toEqual(expectedSanitizedPostcode);
  expect(req.body.postcode).toEqual(expectedPostcode);
  expect(next).toHaveBeenCalled();
});
