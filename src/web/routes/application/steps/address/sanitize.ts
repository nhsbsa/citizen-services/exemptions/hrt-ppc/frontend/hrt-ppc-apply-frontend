import { replaceMultipleSpacesAndAddSingleSpaceThenUppercase } from "@nhsbsa/health-charge-exemption-common-frontend";

const sanitize = () => (req, res, next) => {
  req.body.sanitizedPostcode =
    replaceMultipleSpacesAndAddSingleSpaceThenUppercase(req.body.postcode);
  next();
};

export { sanitize };
