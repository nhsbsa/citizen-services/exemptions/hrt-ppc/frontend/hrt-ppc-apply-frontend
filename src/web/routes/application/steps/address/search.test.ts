import * as httpStatus from "http-status-codes";
import expect from "expect";
import { SET_NEXT_ALLOWED_PATH } from "../../flow-control/state-machine/actions";

const mockSearchExemptions = jest.fn();
const mockStateMachineDispatch = jest.fn();
const exemptionFoundUrl = "/test-context/existing-exemption-found";

import { searchAndReturnKickoutIfExemptionFound } from "./search";
import { wrapError } from "../../errors";

jest.mock("../../flow-control/state-machine", () => ({
  ...jest.requireActual("../../flow-control/state-machine"),
  stateMachine: { dispatch: mockStateMachineDispatch },
}));
jest.mock("../common/search-citizen-certificates-provider", () => ({
  searchExemptions: mockSearchExemptions,
}));

beforeEach(() => {
  jest.clearAllMocks();
});

describe("searchAndReturnKickoutIfExemptionFound()", () => {
  test("should set redirect to `existing-exemption-found` when `activeExemptions` is true", async () => {
    mockSearchExemptions.mockReturnValue(
      Promise.resolve({ activeExemptions: true }),
    );

    const req = {
      session: { existingExemptionFound: undefined },
    };
    const res = {
      redirect: jest.fn(),
    };
    const next = jest.fn();

    await searchAndReturnKickoutIfExemptionFound(req, res, undefined, next);

    expect(next).toBeCalledTimes(0);
    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toBeCalledWith(exemptionFoundUrl);
    expect(req.session.existingExemptionFound).toBe(true);
    expect(mockSearchExemptions).toBeCalledTimes(1);
    expect(mockSearchExemptions).toBeCalledWith(req);
    expect(mockStateMachineDispatch).toBeCalledTimes(1);
    expect(mockStateMachineDispatch).toBeCalledWith(
      SET_NEXT_ALLOWED_PATH,
      req,
      undefined,
      exemptionFoundUrl,
    );
  });

  test("should return null when `activeExemptions` is false", async () => {
    mockSearchExemptions.mockReturnValue(
      Promise.resolve({ activeExemptions: false }),
    );

    const req = {
      session: { existingExemptionFound: undefined },
    };
    const res = {
      redirect: jest.fn(),
    };
    const next = jest.fn();

    const result = await searchAndReturnKickoutIfExemptionFound(
      req,
      res,
      undefined,
      next,
    );

    expect(next).toBeCalledTimes(0);
    expect(res.redirect).toBeCalledTimes(0);
    expect(req.session.existingExemptionFound).toBe(undefined);
    expect(result).toEqual(null);
    expect(mockSearchExemptions).toBeCalledTimes(1);
    expect(mockSearchExemptions).toBeCalledWith(req);
    expect(mockStateMachineDispatch).toBeCalledTimes(0);
  });

  test("should reset `existingExemptionFound` to true in session when it was false previously", async () => {
    mockSearchExemptions.mockReturnValue(
      Promise.resolve({ activeExemptions: true }),
    );

    const req = {
      session: {
        existingExemptionFound: false,
      },
    };
    const res = {
      redirect: jest.fn(),
    };
    const next = jest.fn();

    await searchAndReturnKickoutIfExemptionFound(req, res, undefined, next);

    expect(next).toBeCalledTimes(0);
    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toBeCalledWith(exemptionFoundUrl);
    expect(req.session.existingExemptionFound).toBe(true);
    expect(mockSearchExemptions).toBeCalledTimes(1);
    expect(mockSearchExemptions).toBeCalledWith(req);
    expect(mockStateMachineDispatch).toBeCalledTimes(1);
    expect(mockStateMachineDispatch).toBeCalledWith(
      SET_NEXT_ALLOWED_PATH,
      req,
      undefined,
      exemptionFoundUrl,
    );
  });

  test("should call next with wrapped error when searchExemptions throws error", async () => {
    mockSearchExemptions.mockReturnValue(
      Promise.reject(new Error("Axios error")),
    );
    const req = {
      path: "/test",
      session: {
        existingExemptionFound: undefined,
      },
    };
    const res = {
      redirect: jest.fn(),
    };
    const next = jest.fn();
    const expectedError = wrapError({
      cause: new Error("Axios error"),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    });

    await searchAndReturnKickoutIfExemptionFound(req, res, undefined, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(expectedError);
    expect(res.redirect).toBeCalledTimes(0);
    expect(req.session.existingExemptionFound).toBe(undefined);
    expect(mockSearchExemptions).toBeCalledTimes(1);
    expect(mockSearchExemptions).toBeCalledWith(req);
    expect(mockStateMachineDispatch).toBeCalledTimes(0);
  });
});
