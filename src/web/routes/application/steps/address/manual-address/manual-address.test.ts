import expect from "expect";
import { assocPath } from "ramda";
import { APPLY_JOURNEY_NAME } from "../../../constants";
import { IN_PROGRESS, IN_REVIEW } from "../../../flow-control/states";

let req;

const applyJourney = { name: APPLY_JOURNEY_NAME };

const mockSearchAndReturnKickoutIfExemptionFound = jest.fn();
const mockGetStateFromSession = jest.fn();
const isEmpty = jest.fn();

jest.mock("../search", () => ({
  searchAndReturnKickoutIfExemptionFound:
    mockSearchAndReturnKickoutIfExemptionFound,
}));

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

jest.mock("../../../flow-control/session-accessors", () => ({
  getStateFromSession: mockGetStateFromSession,
}));

import {
  contentSummary,
  isNavigable,
  requestBody,
  pageContent,
  behaviourForPost,
} from "./manual-address";

beforeEach(() => {
  req = {
    t: (string) => string,
    session: {
      addressSubmittedFromLookup: false,
      claim: {
        addressLine1: "Flat b",
        addressLine2: "123 Fake Street",
        townOrCity: "Springfield",
        postcode: "bs1 4tb",
        sanitizedPostcode: "BS1 4TB",
      },
    },
  };
  jest.clearAllMocks();
  mockSearchAndReturnKickoutIfExemptionFound.mockResolvedValue(null);
  mockGetStateFromSession.mockReturnValue(IN_PROGRESS);
  isEmpty.mockReturnValue(true);
});

test("Address contentSummary() should return content summary in correct format", () => {
  const result = contentSummary(req);
  const expected = {
    id: "#address-line-1",
    key: "address.summaryKey",
    section: "aboutYou",
    value: "Flat b\n123 Fake Street\nSpringfield\nbs1 4tb",
    visuallyHidden: "address.hiddenChangeText",
  };

  expect(result).toEqual(expected);
});

test("Address contentSummary() should return content summary in correct format without address line 2", () => {
  const testReq = assocPath(["session", "claim", "addressLine2"], "", req);
  const result = contentSummary(testReq);
  const expected = {
    id: "#address-line-1",
    key: "address.summaryKey",
    section: "aboutYou",
    value: "Flat b\nSpringfield\nbs1 4tb",
    visuallyHidden: "address.hiddenChangeText",
  };

  expect(result).toEqual(expected);
});

test("Address contentSummary() should return content summary in correct format with address line 2 undefined", () => {
  const testReq = assocPath(
    ["session", "claim", "addressLine2"],
    undefined,
    req,
  );
  const result = contentSummary(testReq);
  const expected = {
    id: "#address-line-1",
    key: "address.summaryKey",
    section: "aboutYou",
    value: "Flat b\nSpringfield\nbs1 4tb",
    visuallyHidden: "address.hiddenChangeText",
  };

  expect(result).toEqual(expected);
});

test("Address contentSummary() is null when there is a selected address on the session, addressSubmittedFromLookup is undefined", () => {
  const testReq = assocPath(
    ["session", "claim", "selectedAddress"],
    "test address",
    req,
  );
  testReq.session.addressSubmittedFromLookup = undefined;

  const result = contentSummary(testReq);

  expect(result).toEqual(null);
});

test("Address contentSummary() is null when there is a selected address on the session, addressSubmittedFromLookup is true", () => {
  const testReq = assocPath(
    ["session", "claim", "selectedAddress"],
    "test address",
    req,
  );
  testReq.session.addressSubmittedFromLookup = true;

  const result = contentSummary(testReq);

  expect(result).toEqual(null);
});

test("Address contentSummary() is null when there is no selected address on the session, addressSubmittedFromLookup is true", () => {
  req.session.addressSubmittedFromLookup = true;

  const result = contentSummary(req);

  expect(result).toEqual(null);
});

test("isNavigable() returns true when there is no session", () => {
  const result = isNavigable(undefined, undefined);

  expect(mockGetStateFromSession).toBeCalledTimes(1);
  expect(mockGetStateFromSession).toBeCalledWith(undefined, applyJourney);
  expect(result).toBe(true);
});

test("isNavigable() returns true when there is no selected address", () => {
  const session = {
    claim: {},
  };

  const result = isNavigable(req, session);

  expect(mockGetStateFromSession).toBeCalledTimes(1);
  expect(mockGetStateFromSession).toBeCalledWith(req, applyJourney);
  expect(result).toBe(true);
});

test("isNavigable() returns true when there is a selected address and state is `IN_REVIEW`", () => {
  mockGetStateFromSession.mockReturnValue(IN_REVIEW);
  const session = {
    claim: {
      selectedAddress:
        "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    },
  };

  const result = isNavigable(req, session);

  expect(mockGetStateFromSession).toBeCalledTimes(1);
  expect(mockGetStateFromSession).toBeCalledWith(req, applyJourney);
  expect(result).toBe(true);
});

test("isNavigable() returns false when there is a selected address", () => {
  const session = {
    claim: {
      selectedAddress:
        "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    },
  };

  const result = isNavigable(req, session);
  expect(mockGetStateFromSession).toBeCalledTimes(1);
  expect(mockGetStateFromSession).toBeCalledWith(req, applyJourney);
  expect(result).toBe(false);
});

test("requestBody() should return request body in correct format", () => {
  const result = requestBody(req.session);

  const expected = {
    address: {
      addressLine1: "Flat b",
      addressLine2: "123 Fake Street",
      townOrCity: "Springfield",
      postcode: "BS1 4TB",
    },
  };

  expect(result).toEqual(expected);
});

test("pageContent() should match expected translated values when fromManual undefined", () => {
  const translate = (string, object?) => `${string}${object}`;
  const result = pageContent({ translate, req });

  const expected = {
    title: translate("address.title"),
    heading: translate("address.heading"),
    paragraphOne: translate("address.paragraphOne"),
    buildingAndStreetLine1of2: translate("address.buildingAndStreetLine1of2"),
    buildingAndStreetLine2of2: translate("address.buildingAndStreetLine2of2"),
    townOrCityLabel: translate("address.townOrCityLabel"),
    postcodeLabel: translate("address.postcodeLabel"),
    buttonText: translate("buttons:continue"),
    cancel: translate("cancel"),
    previous: "/test-context/postcode",
  };

  expect(result).toEqual(expected);
});

test("pageContent() should match expected translated values when fromManual true", () => {
  req.session["fromManual"] = true;
  const translate = (string, object?) => `${string}${object}`;
  const result = pageContent({ translate, req });

  const expected = {
    title: translate("address.title"),
    heading: translate("address.heading"),
    paragraphOne: translate("address.paragraphOne"),
    buildingAndStreetLine1of2: translate("address.buildingAndStreetLine1of2"),
    buildingAndStreetLine2of2: translate("address.buildingAndStreetLine2of2"),
    townOrCityLabel: translate("address.townOrCityLabel"),
    postcodeLabel: translate("address.postcodeLabel"),
    buttonText: translate("buttons:continue"),
    cancel: translate("cancel"),
    previous: "/test-context/postcode",
  };

  expect(result).toEqual(expected);
});

test("pageContent() should match expected translated values when fromManual false", () => {
  req.session["fromManual"] = false;
  const translate = (string, object?) => `${string}${object}`;
  const result = pageContent({ translate, req });

  const expected = {
    title: translate("address.title"),
    heading: translate("address.heading"),
    paragraphOne: translate("address.paragraphOne"),
    buildingAndStreetLine1of2: translate("address.buildingAndStreetLine1of2"),
    buildingAndStreetLine2of2: translate("address.buildingAndStreetLine2of2"),
    townOrCityLabel: translate("address.townOrCityLabel"),
    postcodeLabel: translate("address.postcodeLabel"),
    buttonText: translate("buttons:continue"),
    cancel: translate("cancel"),
    previous: "/test-context/what-is-your-address",
  };

  expect(result).toEqual(expected);
});

test("pageContent() should set previous to `check-your-answers` when state is `IN_REVIEW`", () => {
  mockGetStateFromSession.mockReturnValue(IN_REVIEW);
  const translate = (string, object?) => `${string}${object}`;
  const result = pageContent({ translate, req });

  const expected = {
    title: translate("address.title"),
    heading: translate("address.heading"),
    paragraphOne: translate("address.paragraphOne"),
    buildingAndStreetLine1of2: translate("address.buildingAndStreetLine1of2"),
    buildingAndStreetLine2of2: translate("address.buildingAndStreetLine2of2"),
    townOrCityLabel: translate("address.townOrCityLabel"),
    postcodeLabel: translate("address.postcodeLabel"),
    buttonText: translate("buttons:continue"),
    cancel: translate("cancel"),
    previous: "/test-context/check-your-answers",
  };

  expect(result).toEqual(expected);
});

test("behaviourForPost() handles when async call returns null", async () => {
  const config = {};
  const req = {
    session: { addressSubmittedFromLookup: undefined },
  };
  const res = jest.fn();
  const next = jest.fn();

  await behaviourForPost(config, undefined)(req, res, next);

  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledTimes(1);
  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledWith(
    req,
    res,
    undefined,
    next,
  );
  expect(next).toBeCalledTimes(1);
  expect(next).toBeCalledWith();
  expect(req.session.addressSubmittedFromLookup).toBe(false);
});

test("behaviourForPost() handles when async call returns redirect result", async () => {
  // Set return values for stubs
  const res = {};
  const resAfter = { redirect: "test.com" };
  mockSearchAndReturnKickoutIfExemptionFound.mockResolvedValue(resAfter);

  const config = {};
  const req = jest.fn();
  const next = jest.fn();

  const result = await behaviourForPost(config, undefined)(req, res, next);

  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledTimes(1);
  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledWith(
    req,
    res,
    undefined,
    next,
  );
  expect(next).toBeCalledTimes(0);
  expect(result).toEqual(resAfter);
});

test("behaviourForPost() calls next if validation errors exist", () => {
  // Set return values for stubs
  isEmpty.mockReturnValue(false);

  const claim = {
    addressLine1: ";;INVALID",
  };

  // Create copies of claim object to ensure they are compared by value
  const req = { session: { claim: { ...claim } } };
  const res = {};
  const next = jest.fn();

  behaviourForPost({}, undefined)(req, res, next);

  expect(mockSearchAndReturnKickoutIfExemptionFound).toBeCalledTimes(0);
  expect(next).toBeCalledTimes(1);
  expect(next).toBeCalledWith();
});
