import { validateAddress } from "@nhsbsa/health-charge-exemption-common-frontend";

const validate = () => [validateAddress()];

export { validate };
