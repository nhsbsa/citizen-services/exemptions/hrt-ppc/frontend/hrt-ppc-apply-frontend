import { validationResult } from "express-validator";
import { addressContentSummary } from "../content-summary";
import { requestBody } from "../request-body";
import { isNil, path } from "ramda";
import { sanitize } from "../sanitize";
import { validate } from "./validate";
import { contextPath } from "../../../paths/context-path";
import { searchAndReturnKickoutIfExemptionFound } from "../search";
import { getStateFromSession } from "../../../flow-control/session-accessors";
import { APPLY_JOURNEY_NAME } from "../../../constants";
import { CHECK_ANSWERS_URL } from "../../../paths/paths";
import { IN_REVIEW } from "../../../flow-control/states";

const behaviourForPost = (config, journey) => async (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  const result = await searchAndReturnKickoutIfExemptionFound(
    req,
    res,
    journey,
    next,
  );
  if (result !== null) return result;

  req.session.addressSubmittedFromLookup = false;
  next();
};

const pageContent = ({ translate, req }) => {
  let previous;
  const state = getStateFromSession(req, { name: APPLY_JOURNEY_NAME });
  if (state === IN_REVIEW) {
    previous = contextPath(CHECK_ANSWERS_URL);
  } else {
    previous =
      req.session.fromManual === false
        ? contextPath("/what-is-your-address")
        : contextPath("/postcode");
  }
  return {
    title: translate("address.title"),
    heading: translate("address.heading"),
    paragraphOne: translate("address.paragraphOne"),
    buildingAndStreetLine1of2: translate("address.buildingAndStreetLine1of2"),
    buildingAndStreetLine2of2: translate("address.buildingAndStreetLine2of2"),
    townOrCityLabel: translate("address.townOrCityLabel"),
    postcodeLabel: translate("address.postcodeLabel"),
    buttonText: translate("buttons:continue"),
    cancel: translate("cancel"),
    previous: previous,
  };
};

const contentSummary = (req) =>
  isNavigable(req, req.session) &&
  path(["session", "addressSubmittedFromLookup"], req) === false
    ? addressContentSummary(req)
    : null;

const isNavigable = (req, session) => {
  const state = getStateFromSession(req, { name: APPLY_JOURNEY_NAME });
  if (state === IN_REVIEW) {
    return true;
  }
  return isNil(path(["claim", "selectedAddress"], session));
};

const address = {
  path: "/manual-address",
  template: "manual-address",
  pageContent,
  behaviourForPost,
  requestBody,
  sanitize,
  validate,
  contentSummary,
  isNavigable,
};

export {
  contentSummary,
  requestBody,
  address,
  isNavigable,
  pageContent,
  behaviourForPost,
};
