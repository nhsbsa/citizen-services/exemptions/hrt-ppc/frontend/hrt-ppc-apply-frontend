import expect from "expect";

const mockValidateAddress = jest.fn();

jest.mock("@nhsbsa/health-charge-exemption-common-frontend", () => ({
  ...jest.requireActual("@nhsbsa/health-charge-exemption-common-frontend"),
  validateAddress: mockValidateAddress,
}));

import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend";
import { validate } from "./validate";

describe("validate", () => {
  test("calls validateAddress", async () => {
    const req = {};

    await applyExpressValidation(req, validate());

    expect(mockValidateAddress).toHaveBeenCalledTimes(1);
    expect(mockValidateAddress).toHaveBeenCalledWith();
  });
});
