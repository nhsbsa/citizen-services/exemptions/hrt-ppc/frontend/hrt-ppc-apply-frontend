import { join, filter, compose } from "ramda";

import { notIsNilOrEmpty } from "../../../../../common/predicates";
import { DEFAULT_LIST } from "../check-your-answers/constants";
import { ADDRESS_KEYS } from "./constants";

const newLineChar = "\n";
const toMultiLineString = compose(join(newLineChar), filter(notIsNilOrEmpty));

const getKeyFromClaim = (claim) => (key) => claim[key];

const addressContentSummaryId = "#address-line-1";

const addressContentSummary = (req) => ({
  id: addressContentSummaryId,
  key: req.t("address.summaryKey"),
  section: DEFAULT_LIST,
  value: toMultiLineString(
    ADDRESS_KEYS.map(getKeyFromClaim(req.session.claim)),
  ),
  visuallyHidden: req.t("address.hiddenChangeText"),
});

export { addressContentSummary, addressContentSummaryId };
