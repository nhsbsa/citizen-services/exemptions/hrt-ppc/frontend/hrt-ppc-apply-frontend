import {
  ADDRESS_LINE_1_KEY,
  TOWN_OR_CITY_KEY,
  POSTCODE_KEY,
  ADDRESS_LINE_2_KEY,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";
import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";

const requestBody = (session) => ({
  address: {
    [ADDRESS_LINE_1_KEY]: session.claim[ADDRESS_LINE_1_KEY],
    [TOWN_OR_CITY_KEY]: session.claim[TOWN_OR_CITY_KEY],
    [POSTCODE_KEY]: session.claim.sanitizedPostcode,
    ...(notIsUndefinedOrNullOrEmpty(session.claim[ADDRESS_LINE_2_KEY]) && {
      [ADDRESS_LINE_2_KEY]: session.claim[ADDRESS_LINE_2_KEY],
    }),
  },
});

export { requestBody };
