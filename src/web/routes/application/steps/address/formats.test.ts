import { toTitleCase, SINGLE_WORD_REGEX } from "./formats";
import safeRegex from "safe-regex";

test("toTitleCase() should uppercase the first letter of every word", () => {
  const result = toTitleCase("10A, MY STREET, WESTON-SUPER-MARE");

  const expected = "10a, My Street, Weston-Super-Mare";

  expect(result).toBe(expected);
});

test("single word regex is safe()", () => {
  expect(safeRegex(SINGLE_WORD_REGEX)).toBe(true);
});
