import { translateValidationMessage } from "@nhsbsa/health-charge-exemption-common-frontend";
import { check } from "express-validator";
import { notIsUndefinedOrNullOrEmpty } from "../../../../../../common/predicates";

const addressIsSelectedWhenThereAreAddressResults = (
  selectedAddress,
  { req },
) => {
  // only validate that an address is selected when there are results to select from
  if (notIsUndefinedOrNullOrEmpty(req.session.postcodeLookupResults)) {
    return notIsUndefinedOrNullOrEmpty(selectedAddress);
  }

  return true;
};

const validate = () => [
  check("selectedAddress")
    .custom(addressIsSelectedWhenThereAreAddressResults)
    .withMessage(translateValidationMessage("validation:selectAddress")),
];

export { validate, addressIsSelectedWhenThereAreAddressResults };
