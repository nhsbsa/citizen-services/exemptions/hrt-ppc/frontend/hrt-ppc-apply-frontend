import { addressIsSelectedWhenThereAreAddressResults } from "./validate";

test("empty selectedAddress field is valid when postcodeLookupResults is empty", () => {
  const selectedAddress = "";
  const req = {
    session: {
      postcodeLookupResults: [],
    },
  };

  const result = addressIsSelectedWhenThereAreAddressResults(selectedAddress, {
    req,
  });

  expect(result).toBe(true);
});

test("empty selectedAddress field is valid when postcodeLookupResults is null", () => {
  const selectedAddress = "";
  const req = {
    session: {
      postcodeLookupResults: null,
    },
  };

  const result = addressIsSelectedWhenThereAreAddressResults(selectedAddress, {
    req,
  });

  expect(result).toBe(true);
});

test("empty selectedAddress field is valid when postcodeLookupResults is undefined", () => {
  const selectedAddress = undefined;
  const req = {
    session: {
      postcodeLookupResults: null,
    },
  };

  const result = addressIsSelectedWhenThereAreAddressResults(selectedAddress, {
    req,
  });

  expect(result).toBe(true);
});

test("empty selectedAddress is invalid when there are postcodeLookupResults", () => {
  const selectedAddress = "";
  const req = {
    session: {
      postcodeLookupResults: ["Address 1, BS1 1AA"],
    },
  };

  const result = addressIsSelectedWhenThereAreAddressResults(selectedAddress, {
    req,
  });

  expect(result).toBe(false);
});

test("null selectedAddress is invalid when there are postcodeLookupResults", () => {
  const selectedAddress = null;
  const req = {
    session: {
      postcodeLookupResults: ["Address 1, BS1 1AA"],
    },
  };

  const result = addressIsSelectedWhenThereAreAddressResults(selectedAddress, {
    req,
  });

  expect(result).toBe(false);
});

test("undefined selectedAddress is invalid when there are postcodeLookupResults", () => {
  const selectedAddress = undefined;
  const req = {
    session: {
      postcodeLookupResults: ["Address 1, BS1 1AA"],
    },
  };

  const result = addressIsSelectedWhenThereAreAddressResults(selectedAddress, {
    req,
  });

  expect(result).toBe(false);
});
