import { partial } from "ramda";
import { states, testUtils } from "../../../flow-control";
import {
  behaviourForGet,
  behaviourForPost,
  contentSummary,
  isNavigable,
  findAddress,
} from "./select-address";
import { IN_REVIEW } from "../../../flow-control/states";
import { CHECK_ANSWERS_URL } from "../../../paths/paths";

const { buildSessionForJourney, getNextAllowedPathForJourney } = testUtils;
const { IN_PROGRESS } = states;

const CONFIG = {};
const APPLY = "apply";
const JOURNEY = { name: APPLY };
const STEP = { path: "/step-path" };

const getNextAllowedPathForApplyJourney = partial(
  getNextAllowedPathForJourney,
  [APPLY],
);

const POSTCODE_LOOKUP_RESULTS = [
  {
    ADDRESS: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
    BUILDING_NUMBER: "1",
    THOROUGHFARE_NAME: "VALLEY ROAD",
    DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
    POST_TOWN: "PLYMOUTH",
    POSTCODE: "PL7 1RF",
    LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    UDPRN: "50265368",
  },
  {
    ADDRESS: "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    ORGANISATION_NAME: "DULUX DECORATOR CENTRE",
    BUILDING_NUMBER: "2",
    THOROUGHFARE_NAME: "VALLEY ROAD",
    DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
    POST_TOWN: "PLYMOUTH",
    POSTCODE: "PL7 1RF",
    LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    UDPRN: "19000955",
  },
  {
    ADDRESS: "MILL AUTOS, 3, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    ORGANISATION_NAME: "MILL AUTOS",
    BUILDING_NUMBER: "3",
    THOROUGHFARE_NAME: "VALLEY ROAD",
    DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
    POST_TOWN: "PLYMOUTH",
    POSTCODE: "PL7 1RF",
    LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    UDPRN: "19000927",
  },
];

test("behaviourForGet() adds addresses to session.stepData and resets the address", () => {
  const req = {
    t: (key, parameters) => `${parameters.count} addresses found`,
    session: {
      postcodeLookupResults: POSTCODE_LOOKUP_RESULTS,
      postcodeLookupError: false,
      claim: {
        selectedAddress:
          "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
        addressId: "19000955",
      },
      stepData: {},
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = { locals: { postcodeLookupError: undefined } };
  const next = jest.fn();
  const expected = [
    {
      value: "",
      text: "3 addresses found",
      disabled: true,
      selected: false,
    },
    {
      text: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
      value: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
      selected: false,
    },
    {
      text: "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
      value: "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
      selected: true,
    },
    {
      text: "MILL AUTOS, 3, VALLEY ROAD, PLYMOUTH, PL7 1RF",
      value: "MILL AUTOS, 3, VALLEY ROAD, PLYMOUTH, PL7 1RF",
      selected: false,
    },
  ];

  behaviourForGet(CONFIG, JOURNEY, STEP)(req, res, next);

  expect(req.session.claim.selectedAddress).toBe(undefined);
  expect(getNextAllowedPathForApplyJourney(req)).toBe(
    "/test-context/manual-address",
  );
  const stepData = req.session.stepData["/step-path"];
  expect(stepData.addresses).toEqual(expected);
  expect(res.locals.postcodeLookupError).toBe(false);
  expect(next).toHaveBeenCalled();
});

test("behaviourForGet() adds an empty array to stepData if no addresses found", () => {
  const req = {
    t: (key, parameters) => `${parameters.count} addresses found`,
    session: {
      postcodeLookupResults: [],
      postcodeLookupError: false,
      claim: {
        selectedAddress:
          "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
        addressId: "19000955",
      },
      stepData: {},
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = { locals: { postcodeLookupError: undefined } };
  const next = jest.fn();
  const expected = [];

  behaviourForGet(CONFIG, JOURNEY, STEP)(req, res, next);

  const stepData = req.session.stepData["/step-path"];
  expect(stepData.addresses).toEqual(expected);
  expect(res.locals.postcodeLookupError).toBe(false);
  expect(next).toHaveBeenCalled();
});

test("behaviourForGet() handles postcodeLookupErrors", () => {
  const req = {
    session: {
      postcodeLookupError: true,
      claim: {
        selectedAddress: {},
      },
      stepData: {},
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = { locals: { postcodeLookupError: undefined } };
  const next = jest.fn();

  behaviourForGet(CONFIG, JOURNEY, STEP)(req, res, next);

  expect(res.locals.postcodeLookupError).toBe(true);
  expect(getNextAllowedPathForApplyJourney(req)).toBe(
    "/test-context/manual-address",
  );
});

test("behaviourForGet() handles postcodeLookupErrors and does not set next allowed path if state is `IN_REVIEW`", () => {
  const req = {
    session: {
      postcodeLookupError: true,
      claim: {
        selectedAddress: {},
      },
      stepData: {},
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_REVIEW,
        nextAllowedPath: CHECK_ANSWERS_URL,
      }),
    },
  };

  const res = { locals: { postcodeLookupError: undefined } };
  const next = jest.fn();

  behaviourForGet(CONFIG, JOURNEY, STEP)(req, res, next);

  expect(req.session.claim.selectedAddress).toStrictEqual({});
  expect(res.locals.postcodeLookupError).toBe(true);
  expect(getNextAllowedPathForApplyJourney(req)).toBe(CHECK_ANSWERS_URL);
});

test("Address contentSummary() should return content summary in correct format when there is a selected address on the session", () => {
  const req = {
    t: (string) => string,
    session: {
      addressSubmittedFromLookup: true,
      claim: {
        addressLine1: "Flat b",
        addressLine2: "123 Fake Street",
        townOrCity: "Springfield",
        postcode: "bs1 4tb",
        selectedAddress: "Flat b, 123 Fake Street, Springfield, Devon, bs1 4tb",
      },
    },
  };

  const result = contentSummary(req);

  const expected = {
    id: "#address-line-1",
    key: "address.summaryKey",
    section: "aboutYou",
    value: "Flat b\n123 Fake Street\nSpringfield\nbs1 4tb",
    visuallyHidden: "address.hiddenChangeText",
  };

  expect(result).toEqual(expected);
});

test("Address contentSummary() should return null when there is no selected address on the session", () => {
  const req = {
    claim: {},
  };

  const result = contentSummary(req);

  expect(result).toEqual(null);
});

test("Address contentSummary() is null when there is a selected address on the session, addressSubmittedFromLookup is undefined", () => {
  const req = {
    session: {
      addressSubmittedFromLookup: undefined,
      claim: {
        selectedAddress: "test address",
      },
    },
  };

  const result = contentSummary(req);

  expect(result).toEqual(null);
});

test("Address contentSummary() is null when there is a selected address on the session, addressSubmittedFromLookup is false", () => {
  const req = {
    session: {
      addressSubmittedFromLookup: false,
      claim: {
        selectedAddress: "test address",
      },
    },
  };

  const result = contentSummary(req);

  expect(result).toEqual(null);
});

test(`isNavigable() returns false if session is undefined`, () => {
  const result = isNavigable(undefined, undefined);
  expect(result).toBe(false);
});

test(`isNavigable() returns false if session.postcodeLookupResults is undefined`, () => {
  const session = {
    postcodeLookupResults: undefined,
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(false);
});

test(`isNavigable() returns false if session.postcodeLookupResults is null`, () => {
  const session = {
    postcodeLookupResults: null,
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(false);
});

test(`isNavigable() returns false if session.postcodeLookupResults is empty`, () => {
  const session = {
    postcodeLookupResults: [],
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(false);
});

test(`isNavigable() returns true if session.postcodeLookupResults has value`, () => {
  const session = {
    postcodeLookupResults: ["value"],
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(true);
});

test("behaviourForPost() adds transformed address to claim", () => {
  const req = {
    session: {
      addressSubmittedFromLookup: undefined,
      claim: {
        nino: "QQ123456C",
      },
      postcodeLookupResults: [
        {
          ADDRESS:
            "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
          ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
          BUILDING_NUMBER: "1",
          THOROUGHFARE_NAME: "VALLEY ROAD",
          DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
          POST_TOWN: "PLYMOUTH",
          POSTCODE: "PL7 1RF",
          LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
          UDPRN: "50265368",
        },
        {
          ADDRESS: "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
          ORGANISATION_NAME: "DULUX DECORATOR CENTRE",
          BUILDING_NUMBER: "2",
          THOROUGHFARE_NAME: "VALLEY ROAD",
          DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
          POST_TOWN: "PLYMOUTH",
          POSTCODE: "PL7 1RF",
          LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
          UDPRN: "19000955",
        },
      ],
    },
    body: {
      selectedAddress:
        "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    },
  };
  const res = {};
  const next = jest.fn();

  const expectedClaim = {
    nino: "QQ123456C",
    addressLine1: "Alan Jeffery Engineering",
    addressLine2: "1 Upper Valley Road, Valley Road",
    townOrCity: "Plymouth",
    county: "City Of Plymouth",
    postcode: "PL7 1RF",
    addressId: "50265368",
  };

  behaviourForPost()(req, res, next);

  expect(req.session.addressSubmittedFromLookup).toBe(true);
  expect(req.session.claim).toEqual(expectedClaim);
  expect(next).toHaveBeenCalled();
});

test("behaviourForPost calls next if no address is selected", () => {
  const req = {
    session: {
      addressSubmittedFromLookup: undefined,
      claim: {
        nino: "QQ123456C",
      },
      postcodeLookupResults: [
        {
          ADDRESS:
            "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
          ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
          BUILDING_NUMBER: "1",
          THOROUGHFARE_NAME: "VALLEY ROAD",
          DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
          POST_TOWN: "PLYMOUTH",
          POSTCODE: "PL7 1RF",
          LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
        },
        {
          ADDRESS: "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
          ORGANISATION_NAME: "DULUX DECORATOR CENTRE",
          BUILDING_NUMBER: "2",
          THOROUGHFARE_NAME: "VALLEY ROAD",
          DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
          POST_TOWN: "PLYMOUTH",
          POSTCODE: "PL7 1RF",
          LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
        },
      ],
    },
    body: {},
  };
  const res = {};
  const next = jest.fn();

  behaviourForPost()(req, res, next);

  expect(req.session.addressSubmittedFromLookup).toBeUndefined();
  expect(next).toHaveBeenCalled();
});

test("findAddress finds the address matching the selected address passed in", () => {
  const postcodeLookupResults = [
    {
      ADDRESS: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
      ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
      BUILDING_NUMBER: "1",
      THOROUGHFARE_NAME: "VALLEY ROAD",
      DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
      POST_TOWN: "PLYMOUTH",
      POSTCODE: "PL7 1RF",
      LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    },
    {
      ADDRESS: "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
      ORGANISATION_NAME: "DULUX DECORATOR CENTRE",
      BUILDING_NUMBER: "2",
      THOROUGHFARE_NAME: "VALLEY ROAD",
      DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
      POST_TOWN: "PLYMOUTH",
      POSTCODE: "PL7 1RF",
      LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    },
  ];
  const selectedAddress =
    "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF";

  const expectedAddress = {
    ADDRESS: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
    BUILDING_NUMBER: "1",
    THOROUGHFARE_NAME: "VALLEY ROAD",
    DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
    POST_TOWN: "PLYMOUTH",
    POSTCODE: "PL7 1RF",
    LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
  };

  const address = findAddress(selectedAddress, postcodeLookupResults);

  expect(address).toEqual(expectedAddress);
});

test("findAddress throws an error when the address is not found", () => {
  const postcodeLookupResults = [];
  const selectedAddress =
    "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF";

  expect(
    findAddress.bind(null, selectedAddress, postcodeLookupResults),
  ).toThrow(
    /Unable to find selected address in list of postcode lookup results/,
  );
});
