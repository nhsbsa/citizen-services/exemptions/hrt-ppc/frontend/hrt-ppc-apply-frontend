import {
  normaliseAddressFields,
  convertNumericBuildingName,
  constructAddressParts,
  joinAddressFields,
  constructThoroughfare,
  constructBuildingName,
  constructLocality,
  constructNumberAndStreet,
  parseSinglePartAddress,
  constructTwoPartAddress,
} from "./adapters";

test("normaliseAddressFields() replaces missing fields with empty strings", () => {
  const fields = {
    ADDRESS: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
    BUILDING_NUMBER: "1",
    THOROUGHFARE_NAME: "VALLEY ROAD",
    POST_TOWN: "PLYMOUTH",
    POSTCODE: "PL7 1RF",
    LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    UDPRN: "50265368",
  };

  const expected = {
    ADDRESS: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
    SUB_BUILDING_NAME: "",
    BUILDING_NAME: "",
    BUILDING_NUMBER: "1",
    DEPENDENT_THOROUGHFARE_NAME: "",
    THOROUGHFARE_NAME: "VALLEY ROAD",
    DOUBLE_DEPENDENT_LOCALITY: "",
    DEPENDENT_LOCALITY: "",
    POST_TOWN: "PLYMOUTH",
    POSTCODE: "PL7 1RF",
    LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    UDPRN: "50265368",
  };

  const result = normaliseAddressFields(fields);
  expect(expected).toEqual(result);
});

test("joinAddressFields() correctly joins address fields", () => {
  const fields = { NAME: "NAME", NUMBER: "NUMBER", STREET: "STREET" };
  const allFields = ["NAME", "NUMBER", "STREET"];
  const missingFirstField = ["NUMBER", "STREET"];
  const missingMiddleField = ["NAME", "STREET"];
  const missingLastField = ["NAME", "NUMBER"];
  const singleField = ["NUMBER"];

  expect(joinAddressFields(allFields)(fields)).toBe("NAME, NUMBER, STREET");
  expect(joinAddressFields(missingFirstField)(fields)).toBe("NUMBER, STREET");
  expect(joinAddressFields(missingMiddleField)(fields)).toBe("NAME, STREET");
  expect(joinAddressFields(missingLastField)(fields)).toBe("NAME, NUMBER");
  expect(joinAddressFields(singleField)(fields)).toBe("NUMBER");
});

test("constructThoroughfare() correctly constructs thoroughfare", () => {
  const fields = {
    ADDRESS: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
    THOROUGHFARE_NAME: "VALLEY ROAD",
  };

  const expected = "UPPER VALLEY ROAD, VALLEY ROAD";
  const result = constructThoroughfare(fields);

  expect(result).toBe(expected);
});

test("constructBuildingName() correctly constructs building name", () => {
  const fields = {
    ADDRESS:
      "10, CONCERT SQUARE APARTMENTS, 29, FLEET STREET, LIVERPOOL, L1 4AR",
    SUB_BUILDING_NAME: "10",
    BUILDING_NAME: "CONCERT SQUARE APARTMENTS",
  };

  const expected = "10, CONCERT SQUARE APARTMENTS";
  const result = constructBuildingName(fields);

  expect(result).toBe(expected);
});

test("convertNumericBuildingName() converts building name of format 10A into building number", () => {
  const fields = {
    ADDRESS: "10A FLEET STREET, LIVERPOOL, L1 4AR",
    BUILDING_NAME: "10A",
    BUILDING_NUMBER: "",
  };

  const expected = {
    ADDRESS: "10A FLEET STREET, LIVERPOOL, L1 4AR",
    BUILDING_NAME: "",
    BUILDING_NUMBER: "10A",
  };
  const result = convertNumericBuildingName(fields);

  expect(result).toEqual(expected);
});

test("convertNumericBuildingName() does not convert building names not of the form 10A", () => {
  const fields = {
    ADDRESS: "101A ELECTRICITY HOUSE, FLEET STREET, LIVERPOOL, L1 4AR",
    BUILDING_NAME: "101A ELECTRICITY HOUSE",
    BUILDING_NUMBER: "",
  };

  const expected = {
    ADDRESS: "101A ELECTRICITY HOUSE, FLEET STREET, LIVERPOOL, L1 4AR",
    BUILDING_NAME: "101A ELECTRICITY HOUSE",
    BUILDING_NUMBER: "",
  };
  const result = convertNumericBuildingName(fields);

  expect(result).toEqual(expected);
});

test("constructLocality() correctly constructs locality", () => {
  const fields = {
    ADDRESS:
      "TRELLEBORG MARINE SYSTEMS LTD, AIRFIELD VIEW, MANOR LANE, HAWARDEN INDUSTRIAL PARK, PENARLAG, GLANNAU DYFRDWY, CH5 3QW",
    DOUBLE_DEPENDENT_LOCALITY: "HAWARDEN INDUSTRIAL PARK",
    DEPENDENT_LOCALITY: "PENARLAG",
  };

  const expected = "HAWARDEN INDUSTRIAL PARK, PENARLAG";
  const result = constructLocality(fields);

  expect(result).toBe(expected);
});

test("constructNumberAndStreet() correctly constructs number and street", () => {
  const fields = {
    BUILDING_NUMBER: "1",
    DEPENDENT_THOROUGHFARE_NAME: "",
    THOROUGHFARE_NAME: "VALLEY ROAD",
  };

  const expected = "1 VALLEY ROAD";
  const result = constructNumberAndStreet(fields);

  expect(result).toBe(expected);
});

test("constructAddressParts() constructs address parts correctly", () => {
  const fields = {
    ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
    SUB_BUILDING_NAME: "10",
    BUILDING_NAME: "VALLEY APARTMENTS",
    BUILDING_NUMBER: "1",
    DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
    THOROUGHFARE_NAME: "VALLEY ROAD",
    DOUBLE_DEPENDENT_LOCALITY: "PLYMOUTH INDUSTRIAL PARK",
    DEPENDENT_LOCALITY: "EDGE OF PLYMOUTH",
    POST_TOWN: "PLYMOUTH",
    POSTCODE: "PL7 1RF",
    LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
  };

  const expected = [
    "ALAN JEFFERY ENGINEERING",
    "10, VALLEY APARTMENTS",
    "1 UPPER VALLEY ROAD, VALLEY ROAD",
    "PLYMOUTH INDUSTRIAL PARK, EDGE OF PLYMOUTH",
  ];

  const result = constructAddressParts(fields);

  expect(result).toEqual(expected);
});

test("parseSinglePartAddress() splits a single part address into multiple parts", () => {
  const singlePartAddress = ["29, ACACIA ROAD, ENGLAND"];
  const singlePartAddressWithNoComma = ["29 ACACIA ROAD"];
  const multiPartAddress = ["29", "ACACIA ROAD", "ENGLAND"];

  expect(parseSinglePartAddress(singlePartAddress)).toEqual([
    "29",
    "ACACIA ROAD",
    "ENGLAND",
  ]);
  expect(parseSinglePartAddress(singlePartAddressWithNoComma)).toEqual([
    "29 ACACIA ROAD",
  ]);
  expect(parseSinglePartAddress(multiPartAddress)).toEqual([
    "29",
    "ACACIA ROAD",
    "ENGLAND",
  ]);
});

test("constructTwoPartAddress() converts multipart address to two parts", () => {
  const multiPartAddress = ["29", "ACACIA ROAD", "ENGLAND", "SW1 5EE"];
  const expected = ["29", "ACACIA ROAD, ENGLAND, SW1 5EE"];
  const result = constructTwoPartAddress(multiPartAddress);

  expect(expected).toEqual(result);
});
