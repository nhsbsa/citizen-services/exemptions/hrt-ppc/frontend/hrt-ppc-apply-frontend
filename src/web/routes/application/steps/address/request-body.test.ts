import expect from "expect";
import { requestBody } from "./request-body";

const addressWithAddressLineTwo = {
  address: {
    addressLine1: "add1",
    addressLine2: "mockAddress2",
    postcode: "AA11AA",
    townOrCity: "london",
  },
};

const addressWithoutAddressLineTwo = {
  address: {
    addressLine1: "add1",
    postcode: "AA11AA",
    townOrCity: "london",
  },
};

describe("requestBody() function", () => {
  it.each([
    ["mockAddress2", addressWithAddressLineTwo],
    ["", addressWithoutAddressLineTwo],
    [null, addressWithoutAddressLineTwo],
    [undefined, addressWithoutAddressLineTwo],
  ])(
    "should return request body in correct format when addressline2 is set to %p",
    (addressLine2, expected) => {
      const req = {
        session: {
          claim: {
            addressLine1: "add1",
            addressLine2: addressLine2,
            postcode: "aa11aa",
            sanitizedPostcode: "AA11AA",
            townOrCity: "london",
          },
        },
      };

      const result = requestBody(req.session);

      expect(result).toEqual(expected);
    },
  );
});
