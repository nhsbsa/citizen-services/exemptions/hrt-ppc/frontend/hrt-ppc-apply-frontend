import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { stateMachine } from "../../flow-control/state-machine";
import { SET_NEXT_ALLOWED_PATH } from "../../flow-control/state-machine/actions";
import { contextPath } from "../../paths/context-path";
import { searchExemptions } from "../common/search-citizen-certificates-provider";

const searchAndReturnKickoutIfExemptionFound = async (
  req,
  res,
  journey,
  next,
) => {
  try {
    const result = await searchExemptions(req);

    delete req.session.existingExemptionFound;
    if (result.activeExemptions) {
      req.session.existingExemptionFound = true;
      stateMachine.dispatch(
        SET_NEXT_ALLOWED_PATH,
        req,
        journey,
        contextPath("/existing-exemption-found"),
      );
      return res.redirect(contextPath("/existing-exemption-found"));
    }
    return null;
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      }),
    );
  }
};

export { searchAndReturnKickoutIfExemptionFound };
