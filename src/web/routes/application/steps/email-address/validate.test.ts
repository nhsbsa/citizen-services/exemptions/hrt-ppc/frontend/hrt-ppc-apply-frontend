import expect from "expect";
import { validate } from "./validate";
import { YES, NO } from "../common/constants";
import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend";
import { FieldValidationError } from "express-validator";

test("validation middleware passes when doYouWantEmail is selected as 'YES' & emailAddress is provided", async () => {
  const req = {
    body: {
      doYouWantEmail: YES,
      emailAddress: "test@domain.com",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.isEmpty()).toBe(true);
});

test("validation middleware passes when doYouWantEmail is selected as 'NO'", async () => {
  const req = {
    body: {
      doYouWantEmail: NO,
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.isEmpty()).toBe(true);
});

describe("validation middleware throws error when doYouWantEmail is empty, null or undefined", () => {
  it.each(["", null, undefined])(
    "throws error when doYouWantEmail is %p",
    async (doYouWantEmail) => {
      const req = {
        body: {
          doYouWantEmail: doYouWantEmail,
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(1);

      const error = result.array()[0] as FieldValidationError;
      expect(error.path).toBe("doYouWantEmail");
      expect(error.msg).toBe("validation:selectEmailAddress");
    },
  );
});

describe("validation middleware throws error when doYouWantEmail is selected as 'YES' & emailAddress is empty, null or undefined", () => {
  it.each(["", null, undefined])(
    "throws error when emailAddress is %p",
    async (emailAddress) => {
      const req = {
        body: {
          doYouWantEmail: YES,
          emailAddress: emailAddress,
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(1);

      const error = result.array()[0] as FieldValidationError;
      expect(error.path).toBe("emailAddress");
      expect(error.msg).toBe("validation:missingEmailAddress");
    },
  );
});

describe("covers valid email addresses", () => {
  it.each([
    "email@domain.com",
    "firstname.lastname@domain.com",
    "email@subdomain.domain.com",
    "firstname+lastname@domain.com",
    "1234567890@domain.com",
    "email@domain-one.com",
    "_______@domain.com",
    "email@domain.name",
    "email@domain.co.jp",
    "firstname-lastname@domain.com",
    "test@domain.com",
    "Pälé@example.com",
  ])(
    "check email-address: %p is expecting no validation errors",
    async (emailAddress: string) => {
      const req = {
        body: {
          doYouWantEmail: YES,
          emailAddress: emailAddress,
        },
      };

      const result = await applyExpressValidation(req, validate());

      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(0);
    },
  );
});

describe("covers invalid email addresses", () => {
  it.each([
    "test@@domain.com",
    "@@@@domain.com",
    "123.com",
    "plainaddress",
    "#@%^%#$@#$@#.com",
    "@domain.com",
    "Joe Smith <email@domain.com>",
    "email.domain.com",
    "email@domain@domain.com",
    "email@domain.com (Joe Smith)",
    "email@domain",
    "email@domain..com",
    ".email@domain.com",
    "email.@domain.com",
    "email..email@domain.com",
  ])(
    "check email-address: %p is expecting one validation error",
    async (emailAddress: string) => {
      const req = {
        body: {
          doYouWantEmail: YES,
          emailAddress: emailAddress,
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(1);

      const errorOne = result.array()[0] as FieldValidationError;
      expect(errorOne.path).toBe("emailAddress");
      expect(errorOne.msg).toBe("validation:invalidEmailAddress");
    },
  );
});
