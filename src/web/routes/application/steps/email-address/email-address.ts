import { validate } from "./validate";
import { NO, YES } from "../common/constants";
import { validationResult } from "express-validator";
import { DEFAULT_LIST } from "../check-your-answers/constants";

const pageContent = ({ translate, req }) => ({
  title: translate("emailAddress.title"),
  heading: translate("emailAddress.heading"),
  paragraphOne: translate("emailAddress.paragraphOne"),
  paragraphTwo: translate("emailAddress.paragraphTwo"),
  optionYesInputLabel: translate("emailAddress.optionYesInputLabel"),
  optionNoContent: translate("emailAddress.optionNoContent"),
  receiveCertificate: translate("emailAddress.receiveCertificate"),
  hiddenChangeText: translate("emailAddress.hiddenChangeText"),
  reminderByEmailHiddenChangeText: translate(
    "emailAddress.reminderByEmailHiddenChangeText",
  ),
  yes: translate("yes"),
  no: translate("no"),
  cancel: req.t("cancel"),
  buttonText: translate("buttons:continue"),
});

const behaviourForPost = () => (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  if (req.body.doYouWantEmail === NO) {
    delete req.body.emailAddress;
    delete req.session.claim.emailAddress;
  }
  next();
};

const requestBody = (session) => {
  if (session.claim.doYouWantEmail === YES) {
    return {
      emailAddress: session.claim.emailAddress,
    };
  }
  return undefined;
};

const doYouWantEmailContentSummary = (req) => ({
  id: doYouKnowEmailIdContentSummaryId,
  key: req.t("emailAddress.receiveCertificate"),
  section: DEFAULT_LIST,
  value: req.session.claim.doYouWantEmail === YES ? req.t("yes") : req.t("no"),
  visuallyHidden: req.t("emailAddress.reminderByEmailHiddenChangeText"),
});

const emailIdContentSummaryId = "#email-address";
const doYouKnowEmailIdContentSummaryId = "#do-you-know-your-email-address";

const contentSummary = (req) => {
  if (req.session.claim.doYouWantEmail === YES) {
    return [
      doYouWantEmailContentSummary(req),
      {
        id: emailIdContentSummaryId,
        key: req.t("emailAddress.optionYesInputLabel"),
        section: DEFAULT_LIST,
        value: `${req.session.claim.emailAddress}`,
        visuallyHidden: req.t("emailAddress.hiddenChangeText"),
      },
    ];
  } else {
    return doYouWantEmailContentSummary(req);
  }
};

const emailAddress = {
  path: "/do-you-want-email",
  template: "email-address",
  toggle: "CAPTURE_EMAIL_ENABLED",
  pageContent,
  validate,
  requestBody,
  contentSummary,
  behaviourForPost,
};

export {
  emailAddress,
  pageContent,
  validate,
  requestBody,
  behaviourForPost,
  contentSummary,
  emailIdContentSummaryId,
  doYouKnowEmailIdContentSummaryId,
};
