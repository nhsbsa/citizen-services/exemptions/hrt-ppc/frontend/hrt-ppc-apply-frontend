import { translateValidationMessage } from "@nhsbsa/health-charge-exemption-common-frontend";
import { check } from "express-validator";
import { YES, NO } from "../common/constants";

const validate = () => [
  check("doYouWantEmail")
    .isIn([YES, NO])
    .withMessage(translateValidationMessage("validation:selectEmailAddress"))
    .bail(),
  check("emailAddress")
    .if((value, { req }) => req.body["doYouWantEmail"] === YES)
    .notEmpty()
    .withMessage(translateValidationMessage("validation:missingEmailAddress"))
    .bail()
    .isEmail()
    .withMessage(translateValidationMessage("validation:invalidEmailAddress")),
];

export { validate };
