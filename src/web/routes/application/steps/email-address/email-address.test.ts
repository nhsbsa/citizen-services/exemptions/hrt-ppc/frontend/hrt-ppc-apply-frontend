import expect from "expect";

const isEmpty = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

import {
  pageContent,
  validate,
  emailAddress,
  requestBody,
  behaviourForPost,
  contentSummary,
} from "./email-address";

const req = {
  t: (string) => string,
  session: {
    claim: {
      doYouWantEmail: "yes",
      emailAddress: "test@domain.com",
    },
  },
};

beforeEach(() => {
  jest.clearAllMocks();
  isEmpty.mockReturnValue(true);
});

describe("requestBody() function", () => {
  test("should return request body in correct format' when 'doYouWantEmail' is selected as 'YES'", () => {
    const req = {
      session: {
        claim: {
          doYouWantEmail: "yes",
          emailAddress: "test@domain.com",
        },
      },
    };

    const result = requestBody(req.session);

    const expected = {
      emailAddress: "test@domain.com",
    };

    expect(result).toEqual(expected);
  });

  test("should return undefined when 'doYouWantEmail' is selected as 'NO'", () => {
    const req = {
      session: {
        claim: {
          doYouWantEmail: "no",
        },
      },
    };

    const result = requestBody(req.session);

    expect(result).toEqual(undefined);
  });
});

describe("contentSummary() function", () => {
  test("should return array of objects which has properties for 'Receive certificate and reminder by email?' and 'Email address' when doYouWantEmail is 'yes'", () => {
    const result = contentSummary(req);

    const expected = [
      {
        id: "#do-you-know-your-email-address",
        key: "emailAddress.receiveCertificate",
        section: "aboutYou",
        value: "yes",
        visuallyHidden: "emailAddress.reminderByEmailHiddenChangeText",
      },
      {
        id: "#email-address",
        key: "emailAddress.optionYesInputLabel",
        section: "aboutYou",
        value: "test@domain.com",
        visuallyHidden: "emailAddress.hiddenChangeText",
      },
    ];
    expect(result).toEqual(expected);
  });

  test("should return object which has property for 'Receive certificate and reminder by email?' when doYouWantEmail is 'no'", () => {
    const req = {
      t: (string) => string,
      session: {
        claim: {
          doYouWantEmail: "no",
        },
      },
    };

    const result = contentSummary(req);

    const expected = {
      id: "#do-you-know-your-email-address",
      key: "emailAddress.receiveCertificate",
      section: "aboutYou",
      value: "no",
      visuallyHidden: "emailAddress.reminderByEmailHiddenChangeText",
    };

    expect(result).toEqual(expected);
  });
});

test("pageContent() should match expected translated values", () => {
  const translate = (string, object?) => `${string}${object}`;
  const result = pageContent({ translate, req });

  const expected = {
    title: translate("emailAddress.title"),
    heading: translate("emailAddress.heading"),
    paragraphOne: translate("emailAddress.paragraphOne"),
    paragraphTwo: translate("emailAddress.paragraphTwo"),
    optionYesInputLabel: translate("emailAddress.optionYesInputLabel"),
    optionNoContent: translate("emailAddress.optionNoContent"),
    receiveCertificate: translate("emailAddress.receiveCertificate"),
    hiddenChangeText: translate("emailAddress.hiddenChangeText"),
    reminderByEmailHiddenChangeText: translate(
      "emailAddress.reminderByEmailHiddenChangeText",
    ),
    yes: translate("yes"),
    no: translate("no"),
    cancel: req.t("cancel"),
    buttonText: translate("buttons:continue"),
  };

  expect(result).toEqual(expected);
});

test(`emailAddress should match expected outcomes`, () => {
  const expected = {
    path: "/do-you-want-email",
    template: "email-address",
    toggle: "CAPTURE_EMAIL_ENABLED",
    pageContent,
    validate,
    requestBody,
    contentSummary,
    behaviourForPost,
  };

  expect(emailAddress).toEqual(expected);
});

test("behaviourForPost() should delete emailAddress from req.session.claim and req.body when doYouWantEmail is set to `NO` in req.body", () => {
  const req = {
    body: {
      doYouWantEmail: "no",
      emailAddress: "test@email.com",
    },
    session: {
      claim: {
        emailAddress: "test@email.com",
      },
    },
  };

  const res = jest.fn();
  const next = jest.fn();

  behaviourForPost()(req, res, next);

  expect(req.body.emailAddress).toEqual(undefined);
  expect(req.session.claim.emailAddress).toEqual(undefined);
  expect(next).toHaveBeenCalledTimes(1);
});

test("behaviourForPost() should call next() when validation error occurs", () => {
  isEmpty.mockReturnValue(false);

  const req = {
    body: {
      doYouWantEmail: "no",
      emailAddress: "test@email.com",
    },
    session: {
      claim: {
        emailAddress: "test@email.com",
      },
    },
  };

  const res = jest.fn();
  const next = jest.fn();

  behaviourForPost()(req, res, next);

  expect(req.body.emailAddress).toEqual("test@email.com");
  expect(req.session.claim.emailAddress).toEqual("test@email.com");
  expect(next).toHaveBeenCalledTimes(1);
});
