import expect from "expect";
import {
  isMedicineCovered,
  pageContent,
  validate,
  isMedicineCoveredClaimant,
} from "./medicine-covered";

const req = {
  t: (string) => string,
  session: {
    claim: {
      isMedicineCovered: "no",
    },
  },
};

test(`isMedicineCovered should match expected object`, () => {
  const expectedResults = {
    path: "/is-your-medicine-covered",
    template: "medicine-covered",
    pageContent,
    validate,
    shouldInvalidateReview: isMedicineCoveredClaimant,
  };

  expect(isMedicineCovered).toEqual(expectedResults);
});

test(`pageContent() should return expected results`, () => {
  const translate = (string) => string;
  const expected = {
    title: translate("medicineCovered.title"),
    heading: translate("medicineCovered.heading"),
    paragraphOne: translate("medicineCovered.paragraphOne"),
    buttonText: translate("buttons:continue"),
    expanderText: translate("medicineCovered.expanderText"),
    expanderContent: translate("medicineCovered.expanderContent"),
    products: translate("medicineCovered.products"),
    medicineCovered: translate("medicineCovered.medicineCovered"),
    medicineNotCovered: translate("medicineCovered.medicineNotCovered"),
    someMedicineCovered: translate("medicineCovered.someMedicineCovered"),
    expanderHeading: translate("medicineCovered.expanderHeading"),
    expanderParagraphOne: translate("medicineCovered.expanderParagraphOne"),
    expanderParagraphTwo: translate("medicineCovered.expanderParagraphTwo"),
    cancel: req.t("cancel"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test(`isMedicineCoveredClaimant() returns true when medicine is covered by HRT PPC`, () => {
  const claim = {
    isMedicineCovered: "yes",
  };
  const result = isMedicineCoveredClaimant(claim);

  expect(result).toBeTruthy();
});

test(`isMedicineCoveredClaimant() returns true when some medicine is covered by HRT PPC`, () => {
  const claim = {
    isMedicineCovered: "some",
  };
  const result = isMedicineCoveredClaimant(claim);

  expect(result).toBeTruthy();
});

test(`isMedicineCoveredClaimant() returns false when medicine is not covered by HRT PPC`, () => {
  const claim = {
    isMedicineCovered: "no",
  };
  const result = isMedicineCoveredClaimant(claim);

  expect(result).toBeFalsy();
});
