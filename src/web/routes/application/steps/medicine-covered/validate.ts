import { translateValidationMessage } from "@nhsbsa/health-charge-exemption-common-frontend";
import { check } from "express-validator";
import { YES, NO, SOME } from "../common/constants";

const validate = () => [
  check("isMedicineCovered")
    .isIn([YES, NO, SOME])
    .withMessage(
      translateValidationMessage("validation:selectIfMedicineCovered"),
    ),
];

export { validate };
