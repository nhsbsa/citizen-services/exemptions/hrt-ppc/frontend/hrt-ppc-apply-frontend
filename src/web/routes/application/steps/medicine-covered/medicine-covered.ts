import { validate } from "./validate";
import { YES, SOME } from "../common/constants";

const pageContent = ({ translate, req }) => ({
  title: translate("medicineCovered.title"),
  heading: translate("medicineCovered.heading"),
  paragraphOne: translate("medicineCovered.paragraphOne"),
  buttonText: translate("buttons:continue"),
  expanderText: translate("medicineCovered.expanderText"),
  expanderContent: translate("medicineCovered.expanderContent"),
  products: translate("medicineCovered.products"),
  medicineCovered: translate("medicineCovered.medicineCovered"),
  medicineNotCovered: translate("medicineCovered.medicineNotCovered"),
  someMedicineCovered: translate("medicineCovered.someMedicineCovered"),
  expanderHeading: translate("medicineCovered.expanderHeading"),
  expanderParagraphOne: translate("medicineCovered.expanderParagraphOne"),
  expanderParagraphTwo: translate("medicineCovered.expanderParagraphTwo"),
  cancel: req.t("cancel"),
});

const isMedicineCoveredClaimant = (claim) =>
  claim.isMedicineCovered === YES || claim.isMedicineCovered === SOME;

const isMedicineCovered = {
  path: "/is-your-medicine-covered",
  template: "medicine-covered",
  pageContent,
  validate,
  shouldInvalidateReview: isMedicineCoveredClaimant,
};

export { isMedicineCovered, pageContent, validate, isMedicineCoveredClaimant };
