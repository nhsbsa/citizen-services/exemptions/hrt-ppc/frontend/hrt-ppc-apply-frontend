import expect from "expect";
import { assocPath } from "ramda";
import { validate } from "./validate";
import { YES, NO, SOME } from "../common/constants";
import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend";
import { FieldValidationError } from "express-validator";

const translate = (string) => string;
const req = {
  t: translate,
  body: {
    isMedicineCovered: YES,
  },
};

test("validation middleware passes with valid option YES in body", async () => {
  const testReq = { ...req };
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(true);
});

test("validation middleware passes with valid option NO in body", async () => {
  const testReq = assocPath(["body", "isMedicineCovered"], NO, req);
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(true);
});

test("validation middleware passes with valid option SOME in body", async () => {
  const testReq = assocPath(["body", "isMedicineCovered"], SOME, req);
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(true);
});

test("validation middleware fails with invalid option invalidContent in body", async () => {
  const testReq = assocPath(
    ["body", "isMedicineCovered"],
    "invalidContent",
    req,
  );
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(false);
});

test("validation middleware errors for medicine field when not set correctly", async () => {
  const testReq = assocPath(["body", "isMedicineCovered"], "maybe", req);
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0] as FieldValidationError;
  expect(result.array().length).toBe(1);
  expect(error.path).toBe("isMedicineCovered");
  expect(error.msg).toBe("validation:selectIfMedicineCovered");
});

test("validation middleware errors for medicine field is empty", async () => {
  const testReq = assocPath(["body", "isMedicineCovered"], null, req);
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0] as FieldValidationError;
  expect(result.array().length).toBe(1);
  expect(error.path).toBe("isMedicineCovered");
  expect(error.msg).toBe("validation:selectIfMedicineCovered");
});
