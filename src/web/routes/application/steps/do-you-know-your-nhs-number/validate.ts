import { check } from "express-validator";
import { YES, NO } from "../common/constants";
import { isEmpty } from "ramda";
import validateNhsNumber from "nhs-numbers/lib/validateNhsNumber";
import { translateValidationMessage } from "@nhsbsa/health-charge-exemption-common-frontend";

const validate = () => [
  check("doYouKnowYourNhsNumber")
    .isIn([YES, NO])
    .withMessage(translateValidationMessage("validation:selectYourNhsNumber"))
    .bail(),
  check("nhsNumber").custom((value, { req }) => {
    if (req.body["doYouKnowYourNhsNumber"] === YES) {
      if (isEmpty(value)) {
        throw new Error(req.t("validation:missingNhsNumber"));
      }
      if (!validateNhsNumber(value)) {
        throw new Error(req.t("validation:invalidNhsNumber"));
      }
      if (value.length > 10) {
        throw new Error(req.t("validation:invalidNhsNumber"));
      }
      return true;
    }
    return true;
  }),
];

export { validate };
