import expect from "expect";
import {
  pageContent,
  validate,
  doYouKnowYourNhsNumber,
  contentSummary,
  requestBody,
} from "./do-you-know-your-nhs-number";

const req = {
  t: (string) => string,
  session: {
    claim: {
      doYouKnowYourNhsNumber: "yes",
      nhsNumber: 9992158107,
    },
  },
};

describe("contentSummary() function", () => {
  test("should return array of objects which has properties for 'Do you know your NHS number?' and 'NHS number' when doYouKnowYourNhsNumber is 'yes'", () => {
    const result = contentSummary(req);
    const expected = [
      {
        id: "#do-you-know-your-nhs-number",
        key: "doYouKnowYourNhsNumber.heading",
        section: "aboutYou",
        value: "yes",
        visuallyHidden:
          "doYouKnowYourNhsNumber.knowYourNhsNumberHiddenChangeText",
      },
      {
        id: "#nhs-number",
        key: "doYouKnowYourNhsNumber.nhsNumber",
        section: "aboutYou",
        value: "9992158107",
        visuallyHidden: "doYouKnowYourNhsNumber.hiddenChangeText",
      },
    ];
    expect(result).toEqual(expected);
  });

  test("should return object which has property for 'Do you know your NHS number?'  when doYouKnowYourNhsNumber is 'no'", () => {
    const req = {
      t: (string) => string,
      session: {
        claim: {
          doYouKnowYourNhsNumber: "no",
        },
      },
    };

    const result = contentSummary(req);

    const expected = {
      id: "#do-you-know-your-nhs-number",
      key: "doYouKnowYourNhsNumber.heading",
      section: "aboutYou",
      value: "no",
      visuallyHidden:
        "doYouKnowYourNhsNumber.knowYourNhsNumberHiddenChangeText",
    };

    expect(result).toEqual(expected);
  });
});

describe("requestBody() function", () => {
  test("should return request body in correct format' ", () => {
    const req = {
      session: {
        claim: {
          doYouKnowYourNhsNumber: "yes",
          nhsNumber: 9992158107,
        },
      },
    };

    const result = requestBody(req.session);

    const expected = {
      nhsNumber: 9992158107,
    };
    expect(result).toEqual(expected);
  });

  test("should return undefined when 'doYouKnowYourNhsNumber' is selected as 'NO'", () => {
    const req = {
      session: {
        claim: {
          doYouKnowYourNhsNumber: "no",
        },
      },
    };

    const result = requestBody(req.session);

    expect(result).toEqual(undefined);
  });
});

test("pageContent() should match expected translated values", () => {
  const translate = (string, object?) => `${string}${object}`;
  const result = pageContent({ translate, req });

  const expected = {
    title: translate("doYouKnowYourNhsNumber.title"),
    heading: translate("doYouKnowYourNhsNumber.heading"),
    paragraphOne: translate("doYouKnowYourNhsNumber.paragraphOne"),
    hint: translate("doYouKnowYourNhsNumber.hint"),
    yes: translate("doYouKnowYourNhsNumber.yes"),
    no: translate("doYouKnowYourNhsNumber.no"),
    nhsNumber: translate("doYouKnowYourNhsNumber.nhsNumber"),
    hiddenChangeText: translate("doYouKnowYourNhsNumber.hiddenChangeText"),
    knowYourNhsNumberHiddenChangeText: translate(
      "doYouKnowYourNhsNumber.knowYourNhsNumberHiddenChangeText",
    ),
    detailsHeading: translate("doYouKnowYourNhsNumber.detailsHeading"),
    detailsParagraphOne: translate(
      "doYouKnowYourNhsNumber.detailsParagraphOne",
    ),
    detailsParagraphTwo: translate(
      "doYouKnowYourNhsNumber.detailsParagraphTwo",
    ),
    detailsParagraphThree: translate(
      "doYouKnowYourNhsNumber.detailsParagraphThree",
    ),
    detailsListItemOne: translate("doYouKnowYourNhsNumber.detailsListItemOne"),
    detailsListItemTwo: translate("doYouKnowYourNhsNumber.detailsListItemTwo"),
    detailsListItemThree: translate(
      "doYouKnowYourNhsNumber.detailsListItemThree",
    ),
    detailsListItemFour: translate(
      "doYouKnowYourNhsNumber.detailsListItemFour",
    ),
    cancel: req.t("cancel"),
    buttonText: translate("buttons:continue"),
  };

  expect(result).toEqual(expected);
});

test(`doYouKnowYourNhsNumber should match expected outcomes`, () => {
  const expectedResults = {
    path: "/nhs-number",
    template: "do-you-know-your-nhs-number",
    pageContent,
    validate,
    requestBody,
    contentSummary,
  };
  expect(doYouKnowYourNhsNumber).toEqual(expectedResults);
});
