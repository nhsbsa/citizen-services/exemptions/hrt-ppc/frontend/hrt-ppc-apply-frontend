import { validate } from "./validate";
import { YES } from "../common/constants";
import { DEFAULT_LIST } from "../check-your-answers/constants";

const pageContent = ({ translate, req }) => ({
  title: translate("doYouKnowYourNhsNumber.title"),
  heading: translate("doYouKnowYourNhsNumber.heading"),
  paragraphOne: translate("doYouKnowYourNhsNumber.paragraphOne"),
  hint: translate("doYouKnowYourNhsNumber.hint"),
  yes: translate("doYouKnowYourNhsNumber.yes"),
  no: translate("doYouKnowYourNhsNumber.no"),
  nhsNumber: translate("doYouKnowYourNhsNumber.nhsNumber"),
  hiddenChangeText: translate("doYouKnowYourNhsNumber.hiddenChangeText"),
  knowYourNhsNumberHiddenChangeText: translate(
    "doYouKnowYourNhsNumber.knowYourNhsNumberHiddenChangeText",
  ),
  detailsHeading: translate("doYouKnowYourNhsNumber.detailsHeading"),
  detailsParagraphOne: translate("doYouKnowYourNhsNumber.detailsParagraphOne"),
  detailsParagraphTwo: translate("doYouKnowYourNhsNumber.detailsParagraphTwo"),
  detailsParagraphThree: translate(
    "doYouKnowYourNhsNumber.detailsParagraphThree",
  ),
  detailsListItemOne: translate("doYouKnowYourNhsNumber.detailsListItemOne"),
  detailsListItemTwo: translate("doYouKnowYourNhsNumber.detailsListItemTwo"),
  detailsListItemThree: translate(
    "doYouKnowYourNhsNumber.detailsListItemThree",
  ),
  detailsListItemFour: translate("doYouKnowYourNhsNumber.detailsListItemFour"),
  cancel: req.t("cancel"),
  buttonText: translate("buttons:continue"),
});

const requestBody = (session) => {
  if (session.claim.doYouKnowYourNhsNumber === YES) {
    return {
      nhsNumber: session.claim.nhsNumber,
    };
  }
  return undefined;
};

const doYouKnowYourNhsNumberContentSummary = (req) => ({
  id: doYouKnowNhsNumberContentSummaryId,
  key: req.t("doYouKnowYourNhsNumber.heading"),
  section: DEFAULT_LIST,
  value:
    req.session.claim.doYouKnowYourNhsNumber === YES
      ? req.t("yes")
      : req.t("no"),
  visuallyHidden: req.t(
    "doYouKnowYourNhsNumber.knowYourNhsNumberHiddenChangeText",
  ),
});

const nhsNumberContentSummaryId = "#nhs-number";
const doYouKnowNhsNumberContentSummaryId = "#do-you-know-your-nhs-number";

const contentSummary = (req) => {
  if (req.session.claim.doYouKnowYourNhsNumber === YES) {
    return [
      doYouKnowYourNhsNumberContentSummary(req),
      {
        id: nhsNumberContentSummaryId,
        key: req.t("doYouKnowYourNhsNumber.nhsNumber"),
        section: DEFAULT_LIST,
        value: `${req.session.claim.nhsNumber}`,
        visuallyHidden: req.t("doYouKnowYourNhsNumber.hiddenChangeText"),
      },
    ];
  } else {
    return doYouKnowYourNhsNumberContentSummary(req);
  }
};

const doYouKnowYourNhsNumber = {
  path: "/nhs-number",
  template: "do-you-know-your-nhs-number",
  pageContent,
  validate,
  requestBody,
  contentSummary,
};

export {
  doYouKnowYourNhsNumber,
  pageContent,
  validate,
  requestBody,
  contentSummary,
  doYouKnowNhsNumberContentSummaryId,
  nhsNumberContentSummaryId,
};
