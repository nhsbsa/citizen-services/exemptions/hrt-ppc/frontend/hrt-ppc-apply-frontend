import expect from "expect";
import { validate } from "./validate";
import { assocPath } from "ramda";
import { YES, NO } from "../common/constants";
import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend";
import { FieldValidationError } from "express-validator";

test("validation middleware passes when doYouKnowYourNhsNumber is selected 'YES' & valid nhsNumber body", async () => {
  const req = {
    body: {
      doYouKnowYourNhsNumber: YES,
      nhsNumber: "9992158107",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.isEmpty()).toBe(true);
});

test("validation middleware passes with valid option NO body", async () => {
  const req = {
    body: {
      doYouKnowYourNhsNumber: NO,
    },
  };

  const testReq = assocPath(["body", "doYouKnowYourNhsNumber"], NO, req);
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.isEmpty()).toBe(true);
});

describe("nhsNumberValidation", () => {
  it.each([
    ["9992158107", 0],
    ["9999999999", 0],
    ["9979989998", 0],
    ["9979989696", 0],
    ["1123443986", 1],
    ["@!@££*£*£test", 1],
    ["9999999999999999999999", 1],
    ["9999", 1],
  ])(
    "check %p is valid nhs number expecting %p validation errors",
    async (nhsNumber: string, errorCount: number) => {
      const req = {
        body: {
          doYouKnowYourNhsNumber: YES,
          nhsNumber: nhsNumber,
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(errorCount);
      if (errorCount > 0) {
        const errorOne = result.array()[0] as FieldValidationError;
        expect(errorOne.path).toBe("nhsNumber");
        expect(errorOne.msg).toBe("validation:invalidNhsNumber");
      }
    },
  );
});

test("validation middleware errors for do you know your 'nhsNumber' field is empty", async () => {
  const req = {
    body: {
      doYouKnowYourNhsNumber: YES,
      nhsNumber: "",
    },
  };

  const testReq = assocPath(["body", "doYouKnowYourNhsNumber"], null, req);
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0] as FieldValidationError;

  expect(result.array().length).toBe(1);
  expect(error.path).toBe("doYouKnowYourNhsNumber");
  expect(error.msg).toBe("validation:selectYourNhsNumber");
});
