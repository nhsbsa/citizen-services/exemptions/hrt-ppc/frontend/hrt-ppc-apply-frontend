import expect from "expect";
import {
  behaviourForGet,
  behaviourForPost,
  pageContent,
} from "./cookie-declaration";
import { YES, NO } from "../common/constants";

test("behaviourForGet() on cookie declaration", () => {
  const cookieConsent =
    '{"necessary":true,"preferences":false,"statistics":true,"marketing":false,"consented":true,"version":3}';
  const req = {
    cookies: {
      "nhsuk-cookie-consent": cookieConsent,
    },
  };

  const res = {
    locals: { measureAnalytics: undefined },
  };

  const next = jest.fn();

  behaviourForGet()(req, res, next);

  expect(res.locals.measureAnalytics).toEqual(true);
  expect(next).toHaveBeenCalled();
});

test("behaviourForPost() set session when “Use cookies to measure my website use” is selected", () => {
  const req = {
    session: { measureAnalytics: undefined },
    body: {
      measureStatistics: YES,
    },
  };

  const res = {};
  const next = jest.fn();

  behaviourForPost()(req, res, next);

  expect(req.session.measureAnalytics).toBe(true);
  expect(next).toHaveBeenCalled();
});

test("behaviourForPost() set session when “Do not use cookies to measure my website use” is selected", () => {
  const req = {
    session: { measureAnalytics: undefined },
    body: {
      measureStatistics: NO,
    },
  };

  const res = {};
  const next = jest.fn();

  behaviourForPost()(req, res, next);

  expect(req.session.measureAnalytics).toBe(false);
  expect(next).toHaveBeenCalled();
});

test("pageContent() should match expected translated values", () => {
  const translate = (string, object?) => `${string}${object}`;
  const result = pageContent({ translate });

  const expected = {
    title: translate("cookiesDeclaration.title"),
    heading: translate("cookiesDeclaration.heading"),
    headingTwo: translate("cookiesDeclaration.headingTwo"),
    paragraphOne: translate("cookiesDeclaration.paragraphOne"),
    paragraphTwo: translate("cookiesDeclaration.paragraphTwo"),
    detailsText: translate("cookiesDeclaration.detailsText"),
    tableHeadOne: translate("cookiesDeclaration.table.headOne"),
    tableHeadTwo: translate("cookiesDeclaration.table.headTwo"),
    tableHeadThree: translate("cookiesDeclaration.table.headThree"),
    tableRowOneTextOne: translate("cookiesDeclaration.table.rowOneTextOne", {
      gaTrackingId: "G-WDL00001T",
    }),
    tableRowOneTextTwo: translate("cookiesDeclaration.table.rowOneTextTwo"),
    tableRowOneTextThree: translate("cookiesDeclaration.table.rowOneTextThree"),
    tableRowTwoTextOne: translate("cookiesDeclaration.table.rowTwoTextOne"),
    tableRowTwoTextTwo: translate("cookiesDeclaration.table.rowTwoTextTwo"),
    tableRowTwoTextThree: translate("cookiesDeclaration.table.rowTwoTextThree"),
    measureAnalyticsQuestion: translate("cookiesDeclaration.question"),
    measureAnalyticsYes: translate("yes"),
    measureAnalyticsNo: translate("no"),
    buttonText: translate("cookiesDeclaration.buttonText"),
    hideCookieBanner: true,
  };

  expect(result).toEqual(expected);
});
