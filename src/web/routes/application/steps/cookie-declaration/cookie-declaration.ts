import { isConsentedToStatistics } from "../common/cookie-consent";
import { YES } from "../common/constants";
import { config } from "../../../../../config";

const pageContent = ({ translate }) => ({
  title: translate("cookiesDeclaration.title"),
  heading: translate("cookiesDeclaration.heading"),
  headingTwo: translate("cookiesDeclaration.headingTwo"),
  paragraphOne: translate("cookiesDeclaration.paragraphOne"),
  paragraphTwo: translate("cookiesDeclaration.paragraphTwo"),
  detailsText: translate("cookiesDeclaration.detailsText"),
  tableHeadOne: translate("cookiesDeclaration.table.headOne"),
  tableHeadTwo: translate("cookiesDeclaration.table.headTwo"),
  tableHeadThree: translate("cookiesDeclaration.table.headThree"),
  tableRowOneTextOne: translate("cookiesDeclaration.table.rowOneTextOne", {
    gaTrackingId: config.environment.GA_TRACKING_ID?.replace(/G-/, ""),
  }),
  tableRowOneTextTwo: translate("cookiesDeclaration.table.rowOneTextTwo"),
  tableRowOneTextThree: translate("cookiesDeclaration.table.rowOneTextThree"),
  tableRowTwoTextOne: translate("cookiesDeclaration.table.rowTwoTextOne"),
  tableRowTwoTextTwo: translate("cookiesDeclaration.table.rowTwoTextTwo"),
  tableRowTwoTextThree: translate("cookiesDeclaration.table.rowTwoTextThree"),
  measureAnalyticsQuestion: translate("cookiesDeclaration.question"),
  measureAnalyticsYes: translate("yes"),
  measureAnalyticsNo: translate("no"),
  buttonText: translate("cookiesDeclaration.buttonText"),
  hideCookieBanner: true,
});

const behaviourForGet = () => (req, res, next) => {
  res.locals.measureAnalytics = isConsentedToStatistics(req);
  next();
};

const behaviourForPost = () => (req, res, next) => {
  if (req.body.measureStatistics === YES) {
    req.session.measureAnalytics = true;
  } else {
    req.session.measureAnalytics = false;
  }

  next();
};

const cookieDeclaration = {
  path: "/choose-cookies",
  template: "cookies-declaration",
  pageContent,
  behaviourForGet,
  behaviourForPost,
};

export { behaviourForGet, behaviourForPost, cookieDeclaration, pageContent };
