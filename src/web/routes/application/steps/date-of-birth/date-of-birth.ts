import { validate } from "./validate";
import { sanitize } from "./sanitize";
import { DEFAULT_LIST } from "../check-your-answers/constants";
import { formatDateForDisplay } from "../common/formatters/dates";
import {
  isDateSixteenOrMoreYearsInThePast,
  isDateSixtyOrMoreYearsInThePast,
} from "@nhsbsa/health-charge-exemption-common-frontend";

const pageContent = ({ translate }) => ({
  title: translate("dateOfBirth.title"),
  heading: translate("dateOfBirth.heading"),
  hint: translate("dateOfBirth.hint"),
  buttonText: translate("buttons:continue"),
  dayLabel: translate("dateOfBirth.dayLabel"),
  monthLabel: translate("dateOfBirth.monthLabel"),
  yearLabel: translate("dateOfBirth.yearLabel"),
  explanation: translate("dateOfBirth.explanation"),
  cancel: translate("cancel"),
});

const dateOfBirthDateContentSummaryId = "#date-of-birth-day";

const contentSummary = (req) => ({
  id: "#date-of-birth-day",
  key: req.t("dateOfBirth.summaryKey"),
  section: DEFAULT_LIST,
  value: formatDateForDisplay(
    req.session.claim["dateOfBirth-day"],
    req.session.claim["dateOfBirth-month"],
    req.session.claim["dateOfBirth-year"],
  ),
  visuallyHidden: req.t("dateOfBirth.hiddenChangeText"),
});

const requestBody = (session) => ({
  dateOfBirth: session.claim.dateOfBirth,
});

const isAgeAboveSixtyOrBelowSixteenYears = (claim) =>
  !isDateSixteenOrMoreYearsInThePast(claim.dateOfBirth) === true ||
  isDateSixtyOrMoreYearsInThePast(claim.dateOfBirth) === true;

const dateOfBirth = {
  path: "/dob",
  template: "date-of-birth",
  pageContent,
  sanitize,
  validate,
  contentSummary,
  requestBody,
  shouldInvalidateReview: isAgeAboveSixtyOrBelowSixteenYears,
};

export {
  dateOfBirth,
  contentSummary,
  requestBody,
  dateOfBirthDateContentSummaryId,
  isAgeAboveSixtyOrBelowSixteenYears,
};
