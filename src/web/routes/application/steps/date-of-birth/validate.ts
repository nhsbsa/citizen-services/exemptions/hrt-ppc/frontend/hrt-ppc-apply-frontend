import { check } from "express-validator";
import {
  DATE_OF_BIRTH_DAY_KEY,
  DATE_OF_BIRTH_MONTH_KEY,
  DATE_OF_BIRTH_YEAR_KEY,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";
import {
  validateDateOfBirth,
  validateDateOfBirthIsEmpty,
  validateDateOfBirthDay,
  validateDateOfBirthMonth,
  validateDateOfBirthYear,
  addDateToBody as addDateOfBirthDateToBody,
} from "@nhsbsa/health-charge-exemption-common-frontend";

const validate = () => [
  addDateOfBirthDateToBody,
  check("dateOfBirth")
    .custom(validateDateOfBirth)
    .bail()
    .custom(validateDateOfBirthIsEmpty)
    .bail(),
  check(DATE_OF_BIRTH_DAY_KEY).custom(validateDateOfBirthDay),
  check(DATE_OF_BIRTH_MONTH_KEY).custom(validateDateOfBirthMonth),
  check(DATE_OF_BIRTH_YEAR_KEY).custom(validateDateOfBirthYear),
];

export { validate };
