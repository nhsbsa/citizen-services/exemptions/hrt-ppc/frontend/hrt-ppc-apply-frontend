import expect from "expect";
const mockSanitizeDateOfBirth = jest.fn();
import { sanitize } from "./sanitize";

jest.mock("@nhsbsa/health-charge-exemption-common-frontend", () => ({
  sanitizeDateOfBirth: mockSanitizeDateOfBirth,
}));

const req = {
  body: {
    "dateOfBirth-day": "030",
    "dateOfBirth-month": "005",
    "dateOfBirth-year": "00001920",
    dateOfBirth: "1920-05-30",
  },
};
const next = jest.fn();

test("sanitize() should call sanitizeDateOfBirth", () => {
  sanitize()(req, {}, next);

  expect(mockSanitizeDateOfBirth).toHaveBeenCalledTimes(1);
  expect(mockSanitizeDateOfBirth).toHaveBeenCalledWith(req);
  expect(next).toHaveBeenCalledTimes(1);
  expect(next).toHaveBeenCalledWith();
});
