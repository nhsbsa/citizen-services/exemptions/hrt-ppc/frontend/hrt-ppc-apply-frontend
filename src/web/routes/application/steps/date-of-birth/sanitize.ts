import { sanitizeDateOfBirth } from "@nhsbsa/health-charge-exemption-common-frontend";

const sanitize = () => (req, res, next) => {
  sanitizeDateOfBirth(req);

  next();
};

export { sanitize };
