import expect from "expect";
const mockValidateDateOfBirth = jest.fn();
const mockValidateDateOfBirthIsEmpty = jest.fn();
const mockValidateDateOfBirthDay = jest.fn();
const mockValidateDateOfBirthMonth = jest.fn();
const mockValidateDateOfBirthYear = jest.fn();
const mockAddDateToBody = jest.fn();
import { validate } from "./validate";
import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend";
import { FieldValidationError } from "express-validator";

let request;

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date("2023-04-01"));
  request = {
    t: (string) => string,
    body: {
      dateOfBirth: "1997-03-24",
      "dateOfBirth-day": "24",
      "dateOfBirth-month": "03",
      "dateOfBirth-year": "1997",
    },
  };

  mockValidateDateOfBirth.mockReturnValue(true);
  mockValidateDateOfBirthIsEmpty.mockReturnValue(true);
  mockValidateDateOfBirthDay.mockReturnValue(true);
  mockValidateDateOfBirthMonth.mockReturnValue(true);
  mockValidateDateOfBirthYear.mockReturnValue(true);
});

jest.mock("@nhsbsa/health-charge-exemption-common-frontend", () => ({
  ...jest.requireActual("@nhsbsa/health-charge-exemption-common-frontend"),
  addDateToBody: mockAddDateToBody,
  validateDateOfBirth: mockValidateDateOfBirth,
  validateDateOfBirthIsEmpty: mockValidateDateOfBirthIsEmpty,
  validateDateOfBirthDay: mockValidateDateOfBirthDay,
  validateDateOfBirthMonth: mockValidateDateOfBirthMonth,
  validateDateOfBirthYear: mockValidateDateOfBirthYear,
}));

describe("validate date of birth", () => {
  test("returns error when dateOfBirth field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirth");
    mockValidateDateOfBirth.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirth");
  });

  test("returns error when dateOfBirth-day field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthDay");
    mockValidateDateOfBirthDay.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-day");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthDay");
  });

  test("returns error when dateOfBirth-month field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthMonth");
    mockValidateDateOfBirthMonth.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-month");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthMonth");
  });

  test("returns error when dateOfBirth-year field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthYear");
    mockValidateDateOfBirthYear.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-year");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthYear");
  });

  test("calls addDateToBody", async () => {
    await applyExpressValidation(request, validate());

    expect(mockAddDateToBody).toHaveBeenCalledTimes(1);
  });
});
