import expect from "expect";
import {
  contentSummary,
  requestBody,
  isAgeAboveSixtyOrBelowSixteenYears,
} from "./date-of-birth";
import { dateAsString } from "@nhsbsa/health-charge-exemption-common-frontend";

const req = {
  t: (string) => string,
  session: {
    claim: {
      "dateOfBirth-day": "30",
      "dateOfBirth-month": "05",
      "dateOfBirth-year": "1920",
      dateOfBirth: "1920-05-30",
    },
  },
};

test("Date of birth contentSummary() should return content summary in correct format", () => {
  const result = contentSummary(req);

  const expected = {
    id: "#date-of-birth-day",
    key: "dateOfBirth.summaryKey",
    section: "aboutYou",
    value: "30 May 1920",
    visuallyHidden: "dateOfBirth.hiddenChangeText",
  };

  expect(result).toEqual(expected);
});

test("Date of birth requestBody() should return request body in correct format", () => {
  const result = requestBody(req.session);

  const expected = {
    dateOfBirth: "1920-05-30",
  };

  expect(result).toEqual(expected);
});

describe("isAgeAboveSixtyOrBelowSixteenYears()", () => {
  test(`returns true if age is more than sixty years`, () => {
    const claim = {
      dateOfBirth: "1955-05-30",
    };

    const result = isAgeAboveSixtyOrBelowSixteenYears(claim);
    expect(result).toBe(true);
  });

  test(`returns true if age equals to sixty years`, () => {
    const sixtyYearsAgo = dateAsString({
      yearAdjustment: -60,
    });

    const claim = {
      dateOfBirth: sixtyYearsAgo,
    };

    const result = isAgeAboveSixtyOrBelowSixteenYears(claim);
    expect(result).toBe(true);
  });

  test(`returns true if age is less than sixteen years`, () => {
    const fifteenYearsElevenMonthsAgo = dateAsString({
      yearAdjustment: -15,
      monthAdjustment: -11,
    });

    const claim = {
      dateOfBirth: fifteenYearsElevenMonthsAgo,
    };

    const result = isAgeAboveSixtyOrBelowSixteenYears(claim);
    expect(result).toBe(true);
  });

  test(`returns false if age is equal to sixteen years`, () => {
    const sixteenYearsAgo = dateAsString({
      yearAdjustment: -16,
    });

    const claim = {
      dateOfBirth: sixteenYearsAgo,
    };

    const result = isAgeAboveSixtyOrBelowSixteenYears(claim);
    expect(result).toBe(false);
  });

  test(`returns false if age is more than or equals to sixteen years and less than sixty years`, () => {
    const moreThenSixteenAndLessThenSixtyYearsAgo = dateAsString({
      yearAdjustment: -40,
      monthAdjustment: -11,
    });

    const claim = {
      dateOfBirth: moreThenSixteenAndLessThenSixtyYearsAgo,
    };

    const result = isAgeAboveSixtyOrBelowSixteenYears(claim);
    expect(result).toBe(false);
  });
});
