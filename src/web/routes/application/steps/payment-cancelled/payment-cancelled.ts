import { handleStateDestroySession } from "../../flow-control/middleware/handle-get";
import { PAYMENT_CANCELLED_URL } from "../../paths/paths";
import { prefixPath } from "../../paths/prefix-path";

const pageContent = (translate) => ({
  title: translate("paymentCancelled.title"),
  heading: translate("paymentCancelled.heading"),
  paragraphOne: translate("paymentCancelled.paragraphOne"),
  paragraphTwo: translate("paymentCancelled.paragraphTwo"),
  previous: false,
});

const renderContent = (req, res) => {
  return res.render("payment-cancelled", {
    ...pageContent(req.t),
  });
};

const registerPaymentCancelled = (journey, app) => {
  app.get(
    prefixPath(journey.pathPrefix, PAYMENT_CANCELLED_URL),
    handleStateDestroySession(journey),
    renderContent,
  );
};

export { registerPaymentCancelled, renderContent };
