import expect from "expect";
import { assocPath } from "ramda";
import { validate } from "./validate";
import { Countries } from "../common/enums";
import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend";
import { FieldValidationError } from "express-validator";

const translate = (string) => string;
const req = {
  t: translate,
  body: {
    prescriptionCountryName: Countries.ENGLAND,
  },
};

test("validation middleware passes with valid option ENGLAND body", async () => {
  const testReq = { ...req };

  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(true);
});

test("validation middleware passes with valid option SCOTLAND body", async () => {
  const testReq = assocPath(
    ["body", "prescriptionCountryName"],
    Countries.SCOTLAND,
    req,
  );
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(true);
});

test("validation middleware passes with valid option WALES body", async () => {
  const testReq = assocPath(
    ["body", "prescriptionCountryName"],
    Countries.WALES,
    req,
  );
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(true);
});

test("validation middleware passes with valid option NORTHERNIRELAND body", async () => {
  const testReq = assocPath(
    ["body", "prescriptionCountryName"],
    Countries.NORTHERNIRELAND,
    req,
  );
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(true);
});

test("validation middleware fails with invalid option Test body", async () => {
  const testReq = assocPath(["body", "prescriptionCountryName"], "Test", req);
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }
  expect(result.isEmpty()).toBe(false);
});

test("validation middleware errors for prescription country field", async () => {
  const testReq = assocPath(["body", "prescriptionCountryName"], "maybe", req);
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0] as FieldValidationError;
  expect(result.array().length).toBe(1);
  expect(error.path).toBe("prescriptionCountryName");
  expect(error.msg).toBe("validation:selectPrescriptionCountry");
});

test("validation middleware errors for prescription country field is empty", async () => {
  const testReq = assocPath(["body", "prescriptionCountryName"], null, req);
  const result = await applyExpressValidation(testReq, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0] as FieldValidationError;
  expect(result.array().length).toBe(1);
  expect(error.path).toBe("prescriptionCountryName");
  expect(error.msg).toBe("validation:selectPrescriptionCountry");
});
