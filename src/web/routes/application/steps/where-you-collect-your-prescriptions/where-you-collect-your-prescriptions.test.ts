import expect from "expect";
import { claimantInEnglandOrOther } from "./where-you-collect-your-prescriptions";
import { Countries } from "../common/enums";

test(`claimantInEnglandOrOther returns true when in England`, () => {
  const claim = {
    prescriptionCountryName: Countries.ENGLAND,
  };
  const result = claimantInEnglandOrOther(claim);
  expect(result).toBeTruthy();
});

test(`claimantInEnglandOrOther returns false when in Scotland`, () => {
  const claim = {
    prescriptionCountryName: Countries.SCOTLAND,
  };
  const result = claimantInEnglandOrOther(claim);
  expect(result).toBeFalsy();
});

test(`claimantInEnglandOrOther returns false when in Wales`, () => {
  const claim = {
    prescriptionCountryName: Countries.WALES,
  };
  const result = claimantInEnglandOrOther(claim);
  expect(result).toBeFalsy();
});

test(`claimantInEnglandOrOther returns false when in Northern Ireland`, () => {
  const claim = {
    prescriptionCountryName: Countries.NORTHERNIRELAND,
  };
  const result = claimantInEnglandOrOther(claim);
  expect(result).toBeFalsy();
});
