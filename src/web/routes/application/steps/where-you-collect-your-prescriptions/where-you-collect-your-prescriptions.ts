import { validate } from "./validate";
import { Countries } from "../common/enums";

const pageContent = ({ translate, req }) => ({
  title: translate("prescriptionCountryName.title"),
  heading: translate("prescriptionCountryName.heading"),
  buttonText: translate("buttons:continue"),
  england: translate("prescriptionCountryName.england"),
  scotland: translate("prescriptionCountryName.scotland"),
  wales: translate("prescriptionCountryName.wales"),
  northernIreland: translate("prescriptionCountryName.northernIreland"),
  cancel: req.t("cancel"),
});

const claimantInEnglandOrOther = (claim) =>
  claim.prescriptionCountryName === Countries.ENGLAND;

const prescriptionCountryName = {
  path: "/where-you-collect",
  template: "where-you-collect-your-prescriptions",
  pageContent,
  validate,
  shouldInvalidateReview: claimantInEnglandOrOther,
};

export { prescriptionCountryName, claimantInEnglandOrOther, pageContent };
