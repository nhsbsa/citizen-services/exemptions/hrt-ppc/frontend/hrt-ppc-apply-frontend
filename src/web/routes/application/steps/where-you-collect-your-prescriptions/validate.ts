import { translateValidationMessage } from "@nhsbsa/health-charge-exemption-common-frontend";
import { check } from "express-validator";
import { Countries } from "../common/enums";

const validate = () => [
  check("prescriptionCountryName")
    .isIn([
      Countries.ENGLAND,
      Countries.SCOTLAND,
      Countries.WALES,
      Countries.NORTHERNIRELAND,
    ])
    .withMessage(
      translateValidationMessage("validation:selectPrescriptionCountry"),
    ),
];

export { validate };
