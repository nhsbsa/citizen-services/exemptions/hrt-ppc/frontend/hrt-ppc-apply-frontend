import { validate } from "./validate";
import { SOME } from "../common/constants";
import { path } from "ramda";

const pageContent = ({ translate, req }) => ({
  title: translate("someMedicinesNotCovered.title"),
  heading: translate("someMedicinesNotCovered.heading"),
  paragraphOne: translate("someMedicinesNotCovered.paragraphOne"),
  paragraphTwo: translate("someMedicinesNotCovered.paragraphTwo"),
  paragraphThree: translate("someMedicinesNotCovered.paragraphThree"),
  listItemOne: translate("someMedicinesNotCovered.listItemOne"),
  listItemTwo: translate("someMedicinesNotCovered.listItemTwo"),
  findOutMoreLink: translate("someMedicinesNotCovered.findOutMoreLink"),
  findOutMoreLinkText: translate("someMedicinesNotCovered.findOutMoreLinkText"),
  radioQuestion: translate("someMedicinesNotCovered.radioQuestion"),
  buttonText: translate("buttons:continue"),
  yes: req.t("yes"),
  no: req.t("no"),
  cancel: req.t("cancel"),
});

const isNavigable = (req, session) =>
  path(["claim", "isMedicineCovered"], session) === SOME;

const someMedicinesNotCovered = {
  path: "/some-medicine-covered",
  template: "some-of-hrt-medicines-are-not-covered",
  pageContent,
  validate,
  isNavigable,
};

export { someMedicinesNotCovered, pageContent, isNavigable };
