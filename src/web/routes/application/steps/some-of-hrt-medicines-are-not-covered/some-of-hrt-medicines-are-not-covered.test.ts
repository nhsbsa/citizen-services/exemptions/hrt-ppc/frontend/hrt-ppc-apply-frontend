import expect from "expect";
import {
  pageContent,
  isNavigable,
} from "./some-of-hrt-medicines-are-not-covered";

const req = {
  t: (string) => string,
  session: {
    claim: {
      doYouStillWantToBuySomeMedicineNotCovered: "no",
    },
  },
};

test(`pageContent() should return expected results`, () => {
  const translate = (string, object?) => `${string}${object}`;
  const result = pageContent({ translate, req });

  const expected = {
    title: translate("someMedicinesNotCovered.title"),
    heading: translate("someMedicinesNotCovered.heading"),
    paragraphOne: translate("someMedicinesNotCovered.paragraphOne"),
    paragraphTwo: translate("someMedicinesNotCovered.paragraphTwo"),
    paragraphThree: translate("someMedicinesNotCovered.paragraphThree"),
    listItemOne: translate("someMedicinesNotCovered.listItemOne"),
    listItemTwo: translate("someMedicinesNotCovered.listItemTwo"),
    findOutMoreLink: translate("someMedicinesNotCovered.findOutMoreLink"),
    findOutMoreLinkText: translate(
      "someMedicinesNotCovered.findOutMoreLinkText",
    ),
    radioQuestion: translate("someMedicinesNotCovered.radioQuestion"),
    buttonText: translate("buttons:continue"),
    yes: req.t("yes"),
    no: req.t("no"),
    cancel: req.t("cancel"),
  };

  expect(result).toEqual(expected);
});

test(`isNavigable() returns true if medicine covered is selected as SOME`, () => {
  const session = {
    claim: {
      isMedicineCovered: "some",
    },
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(true);
});

test(`isNavigable() returns false if medicine covered selected as NO`, () => {
  const session = {
    claim: {
      isMedicineCovered: "no",
    },
  };

  const result = isNavigable(undefined, session);
  expect(result).toBeFalsy();
});

test(`isNavigable() returns false if medicine covered selected as YES`, () => {
  const session = {
    claim: {
      isMedicineCovered: "yes",
    },
  };

  const result = isNavigable(undefined, session);
  expect(result).toBeFalsy();
});
