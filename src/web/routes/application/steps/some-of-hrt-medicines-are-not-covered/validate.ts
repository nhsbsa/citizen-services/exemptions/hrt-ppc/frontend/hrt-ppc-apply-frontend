import { translateValidationMessage } from "@nhsbsa/health-charge-exemption-common-frontend";
import { check } from "express-validator";
import { YES, NO } from "../common/constants";

const validate = () => [
  check("doYouStillWantToBuySomeMedicineNotCovered")
    .isIn([YES, NO])
    .withMessage(
      translateValidationMessage("validation:selectDoYouStillWantToBuy"),
    ),
];

export { validate };
