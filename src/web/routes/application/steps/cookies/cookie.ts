import { stateMachine } from "../../flow-control/state-machine";
import * as actions from "../../flow-control/state-machine/actions";
import * as states from "../../flow-control/states";
import { contextPath } from "../../paths/context-path";
const { SET_NEXT_ALLOWED_PATH } = actions;

const pageContent = ({ translate }) => ({
  title: translate("cookies.title"),
  heading: translate("cookies.heading"),
  linkName: translate("cookies.linkName"),
  headingOne: translate("cookies.headingOne"),
  paragraphOne: translate("cookies.paragraphOne"),
  paragraphTwo: translate("cookies.paragraphTwo"),
  paragraphThree: translate("cookies.paragraphThree"),
  headingTwo: translate("cookies.headingTwo"),
  paragraphFour: translate("cookies.paragraphFour"),
  listItemOne: translate("cookies.listItemOne"),
  listItemTwo: translate("cookies.listItemTwo"),
  listItemThree: translate("cookies.listItemThree"),
  listItemFour: translate("cookies.listItemFour"),
  detailsHeading: translate("cookies.detailsHeading"),
  tableHeadOne: translate("cookies.table.headOne"),
  tableHeadTwo: translate("cookies.table.headTwo"),
  tableHeadThree: translate("cookies.table.headThree"),
  tableRowOneTextOne: translate("cookies.table.rowOneTextOne"),
  tableRowOneTextTwo: translate("cookies.table.rowOneTextTwo"),
  tableRowOneTextThree: translate("cookies.table.rowOneTextThree"),
  tableRowTwoTextOne: translate("cookies.table.rowTwoTextOne"),
  tableRowTwoTextTwo: translate("cookies.table.rowTwoTextTwo"),
  tableRowTwoTextThree: translate("cookies.table.rowTwoTextThree"),
  tableRowThreeTextOne: translate("cookies.table.rowThreeTextOne"),
  tableRowThreeTextTwo: translate("cookies.table.rowThreeTextTwo"),
  tableRowThreeTextThree: translate("cookies.table.rowThreeTextThree"),
  tableRowFourTextOne: translate("cookies.table.rowFourTextOne"),
  tableRowFourTextTwo: translate("cookies.table.rowFourTextTwo"),
  tableRowFourTextThree: translate("cookies.table.rowFourTextThree"),
  headingThree: translate("cookies.headingThree"),
  paragraphFive: translate("cookies.paragraphFive"),
  paragraphSix: translate("cookies.paragraphSix"),
  buttonText: translate("cookies.buttonText"),
  headingFour: translate("cookies.headingFour"),
  paragraphSeven: translate("cookies.paragraphSeven"),
  paragraphSevenLinkText: translate("cookies.paragraphSevenLinkText"),
});

const behaviourForGet = (config, journey) => (req, res, next) => {
  // There is no submit button on this page, instead hyperlink to take to next page
  // therefore this line is needed to prevent the state machine from redirecting the user back to cookies.
  stateMachine.setState(states.IN_PROGRESS_COOKIES, req, journey);
  stateMachine.dispatch(
    SET_NEXT_ALLOWED_PATH,
    req,
    journey,
    contextPath("/choose-cookies"),
  );
  next();
};

const cookies = {
  embeddedMainJourney: true,
  path: "/cookie-information",
  template: "cookie-information",
  pageContent,
  behaviourForGet,
};

export { behaviourForGet, cookies };
