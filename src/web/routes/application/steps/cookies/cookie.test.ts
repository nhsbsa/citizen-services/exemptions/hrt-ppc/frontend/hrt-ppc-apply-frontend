import expect from "expect";
import { partial } from "ramda";
import { behaviourForGet, cookies } from "./cookie";
import { states, testUtils } from "../../flow-control";

const {
  buildSessionForJourney,
  getNextAllowedPathForJourney,
  getStateForJourney,
} = testUtils;
const { IN_PROGRESS } = states;

const CONFIG = {};
const COOKIE = "cookie";
const JOURNEY = { name: COOKIE };

const getNextAllowedPathForApplyJourney = partial(
  getNextAllowedPathForJourney,
  [COOKIE],
);

test("cookies should have the correct functions", () => {
  expect("behaviourForGet" in cookies).toBe(true);
  expect("path" in cookies).toBe(true);
  expect(cookies.path).toBe("/cookie-information");
  expect("template" in cookies).toBe(true);
  expect(cookies.template).toBe("cookie-information");
  expect("pageContent" in cookies).toBe(true);
  expect("embeddedMainJourney" in cookies).toBe(true);
  expect(cookies.embeddedMainJourney).toBe(true);
});

test("behaviourForGet() on cookie guidance", () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: COOKIE,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };
  const res = {};

  const next = jest.fn();
  const expectedState = states.IN_PROGRESS_COOKIES;

  behaviourForGet(CONFIG, JOURNEY)(req, res, next);

  expect(getNextAllowedPathForApplyJourney(req)).toBe(
    "/test-context/choose-cookies",
  );
  expect(next).toHaveBeenCalled();
  expect(getStateForJourney(JOURNEY.name, req)).toBe(expectedState);
});
