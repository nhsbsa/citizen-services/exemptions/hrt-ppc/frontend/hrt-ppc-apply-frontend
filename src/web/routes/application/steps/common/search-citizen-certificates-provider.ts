import { searchClient } from "../../../../client/search-client";
import { logger } from "../../../../logger/logger";
import { config } from "../../../../../config";
import { requestBody as requestBodyName } from "../name/name";
import { requestBody as requestBodyDateOfBirth } from "../date-of-birth/date-of-birth";
import { isNilOrEmpty } from "../../../../../common/predicates";
import { CertificateStatus } from "@nhsbsa/health-charge-exemption-common-frontend";
import moment from "moment";
import Redactyl from "redactyl.js";

const redactyl = new Redactyl({
  properties: ["firstName", "lastName", "dateOfBirth", "addresses"],
});

const makeSearchRequest = async (req) => {
  const client = new searchClient(config);
  const body = createRequestBody(req);
  const response = await client.makeRequest(
    {
      method: "POST",
      url: "/v1/hrt-exemptions/match",
      data: body,
      responseType: "json",
    },
    req,
  );
  return response;
};

const createRequestBody = (req) => {
  const postcode = req.body.sanitizedPostcode;
  const session = req.session;
  return {
    citizen: {
      ...{ address: { postcode: postcode } },
      ...requestBodyName(session),
      ...requestBodyDateOfBirth(session),
    },
    certificate: {
      status: CertificateStatus.ACTIVE,
    },
  };
};

const searchExemptions = async (req) => {
  const response = await makeSearchRequest(req);

  const data: any = response.data;

  logger.info(
    `Exemption search result: ` + JSON.stringify(redactyl.redact(data)),
    req,
  );

  const hrtPpcs = data.exemptions["hrt-ppcs"];
  const others = data.exemptions["others"];

  const filterOutCitizensExpiring = (exemption: Exemption): void =>
    filterOutCitizensWithCertificatesExpiringOrExpired(exemption);

  const filterCitizensWithEmptyCertificate = (exemption: Exemption): void => {
    exemption.citizens = exemption.citizens.filter(
      (citizen) => !isNilOrEmpty(citizen.certificates),
    );
  };

  const filterEmptyCitizens = (exemption: Exemption): boolean =>
    !isNilOrEmpty(exemption.citizens);

  hrtPpcs.forEach(filterOutCitizensExpiring);
  hrtPpcs.forEach(filterCitizensWithEmptyCertificate);
  others.forEach(filterOutCitizensExpiring);
  others.forEach(filterCitizensWithEmptyCertificate);
  const filteredHrtPpcs = hrtPpcs.filter(filterEmptyCitizens);
  const filteredOthers = others.filter(filterEmptyCitizens);

  const activeExemptions =
    !isNilOrEmpty(filteredHrtPpcs) || !isNilOrEmpty(filteredOthers);

  logger.info(`Result active exemptions: ` + activeExemptions, req);

  return {
    activeExemptions: activeExemptions,
  };
};

const filterOutCitizensWithCertificatesExpiringOrExpired = function (
  exemption,
) {
  const gracePeriodExpiringExemption =
    config.environment.EXPIRING_EXEMPTION_GRACE_PERIOD;
  const todayPlusMonths = moment(new Date()).add(
    gracePeriodExpiringExemption,
    "M",
  );
  const today = moment(new Date());
  exemption.citizens.forEach(function (citizen) {
    citizen.certificates = citizen.certificates.filter((certificate) => {
      const startDate = moment(certificate.startDate);
      const endDate = moment(certificate.endDate);
      /*
      If the exemption starts before today and the end date is after today, and its also within a defined time period i.e 1 Month
      then this will not be filtered out since it is expiring soon and we want to allow the user to renew.
      If the exemption has already expired or is same as todays date then we want them to renew.
      */
      const isExpiringSoon =
        startDate.isBefore(today) &&
        endDate.isAfter(today) &&
        endDate.isBefore(todayPlusMonths);
      const isEndDateSameOrBeforeToday = endDate.isSameOrBefore(today);
      return !(isExpiringSoon || isEndDateSameOrBeforeToday);
    });
  });
};

export {
  createRequestBody,
  searchExemptions,
  filterOutCitizensWithCertificatesExpiringOrExpired,
};
