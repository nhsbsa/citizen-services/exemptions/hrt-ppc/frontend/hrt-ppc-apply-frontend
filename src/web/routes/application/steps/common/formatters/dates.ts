import moment from "moment";
import { DATE_FORMAT_FULL } from "@nhsbsa/health-charge-exemption-common-frontend";

// TODO - When we are no longer able to go directly to a page within the flow (which makes ATs simpler)
// we should reinforce the fact that day, month and year should be mandatory.
const formatDateForDisplay = (day, month, year) => {
  return moment({ years: year, months: month - 1, date: day }).format(
    DATE_FORMAT_FULL,
  );
};

const formatDateForDisplayFromDate = (date) => {
  if (date === undefined || !(date instanceof Date)) {
    throw new Error("A date must be provided");
  }
  return moment(date).format(DATE_FORMAT_FULL);
};

export { formatDateForDisplay, formatDateForDisplayFromDate };
