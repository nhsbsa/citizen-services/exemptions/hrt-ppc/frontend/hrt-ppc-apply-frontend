import expect from "expect";
import { formatDateForDisplay, formatDateForDisplayFromDate } from "./dates";

test("formatDateForDisplay", () => {
  expect(formatDateForDisplay("30", "1", "1990")).toBe("30 January 1990");
  expect(formatDateForDisplay("02", "12", "2000")).toBe("2 December 2000");
  expect(formatDateForDisplay(5, 5, 2010)).toBe("5 May 2010");
});

test("formatDateForDisplayFromDate", () => {
  expect(formatDateForDisplayFromDate(new Date(1990, 0, 30))).toBe(
    "30 January 1990",
  );
  expect(
    formatDateForDisplayFromDate.bind(null, { date: undefined }),
  ).toThrowError(/A date must be provided/);
  expect(formatDateForDisplayFromDate.bind(null, { date: [] })).toThrowError(
    /A date must be provided/,
  );
});
