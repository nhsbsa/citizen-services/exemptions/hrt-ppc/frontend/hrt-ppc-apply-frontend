export type CertificateDetail = {
  key: string | number;
  value: string;
  href?: string;
};

export type RowHeader = {
  key: {
    text: string | number;
  };
  value: {
    text: string;
  };
  actions?: {
    items: {
      href: string;
      text: string;
      visuallyHiddenText: string | number;
    }[];
  };
};

export type ErrorObject = {
  text: string;
  href: string;
  attributes: {
    id: string;
  };
};

export type DownloadCertificateRequestBody = {
  personalisation: {
    firstName: string;
    lastName: string;
    reference: string;
    startDate: string;
    endDate: string;
  };
  service: string;
  template: string;
  accessibilitySwitch: boolean;
};
