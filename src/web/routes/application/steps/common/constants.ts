export const YES: string = "yes";
export const NO: string = "no";
export const SOME: string = "some";
export const CONFIRMATION_CODE_SESSION_PROPERTY: string = "confirmationCode";
export const CONFIRMATION_CODE_ENTERED_SESSION_PROPERTY: string =
  "confirmationCodeEntered";
export const CONTACT_TELEPHONE: string | undefined =
  process.env.CONTACT_TELEPHONE;
export const CONTACT_EMAIL: string | undefined = process.env.CONTACT_EMAIL;
