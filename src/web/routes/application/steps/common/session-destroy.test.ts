import expect from "expect";
import { sessionDestroy } from "./session-destroy";
import { IN_PROGRESS } from "../../flow-control/states";
import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";

const APPLY = "apply";

test(`sessionDestroy() should destroy the session and clear the cookie`, () => {
  const destroy = jest.fn();
  const clearCookie = jest.fn();

  const req = {
    path: "/second",
    session: {
      destroy,
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = {
    clearCookie,
  };

  sessionDestroy(req, res);

  expect(destroy).toHaveBeenCalled();
  expect(clearCookie).toHaveBeenCalledWith("lang");
});
