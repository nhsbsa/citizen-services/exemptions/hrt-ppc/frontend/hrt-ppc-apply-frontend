import { logger } from "../../../../logger/logger";

const sessionDestroy = (req, res) => {
  logger.info("Session has been destroyed", req);
  req.session.destroy();
  res.clearCookie("lang");
};

export { sessionDestroy };
