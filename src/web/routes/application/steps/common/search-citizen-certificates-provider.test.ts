import expect from "expect";
import moment from "moment";
import { config } from "../../../../../config";
import {
  filterOutCitizensWithCertificatesExpiringOrExpired,
  searchExemptions,
  createRequestBody,
} from "./search-citizen-certificates-provider";
import * as exemptionsNoMatch from "../../../../../__mocks__/api-responses/search-api/exemptions-no-match.json";
import * as exemptionsHrtExpired from "../../../../../__mocks__/api-responses/search-api/exemptions-hrt-expired.json";
import * as exemptionsOtherExpired from "../../../../../__mocks__/api-responses/search-api/exemptions-others-expired.json";
import * as exemptionsMixedExpiredAndActive from "../../../../../__mocks__/api-responses/search-api/exemptions-mixed-expired-and-active.json";
import * as exemptionsHrtCertificatesEmpty from "../../../../../__mocks__/api-responses/search-api/exemptions-hrt-certificates-empty.json";
import * as exemptionsOtherCertificatesEmpty from "../../../../../__mocks__/api-responses/search-api/exemptions-others-certificates-empty.json";
import * as exemptionsHrtCitizensEmpty from "../../../../../__mocks__/api-responses/search-api/exemptions-hrt-citizens-empty.json";
import * as exemptionsOtherCitizensEmpty from "../../../../../__mocks__/api-responses/search-api/exemptions-others-citizens-empty.json";
import { CertificateStatus } from "@nhsbsa/health-charge-exemption-common-frontend";
import { searchClient } from "../../../../client/search-client";
jest.mock("../../../../client/search-client");

const req = {
  session: {
    firstName: "Fname",
    lastName: "Lname",
    dateOfBirth: "2022-01-01",
  },
  body: {
    sanitizedPostcode: "NE15 8NY",
  },
};

const makeRequestMock = jest.fn();

const expectedCitizenAndCertificate = {
  citizen: {
    address: { postcode: "NE15 8NY" },
    firstName: "Fname",
    lastName: "Lname",
    dateOfBirth: "2022-01-01",
  },
  certificate: {
    status: CertificateStatus.ACTIVE,
  },
};
const expectedMockRequestCall = {
  method: "POST",
  url: "/v1/hrt-exemptions/match",
  data: expectedCitizenAndCertificate,
  responseType: "json",
};

jest.mock("../address/request-body", () => ({
  requestBody: jest.fn().mockReturnValue({ address: { postcode: "NE15 8NY" } }),
}));
jest.mock("../name/name", () => ({
  requestBody: jest
    .fn()
    .mockReturnValue({ firstName: "Fname", lastName: "Lname" }),
}));
jest.mock("../date-of-birth/date-of-birth", () => ({
  requestBody: jest.fn().mockReturnValue({ dateOfBirth: "2022-01-01" }),
}));

beforeEach(() => {
  jest.clearAllMocks();
  searchClient.prototype.makeRequest = makeRequestMock;
});

describe("createRequestBody()", () => {
  test("formats and populates the request body", () => {
    const result = createRequestBody(req);
    expect(result).toEqual(expectedCitizenAndCertificate);
  });
});

describe("searchExemptions()", () => {
  test("should return false `activeExemptions` when `hrt-ppcs` and `others` empty", async () => {
    makeRequestMock.mockReturnValue(
      Promise.resolve({ data: exemptionsNoMatch }),
    );

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toEqual({ activeExemptions: false });
  });

  test("should return false `activeExemptions` when `hrt-ppcs` has expired certificate", async () => {
    makeRequestMock.mockReturnValue(
      Promise.resolve({ data: exemptionsHrtExpired }),
    );

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toEqual({ activeExemptions: false });
  });

  test("should return false `activeExemptions` when `others` has expired certificate", async () => {
    makeRequestMock.mockReturnValue(
      Promise.resolve({ data: exemptionsOtherExpired }),
    );

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toEqual({ activeExemptions: false });
  });

  test("should return true `activeExemptions` when there is both a hrt and other certificate not expired/ expiring", async () => {
    makeRequestMock.mockReturnValue(
      Promise.resolve({ data: exemptionsMixedExpiredAndActive }),
    );

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toEqual({ activeExemptions: true });
  });

  test("should return false `activeExemptions` when the `hrt-ppcs[].certificates` is empty", async () => {
    makeRequestMock.mockReturnValue(
      Promise.resolve({ data: exemptionsHrtCertificatesEmpty }),
    );

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toEqual({ activeExemptions: false });
  });

  test("should return false `activeExemptions` when the `others[].certificates` is empty", async () => {
    makeRequestMock.mockReturnValue(
      Promise.resolve({ data: exemptionsOtherCertificatesEmpty }),
    );

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toEqual({ activeExemptions: false });
  });

  test("should return false `activeExemptions` when the `hrt-ppcs[].citizens` is empty", async () => {
    makeRequestMock.mockReturnValue(
      Promise.resolve({ data: exemptionsHrtCitizensEmpty }),
    );

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toEqual({ activeExemptions: false });
  });

  test("should return false `activeExemptions` when the `others[].citizens` is empty", async () => {
    makeRequestMock.mockReturnValue(
      Promise.resolve({ data: exemptionsOtherCitizensEmpty }),
    );

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toEqual({ activeExemptions: false });
  });
});

describe("filterOutCitizensExpiringOrExpired()", () => {
  test("should remain same if certificates empty", () => {
    const actual = {
      citizens: [
        {
          certificates: [],
        },
      ],
    };
    const expected = {
      citizens: [
        {
          certificates: [],
        },
      ],
    };

    filterOutCitizensWithCertificatesExpiringOrExpired(actual);

    expect(actual).toEqual(expected);
  });

  test("should not filter if start date before today and end date after grace period", () => {
    config.environment.EXPIRING_EXEMPTION_GRACE_PERIOD = "1";
    const startDate = moment(new Date()).subtract(1, "days");
    const endDate = moment(new Date()).add(60, "days");
    const actual = {
      citizens: [
        {
          certificates: [
            { reference: "1", startDate: startDate, endDate: endDate },
          ],
        },
      ],
    };
    const expected = {
      citizens: [
        {
          certificates: [
            { reference: "1", startDate: startDate, endDate: endDate },
          ],
        },
      ],
    };

    filterOutCitizensWithCertificatesExpiringOrExpired(actual);

    expect(actual).toEqual(expected);
  });

  test("should not filter if start date is today and end date after grace period", () => {
    config.environment.EXPIRING_EXEMPTION_GRACE_PERIOD = "1";
    const startDate = moment(new Date());
    const endDate = moment(new Date()).add(60, "days");
    const actual = {
      citizens: [
        {
          certificates: [
            { reference: "1", startDate: startDate, endDate: endDate },
          ],
        },
      ],
    };
    const expected = {
      citizens: [
        {
          certificates: [
            { reference: "1", startDate: startDate, endDate: endDate },
          ],
        },
      ],
    };

    filterOutCitizensWithCertificatesExpiringOrExpired(actual);

    expect(actual).toEqual(expected);
  });

  test("should not filter if start date is after today and end date after grace period", () => {
    config.environment.EXPIRING_EXEMPTION_GRACE_PERIOD = "1";
    const startDate = moment(new Date()).add(1, "days");
    const endDate = moment(new Date()).add(60, "days");
    const actual = {
      citizens: [
        {
          certificates: [
            { reference: "1", startDate: startDate, endDate: endDate },
          ],
        },
      ],
    };
    const expected = {
      citizens: [
        {
          certificates: [
            { reference: "1", startDate: startDate, endDate: endDate },
          ],
        },
      ],
    };

    filterOutCitizensWithCertificatesExpiringOrExpired(actual);

    expect(actual).toEqual(expected);
  });

  test("should filter if start date is before today and end date within grace period", () => {
    config.environment.EXPIRING_EXEMPTION_GRACE_PERIOD = "1";
    const startDate = moment(new Date()).subtract(1, "days");
    const endDate = moment(new Date()).add(27, "days");
    const actual = {
      citizens: [
        {
          certificates: [
            { reference: "1", startDate: startDate, endDate: endDate },
          ],
        },
      ],
    };
    const expected = {
      citizens: [
        {
          certificates: [],
        },
      ],
    };

    filterOutCitizensWithCertificatesExpiringOrExpired(actual);

    expect(actual).toEqual(expected);
  });

  test("should filter if start date is before today and end date is today", () => {
    config.environment.EXPIRING_EXEMPTION_GRACE_PERIOD = "1";
    const startDate = moment(new Date()).subtract(1, "days");
    const endDate = moment(new Date());
    const actual = {
      citizens: [
        {
          certificates: [
            { reference: "1", startDate: startDate, endDate: endDate },
          ],
        },
      ],
    };
    const expected = {
      citizens: [
        {
          certificates: [],
        },
      ],
    };

    filterOutCitizensWithCertificatesExpiringOrExpired(actual);

    expect(actual).toEqual(expected);
  });

  test("should filter if start date is before today and expired", () => {
    config.environment.EXPIRING_EXEMPTION_GRACE_PERIOD = "1";
    const startDate = moment(new Date()).subtract(27, "days");
    const endDate = moment(new Date()).subtract(1, "days");
    const actual = {
      citizens: [
        {
          certificates: [
            { reference: "1", startDate: startDate, endDate: endDate },
          ],
        },
      ],
    };
    const expected = {
      citizens: [
        {
          certificates: [],
        },
      ],
    };

    filterOutCitizensWithCertificatesExpiringOrExpired(actual);

    expect(actual).toEqual(expected);
  });

  test("should filter one certificate if only one has expired and start date before today", () => {
    config.environment.EXPIRING_EXEMPTION_GRACE_PERIOD = "1";
    const startDateIsBefore = moment(new Date()).subtract(27, "days");
    const endDateInPastCertificate1 = moment(new Date()).subtract(1, "days");
    const endDateInFutureCertificate2 = moment(new Date()).add(60, "days");
    const actual = {
      citizens: [
        {
          certificates: [
            {
              reference: "1",
              startDate: startDateIsBefore,
              endDate: endDateInPastCertificate1,
            },
            {
              reference: "2",
              startDate: startDateIsBefore,
              endDate: endDateInFutureCertificate2,
            },
          ],
        },
      ],
    };
    const expected = {
      citizens: [
        {
          certificates: [
            {
              reference: "2",
              startDate: startDateIsBefore,
              endDate: endDateInFutureCertificate2,
            },
          ],
        },
      ],
    };

    filterOutCitizensWithCertificatesExpiringOrExpired(actual);

    expect(actual).toEqual(expected);
  });
});
