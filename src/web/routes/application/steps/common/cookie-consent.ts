/**
 * Get the consent cookie and parse it into an object
 */
function getCookie(req) {
  const rawCookie = req.cookies["nhsuk-cookie-consent"];
  if (rawCookie === undefined) {
    return undefined;
  }
  return JSON.parse(rawCookie);
}

/**
 * returns an object containing consent boolean values for each consent type
 */
function getConsent(req) {
  const cookieValue = getCookie(req);
  if (!cookieValue) {
    return {};
  }
  delete cookieValue.version;
  return cookieValue;
}

const isConsentedToStatistics = (req) => {
  const consent = getConsent(req);
  return !!consent.statistics;
};

export { getCookie, getConsent, isConsentedToStatistics };
