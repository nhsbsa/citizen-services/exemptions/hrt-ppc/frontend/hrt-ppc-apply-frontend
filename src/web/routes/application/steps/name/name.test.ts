import expect from "expect";
import { contentSummary, requestBody } from "./name";

const req = {
  t: (string) => string,
  session: {
    claim: {
      firstName: "Lisa",
      lastName: "Smith",
    },
  },
};

test("Enter name contentSummary() should return content summary in correct format", () => {
  const result = contentSummary(req);

  const expected = {
    id: "#first-name",
    key: "name.summaryKey",
    section: "aboutYou",
    value: "Lisa Smith",
    visuallyHidden: "name.hiddenChangeText",
  };

  expect(result).toEqual(expected);
});

test("requestBody() returns request body in correct format", () => {
  const result = requestBody(req.session);

  const expected = {
    firstName: "Lisa",
    lastName: "Smith",
  };

  expect(result).toEqual(expected);
});
