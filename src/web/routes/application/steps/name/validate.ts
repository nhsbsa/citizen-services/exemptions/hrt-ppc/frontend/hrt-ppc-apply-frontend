import { translateValidationMessage } from "@nhsbsa/health-charge-exemption-common-frontend";
import { check } from "express-validator";

const firstNameMaxLength = 50;
const lastNameMaxLength = 50;
const namePattern =
  /^(?!.*[\u00D7\u00F7])[A-Za-z\u1E00-\u1EFF\u0100-\u017F\u00c0-\u00ff\s\-']*$/;

const validate = () => [
  check("firstName")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingFirstName"))
    .isLength({ max: firstNameMaxLength })
    .bail()
    .withMessage(translateValidationMessage("validation:firstNameTooLong"))
    .matches(namePattern)
    .bail()
    .withMessage(translateValidationMessage("validation:patternFirstName")),

  check("lastName")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingLastName"))
    .isLength({ max: lastNameMaxLength })
    .bail()
    .withMessage(translateValidationMessage("validation:lastNameTooLong"))
    .matches(namePattern)
    .bail()
    .withMessage(translateValidationMessage("validation:patternLastName")),
];

export { validate, namePattern, firstNameMaxLength, lastNameMaxLength };
