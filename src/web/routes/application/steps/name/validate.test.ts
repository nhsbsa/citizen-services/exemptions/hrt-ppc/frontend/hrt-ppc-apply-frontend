import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend";
import expect from "expect";
import { validate } from "./validate";
import { FieldValidationError } from "express-validator";

test("validation middleware errors for firsrName field is empty", async () => {
  const req = {
    body: {
      firstName: "",
      lastName: "Smith",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const errorOne = result.array()[0] as FieldValidationError;

  expect(result.array().length).toBe(1);
  expect(errorOne.path).toBe("firstName");
  expect(errorOne.msg).toBe("validation:missingFirstName");
});

test("validation middleware errors for lastName field is empty", async () => {
  const req = {
    body: {
      firstName: "Lisa",
      lastName: "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const errorOne = result.array()[0] as FieldValidationError;

  expect(result.array().length).toBe(1);
  expect(errorOne.path).toBe("lastName");
  expect(errorOne.msg).toBe("validation:missingLastName");
});

test("validation middleware errors for firsrName field is invalid", async () => {
  const req = {
    body: {
      firstName: "L1$a",
      lastName: "Smith",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const errorOne = result.array()[0] as FieldValidationError;

  expect(result.array().length).toBe(1);
  expect(errorOne.path).toBe("firstName");
  expect(errorOne.msg).toBe("validation:patternFirstName");
});

test("validation middleware errors for lastName field is invalid", async () => {
  const req = {
    body: {
      firstName: "Lisa",
      lastName: "$m1th",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const errorOne = result.array()[0] as FieldValidationError;

  expect(result.array().length).toBe(1);
  expect(errorOne.path).toBe("lastName");
  expect(errorOne.msg).toBe("validation:patternLastName");
});

test("validation middleware errors for firstName field exceeds min length", async () => {
  const req = {
    body: {
      firstName:
        "DavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavid",
      lastName: "Smith",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const errorOne = result.array()[0] as FieldValidationError;

  expect(result.array().length).toBe(1);
  expect(errorOne.path).toBe("firstName");
  expect(errorOne.msg).toBe("validation:firstNameTooLong");
});

test("validation middleware errors for lastName field exceeds min length", async () => {
  const req = {
    body: {
      firstName: "David",
      lastName:
        "CamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonC",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const errorOne = result.array()[0] as FieldValidationError;

  expect(result.array().length).toBe(1);
  expect(errorOne.path).toBe("lastName");
  expect(errorOne.msg).toBe("validation:lastNameTooLong");
});

test("No validation error when firstName and lastName fields are valid", async () => {
  const req = {
    body: {
      firstName: "David",
      lastName: "Smith",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.array().length).toBe(0);
});
