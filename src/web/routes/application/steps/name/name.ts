import { DEFAULT_LIST } from "../check-your-answers/constants";
import { validate } from "./validate";

const pageContent = ({ translate, req }) => ({
  title: translate("name.title"),
  heading: translate("name.heading"),
  paragraphOne: translate("name.paragraphOne"),
  firstNameLabel: translate("name.firstNameLabel"),
  lastNameLabel: translate("name.lastNameLabel"),
  buttonText: translate("buttons:continue"),
  cancel: req.t("cancel"),
});

const nameContentSummaryId = "#first-name";

const contentSummary = (req) => ({
  id: nameContentSummaryId,
  key: req.t("name.summaryKey"),
  section: DEFAULT_LIST,
  value: `${req.session.claim.firstName} ${req.session.claim.lastName}`.trim(),
  visuallyHidden: req.t("name.hiddenChangeText"),
});

const requestBody = (session) => ({
  firstName: session.claim.firstName,
  lastName: session.claim.lastName,
});

const name = {
  path: "/name",
  template: "name",
  validate,
  pageContent,
  contentSummary,
  requestBody,
};

export { contentSummary, requestBody, name, nameContentSummaryId };
