import expect from "expect";
import {
  pageContent,
  isNavigable,
  behaviourForGet,
  medicineNotCovered,
} from "./medicine-not-covered";

const translate = jest.fn();

test("pageContent() invokes translate function with expected outcomes", () => {
  const result = pageContent({ translate });

  expect(result).toBeTruthy();
  expect(translate.mock.calls.length).toBe(6);
});

test("isNavigable() returns true when medicine is not covered by HRT PPC", () => {
  const session = {
    claim: {
      isMedicineCovered: "no",
    },
  };
  const result = isNavigable(undefined, session);

  expect(result).toBe(true);
});

test("isNavigable() returns false when medicine is covered by HRT PPC", () => {
  const session = {
    claim: {
      isMedicineCovered: "yes",
    },
  };
  const result = isNavigable(undefined, session);

  expect(result).toBe(false);
});

test("pageContent() should match expected translated values", () => {
  const translate = (string, object?) => `${string}${object}`;
  const expected = {
    title: translate("medicineNotCovered.title"),
    heading: translate("medicineNotCovered.heading"),
    paragraphOne: translate("medicineNotCovered.paragraphOne"),
    paragraphTwo: translate("medicineNotCovered.paragraphTwo"),
    paragraphTwoLinkText: translate("medicineNotCovered.paragraphTwoLinkText"),
    paragraphTwoPartTwo: translate("medicineNotCovered.paragraphTwoPartTwo"),
    previous: false,
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

test("medicineNotCovered should match expected outcomes", () => {
  const expectedResults = {
    path: "/medicine-not-covered",
    template: "medicine-not-covered",
    pageContent,
    behaviourForGet,
    isNavigable,
  };

  expect(medicineNotCovered).toEqual(expectedResults);
});
