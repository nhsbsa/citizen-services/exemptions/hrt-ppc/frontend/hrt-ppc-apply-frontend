import { path } from "ramda";
import { behaviourForGet } from "./behaviour-for-get";
import { NO } from "../common/constants";

const pageContent = ({ translate }) => ({
  title: translate("medicineNotCovered.title"),
  heading: translate("medicineNotCovered.heading"),
  paragraphOne: translate("medicineNotCovered.paragraphOne"),
  paragraphTwo: translate("medicineNotCovered.paragraphTwo"),
  paragraphTwoLinkText: translate("medicineNotCovered.paragraphTwoLinkText"),
  paragraphTwoPartTwo: translate("medicineNotCovered.paragraphTwoPartTwo"),
  previous: false,
});

const isNavigable = (req, session) =>
  path(["claim", "doYouStillWantToBuySomeMedicineNotCovered"], session) == NO ||
  path(["claim", "isMedicineCovered"], session) == NO;

const medicineNotCovered = {
  path: "/medicine-not-covered",
  template: "medicine-not-covered",
  pageContent,
  behaviourForGet,
  isNavigable,
};

export { medicineNotCovered, pageContent, isNavigable, behaviourForGet };
