import expect from "expect";
import { isNavigable } from "./outside-england";

test(`isNavigable() returns true if doYouStillWantToBuyCountry is selected as No`, () => {
  const session = {
    claim: {
      doYouStillWantToBuyCountry: "no",
    },
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(true);
});
