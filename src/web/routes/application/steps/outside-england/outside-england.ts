import { path } from "ramda";
import { NO } from "../common/constants";
import { behaviourForGet } from "./behaviour-for-get";

const pageContent = ({ translate }) => ({
  title: translate("outsideEngland.title"),
  heading: translate("outsideEngland.heading"),
  paragraphOne: translate("outsideEngland.paragraphOne"),
  previous: false,
});

const isNavigable = (req, session) =>
  path(["claim", "doYouStillWantToBuyCountry"], session) == NO;

const outsideEngland = {
  path: "/outside-england",
  template: "outside-england",
  isNavigable,
  pageContent,
  behaviourForGet,
};

export { outsideEngland, pageContent, isNavigable };
