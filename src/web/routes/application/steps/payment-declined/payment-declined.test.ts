import expect from "expect";
import { renderContent, registerPaymentDeclined } from "./payment-declined";
import { handleStateDestroySession } from "../../flow-control/middleware/handle-get";

jest.mock("../../flow-control/middleware/handle-get");

test(`registerPaymentDeclined() should register page route`, () => {
  const app = {
    ...jest.requireActual("express"),
    route: jest.fn(),
    get: jest.fn(),
    post: jest.fn(),
  };

  const journey = {
    name: "apply",
    pathPrefix: "/test-context",
    steps: [{ path: "/first", next: () => "/second" }, { path: "/second" }],
  };

  registerPaymentDeclined(journey, app);

  expect(app.route).toBeCalledTimes(0);
  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toHaveBeenNthCalledWith(
    1,
    "/test-context/payment-declined",
    undefined,
    renderContent,
  );
  expect(app.post).toBeCalledTimes(0);
  expect(handleStateDestroySession).toBeCalledTimes(1);
  expect(handleStateDestroySession).toHaveBeenNthCalledWith(1, journey);
});

test(`renderContent() should render the page with page translation values`, () => {
  const req = { t: (string, object?) => `${string}${JSON.stringify(object)}` };
  const res = { render: jest.fn() };
  const translate = req.t;
  const expectedPaymentDeclinedTranslation = {
    title: translate("paymentDeclined.title"),
    heading: translate("paymentDeclined.heading"),
    paragraphOne: translate("paymentDeclined.paragraphOne"),
    paragraphTwo: translate("paymentDeclined.paragraphTwo"),
    paragraphThree: translate("paymentDeclined.paragraphThree"),
    paragraphFour: translate("paymentDeclined.paragraphFour"),
    previous: false,
  };

  renderContent(req, res);
  expect(res.render).toBeCalledTimes(1);
  expect(res.render).toBeCalledWith(
    "payment-declined",
    expectedPaymentDeclinedTranslation,
  );
});
