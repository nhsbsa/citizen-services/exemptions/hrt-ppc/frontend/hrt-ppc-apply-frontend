import { handleStateDestroySession } from "../../flow-control/middleware/handle-get";
import { PAYMENT_DECLINED_URL } from "../../paths/paths";
import { prefixPath } from "../../paths/prefix-path";

const pageContent = (translate) => ({
  title: translate("paymentDeclined.title"),
  heading: translate("paymentDeclined.heading"),
  paragraphOne: translate("paymentDeclined.paragraphOne"),
  paragraphTwo: translate("paymentDeclined.paragraphTwo"),
  paragraphThree: translate("paymentDeclined.paragraphThree"),
  paragraphFour: translate("paymentDeclined.paragraphFour"),
  previous: false,
});

const renderContent = (req, res) => {
  return res.render("payment-declined", {
    ...pageContent(req.t),
  });
};

const registerPaymentDeclined = (journey, app) => {
  app.get(
    prefixPath(journey.pathPrefix, PAYMENT_DECLINED_URL),
    handleStateDestroySession(journey),
    renderContent,
  );
};

export { registerPaymentDeclined, renderContent };
