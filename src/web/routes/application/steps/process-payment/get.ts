import * as httpStatus from "http-status-codes";
import jwt from "jsonwebtoken";
import { stateMachine } from "../../flow-control/state-machine";
import * as states from "../../flow-control/states";
import { isNilOrEmpty } from "../../../../../common/predicates";
import { wrapError } from "../../errors";
import { logger } from "../../../../logger/logger";
import { RetrievePaymentResponse } from "../../../../client/cps-payment-types";
import { AxiosResponse } from "axios";
import { cardPaymentsClient } from "../../../../client/cps-client";
import { CpsStatus } from "../../../../client/cps-payment-status";
import { contextPath } from "../../paths/context-path";
import {
  APPLICATION_COMPLETE_URL,
  PAYMENT_CANCELLED_URL,
  PAYMENT_DECLINED_URL,
} from "../../paths/paths";

const getProcessPayment = (journey, config) => async (req, res, next) => {
  const token = req.query.token;
  try {
    if (isNilOrEmpty(token)) {
      throw new Error(`The token is empty`);
      return;
    }
    if (!req.session || !req.session.transactionId) {
      // Verify ignoring expiry because we just want to log the encoded body
      const decoded = jwt.verify(
        token,
        config.environment.CARD_PAYMENTS_JWT_SECRET,
        {
          ignoreExpiration: true,
        },
      );
      throw new Error(
        `Session has expired or 'transactionId' not found in session, JWT decoded: ${JSON.stringify(
          decoded,
        )}`,
      );
      return;
    }

    const transactionId = req.session.transactionId;
    const { certificateId, citizenId, certificateReference } =
      req.session.issuedCertificateDetails;

    logger.info(
      `Processing payment return for certificateId: ${certificateId}, citizenId: ${citizenId}, certificateReference: ${certificateReference}, transactionId: ${transactionId}`,
      req,
    );

    // The .verify will return the decoded value or throw an error i.e. if its expired
    const decoded = jwt.verify(
      token,
      config.environment.CARD_PAYMENTS_JWT_SECRET,
    );
    const decodedTransactionId = decoded.transactionId;

    // Additional safety check if user has managed to get someone elses JWT
    if (transactionId !== decodedTransactionId) {
      const err = Error(
        `The transactionId in the session ${transactionId} does not match with decoded transactionId in JWT ${decodedTransactionId}`,
      );
      throw err;
      return;
    }

    const response: AxiosResponse<RetrievePaymentResponse, any> =
      await new cardPaymentsClient(config).makeRequestRetrievePayment({
        method: "GET",
        headers: setHeadersIfMockEnabled(config, req),
        url: `/payments/${transactionId}`,
      });

    logger.info(
      `Payment status is: [${response.data.status}] for transactionId: [${transactionId}]`,
      req,
    );

    switch (response.data.status) {
      case CpsStatus.SUCCESS:
        stateMachine.setState(states.COMPLETED, req, journey);
        res.redirect(contextPath(APPLICATION_COMPLETE_URL));
        break;
      case CpsStatus.FAILED_PAYMENT_METHOD_REJECTED:
      case CpsStatus.FAILED_PAYMENT_EXPIRED:
      case CpsStatus.ERROR:
        res.redirect(contextPath(PAYMENT_DECLINED_URL));
        break;
      case CpsStatus.FAILED_PAYMENT_CANCELLED_BY_USER:
      case CpsStatus.CANCELLED:
        res.redirect(contextPath(PAYMENT_CANCELLED_URL));
        break;
      case CpsStatus.ERROR_PAYMENT_NOT_FOUND:
      case CpsStatus.ERROR_PAYMENT_NOT_FINISHED:
      case CpsStatus.UNKNOWN:
      default:
        res.redirect(contextPath(`/problem-with-service`));
        break;
    }
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      }),
    );
  }
};

/** The headers are set only when mock is enabled as we need to
 * simulate different status's, we are using firstName for this
 *
 * @param config the app config
 * @param req the request
 * @returns the headers or null
 */
const setHeadersIfMockEnabled = (config, req) => {
  if (config.environment.CARD_PAYMENTS_USE_MOCK === true) {
    return { firstName: req.query.firstName };
  }
  return {};
};

export { getProcessPayment };
