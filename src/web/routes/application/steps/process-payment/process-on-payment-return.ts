import { prefixPath } from "../../paths/prefix-path";
import { PROCESS_PAYMENT_RETURN_URL } from "../../paths/paths";
import { getProcessPayment } from "./get";

// this url is passed to gov.pay and is called back with the status of the payment
const registerProcessPaymentReturnRoute = (journey, app, config) => {
  app
    .route(prefixPath(journey.pathPrefix, PROCESS_PAYMENT_RETURN_URL))
    .get(getProcessPayment(journey, config));
};

export { registerProcessPaymentReturnRoute };
