import { registerProcessPaymentReturnRoute } from "./process-on-payment-return";

const mockGet = jest.fn();
const mockGetProcessPayment = jest.fn();
const app = {
  route: jest.fn(() => ({
    get: mockGet,
  })),
};
jest.mock("./get", () => ({
  getProcessPayment: () => mockGetProcessPayment,
}));
const config = jest.fn();
describe("registerProcessPaymentReturnRoute() function", () => {
  test("should set the app route and get function", () => {
    const journey = {
      pathPrefix: "/prefix",
    };

    registerProcessPaymentReturnRoute(journey, app, config);
    expect(app.route).toHaveBeenCalledWith("/prefix/p");
    expect(app.route).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledWith(mockGetProcessPayment);
  });
});
