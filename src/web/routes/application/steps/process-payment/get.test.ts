import * as httpStatus from "http-status-codes";
import jwt from "jsonwebtoken";
import expect from "expect";
import { cardPaymentsClient } from "../../../../client/cps-client";

jest.mock("../../../../client/cps-client");
const makeRequestRetrievePaymentMock = jest.fn();
const mockJwtVerify = jest.spyOn(jwt, "verify");

import { getProcessPayment } from "./get";

import { states, testUtils } from "../../flow-control";
import { wrapError } from "../../errors";
import { CpsStatus } from "../../../../client/cps-payment-status";
import { IN_REVIEW } from "../../flow-control/states";
import { stateMachine } from "../../flow-control/state-machine";
const { buildSessionForJourney } = testUtils;

const APPLY = "apply";
const journey = {
  name: APPLY,
  steps: [{ path: "/first", next: () => "/second" }, { path: "/second" }],
};

const jwtSecret = process.env.CARD_PAYMENTS_JWT_SECRET;
const transactionId = "ABCD12345";
const signedToken = jwt.sign({ transactionId: transactionId }, jwtSecret, {
  expiresIn: "10m",
});
const decoded = {
  transactionId: transactionId,
  iat: 1674211651,
  exp: 1674212251,
};

const res = { redirect: jest.fn() };
const next = jest.fn();
let config;

beforeAll(() => {
  cardPaymentsClient.prototype.makeRequestRetrievePayment =
    makeRequestRetrievePaymentMock;
});

beforeEach(() => {
  jest.clearAllMocks();
  mockJwtVerify.mockReturnValue(decoded);
  config = {
    environment: {
      CARD_PAYMENTS_JWT_SECRET: "test-secret",
      CARD_PAYMENTS_USE_MOCK: false,
    },
  };
});

describe("getProcessPayment()", () => {
  test("calls next() with wrapError() if the token is not defined in query parameter", async () => {
    const req = { path: "/first", session: {}, query: {} };

    const result = await getProcessPayment(journey, config)(req, res, next);

    const error = wrapError({
      cause: new Error(`The token is empty`),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    });
    expect(result).toBeUndefined();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(error);
    expect(res.redirect).toBeCalledTimes(0);
    expect(req.session).toEqual({});
    expect(mockJwtVerify).toBeCalledTimes(0);
    expect(makeRequestRetrievePaymentMock).toBeCalledTimes(0);
  });

  test("calls next() with wrapError() if the token is empty in query parameter", async () => {
    const req = { path: "/first", session: {}, query: { token: "" } };

    const result = await getProcessPayment(journey, config)(req, res, next);

    const error = wrapError({
      cause: new Error(`The token is empty`),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    });
    expect(result).toBeUndefined();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(error);
    expect(res.redirect).toBeCalledTimes(0);
    expect(req.session).toEqual({});
    expect(mockJwtVerify).toBeCalledTimes(0);
    expect(makeRequestRetrievePaymentMock).toBeCalledTimes(0);
  });

  test("calls next() with wrapError() if the session does not exist", async () => {
    const req = {
      path: "/first",
      session: undefined,
      query: { token: signedToken },
    };

    const result = await getProcessPayment(journey, config)(req, res, next);

    const error = wrapError({
      cause: new Error(
        `Session has expired or 'transactionId' not found in session, JWT decoded: ${JSON.stringify(
          decoded,
        )}`,
      ),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    });
    expect(result).toBeUndefined();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(error);
    expect(res.redirect).toBeCalledTimes(0);
    expect(req.session).toBeUndefined();
    expect(mockJwtVerify).toBeCalledTimes(1);
    expect(makeRequestRetrievePaymentMock).toBeCalledTimes(0);
    expect(mockJwtVerify).toHaveBeenCalledWith(signedToken, jwtSecret, {
      ignoreExpiration: true,
    });
  });

  test("calls next() with wrapError() if the session does not have `transactionId`", async () => {
    const req = { path: "/first", session: {}, query: { token: signedToken } };

    const result = await getProcessPayment(journey, config)(req, res, next);

    const error = wrapError({
      cause: new Error(
        `Session has expired or 'transactionId' not found in session, JWT decoded: ${JSON.stringify(
          decoded,
        )}`,
      ),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    });
    expect(result).toBeUndefined();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(error);
    expect(res.redirect).toBeCalledTimes(0);
    expect(req.session).toEqual({});
    expect(mockJwtVerify).toBeCalledTimes(1);
    expect(makeRequestRetrievePaymentMock).toBeCalledTimes(0);
    expect(mockJwtVerify).toHaveBeenCalledWith(signedToken, jwtSecret, {
      ignoreExpiration: true,
    });
  });

  test("calls next() with wrapError() if the `transactionId` in session does not match with the decoded Jwt", async () => {
    const issuedCertificateDetails = {
      certificateId: "cert-123",
      citizenId: "citizen-123",
      certificateReference: "ref123",
    };
    const session = {
      transactionId: "some-other",
      issuedCertificateDetails: issuedCertificateDetails,
    };
    const req = {
      path: "/first",
      session: session,
      query: { token: signedToken },
    };
    const expectedSession = {
      transactionId: "some-other",
      issuedCertificateDetails: issuedCertificateDetails,
    };

    const result = await getProcessPayment(journey, config)(req, res, next);

    const error = wrapError({
      cause: new Error(
        `The transactionId in the session some-other does not match with decoded transactionId in JWT ${transactionId}`,
      ),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    });

    expect(result).toBeUndefined();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(error);
    expect(res.redirect).toBeCalledTimes(0);
    expect(req.session).toEqual(expectedSession);
    expect(mockJwtVerify).toBeCalledTimes(1);
    expect(makeRequestRetrievePaymentMock).toBeCalledTimes(0);
    expect(mockJwtVerify).toHaveBeenCalledWith(signedToken, jwtSecret);
  });

  it.each([CpsStatus.SUCCESS])(
    "calls card payments client to retrieve status and redirects to `/application-complete` on %p",
    async (status: CpsStatus) => {
      makeRequestRetrievePaymentMock.mockResolvedValue({
        data: { status: status },
      });
      const issuedCertificateDetails = {
        certificateId: "cert-123",
        citizenId: "citizen-123",
        certificateReference: "ref123",
      };
      const session = {
        transactionId: transactionId,
        issuedCertificateDetails: issuedCertificateDetails,
        ...buildSessionForJourney({
          journeyName: APPLY,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      };
      const req = {
        path: "/first",
        session: session,
        query: { token: signedToken },
      };

      const result = await getProcessPayment(journey, config)(req, res, next);

      expect(result).toBeUndefined();
      expect(next).toBeCalledTimes(0);
      expect(res.redirect).toBeCalledTimes(1);
      expect(res.redirect).toBeCalledWith("/test-context/application-complete");
      expect(mockJwtVerify).toBeCalledTimes(1);
      expect(makeRequestRetrievePaymentMock).toBeCalledTimes(1);
      expect(makeRequestRetrievePaymentMock).toBeCalledWith({
        headers: {},
        method: "GET",
        url: `/payments/${transactionId}`,
      });
      expect(mockJwtVerify).toHaveBeenCalledWith(signedToken, jwtSecret);
      expect(stateMachine.getState(req, journey)).toEqual(states.COMPLETED);
    },
  );

  it.each([CpsStatus.SUCCESS])(
    "calls card payments client to retrieve status and redirects to `/application-complete` on %p with mock enabled",
    async (status: CpsStatus) => {
      makeRequestRetrievePaymentMock.mockResolvedValue({
        data: { status: status },
      });
      config.environment["CARD_PAYMENTS_USE_MOCK"] = true;
      const issuedCertificateDetails = {
        certificateId: "cert-123",
        citizenId: "citizen-123",
        certificateReference: "ref123",
      };
      const session = {
        transactionId: transactionId,
        issuedCertificateDetails: issuedCertificateDetails,
        ...buildSessionForJourney({
          journeyName: APPLY,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      };
      const req = {
        path: "/first",
        session: session,
        query: { token: signedToken, firstName: "Test" },
      };

      const result = await getProcessPayment(journey, config)(req, res, next);

      expect(result).toBeUndefined();
      expect(next).toBeCalledTimes(0);
      expect(res.redirect).toBeCalledTimes(1);
      expect(res.redirect).toBeCalledWith("/test-context/application-complete");
      expect(mockJwtVerify).toBeCalledTimes(1);
      expect(makeRequestRetrievePaymentMock).toBeCalledTimes(1);
      expect(makeRequestRetrievePaymentMock).toBeCalledWith({
        headers: { firstName: "Test" },
        method: "GET",
        url: `/payments/${transactionId}`,
      });
      expect(mockJwtVerify).toHaveBeenCalledWith(signedToken, jwtSecret);
      expect(stateMachine.getState(req, journey)).toEqual(states.COMPLETED);
    },
  );

  it.each([
    CpsStatus.FAILED_PAYMENT_METHOD_REJECTED,
    CpsStatus.FAILED_PAYMENT_EXPIRED,
    CpsStatus.ERROR,
  ])(
    "calls card payments client to retrieve status and redirects to `/payment-declined` on %p",
    async (status: CpsStatus) => {
      makeRequestRetrievePaymentMock.mockResolvedValue({
        data: { status: status },
      });
      const issuedCertificateDetails = {
        certificateId: "cert-123",
        citizenId: "citizen-123",
        certificateReference: "ref123",
      };
      const session = {
        transactionId: transactionId,
        issuedCertificateDetails: issuedCertificateDetails,
        ...buildSessionForJourney({
          journeyName: APPLY,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      };
      const req = {
        path: "/first",
        session: session,
        query: { token: signedToken },
      };

      const result = await getProcessPayment(journey, config)(req, res, next);

      expect(result).toBeUndefined();
      expect(next).toBeCalledTimes(0);
      expect(res.redirect).toBeCalledTimes(1);
      expect(res.redirect).toBeCalledWith("/test-context/payment-declined");
      expect(mockJwtVerify).toBeCalledTimes(1);
      expect(makeRequestRetrievePaymentMock).toBeCalledTimes(1);
      expect(makeRequestRetrievePaymentMock).toBeCalledWith({
        headers: {},
        method: "GET",
        url: `/payments/${transactionId}`,
      });
      expect(mockJwtVerify).toHaveBeenCalledWith(signedToken, jwtSecret);
      expect(stateMachine.getState(req, journey)).toEqual(states.IN_REVIEW);
    },
  );

  it.each([CpsStatus.FAILED_PAYMENT_CANCELLED_BY_USER, CpsStatus.CANCELLED])(
    "calls card payments client to retrieve status and redirects to `/payment-cancelled` on %p",
    async (status: CpsStatus) => {
      makeRequestRetrievePaymentMock.mockResolvedValue({
        data: { status: status },
      });
      const issuedCertificateDetails = {
        certificateId: "cert-123",
        citizenId: "citizen-123",
        certificateReference: "ref123",
      };
      const session = {
        transactionId: transactionId,
        issuedCertificateDetails: issuedCertificateDetails,
        ...buildSessionForJourney({
          journeyName: APPLY,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      };
      const req = {
        path: "/first",
        session: session,
        query: { token: signedToken },
      };

      const result = await getProcessPayment(journey, config)(req, res, next);

      expect(result).toBeUndefined();
      expect(next).toBeCalledTimes(0);
      expect(res.redirect).toBeCalledTimes(1);
      expect(res.redirect).toBeCalledWith("/test-context/payment-cancelled");
      expect(mockJwtVerify).toBeCalledTimes(1);
      expect(makeRequestRetrievePaymentMock).toBeCalledTimes(1);
      expect(makeRequestRetrievePaymentMock).toBeCalledWith({
        headers: {},
        method: "GET",
        url: `/payments/${transactionId}`,
      });
      expect(mockJwtVerify).toHaveBeenCalledWith(signedToken, jwtSecret);
      expect(stateMachine.getState(req, journey)).toEqual(states.IN_REVIEW);
    },
  );

  it.each([
    CpsStatus.ERROR_PAYMENT_NOT_FOUND,
    CpsStatus.ERROR_PAYMENT_NOT_FINISHED,
    CpsStatus.UNKNOWN,
  ])(
    "calls card payments client to retrieve status and redirects to `/problem-with-service` on %p",
    async (status: CpsStatus) => {
      makeRequestRetrievePaymentMock.mockResolvedValue({
        data: { status: status },
      });
      const issuedCertificateDetails = {
        certificateId: "cert-123",
        citizenId: "citizen-123",
        certificateReference: "ref123",
      };
      const session = {
        transactionId: transactionId,
        issuedCertificateDetails: issuedCertificateDetails,
        ...buildSessionForJourney({
          journeyName: APPLY,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      };
      const req = {
        path: "/first",
        session: session,
        query: { token: signedToken },
      };

      const result = await getProcessPayment(journey, config)(req, res, next);

      expect(result).toBeUndefined();
      expect(next).toBeCalledTimes(0);
      expect(res.redirect).toBeCalledTimes(1);
      expect(res.redirect).toBeCalledWith("/test-context/problem-with-service");
      expect(mockJwtVerify).toBeCalledTimes(1);
      expect(makeRequestRetrievePaymentMock).toBeCalledTimes(1);
      expect(makeRequestRetrievePaymentMock).toBeCalledWith({
        headers: {},
        method: "GET",
        url: `/payments/${transactionId}`,
      });
      expect(mockJwtVerify).toHaveBeenCalledWith(signedToken, jwtSecret);
      expect(stateMachine.getState(req, journey)).toEqual(states.IN_REVIEW);
    },
  );
});
