import { path } from "ramda";
import { behaviourForGet } from "./behaviour-for-get";

const pageContent = ({ translate }) => ({
  title: translate("existingExemptionFound.title"),
  heading: translate("existingExemptionFound.heading"),
  paragraphOne: translate("existingExemptionFound.paragraphOne"),
  paragraphTwo: translate("existingExemptionFound.paragraphTwo"),
  paragraphTwoLinkText: translate(
    "existingExemptionFound.paragraphTwoLinkText",
  ),
  paragraphTwoPartTwo: translate("existingExemptionFound.paragraphTwoPartTwo"),
  paragraphThree: translate("existingExemptionFound.paragraphThree"),
  paragraphThreeLinkText: translate(
    "existingExemptionFound.paragraphThreeLinkText",
  ),
  previous: false,
});

const isNavigable = (req, session) =>
  path(["existingExemptionFound"], session) === true;

const existingExemptionFound = {
  path: "/existing-exemption-found",
  template: "existing-certificate-found",
  pageContent,
  behaviourForGet,
  isNavigable,
};

export { existingExemptionFound, pageContent, isNavigable, behaviourForGet };
