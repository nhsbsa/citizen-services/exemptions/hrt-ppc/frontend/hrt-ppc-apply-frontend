import expect from "expect";
import {
  pageContent,
  isNavigable,
  behaviourForGet,
  existingExemptionFound,
} from "./existing-exemption-found";

const translate = jest.fn();

test("pageContent() is called when translate function invoked", () => {
  const result = pageContent({ translate });
  expect(result).toBeTruthy();
  expect(translate.mock.calls.length).toBe(8);
});

test(`isNavigable() returns true if existingExemptionFound true`, () => {
  const session = {
    existingExemptionFound: true,
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(true);
});

test(`isNavigable() returns false if existingExemptionFound false`, () => {
  const session = {
    existingExemptionFound: false,
  };

  const result = isNavigable(undefined, session);
  expect(result).toBe(false);
});

test("pageContent() should match expected translated values", () => {
  const translate = (string, object?) => `${string}${object}`;
  const result = pageContent({ translate });

  const expected = {
    title: translate("existingExemptionFound.title"),
    heading: translate("existingExemptionFound.heading"),
    paragraphOne: translate("existingExemptionFound.paragraphOne"),
    paragraphTwo: translate("existingExemptionFound.paragraphTwo"),
    paragraphTwoLinkText: translate(
      "existingExemptionFound.paragraphTwoLinkText",
    ),
    paragraphTwoPartTwo: translate(
      "existingExemptionFound.paragraphTwoPartTwo",
    ),
    paragraphThree: translate("existingExemptionFound.paragraphThree"),
    paragraphThreeLinkText: translate(
      "existingExemptionFound.paragraphThreeLinkText",
    ),
    previous: false,
  };

  expect(result).toEqual(expected);
});

test(`existingExemptionFound should match expected outcomes`, () => {
  const expectedResults = {
    path: "/existing-exemption-found",
    template: "existing-certificate-found",
    pageContent,
    behaviourForGet,
    isNavigable,
  };
  expect(existingExemptionFound).toEqual(expectedResults);
});
