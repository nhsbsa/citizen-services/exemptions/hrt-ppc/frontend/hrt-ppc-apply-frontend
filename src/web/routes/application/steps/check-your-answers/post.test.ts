const mockMakeRequest = jest.fn();
const mockMakeRequestToCardPaymentService = jest.fn();

import expect from "expect";
import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";
import { stateMachine } from "../../flow-control/state-machine";
import { states } from "../../flow-control";
import { config } from "../../../../../config";
import { REQUEST_ID_HEADER } from "../../../../server/headers";
import * as exemptionsIssueCertificate from "../../../../../__mocks__/api-responses/issue-certificate-api/exemption-hrt-issue-certificate-lambda.json";
import * as exemptionsPayment from "../../../../../__mocks__/api-responses/payment-api/exemption-payment.json";
import { issueCertificateClient } from "../../../../client/issue-certificate-client";
import { paymentClient } from "../../../../client/payment-client";
import * as createRequestBody from "./create-request-body";
import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { IN_REVIEW } from "../../flow-control/states";
import { postCheckAnswers } from "./post";
import {
  CertificateType,
  Duration,
  PaymentMethod,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import moment from "moment";

const res = {
  redirect: jest.fn(),
};
const next = jest.fn();

jest.mock("../../../../client/issue-certificate-client");
jest.mock("../../../../client/payment-client");
jest.mock("./create-payment-redirect", () => ({
  makeRequestToCardPaymentService: mockMakeRequestToCardPaymentService,
}));
jest.mock("moment", () => {
  const mockMoment = jest.fn(() => ({
    toDate: jest.fn(() => new Date("2023-01-19")),
  }));

  return mockMoment;
});

const mockResponseBody = jest
  .spyOn(createRequestBody, "createRequestBody")
  .mockReturnValue({
    citizen: {
      emails: [
        {
          emailAddress: "test@example.com",
        },
      ],
      addresses: [
        {
          addressLine1: "Stella House",
          addressLine2: "Goldcrest Way",
          townOrCity: "Newcastle upon Tyne",
          postcode: "NE15 8NY",
        },
      ],
      firstName: "Fname",
      lastName: "Lname",
      dateOfBirth: "2022-01-01",
      nhsNumber: "9999999999",
    },
    certificate: {
      startDate: "2023-04-01",
      endDate: "2024-04-01",
      type: CertificateType.HRT_PPC,
      duration: Duration.P12M,
      cost: 1570,
      applicationDate: "2023-01-19",
    },
  });

const mockPaymentResponseBody = jest
  .spyOn(createRequestBody, "createPaymentRequestBody")
  .mockReturnValue({
    amount: "1570",
    date: moment("2023-01-19T00:00:00.000Z").toDate(),
    method: PaymentMethod.CARD,
  });

const journey = {
  name: "apply",
};
const req = {
  path: "/test",
  session: {
    ...buildSessionForJourney({
      journeyName: "apply",
      state: states.IN_REVIEW,
      nextAllowedPath: "/next-path",
    }),
  },
  headers: {
    [REQUEST_ID_HEADER]: "123456",
  },
};

const expectedError = wrapError({
  cause: new Error("Axios error"),
  message: `Error posting ${req.path}`,
  statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
});

const makePaymentRequestMock = jest.fn();

const expectedRequestBody = {
  citizen: {
    emails: [
      {
        emailAddress: "test@example.com",
      },
    ],
    addresses: [
      {
        addressLine1: "Stella House",
        addressLine2: "Goldcrest Way",
        townOrCity: "Newcastle upon Tyne",
        postcode: "NE15 8NY",
      },
    ],
    firstName: "Fname",
    lastName: "Lname",
    dateOfBirth: "2022-01-01",
    nhsNumber: "9999999999",
  },
  certificate: {
    startDate: "2023-04-01",
    endDate: "2024-04-01",
    type: CertificateType.HRT_PPC,
    duration: Duration.P12M,
    cost: 1570,
    applicationDate: "2023-01-19",
  },
};
const expectedMockRequestCall = {
  method: "POST",
  url: "/v1/issue-certificate",
  data: expectedRequestBody,
  responseType: "json",
};

const expectedPaymentRequestBody = {
  amount: "1570",
  date: moment("2023-01-19T00:00:00.000Z").toDate(),
  method: PaymentMethod.CARD,
};
const expectedPaymentMockRequestCall = {
  method: "POST",
  url: "/v1/payments?certificateId=c0a81da7-85b5-18cc-8185-b7253fb6001a",
  data: expectedPaymentRequestBody,
  responseType: "json",
};

beforeEach(() => {
  jest.clearAllMocks();
  issueCertificateClient.prototype.makeRequest = mockMakeRequest;
  mockMakeRequestToCardPaymentService.mockResolvedValue({});
  req.session["issuedCertificateDetails"] = undefined;
  paymentClient.prototype.makeRequest = makePaymentRequestMock;
  req.session["paymentDetails"] = undefined;
  req.session.journeys["apply"].state = IN_REVIEW;
});

describe("postCheckAnswers()", () => {
  test("sets issuedCertificateDetails and paymentDetails in session and calls card payment service to create payment", async () => {
    mockMakeRequest.mockReturnValue(
      Promise.resolve({ data: exemptionsIssueCertificate }),
    );
    makePaymentRequestMock.mockReturnValue(
      Promise.resolve({ data: exemptionsPayment }),
    );

    await postCheckAnswers(config)(req, res, next);

    const transactionId = "906944761072A2088F71";
    expect(req.session["issuedCertificateDetails"]).toEqual({
      certificateId: "c0a81da7-85b5-18cc-8185-b7253fb6001a",
      certificateReference: "HRTE3542EAA",
      citizenId: "c0a81da7-85b5-1816-8185-b7253f720036",
      certificateStartDate: "2023-01-17",
      certificateEndDate: "2024-01-17",
    });
    expect(req.session["paymentDetails"]).toEqual({
      paymentId: "c0a80196-85cf-1c6d-8185-cf8cf96a0006",
      transactionId: transactionId,
    });
    expect(stateMachine.getState(req, journey)).toEqual(states.IN_REVIEW);
    expect(res.redirect).toBeCalledTimes(0);
    expect(issueCertificateClient).toBeCalledTimes(1);
    expect(issueCertificateClient).toBeCalledWith(config);
    expect(issueCertificateClient.prototype.makeRequest).toBeCalledTimes(1);
    expect(issueCertificateClient.prototype.makeRequest).toBeCalledWith(
      expectedMockRequestCall,
      req,
    );
    expect(mockResponseBody).toBeCalledTimes(1);
    expect(mockResponseBody).toHaveBeenCalledWith(config, req.session);
    expect(paymentClient).toBeCalledTimes(1);
    expect(paymentClient).toBeCalledWith(config);
    expect(paymentClient.prototype.makeRequest).toBeCalledTimes(1);
    expect(paymentClient.prototype.makeRequest).toBeCalledWith(
      expectedPaymentMockRequestCall,
      req,
    );
    expect(mockPaymentResponseBody).toBeCalledTimes(1);
    expect(mockPaymentResponseBody).toHaveBeenCalledWith(config);
    expect(mockMakeRequestToCardPaymentService).toBeCalledTimes(1);
    expect(mockMakeRequestToCardPaymentService).toBeCalledWith(
      config,
      transactionId,
    );
  });

  test("calls next with wrapped error when postCheckAnswers throws error and STATE remains IN_REVIEW", async () => {
    mockMakeRequest.mockReturnValue(Promise.reject(new Error("Axios error")));

    await postCheckAnswers(config)(req, res, next);

    expect(req.session["issuedCertificateDetails"]).toBeUndefined();
    expect(req.session["paymentDetails"]).toBeUndefined();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(expectedError);
    expect(res.redirect).toBeCalledTimes(0);
    expect(req.session["issuedCertificateDetails"]).toBe(undefined);
    expect(req.session["paymentDetails"]).toBe(undefined);
    expect(stateMachine.getState(req, journey)).toEqual(states.IN_REVIEW);
    expect(mockMakeRequestToCardPaymentService).toBeCalledTimes(0);
    expect(paymentClient).toBeCalledTimes(0);
  });
});
