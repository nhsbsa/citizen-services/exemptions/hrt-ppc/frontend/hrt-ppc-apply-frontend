import { stateMachine } from "../../flow-control/state-machine";
import * as states from "../../flow-control/states";
import * as actions from "../../flow-control/state-machine/actions";
import { getPreviousPath } from "../../flow-control/get-previous-path";
import { getAboutYouSummaryListsForSteps } from "./get-row-data";
import { DEFAULT_LIST } from "./constants";
import { getCertificateDetailsRows } from "./get-certificate-detail-rows";
import { config } from "../../../../../config";
import { toPounds } from "../../../../../common/currency";
const { INCREMENT_NEXT_ALLOWED_PATH } = actions;
const { IN_REVIEW } = states;

const pageContent = ({ translate }) => ({
  title: translate("checkAnswers.title"),
  heading: translate("checkAnswers.heading"),
  sendApplicationHeader: translate("checkAnswers.sendApplicationHeader"),
  sendApplicationText: translate("checkAnswers.sendApplicationText"),
  buttonText: translate("buttons:payment"),
  changeText: translate("change"),
  summaryListHeadings: {
    aboutYou: translate("checkAnswers.aboutYou"),
    aboutCertificate: translate("checkAnswers.aboutCertificate"),
  },
  price: toPounds(config.environment.HRT_PPC_VALUE),
  name: translate("checkAnswers.name"),
  dateOfBirth: translate("checkAnswers.dateOfBirth"),
  warningCalloutHeading: translate("checkAnswers.warningCalloutHeading"),
  warningCalloutHTML: translate("checkAnswers.warningCalloutHTML"),
  paymentInfoParagraph: translate("checkAnswers.paymentInfoParagraph"),
  paymentInfoCardText: translate("checkAnswers.paymentInfoCardText"),
  paymentWarningText: translate("checkAnswers.paymentWarningText"),
  paymentWarningIconText: translate("checkAnswers.paymentWarningIconText"),
  paymentInfoAppleAndGooglePayText: translate(
    "checkAnswers.paymentInfoAppleAndGooglePayText",
  ),
  length: translate("checkAnswers.length"),
  startsOn: translate("checkAnswers.startsOn"),
  endsOn: translate("checkAnswers.endsOn"),
  totalCost: translate("checkAnswers.totalCost"),
  paymentServiceLink: translate("checkAnswers.paymentServiceLink"),
  paymentServiceLinkText: translate("checkAnswers.paymentServiceLinkText"),
  cancel: translate("cancel"),
});

// a step is navigable if it hasn't defined an isNavigable function.
const stepIsNavigable = (step, req, session) =>
  !step.hasOwnProperty("isNavigable") || step.isNavigable(req, session);

const getLastNavigablePath = (steps, req) => {
  const lastStep = steps[steps.length - 1];

  return stepIsNavigable(lastStep, req, req.session)
    ? lastStep.path
    : getPreviousPath(steps, lastStep, req.session);
};

const getCheckAnswers = (journey) => (req, res) => {
  const localisation = pageContent({ translate: req.t });
  const { steps } = journey;
  const certificateDetailsRowList = getCertificateDetailsRows(
    req,
    localisation,
  );

  stateMachine.setState(IN_REVIEW, req, journey);
  stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);

  res.render("check-your-answers", {
    ...localisation,
    claimAboutYouSummaryLists: getAboutYouSummaryListsForSteps({
      req,
      steps,
      section: DEFAULT_LIST,
    }),
    claimAboutCertificateRows: certificateDetailsRowList(),
    previous: getLastNavigablePath(steps, req),
    csrfToken: req.session ? req.csrfToken() : null,
  });
};

export { getCheckAnswers, getLastNavigablePath, pageContent };
