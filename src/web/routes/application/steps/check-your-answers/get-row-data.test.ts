import expect from "expect";
import {
  getContentSummary,
  groupByListAboutYou,
  normaliseRows,
  getAboutYouSummaryListsForSteps,
  getSummaryListRows,
} from "./get-row-data";
import { DEFAULT_LIST } from "./constants";
import { nameContentSummaryId } from "../name/name";
import { dateOfBirthDateContentSummaryId } from "../date-of-birth/date-of-birth";
import { nhsNumberContentSummaryId } from "../do-you-know-your-nhs-number/do-you-know-your-nhs-number";
import { addressContentSummaryId } from "../address/content-summary";
import { emailIdContentSummaryId } from "../email-address/email-address";

const sectionAboutYou = "aboutYou";

test("getContentSummary should return an array of object combining path with row data", () => {
  const step = {
    contentSummary: () => ({
      key: "myKey",
      value: "myValue",
      section: sectionAboutYou,
    }),
    path: "mypath",
  };
  const req = {};

  expect(getContentSummary(req, sectionAboutYou)(step)).toEqual([
    {
      key: "myKey",
      value: "myValue",
      path: "mypath",
      section: sectionAboutYou,
    },
  ]);
});

test("getContentSummary should return blank Array if section is not matching", () => {
  const step = {
    contentSummary: () => ({
      key: "myKey",
      value: "myValue",
      section: sectionAboutYou,
    }),
    path: "mypath",
  };
  const req = {};
  const section = "sectionThree";

  expect(getContentSummary(req, section)(step)).toEqual([]);
});

test("getContentSummary should return null when step contentSummary() returns null", () => {
  const step = {
    contentSummary: () => null,
  };
  const req = {};

  expect(getContentSummary(req, step)(step)).toEqual(null);
});

test("getSummaryListRows should return an array of objects combining path with row data", () => {
  const steps = [
    {
      contentSummary: () => ({
        key: "myKey",
        value: "myValue",
        section: "sectionOne",
      }),
      path: "mypath",
    },
    {
      contentSummary: () => ({
        key: "myKey",
        value: "myValue",
        section: "sectionTwo",
      }),
      path: "mypath",
    },
  ];

  const req = {};
  const section = "sectionOne";

  expect(getSummaryListRows({ req, steps, section })).toEqual([
    [{ key: "myKey", path: "mypath", section: "sectionOne", value: "myValue" }],
    [],
  ]);
});

test("normaliseRows() flattens nested iterables and removes empty values", () => {
  const rows = [
    {
      key: "email address",
      value: "my@email.com",
    },
    [
      {
        key: "addressline1",
        value: "10 Downing Street",
      },
      {
        key: "addressline2",
        value: "Springfield",
      },
    ],
    null,
  ];

  const expected = [
    {
      key: "email address",
      value: "my@email.com",
    },
    {
      key: "addressline1",
      value: "10 Downing Street",
    },
    {
      key: "addressline2",
      value: "Springfield",
    },
  ];

  const result = normaliseRows(rows);

  expect(result).toEqual(expected);
});

test("groupByListAboutYou returns an object with row data grouped by list", () => {
  const rowData = [
    {
      list: "About you",
      key: "email address",
      value: "my@email.com",
    },
    {
      list: "About your children",
      key: "Do you have children",
      value: "yes",
    },
    {
      list: "About you",
      key: "Telephone",
      value: "111-111-111-111",
    },
  ];

  const expected = {
    "About you": [
      {
        list: "About you",
        key: "email address",
        value: "my@email.com",
      },
      {
        list: "About you",
        key: "Telephone",
        value: "111-111-111-111",
      },
    ],
    "About your children": [
      {
        list: "About your children",
        key: "Do you have children",
        value: "yes",
      },
    ],
  };

  const result = groupByListAboutYou(rowData);

  expect(result).toEqual(expected);
});

test("getAboutYouSummaryListsForSteps returns row data grouped by list", () => {
  const step1 = {
    contentSummary: () => [
      { keyA: "myKeyA", valueA: "myValueA", list: "list1" },
      { keyB: "myKeyB", valueB: "myValueB", list: "list1" },
    ],
    path: "mypath1",
  };
  const step2 = {
    contentSummary: () => ({
      key2: "myKey2",
      value: "myValue2",
      list: "list2",
    }),
    path: "mypath2",
  };
  const step3 = {
    contentSummary: () => ({
      key3: "myKey3",
      value: "myValue3",
      list: "list1",
    }),
    path: "mypath3",
  };
  const req = {};
  const steps = [step1, step2, step3];

  const expected = {
    list1: [
      { keyA: "myKeyA", valueA: "myValueA", list: "list1", path: "mypath1" },
      { keyB: "myKeyB", valueB: "myValueB", list: "list1", path: "mypath1" },
      { key3: "myKey3", value: "myValue3", list: "list1", path: "mypath3" },
    ],
    list2: [
      { key2: "myKey2", value: "myValue2", path: "mypath2", list: "list2" },
    ],
  };

  const result = getAboutYouSummaryListsForSteps({ req, steps });

  expect(result).toEqual(expected);
});

test("getAboutYouSummaryListsForSteps sets a default list if no list is defined", () => {
  const step1 = {
    contentSummary: () => ({ key1: "myKey1", value: "myValue1" }),
    path: "mypath1",
  };
  const step2 = {
    contentSummary: () => ({
      key2: "myKey2",
      value: "myValue2",
      list: "list2",
    }),
    path: "mypath2",
  };
  const req = {};
  const steps = [step1, step2];

  const expected = {
    [DEFAULT_LIST]: [{ key1: "myKey1", value: "myValue1", path: "mypath1" }],
    list2: [
      { key2: "myKey2", value: "myValue2", list: "list2", path: "mypath2" },
    ],
  };

  const result = getAboutYouSummaryListsForSteps({ req, steps });

  expect(result).toEqual(expected);
});

test("getAboutYouSummaryListsForSteps orders rows by sort order defined", () => {
  const stepName = {
    contentSummary: () => ({
      id: nameContentSummaryId,
      value: "name",
    }),
    path: "mypath1",
  };
  const stepDateOfBirth = {
    contentSummary: () => ({
      id: dateOfBirthDateContentSummaryId,
      value: "dob",
    }),
    path: "mypath2",
  };
  const stepNhsNumber = {
    contentSummary: () => ({
      id: nhsNumberContentSummaryId,
      value: "nhs number",
    }),
    path: "mypath3",
  };
  const stepAddress = {
    contentSummary: () => ({
      id: addressContentSummaryId,
      value: "my address",
    }),
    path: "mypath4",
  };
  const stepEmail = {
    contentSummary: () => ({
      id: emailIdContentSummaryId,
      value: "my email",
    }),
    path: "mypath5",
  };
  const stepOther = {
    contentSummary: () => ({
      id: "otherKey",
      value: "other value",
    }),
    path: "mypath6",
  };
  const req = {};
  // Intentionally put the steps in a random order
  const steps = [
    stepAddress,
    stepOther,
    stepName,
    stepDateOfBirth,
    stepNhsNumber,
    stepEmail,
  ];

  const expected = [
    stepName.path,
    stepDateOfBirth.path,
    stepNhsNumber.path,
    stepAddress.path,
    stepEmail.path,
    stepOther.path,
  ];

  const result = getAboutYouSummaryListsForSteps({ req, steps });

  expect(result[DEFAULT_LIST].map((summary) => summary.path)).toEqual(expected);
});
