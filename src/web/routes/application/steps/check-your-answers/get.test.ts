import expect from "expect";

const getPreviousPath = jest.fn();
const getCertificateDetailsRowsMock = jest.fn();

jest.mock("../../flow-control/get-previous-path", () => ({
  getPreviousPath,
}));
jest.mock("./get-certificate-detail-rows", () => ({
  getCertificateDetailsRows: getCertificateDetailsRowsMock,
}));

beforeAll(() => {
  getCertificateDetailsRowsMock.mockReturnValue(jest.fn());
});

import { getLastNavigablePath, getCheckAnswers } from "./get";

import { states, testUtils } from "../../flow-control";
const { buildSessionForJourney, getStateForJourney } = testUtils;

const redirect = jest.fn();
const render = jest.fn();

test("getLastNavigablePath returns path of last step if last step has no isNavigable function", () => {
  const steps = [{ path: "/first" }, { path: "/last" }];
  const session = {};

  const result = getLastNavigablePath(steps, session);

  expect(result).toBe("/last");
});

test("returns path of last step if last step's isNavigable function returns true", () => {
  const steps = [
    { path: "/first" },
    { path: "/last", isNavigable: () => true },
  ];
  const session = {};

  const result = getLastNavigablePath(steps, session);

  expect(result).toBe("/last");
});

test("getLastNavigablePath calls getPreviousPath if last step isNavigable returns false", () => {
  const steps = [
    { path: "/first" },
    { path: "/last", isNavigable: () => false },
  ];
  const session = {};

  getLastNavigablePath(steps, session);

  expect(getPreviousPath).toHaveBeenCalled();
});

test("getCheckAnswers() Changes State from IN_PROGRESS to IN_REVIEW", () => {
  const journey = {
    name: "apply",
    pathPrefix: "/apply",
    steps: [
      { path: "/first", next: () => "/second" },
      { path: "/second", next: () => "/third" },
      { path: "/third", isNavigable: () => false, next: () => "/fourth" },
    ],
  };

  const req = {
    t: (string) => string,
    csrfToken: jest.fn().mockReturnValue("myCsrfToken"),
    session: {
      ...buildSessionForJourney({
        journeyName: "apply",
        state: states.IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    headers: {
      REQUEST_ID_HEADER: "123456",
    },
  };
  const res = { redirect, render };
  const expectedState = states.IN_REVIEW;

  getCheckAnswers(journey)(req, res);

  expect(getCertificateDetailsRowsMock).toBeCalled();
  expect(getStateForJourney("apply", req)).toBe(expectedState);
  expect(req.csrfToken).toBeCalled();
});
