import { assoc, compose, filter, flatten, isNil, groupBy, propOr } from "ramda";
import { notIsNil } from "../../../../../common/predicates";
import { addressContentSummaryId } from "../address/content-summary";
import { dateOfBirthDateContentSummaryId } from "../date-of-birth/date-of-birth";
import {
  doYouKnowNhsNumberContentSummaryId,
  nhsNumberContentSummaryId,
} from "../do-you-know-your-nhs-number/do-you-know-your-nhs-number";
import {
  doYouKnowEmailIdContentSummaryId,
  emailIdContentSummaryId,
} from "../email-address/email-address";
import { nameContentSummaryId } from "../name/name";
import { DEFAULT_LIST, SUMMARY_LIST_KEY } from "./constants";

const assocPathWithContentSummary = (path, summary) => {
  const assocPathProp = assoc("path", path);
  return Array.isArray(summary)
    ? summary.map(assocPathProp)
    : assocPathProp(summary);
};

const getContentSummary = (req, section) => (step) => {
  let summary = isNil(step.contentSummary) ? null : step.contentSummary(req);
  if (isNil(summary)) {
    return null;
  } else {
    summary = Array.isArray(summary) ? summary : [summary];
    let summaryList: any = [];
    summaryList = summary.map((data) => {
      return data.section !== section
        ? null
        : assocPathWithContentSummary(step.path, data);
    });
    summaryList = summaryList.filter((data) => {
      return data !== null;
    });
    return summaryList;
  }
};

const getSummaryListRows = ({ req, steps, section }) => {
  return steps.map(getContentSummary(req, section));
};

const sortOrder = [
  nameContentSummaryId,
  dateOfBirthDateContentSummaryId,
  doYouKnowNhsNumberContentSummaryId,
  nhsNumberContentSummaryId,
  addressContentSummaryId,
  doYouKnowEmailIdContentSummaryId,
  emailIdContentSummaryId,
];

const itemPositions = {};
const orderListRows = (steps) => {
  for (const [index, id] of sortOrder.entries()) {
    itemPositions[id] = index;
  }

  return steps.sort((a, b) => {
    if (isNil(itemPositions[a.id])) return 1;
    if (isNil(itemPositions[b.id])) return -1;
    return itemPositions[a.id] - itemPositions[b.id];
  });
};

const normaliseRows = compose(flatten, filter(notIsNil));

const listPropAboutYou = propOr(DEFAULT_LIST, SUMMARY_LIST_KEY);

const groupByListAboutYou = groupBy(listPropAboutYou);

const getAboutYouSummaryListsForSteps = compose(
  groupByListAboutYou,
  orderListRows,
  normaliseRows,
  getSummaryListRows,
);

export {
  getContentSummary,
  getSummaryListRows,
  normaliseRows,
  groupByListAboutYou,
  getAboutYouSummaryListsForSteps,
};
