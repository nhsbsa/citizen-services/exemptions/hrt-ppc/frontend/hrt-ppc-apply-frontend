import expect from "expect";
import { isErrorStatusCode } from "./predicates";
import * as httpStatus from "http-status-codes";

test("isErrorStatusCode() returns true for error status code", () => {
  expect(isErrorStatusCode(httpStatus.BAD_REQUEST)).toBe(true);
  expect(isErrorStatusCode(httpStatus.INTERNAL_SERVER_ERROR)).toBe(true);
  expect(isErrorStatusCode(httpStatus.OK)).toBe(false);
});
