export const DEFAULT_LIST: string = "aboutYou";
export const CERTIFICATE_LIST: string = "aboutCertificate";
export const SUMMARY_LIST_KEY: string = "list";
export const PAYMENT_DESCRIPTION_12_MONTH: string = "12 Months single payment";
