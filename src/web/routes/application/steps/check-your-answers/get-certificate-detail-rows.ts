import { getCertificateDetails } from "./certificate-details";
import { RowHeader } from "../common/types";

const buildRowHeader = (listProps, translate): RowHeader => ({
  key: {
    text: listProps.key,
  },
  value: {
    text: listProps.value,
  },
  actions:
    listProps.href !== undefined
      ? {
          items: [
            {
              href: listProps.href,
              text: translate.changeText,
              visuallyHiddenText: listProps.key,
            },
          ],
        }
      : undefined,
});

const getCertificateDetailsRows = (req, localisation) => () => {
  const rows: any = [];
  const listProps = getCertificateDetails(req, localisation);
  for (let i = 0; i < listProps.length; i++) {
    const headerRow = buildRowHeader(listProps[i], localisation);
    rows.push(headerRow);
  }
  return rows;
};

export { getCertificateDetailsRows, buildRowHeader };
