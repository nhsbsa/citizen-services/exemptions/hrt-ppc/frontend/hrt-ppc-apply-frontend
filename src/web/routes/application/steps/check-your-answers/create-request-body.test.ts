import expect from "expect";
import moment from "moment";
import { config } from "../../../../../config";
import {
  createRequestBody,
  createPaymentRequestBody,
} from "./create-request-body";
import {
  CertificateType,
  Duration,
  PaymentMethod,
} from "@nhsbsa/health-charge-exemption-common-frontend";

describe("createRequestBody()", () => {
  test("formats and populates request body", () => {
    const session = {
      claim: {
        firstName: "Fname",
        lastName: "Lname",
        dateOfBirth: "2022-01-01",
        addressLine1: "Stella House",
        addressLine2: "Goldcrest Way",
        townOrCity: "Newcastle upon Tyne",
        sanitizedPostcode: "NE15 8NY",
        doYouKnowYourNhsNumber: "yes",
        nhsNumber: "9999999999",
        certificateStartDate: "2023-04-01",
        certificateEndDate: "2024-04-01",
        doYouWantEmail: "yes",
        emailAddress: "email@example.com",
      },
    };
    const expected = {
      citizen: {
        addresses: [
          {
            addressLine1: "Stella House",
            addressLine2: "Goldcrest Way",
            townOrCity: "Newcastle upon Tyne",
            postcode: "NE15 8NY",
          },
        ],
        firstName: "Fname",
        lastName: "Lname",
        dateOfBirth: "2022-01-01",
        nhsNumber: "9999999999",
        emails: [{ emailAddress: "email@example.com" }],
      },
      certificate: {
        startDate: "2023-04-01",
        endDate: "2024-04-01",
        type: CertificateType.HRT_PPC,
        duration: Duration.P12M,
        cost: Number(config.environment.HRT_PPC_VALUE),
        applicationDate: moment().format("YYYY-MM-DD"),
      },
    };

    const result = createRequestBody(config, session);

    expect(result).toEqual(expected);
  });

  test("create the request body with options selected no", () => {
    const session = {
      claim: {
        firstName: "Fname",
        lastName: "Lname",
        dateOfBirth: "2022-01-01",
        addressLine1: "Stella House",
        addressLine2: "Goldcrest Way",
        townOrCity: "Newcastle upon Tyne",
        sanitizedPostcode: "NE15 8NY",
        doYouKnowYourNhsNumber: "no",
        certificateStartDate: "2023-04-01",
        certificateEndDate: "2024-04-01",
        doYouWantEmail: "no",
      },
    };
    const expected = {
      citizen: {
        addresses: [
          {
            addressLine1: "Stella House",
            addressLine2: "Goldcrest Way",
            townOrCity: "Newcastle upon Tyne",
            postcode: "NE15 8NY",
          },
        ],
        firstName: "Fname",
        lastName: "Lname",
        dateOfBirth: "2022-01-01",
      },
      certificate: {
        startDate: "2023-04-01",
        endDate: "2024-04-01",
        type: CertificateType.HRT_PPC,
        duration: Duration.P12M,
        cost: Number(config.environment.HRT_PPC_VALUE),
        applicationDate: moment().format("YYYY-MM-DD"),
      },
    };

    const result = createRequestBody(config, session);

    expect(result).toEqual(expected);
  });
});

describe("createPaymentRequestBody()", () => {
  test("formats and populates the request payment body", () => {
    const now = moment().toDate();
    const expected = {
      amount: config.environment.HRT_PPC_VALUE,
      date: now,
      method: PaymentMethod.CARD,
    };
    const result = createPaymentRequestBody(config);

    expect(result).toEqual(expected);
  });
});
