import {
  createRequestBody,
  createPaymentRequestBody,
} from "./create-request-body";
import { issueCertificateClient } from "../../../../client/issue-certificate-client";
import { paymentClient } from "../../../../client/payment-client";
import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { makeRequestToCardPaymentService } from "./create-payment-redirect";

const postCheckAnswers = (config) => async (req, res, next) => {
  try {
    const client = new issueCertificateClient(config);
    const body = createRequestBody(config, req.session);
    const response = await client.makeRequest(
      {
        method: "POST",
        url: "/v1/issue-certificate",
        data: body,
        responseType: "json",
      },
      req,
    );
    req.session.issuedCertificateDetails = {
      certificateId: response.data.certificate["id"],
      citizenId: response.data.certificate["citizenId"],
      certificateReference: response.data.certificate["reference"],
      certificateStartDate: response.data.certificate["startDate"],
      certificateEndDate: response.data.certificate["endDate"],
    };
    const payment = new paymentClient(config);
    const paymentBody = createPaymentRequestBody(config);
    const responseFromPayment = await payment.makeRequest(
      {
        method: "POST",
        url: `/v1/payments?certificateId=${req.session.issuedCertificateDetails.certificateId}`,
        data: paymentBody,
        responseType: "json",
      },
      req,
    );
    req.session.paymentDetails = {
      paymentId: responseFromPayment.data.id,
      transactionId: responseFromPayment.data.transactionId,
    };

    await makeRequestToCardPaymentService(
      config,
      responseFromPayment.data.transactionId,
    )(req, res, next);
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      }),
    );
  }
};

export { postCheckAnswers };
