import moment from "moment";
import { requestBody as requestBodyName } from "../name/name";
import { requestBody as requestBodyDateOfBirth } from "../date-of-birth/date-of-birth";
import { requestBody as requestBodyAddress } from "../address/request-body";
import { requestBody as requestBodyCertficateDate } from "../certificate-start-date/certificate-start-date";
import { requestBody as requestBodyNhsNumber } from "../do-you-know-your-nhs-number/do-you-know-your-nhs-number";
import { requestBody as requestBodyEmailAddress } from "../email-address/email-address";
import {
  CertificateType,
  Duration,
  PaymentMethod,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import { PaymentRequest } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/types";

const createRequestBody = (config, session) => {
  return {
    citizen: {
      ...requestBodyName(session),
      ...requestBodyDateOfBirth(session),
      ...{ addresses: [requestBodyAddress(session)["address"]] },
      ...requestBodyNhsNumber(session),
      ...(requestBodyEmailAddress(session) && {
        emails: [requestBodyEmailAddress(session)],
      }),
    },
    certificate: {
      ...requestBodyCertficateDate(session),
      type: CertificateType.HRT_PPC,
      duration: Duration.P12M,
      cost: Number(config.environment.HRT_PPC_VALUE),
      applicationDate: moment().format(`YYYY-MM-DD`),
    },
  };
};

const createPaymentRequestBody = (config): PaymentRequest => {
  return {
    amount: config.environment.HRT_PPC_VALUE,
    date: moment().toDate(),
    method: PaymentMethod.CARD,
  };
};

export { createRequestBody, createPaymentRequestBody };
