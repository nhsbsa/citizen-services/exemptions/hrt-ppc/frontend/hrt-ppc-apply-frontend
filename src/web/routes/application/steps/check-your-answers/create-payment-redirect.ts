import { cardPaymentsClient } from "../../../../client/cps-client";
import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { AxiosResponse } from "axios";
import { PAYMENT_DESCRIPTION_12_MONTH } from "./constants";
import { contextPath } from "../../paths/context-path";
import jwt from "jsonwebtoken";
import { CreatePaymentResponse } from "../../../../client/cps-payment-types";
import { PROCESS_PAYMENT_RETURN_URL } from "../../paths/paths";
import { CardPaymentRequest } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/types";
import { logger } from "../../../../logger/logger";

const JWT_EXPIRY: string = "30m";

const makeRequestToCardPaymentService =
  (config, transactionId) => async (req, res, next) => {
    try {
      const token = jwt.sign(
        { transactionId: transactionId },
        config.environment.CARD_PAYMENTS_JWT_SECRET,
        { expiresIn: JWT_EXPIRY },
      );
      const doneUrl =
        config.environment.APP_BASE_URL +
        contextPath(PROCESS_PAYMENT_RETURN_URL) +
        "?token=" +
        token;

      const client = new cardPaymentsClient(config);
      const body: CardPaymentRequest = {
        reference: req.session.issuedCertificateDetails.certificateReference,
        transactionId: transactionId,
        amount: config.environment.HRT_PPC_VALUE,
        donePageURL: doneUrl,
        serviceDescription: PAYMENT_DESCRIPTION_12_MONTH,
        language: "en",
      };

      const response: AxiosResponse<CreatePaymentResponse, any> =
        await client.makeRequestCreatePayment({
          method: "POST",
          url: "/payments",
          headers: setHeadersIfMockEnabled(config, req),
          data: body,
          responseType: "json",
        });

      req.session.transactionId = transactionId;

      logger.info(
        `Redirecting user to gov.pay for transactionId: ${transactionId}`,
        req,
      );

      return res.redirect(response.data.nextUrl);
    } catch (error) {
      next(
        wrapError({
          cause: error,
          message: `Error posting ${req.path}`,
          statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
        }),
      );
    }
  };

/** The headers are set only when mock is enabled as we need to
 * simulate different status's, we are using firstName for this
 *
 * @param config the app config
 * @param req the request
 * @returns the headers or null
 */
const setHeadersIfMockEnabled = (config, req) => {
  if (config.environment.CARD_PAYMENTS_USE_MOCK === true) {
    return { firstName: req.session.claim.firstName };
  }
  return {};
};

export { makeRequestToCardPaymentService };
