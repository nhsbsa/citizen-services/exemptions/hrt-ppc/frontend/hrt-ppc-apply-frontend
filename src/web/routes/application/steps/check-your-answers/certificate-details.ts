import * as contentSummary from "../certificate-start-date/certificate-start-date";
import { CertificateDetail } from "../common/types";
const CONTEXT_PATH = process.env.CONTEXT_PATH || "";

const getCertificateDetails = (req, translate): CertificateDetail[] => [
  {
    key: translate.length,
    value: "12 months",
  },
  {
    key: translate.startsOn,
    value: contentSummary.contentSummary(req)[0].value,
    href: CONTEXT_PATH + contentSummary.certificateStartDate.path,
  },
  {
    key: translate.endsOn,
    value: contentSummary.contentSummary(req)[1].value,
  },
  {
    key: translate.totalCost,
    value: "£" + translate.price,
  },
];

export { getCertificateDetails };
