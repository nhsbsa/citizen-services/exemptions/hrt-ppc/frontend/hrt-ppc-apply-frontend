import * as httpStatus from "http-status-codes";
import expect from "expect";
import jwt from "jsonwebtoken";
import { makeRequestToCardPaymentService } from "./create-payment-redirect";
import { cardPaymentsClient } from "../../../../client/cps-client";
import { wrapError } from "../../errors";

jest.mock("../../../../client/cps-client");

const res = { redirect: jest.fn() };
const next = jest.fn();
const makeRequestCreatePaymentMock = jest.fn();
const mockJwtSign = jest.spyOn(jwt, "sign");

const jwtSecret = process.env.CARD_PAYMENTS_JWT_SECRET;
const transactionId = "ABCD12345";
const signedToken = jwt.sign({ transactionId: transactionId }, jwtSecret, {
  expiresIn: "10m",
});

let config;

beforeAll(() => {
  cardPaymentsClient.prototype.makeRequestCreatePayment =
    makeRequestCreatePaymentMock;
});

beforeEach(() => {
  jest.clearAllMocks();
  config = {
    environment: {
      APP_BASE_URL: "http://localhost:8080",
      CARD_PAYMENTS_JWT_SECRET: "test-secret",
      CARD_PAYMENTS_USE_MOCK: false,
      HRT_PPC_VALUE: "1870",
    },
  };
  mockJwtSign.mockReturnValue(signedToken);
});

describe("makeRequestToCardPaymentService()", () => {
  test("calls the card payment client to create payment and passes the Jwt token", async () => {
    makeRequestCreatePaymentMock.mockReturnValue(
      Promise.resolve({ data: { nextUrl: "http://gov.pay.test/next" } }),
    );

    const req = {
      path: "/first",
      session: {
        issuedCertificateDetails: {
          certificateReference: "ref-123",
        },
        transactionId: undefined,
      },
    };

    await makeRequestToCardPaymentService(config, transactionId)(
      req,
      res,
      next,
    );

    const expectedCall = {
      data: {
        amount: "1870",
        donePageURL: `http://localhost:8080/test-context/p?token=${signedToken}`,
        language: "en",
        reference: "ref-123",
        serviceDescription: "12 Months single payment",
        transactionId: "ABCD12345",
      },
      headers: {},
      method: "POST",
      responseType: "json",
      url: "/payments",
    };
    expect(mockJwtSign).toBeCalledTimes(1);
    expect(mockJwtSign).toHaveBeenCalledWith(
      { transactionId: transactionId },
      jwtSecret,
      { expiresIn: "30m" },
    );
    expect(makeRequestCreatePaymentMock).toBeCalledTimes(1);
    expect(makeRequestCreatePaymentMock).toBeCalledWith(expectedCall);
    expect(next).toBeCalledTimes(0);
    expect(req.session.transactionId).toEqual(transactionId);
    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toBeCalledWith("http://gov.pay.test/next");
  });

  test("calls the card payment client to create payment and passes the Jwt token and firstName header when mock enabled", async () => {
    config.environment["CARD_PAYMENTS_USE_MOCK"] = true;
    makeRequestCreatePaymentMock.mockReturnValue(
      Promise.resolve({ data: { nextUrl: "http://gov.pay.test/next" } }),
    );

    const req = {
      path: "/first",
      session: {
        claim: {
          firstName: "Test",
        },
        issuedCertificateDetails: {
          certificateReference: "ref-123",
        },
        transactionId: undefined,
      },
    };

    await makeRequestToCardPaymentService(config, transactionId)(
      req,
      res,
      next,
    );

    const expectedCall = {
      data: {
        amount: "1870",
        donePageURL: `http://localhost:8080/test-context/p?token=${signedToken}`,
        language: "en",
        reference: "ref-123",
        serviceDescription: "12 Months single payment",
        transactionId: "ABCD12345",
      },
      headers: {
        firstName: "Test",
      },
      method: "POST",
      responseType: "json",
      url: "/payments",
    };
    expect(mockJwtSign).toBeCalledTimes(1);
    expect(mockJwtSign).toHaveBeenCalledWith(
      { transactionId: transactionId },
      jwtSecret,
      { expiresIn: "30m" },
    );
    expect(makeRequestCreatePaymentMock).toBeCalledTimes(1);
    expect(makeRequestCreatePaymentMock).toBeCalledWith(expectedCall);
    expect(next).toBeCalledTimes(0);
    expect(req.session.transactionId).toEqual(transactionId);
    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toBeCalledWith("http://gov.pay.test/next");
  });

  test("calls next() with wrapError() if there is an error during payment creation", async () => {
    makeRequestCreatePaymentMock.mockReturnValue(
      Promise.reject(new Error("payment error")),
    );

    const req = {
      path: "/first",
      session: {
        issuedCertificateDetails: {
          certificateReference: "ref-123",
        },
        transactionId: undefined,
      },
    };

    await makeRequestToCardPaymentService(config, transactionId)(
      req,
      res,
      next,
    );

    const expectedCall = {
      data: {
        amount: "1870",
        donePageURL: `http://localhost:8080/test-context/p?token=${signedToken}`,
        language: "en",
        reference: "ref-123",
        serviceDescription: "12 Months single payment",
        transactionId: "ABCD12345",
      },
      headers: {},
      method: "POST",
      responseType: "json",
      url: "/payments",
    };
    const error = wrapError({
      cause: new Error(`payment error`),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    });
    expect(mockJwtSign).toBeCalledTimes(1);
    expect(mockJwtSign).toHaveBeenCalledWith(
      { transactionId: transactionId },
      jwtSecret,
      { expiresIn: "30m" },
    );
    expect(makeRequestCreatePaymentMock).toBeCalledTimes(1);
    expect(makeRequestCreatePaymentMock).toBeCalledWith(expectedCall);
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(error);
    expect(req.session.transactionId).toBeUndefined();
    expect(res.redirect).toBeCalledTimes(0);
  });

  test("calls next() with wrapError() if there is an error during Jwt signing", async () => {
    makeRequestCreatePaymentMock.mockReturnValue(
      Promise.resolve({ data: { nextUrl: "http://gov.pay.test/next" } }),
    );
    mockJwtSign.mockImplementation(() => {
      throw new Error("Jwt invalid");
    });

    const req = {
      path: "/first",
      session: {
        issuedCertificateDetails: {
          certificateReference: "ref-123",
        },
        transactionId: undefined,
      },
    };

    await makeRequestToCardPaymentService(config, transactionId)(
      req,
      res,
      next,
    );

    const error = wrapError({
      cause: new Error(`Jwt invalid`),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    });
    expect(mockJwtSign).toBeCalledTimes(1);
    expect(mockJwtSign).toHaveBeenCalledWith(
      { transactionId: transactionId },
      jwtSecret,
      { expiresIn: "30m" },
    );
    expect(makeRequestCreatePaymentMock).toBeCalledTimes(0);
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(error);
    expect(req.session.transactionId).toBeUndefined();
    expect(res.redirect).toBeCalledTimes(0);
  });
});
