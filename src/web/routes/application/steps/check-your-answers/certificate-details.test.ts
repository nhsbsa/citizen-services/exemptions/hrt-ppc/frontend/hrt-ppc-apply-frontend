import expect from "expect";
import * as contentSummary from "../certificate-start-date/certificate-start-date";
import { getCertificateDetails } from "./certificate-details";

jest.spyOn(contentSummary, "contentSummary").mockReturnValue([
  {
    value: "15 April 2023",
    id: "",
    key: "Starts on",
    section: "aboutCertificate",
    visuallyHidden: "Starts on",
  },
  {
    value: "15 April 2024",
    id: "",
    key: "Ends on",
    section: "aboutCertificate",
    visuallyHidden: "Ends on",
  },
]);

test("getCertificateDetails should return an object with correct format", () => {
  const req = jest.fn();

  const translate = {
    length: "Length",
    startsOn: "Starts On",
    endsOn: "Ends on",
    totalCost: "Total cost",
    price: "18.70",
  };

  expect(getCertificateDetails(req, translate)).toEqual([
    { key: "Length", value: "12 months" },
    {
      href: "/test-context/your-start-date",
      key: "Starts On",
      value: "15 April 2023",
    },
    { key: "Ends on", value: "15 April 2024" },
    { key: "Total cost", value: "£18.70" },
  ]);
});
