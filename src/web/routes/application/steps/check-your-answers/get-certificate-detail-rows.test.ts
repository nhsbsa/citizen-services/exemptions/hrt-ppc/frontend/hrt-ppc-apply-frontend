import expect from "expect";
import {
  buildRowHeader,
  getCertificateDetailsRows,
} from "./get-certificate-detail-rows";

import * as getCertificateDetails from "./certificate-details";

jest.spyOn(getCertificateDetails, "getCertificateDetails").mockReturnValue([
  {
    key: "Length",
    value: "12 months",
  },
  {
    key: "Starts On",
    value: "12th December 2022",
    href: "/test-context/start-page",
  },
]);

describe("buildRowHeader", () => {
  test("should return an object without `Action` link if `href` is not provided ", () => {
    const req = {
      key: "Length",
      value: "12 months",
    };
    const translate = {};

    const result = buildRowHeader(req, translate);

    expect(result).toEqual({
      actions: undefined,
      key: { text: "Length" },
      value: { text: "12 months" },
    });
  });

  test("should return an object with `Action` link if `href` is provided ", () => {
    const req = {
      key: "Starts On",
      value: "20 April 2023",
      href: "/test-context/your-start-date",
    };
    const translate = { changeText: "change" };

    const result = buildRowHeader(req, translate);

    expect(result).toEqual({
      actions: {
        items: [
          {
            href: "/test-context/your-start-date",
            text: "change",
            visuallyHiddenText: "Starts On",
          },
        ],
      },
      key: { text: "Starts On" },
      value: { text: "20 April 2023" },
    });
  });
});

describe("getCertificateDetailsRows", () => {
  test("should return a list of rows both with and with-out `Action` links", () => {
    const req = jest.fn();
    const localisation = { changeText: "change" };

    const result = getCertificateDetailsRows(req, localisation)();

    expect(result).toHaveLength(2);
    expect(result[0]).toEqual({
      actions: undefined,
      key: { text: "Length" },
      value: { text: "12 months" },
    });
    expect(result[1]).toEqual({
      actions: {
        items: [
          {
            href: "/test-context/start-page",
            text: "change",
            visuallyHiddenText: "Starts On",
          },
        ],
      },
      key: {
        text: "Starts On",
      },
      value: {
        text: "12th December 2022",
      },
    });
  });
});
