import expect from "expect";
import {
  JOURNEYS_KEY,
  NEXT_ALLOWED_PATH_KEY,
  STATE_KEY,
  CURRENT_STEP_PATH_KEY,
} from "../keys";
import {
  setJourneySessionProp,
  getJourneySessionProp,
  getJourneySessionPropWithoutError,
  setNextAllowedPathInSession,
  setCurrentPathInSession,
  setStateInSession,
  getNextAllowedPathFromSession,
  getStateFromSession,
  getJourneysFromSession,
  getCurrentPathFromSession,
  getAdditionalDataForStep,
  setAdditionalDataForStep,
} from "./session-accessors";

const REPORT_A_CHANGE_JOURNEY = { name: "report-a-change" };

test("setJourneySessionProp() throws an error if journey is undefined", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        "report-a-change": {
          nextPath: "/first",
        },
      },
    },
  };

  const result = () =>
    setJourneySessionProp("nextPath")(req, undefined, "/second");

  expect(result).toThrowError(
    /No journey defined when trying to set "nextPath" as "\/second"/,
  );
});

test("setJourneySessionProp() throws an error if journey name is undefined", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        "report-a-change": {
          nextPath: "/first",
        },
      },
    },
  };

  const result = () => setJourneySessionProp("nextPath")(req, {}, "/second");

  expect(result).toThrowError(
    /No name defined for journey when trying to set "nextPath" as "\/second"/,
  );
});

test("getJourneySessionProp() throws an error if journey is undefined", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        "report-a-change": {
          nextPath: "/first",
        },
      },
    },
  };

  const result = () => getJourneySessionProp("nextPath")(req, undefined);

  expect(result).toThrowError(
    /No journey defined when trying to get "nextPath"/,
  );
});

test("getJourneySessionProp() throws an error if journey name is undefined", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        "report-a-change": {
          nextPath: "/first",
        },
      },
    },
  };

  const result = () => getJourneySessionProp("nextPath")(req, {});

  expect(result).toThrowError(
    /Property "nextPath" does not exist in session for journey name "undefined"/,
  );
});

test("getJourneySessionProp() throws an error if prop is undefined in session", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        "report-a-change": {
          nextPath: "/first",
        },
      },
    },
  };

  const result = () =>
    getJourneySessionProp("state")(req, REPORT_A_CHANGE_JOURNEY);

  expect(result).toThrowError(
    /Property "state" does not exist in session for journey name "report-a-change"/,
  );
});

test("getJourneySessionProp() returns the session prop", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        "report-a-change": {
          nextPath: "/first",
        },
      },
    },
  };

  const result = getJourneySessionProp("nextPath")(
    req,
    REPORT_A_CHANGE_JOURNEY,
  );

  expect(result).toBe("/first");
});

test("getJourneySessionPropWithoutError() throws an error if journey is undefined", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        "report-a-change": {
          nextPath: "/first",
        },
      },
    },
  };

  const result = () =>
    getJourneySessionPropWithoutError("nextPath")(req, undefined);

  expect(result).toThrowError(
    /No journey defined when trying to get "nextPath"/,
  );
});

test("getJourneySessionPropWithoutError() returns the session prop", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        "report-a-change": {
          nextPath: "/first",
        },
      },
    },
  };

  const result = getJourneySessionPropWithoutError("nextPath")(
    req,
    REPORT_A_CHANGE_JOURNEY,
  );

  expect(result).toBe("/first");
});

test("getJourneySessionPropWithoutError() returns undefined if session prop not set", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        "report-a-change": {},
      },
    },
  };

  const result = getJourneySessionPropWithoutError("nextPath")(
    req,
    REPORT_A_CHANGE_JOURNEY,
  );

  expect(result).toBe(undefined);
});

test("setNextAllowedPathInSession() sets the next allowed path for the correct journey in session", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        apply: {
          [NEXT_ALLOWED_PATH_KEY]: "/third",
        },
        "report-a-change": {
          [NEXT_ALLOWED_PATH_KEY]: "/first",
        },
      },
    },
  };

  const expectedJourneysState = {
    apply: {
      [NEXT_ALLOWED_PATH_KEY]: "/third",
    },
    "report-a-change": {
      [NEXT_ALLOWED_PATH_KEY]: "/second",
    },
  };

  setNextAllowedPathInSession(req, REPORT_A_CHANGE_JOURNEY, "/second");
  expect(req.session[JOURNEYS_KEY]).toEqual(expectedJourneysState);
});

test("setCurrentPathInSession() sets the current path for the correct journey in session", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        "report-a-change": {
          [CURRENT_STEP_PATH_KEY]: "/first",
        },
      },
    },
  };

  const expectedJourneysState = {
    "report-a-change": {
      [CURRENT_STEP_PATH_KEY]: "/second",
    },
  };

  setCurrentPathInSession(req, REPORT_A_CHANGE_JOURNEY, "/second");
  expect(req.session[JOURNEYS_KEY]).toEqual(expectedJourneysState);
});

test("setStateInSession() sets the state for the correct journey in session", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        apply: {
          [STATE_KEY]: "IN_PROGRESS",
        },
        "report-a-change": {
          [STATE_KEY]: "IN_PROGRESS",
        },
      },
    },
  };

  const expectedJourneysState = {
    apply: {
      [STATE_KEY]: "IN_PROGRESS",
    },
    "report-a-change": {
      [STATE_KEY]: "IN_REVIEW",
    },
  };

  setStateInSession(req, REPORT_A_CHANGE_JOURNEY, "IN_REVIEW");
  expect(req.session[JOURNEYS_KEY]).toEqual(expectedJourneysState);
});

test("getNextAllowedPathFromSession() gets next allowed path for the correct journey", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        apply: {
          [NEXT_ALLOWED_PATH_KEY]: "/third",
        },
        "report-a-change": {
          [NEXT_ALLOWED_PATH_KEY]: "/first",
        },
      },
    },
  };

  const result = getNextAllowedPathFromSession(req, REPORT_A_CHANGE_JOURNEY);
  expect(result).toBe("/first");
});

test("getStateFromSession() gets state for the correct journey", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        apply: {
          [STATE_KEY]: "IN_PROGRESS",
        },
        "report-a-change": {
          [STATE_KEY]: "IN_REVIEW",
        },
      },
    },
  };

  const result = getStateFromSession(req, REPORT_A_CHANGE_JOURNEY);
  expect(result).toBe("IN_REVIEW");
});

test("getJourneysFromSession() returns list of associated journey name and properties from session", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        apply: {
          [STATE_KEY]: "IN_PROGRESS",
          [NEXT_ALLOWED_PATH_KEY]: "/first",
        },
        "report-a-change": {
          [STATE_KEY]: "IN_REVIEW",
          [NEXT_ALLOWED_PATH_KEY]: "/last",
        },
      },
    },
  };

  const result = getJourneysFromSession(req);

  const expected = [
    [
      "apply",
      { [STATE_KEY]: "IN_PROGRESS", [NEXT_ALLOWED_PATH_KEY]: "/first" },
    ],
    [
      "report-a-change",
      { [STATE_KEY]: "IN_REVIEW", [NEXT_ALLOWED_PATH_KEY]: "/last" },
    ],
  ];

  expect(result).toEqual(expected);
});

test("setAdditionalDataForStep() inserts additionalData into session for step", () => {
  const req = {
    session: {
      stepData: {},
    },
  };
  const step = { path: "/step-path" };
  const stepData = { firstName: "Joe", lastName: "Bloggs" };

  setAdditionalDataForStep(req, step, stepData);

  expect(req.session.stepData[step.path]).toEqual(stepData);
});

test("getAdditionalDataForStep() inserts stepData into session for step", () => {
  const stepData = { firstName: "Joe", lastName: "Bloggs" };
  const req = {
    session: {
      stepData: { "/step-path": stepData },
    },
  };
  const step = { path: "/step-path" };

  const result = getAdditionalDataForStep(req, step);

  expect(result).toEqual(stepData);
});

test("getCurrentPathFromSession() gets the current path for the journey", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        apply: {
          [CURRENT_STEP_PATH_KEY]: "/first",
        },
        "report-a-change": {
          [CURRENT_STEP_PATH_KEY]: "/second",
        },
      },
    },
  };

  const result = getCurrentPathFromSession(req, REPORT_A_CHANGE_JOURNEY);
  expect(result).toBe("/second");
});
