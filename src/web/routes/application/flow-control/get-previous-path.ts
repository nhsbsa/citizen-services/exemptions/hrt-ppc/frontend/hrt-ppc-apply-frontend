import { isNil } from "ramda";
import { stateMachine } from "../flow-control/state-machine";
import { isUndefined } from "../../../../common/predicates";
import { logger } from "../../../../web/logger/logger";
import { APPLY_JOURNEY_NAME } from "../constants";

const CONTEXT_PATH = process.env.CONTEXT_PATH || "";

const getPreviousNavigablePath = (steps, index, req) => {
  if (index === 0) {
    throw new Error("No allowed back route found");
  }

  const previousStep = steps[index - 1];

  const { isNavigable, path, back } = previousStep;

  if (isNil(isNavigable)) {
    return path;
  }

  if (typeof isNavigable !== "function") {
    throw new Error(
      `isNavigable must be a function for step ${JSON.stringify(previousStep)}`,
    );
  }

  if (typeof isNavigable(req, req.session) !== "boolean") {
    throw new Error(
      `isNavigable must return a boolean for step ${JSON.stringify(
        previousStep,
      )}`,
    );
  }

  if (isNavigable(req, req.session)) {
    if (typeof back === "function") {
      return back(req);
    }

    return path;
  }

  return getPreviousNavigablePath(steps, index - 1, req);
};

const getPreviousNavigablePathFromApplyJourney = (req) => {
  const currentPathForApplyJourney = stateMachine.getCurrentPath(req, {
    name: APPLY_JOURNEY_NAME,
  });

  if (isUndefined(currentPathForApplyJourney)) {
    logger.debug("Getting previous navigable path set to root context", req);
    return CONTEXT_PATH + "/";
  }
  logger.debug(
    `Getting previous navigable path set to ${currentPathForApplyJourney}`,
    req,
  );

  return currentPathForApplyJourney;
};

const getPreviousPath = (steps, step, req) => {
  const index = steps.indexOf(step);

  if (index === -1) {
    throw new Error(
      `Unable to find ${JSON.stringify(step)} in the list of steps`,
    );
  }

  // The 'embeddedMainJourney' flag allows another journey i.e. cookies to have a back link to another journey step
  if (index === 0 && step.embeddedMainJourney !== true) {
    logger.debug("Getting previous path set to root context", req);
    return CONTEXT_PATH + "/";
  }

  // If the 'embeddedMainJourney' is set, get the current step url for the apply journey
  if (steps[index].embeddedMainJourney === true) {
    return getPreviousNavigablePathFromApplyJourney(req);
  }

  return getPreviousNavigablePath(steps, index, req);
};

export { getPreviousNavigablePath, getPreviousPath };
