import { unescape } from "./unescape-middleware";

const escape = () => {
  return [
    unescape("&#39;", "'"),
    unescape("&quot;", '"'),
    unescape("&amp;", "&"),
    unescape("&lt;", "<"),
    unescape("&gt;", ">"),
  ];
};

export { escape };
