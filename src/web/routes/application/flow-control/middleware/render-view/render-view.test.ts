import expect from "expect";

const getAdditionalDataForStep = jest.fn().mockReturnValue({ foo: "bar" });

jest.mock("../../session-accessors", () => ({
  getAdditionalDataForStep,
}));

import { renderView } from "./render-view";

const step = {
  template: "template",
  pageContent: () => ({ title: "What’s your name?" }),
  next: "redirect",
};

test("renderView() should call res.render() on GET request", async () => {
  const render = jest.fn();
  const csrfToken = () => "myCsrfToken";

  const req = {
    method: "GET",
    csrfToken,
    t: () => {
      /* explicit empty function */
    },
    session: {},
  };

  const res = {
    render,
    locals: {},
  };

  renderView(step)(req, res);

  expect(render).toHaveBeenCalled();

  const expectedArg = {
    title: "What’s your name?",
    foo: "bar",
    csrfToken: "myCsrfToken",
  };
  expect(render.mock.calls[0][1]).toEqual(expectedArg);
});
