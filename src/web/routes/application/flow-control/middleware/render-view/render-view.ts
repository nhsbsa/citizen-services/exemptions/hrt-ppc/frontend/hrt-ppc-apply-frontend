import { getAdditionalDataForStep } from "../../session-accessors";
const renderView = (step) => (req, res) => {
  res.render(step.template, {
    ...step.pageContent({ translate: req.t, req }),
    ...getAdditionalDataForStep(req, step),
    // Only set the CSRF token if there is an active session
    csrfToken: req.session ? req.csrfToken(true) : null,
  });
};

export { renderView };
