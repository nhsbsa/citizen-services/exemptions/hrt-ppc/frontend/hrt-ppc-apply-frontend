import expect from "expect";
import { checkBrowserCookie } from "./cookies-check";

test("checkBrowserCookie() should redirect if browser cookie is not set", () => {
  const redirect = jest.fn();
  const req = {
    cookies: {},
  };
  const res = {
    redirect,
    end: jest.fn(),
  };
  const next = jest.fn();
  checkBrowserCookie(req, res, next);
  expect(res.redirect).toHaveBeenCalled();
});

test("checkBrowserCookie() should call next() if browser cookie is set", () => {
  const req = {
    cookies: {
      isBrowserCookieEnabled: true,
    },
  };
  const res = {};
  const next = jest.fn();
  checkBrowserCookie(req, res, next);
  expect(next).toHaveBeenCalled();
});
