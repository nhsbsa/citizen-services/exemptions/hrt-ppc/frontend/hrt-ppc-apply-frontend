import expect from "expect";
import { injectAdditionalHeaders } from "./inject-headers";
// process.env.USE_UNSECURE_COOKIE = false
const cookieEnvVariable = process.env.USE_UNSECURE_COOKIE;

test("injectAdditionalHeaders() sets encrypted to true on req and browser cookie check enabled set", () => {
  const req = {
    connection: {
      encrypted: undefined,
    },
  };
  const res = {
    cookie: jest.fn(),
  };
  const next = jest.fn();
  const expected = {
    connection: {
      encrypted: cookieEnvVariable === "false" ? true : undefined,
    },
  };

  injectAdditionalHeaders(req, res, next);
  expect(res.cookie).toHaveBeenCalledWith("isBrowserCookieEnabled", true);
  expect(req).toEqual(expected);
  expect(next).toHaveBeenCalled();
});

test("injectAdditionalHeaders() sets encrypted to true on req and browser cookie check enabled set when next is undefined", () => {
  const req = {
    connection: {
      encrypted: undefined,
    },
  };
  const res = {
    cookie: jest.fn(),
  };
  const next = jest.fn();
  const expected = {
    connection: {
      encrypted: cookieEnvVariable === "false" ? true : undefined,
    },
  };

  injectAdditionalHeaders(req, res);
  expect(res.cookie).toHaveBeenCalledWith("isBrowserCookieEnabled", true);
  expect(req).toEqual(expected);
  expect(next).not.toHaveBeenCalled();
});
