import expect from "expect";
import { unescapeCharacter } from "./unescape-middleware";

test("Characters Matched for Apostrophe", () => {
  const obj = {
    sentence: "It&apos;s a lovely day out, isn&apos;t it?",
  };
  const expected = {
    sentence: "It's a lovely day out, isn't it?",
  };
  const result = unescapeCharacter(obj, "&apos;", "'");
  expect(result).toEqual(expected);
});

test('Characters Matched for " <> &', () => {
  const obj = {
    sentence: "&quot; &lt; &gt &amp;",
  };
  const expected = {
    sentence: "\" <> &'",
  };
  const result = unescapeCharacter(obj, "&quot; &lt; &gt &amp;", "\" <> &'");
  expect(result).toEqual(expected);
});
