import expect from "expect";
import { handlePostRedirects } from "./handle-post-redirects";
import * as states from "../states";
import { buildSessionForJourney } from "../test-utils/test-utils";

const { IN_PROGRESS } = states;

const APPLY = "apply";

const journey = {
  name: APPLY,
  steps: [{ path: "/first", next: () => "/second" }, { path: "/second" }],
};

test("handlePostRedirects() should redirect when no response errors", async () => {
  const redirect = jest.fn();
  const next = jest.fn();
  const req = {
    method: "POST",
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  const res = {
    redirect,
    locals: {},
  };

  handlePostRedirects(journey)(req, res, next);

  expect(redirect).toHaveBeenCalled();
  expect(next).not.toHaveBeenCalled();
});

test("handlePostRedirects() should call next() when response errors are present", async () => {
  const redirect = jest.fn();
  const next = jest.fn();

  const req = {
    method: "POST",
    csrfToken: () => "myCsrfToken",
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  const res = {
    redirect,
    locals: {
      errors: true,
    },
  };

  handlePostRedirects(journey)(req, res, next);

  expect(redirect).not.toHaveBeenCalled();
  expect(next).toHaveBeenCalled();
});
