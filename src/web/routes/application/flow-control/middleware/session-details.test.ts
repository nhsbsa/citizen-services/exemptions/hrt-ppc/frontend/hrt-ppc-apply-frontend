import expect from "expect";
import * as states from "../states";
import { configureSessionDetails } from "./session-details";

const apply = {
  name: "apply",
  pathsInSequence: ["/first", "/second"],
};

const reportChange = {
  name: "report-a-change",
  pathsInSequence: ["/one", "/two"],
};

test("configureSessionDetails() sets trace id locator and claim on res.locals", () => {
  const journey = apply;

  const req = {
    session: {
      claim: {
        name: "Lisa",
      },
      locator: "580847510",
    },
  };

  const res = { locals: {} };
  const next = jest.fn();

  const expectedLocals = {
    claim: {
      name: "Lisa",
    },
    locator: "580847510",
  };

  configureSessionDetails(journey)(req, res, next);
  expect(res.locals).toEqual(expectedLocals);
  expect(next).toHaveBeenCalled();
});

test("configureSessionDetails() initialises journey in session when session.journeys is undefined", () => {
  const journey = apply;
  const req = { session: { journey: undefined } };
  const res = { locals: {} };
  const next = jest.fn();

  const expectedSession = {
    journeys: {
      apply: {
        nextAllowedStep: "/first",
        state: states.IN_PROGRESS,
      },
    },
    journey: undefined,
    stepData: {},
  };

  configureSessionDetails(journey)(req, res, next);
  expect(req.session.journey).toEqual(expectedSession.journey);
  expect(next).toHaveBeenCalled();
});

test("configureSessionDetails() initialises journey in session when session.journeys is defined", () => {
  const journey = reportChange;

  const req = {
    session: {
      journeys: {
        apply,
      },
      journey: undefined,
      stepData: { foo: "bar" },
    },
  };

  const res = { locals: {} };
  const next = jest.fn();

  const expectedSession = {
    journeys: {
      apply,
      "report-a-change": {
        nextAllowedStep: "/one",
        state: states.IN_PROGRESS,
      },
    },
    journey: undefined,
    stepData: { foo: "bar" },
  };

  configureSessionDetails(journey)(req, res, next);
  expect(req.session.journey).toEqual(expectedSession.journey);
  expect(next).toHaveBeenCalled();
});

test("configureSessionDetails() does not reinitialise a journey that already exists in session", () => {
  const journey = apply;

  const req = {
    session: {
      journeys: {
        apply: {
          nextAllowedStep: "/second",
          state: "IN_REVIEW",
        },
      },
      journey: undefined,
      stepData: { foo: "bar" },
    },
  };
  const res = { locals: {} };
  const next = jest.fn();

  const expectedSession = {
    journeys: {
      apply: {
        nextAllowedStep: "/second",
        state: "IN_REVIEW",
      },
    },
    journey: undefined,
    stepData: { foo: "bar" },
  };

  configureSessionDetails(journey)(req, res, next);
  expect(req.session.journey).toEqual(expectedSession.journey);
  expect(next).toHaveBeenCalled();
});
