function unescapeCharacter(obj, escaped, char) {
  for (const key in obj) {
    // replace the escaped version with the character
    obj[key] = obj[key].replace(new RegExp(escaped, "g"), char);
  }
  return obj;
}

const unescape = (escaped, char) =>
  function (req, res, next) {
    unescapeCharacter(req.body, escaped, char);
    unescapeCharacter(req.query, escaped, char);
    unescapeCharacter(req.body, escaped, char);
    return next();
  };

export { unescape, unescapeCharacter };
