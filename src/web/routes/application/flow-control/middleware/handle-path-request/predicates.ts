import { compose, pluck, map, last, any, equals } from "ramda";
import { STATE_KEY } from "../../keys";
import { getJourneysFromSession } from "../../session-accessors";
import { COMPLETED } from "../../states";

const stepNotNavigable = (step, req) =>
  step &&
  typeof step.isNavigable === "function" &&
  !step.isNavigable(req, req.session);

const getJourneyStatesFromSession = compose(
  pluck(STATE_KEY),
  map(last),
  getJourneysFromSession,
);

const completedJourneyExistsInSession = compose(
  any(equals(COMPLETED)),
  getJourneyStatesFromSession,
);

export { stepNotNavigable, completedJourneyExistsInSession };
