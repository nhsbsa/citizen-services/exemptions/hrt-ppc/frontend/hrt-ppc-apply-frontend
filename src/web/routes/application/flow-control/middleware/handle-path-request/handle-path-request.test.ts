import expect from "expect";
import { APPLICATION_COMPLETE_URL } from "../../../paths/paths";
import { handleRequestForPath } from "./handle-path-request";
import { IN_PROGRESS, COMPLETED } from "../../states";
import { buildSessionForJourney } from "../../test-utils/test-utils";

const APPLY = "apply";

const journey = {
  name: APPLY,
  steps: [{ path: "/first", next: () => "/second" }, { path: "/second" }],
  pathsInSequence: ["/first", "/second"],
};

test("handleRequestForPath() should redirect to next allowed step if requested path is not allowed", () => {
  const req = {
    path: "/second",
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: "/first",
      }),
    },
  };

  const redirect = jest.fn();
  const res = { redirect };
  const next = jest.fn();

  handleRequestForPath(journey, undefined)(req, res, next);

  expect(redirect).toBeCalledWith("/first");
  expect(redirect).toBeCalledTimes(1);
  expect(next).toBeCalledTimes(0);
});

test("handleRequestForPath() should call next() if requested path is allowed", () => {
  const req = {
    path: "/second",
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: "/second",
      }),
    },
  };

  const redirect = jest.fn();
  const res = { redirect };
  const next = jest.fn();

  handleRequestForPath(journey, undefined)(req, res, next);

  expect(redirect).toBeCalledTimes(0);
  expect(next).toBeCalledTimes(1);
});

test(`handleRequestForPath() should destroy the session and redirect to first step in journey when navigating away from ${APPLICATION_COMPLETE_URL} to a path in sequence`, () => {
  const destroy = jest.fn();
  const clearCookie = jest.fn();
  const redirect = jest.fn();
  const next = jest.fn();

  const req = {
    path: "/second",
    session: {
      destroy,
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: COMPLETED,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = {
    clearCookie,
    redirect,
  };

  handleRequestForPath(journey, undefined)(req, res, next);

  expect(destroy).toBeCalledTimes(1);
  expect(clearCookie).toBeCalledWith("lang");
  expect(clearCookie).toBeCalledTimes(1);
  expect(redirect).toBeCalledWith("/first");
  expect(redirect).toBeCalledTimes(1);
});

test(`handleRequestForPath() destroys the session and redirects to first step in journey when a ${COMPLETED} journey exists in session`, () => {
  const destroy = jest.fn();
  const clearCookie = jest.fn();
  const redirect = jest.fn();
  const next = jest.fn();

  const req = {
    path: "/second",
    session: {
      destroy,
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
      ...buildSessionForJourney({
        journeyName: "another-journey",
        state: COMPLETED,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = {
    clearCookie,
    redirect,
  };

  handleRequestForPath(journey, undefined)(req, res, next);

  expect(destroy).toBeCalledTimes(1);
  expect(clearCookie).toBeCalledWith("lang");
  expect(clearCookie).toBeCalledTimes(1);
  expect(redirect).toBeCalledWith("/first");
  expect(redirect).toBeCalledTimes(1);
});
