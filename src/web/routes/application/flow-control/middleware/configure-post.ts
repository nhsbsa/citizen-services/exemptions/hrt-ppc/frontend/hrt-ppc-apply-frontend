import { getPreviousPath } from "../get-previous-path";
import { stateMachine } from "../state-machine";
import { IN_REVIEW } from "../states";
import { CHECK_ANSWERS_URL } from "../../paths/paths";
import { prefixPath } from "../../paths/prefix-path";

const configurePost = (steps, step, journey) => (req, res, next) => {
  const { pathPrefix } = journey;
  res.locals.errorTitleText = req.t("validation:errorTitleText");

  if (stateMachine.getState(req, journey) === IN_REVIEW) {
    res.locals.previous = prefixPath(pathPrefix, CHECK_ANSWERS_URL);
  } else {
    res.locals.previous = getPreviousPath(steps, step, req);
  }

  next();
};

export { configurePost };
