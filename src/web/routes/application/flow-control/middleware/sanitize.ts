import { map, trim, compose } from "ramda";
import escapeHtml from "escape-html";

const sanitiseInput = compose(escapeHtml, trim);
const sanitiseBody = map(sanitiseInput);

const sanitize = (req, res, next) => {
  req.body = sanitiseBody(req.body);
  next();
};

export { sanitize, sanitiseBody };
