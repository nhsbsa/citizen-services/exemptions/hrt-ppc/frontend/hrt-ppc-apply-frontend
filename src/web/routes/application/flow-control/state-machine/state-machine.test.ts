import expect from "expect";
import { partial } from "ramda";
import {
  IN_PROGRESS,
  IN_REVIEW,
  COMPLETED,
  IN_PROGRESS_COOKIES,
  COMPLETED_COOKIES,
} from "../states";
import {
  GET_NEXT_PATH,
  INVALIDATE_REVIEW,
  SET_NEXT_ALLOWED_PATH,
  INCREMENT_NEXT_ALLOWED_PATH,
} from "./actions";
import { CHECK_ANSWERS_URL, APPLICATION_COMPLETE_URL } from "../../paths/paths";
import {
  setCurrentPathInSession,
  getCurrentPathFromSession,
} from "../session-accessors";
import {
  buildSessionForJourney,
  getStateForJourney,
  getNextAllowedPathForJourney,
} from "../test-utils/test-utils";

const info = jest.fn();
const logger = { info };

jest.mock("../../../../logger/logger", () => ({
  logger,
}));

import { stateMachine } from "./state-machine";

jest.mock("../session-accessors", () => ({
  ...jest.requireActual("../session-accessors"),
  getCurrentPathFromSession: jest.fn(() => {
    /*do nothing*/
  }),
  setCurrentPathInSession: jest.fn(),
}));

const APPLY = "apply";

const APPLY_JOURNEY = {
  name: APPLY,
  steps: [
    { path: "/first", next: () => "/second" },
    { path: "/second", next: () => "/third" },
  ],
  pathsInSequence: ["/first", "/second", "/third"],
};

const getStateForApplyJourney = partial(getStateForJourney, [APPLY]);

const getNextAllowedPathForApplyJourney = partial(
  getNextAllowedPathForJourney,
  [APPLY],
);

test(`Dispatching ${GET_NEXT_PATH} should return next property of associated step when state of ${IN_PROGRESS} defined in session`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, APPLY_JOURNEY)).toBe(
    "/second",
  );
  expect(setCurrentPathInSession).toBeCalledWith(req, APPLY_JOURNEY, req.path);
});

test(`Dispatching ${GET_NEXT_PATH} should return next property of associated step when state of ${IN_PROGRESS_COOKIES} defined in session`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS_COOKIES,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, APPLY_JOURNEY)).toBe(
    "/second",
  );
  expect(setCurrentPathInSession).toBeCalledWith(req, APPLY_JOURNEY, req.path);
});

test(`Dispatching ${GET_NEXT_PATH} should return next navigable path when state of ${IN_PROGRESS} defined in session and next step is not navigable`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    path: "/second",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, APPLY_JOURNEY)).toBe(
    "/third",
  );
  expect(setCurrentPathInSession).toBeCalledWith(req, APPLY_JOURNEY, req.path);
});

test(`Dispatching ${GET_NEXT_PATH} should return next navigable path when state of ${IN_PROGRESS_COOKIES} defined in session and next step is not navigable`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS_COOKIES,
        nextAllowedPath: undefined,
      }),
    },
    path: "/second",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, APPLY_JOURNEY)).toBe(
    "/third",
  );
  expect(setCurrentPathInSession).toBeCalledWith(req, APPLY_JOURNEY, req.path);
});

test(`Dispatching ${GET_NEXT_PATH} should return ${CHECK_ANSWERS_URL} path when state of ${IN_REVIEW} defined in session`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, APPLY_JOURNEY)).toBe(
    CHECK_ANSWERS_URL,
  );
  expect(setCurrentPathInSession).toBeCalledWith(req, APPLY_JOURNEY, req.path);
});

test(`Dispatching ${GET_NEXT_PATH} should return ${APPLICATION_COMPLETE_URL} path when state of ${COMPLETED} defined in session`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: COMPLETED,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, APPLY_JOURNEY)).toBe(
    APPLICATION_COMPLETE_URL,
  );
  expect(setCurrentPathInSession).toBeCalledWith(req, APPLY_JOURNEY, req.path);
});

test(`Dispatching ${GET_NEXT_PATH} should return ${APPLICATION_COMPLETE_URL} path when state of ${COMPLETED_COOKIES} defined in session`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: COMPLETED_COOKIES,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, APPLY_JOURNEY)).toBe(
    APPLICATION_COMPLETE_URL,
  );
  expect(setCurrentPathInSession).toBeCalledWith(req, APPLY_JOURNEY, req.path);
});

test(`Dispatching an invalid action should return null when state is ${IN_PROGRESS}`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch("INVALID_ACTION", req, APPLY_JOURNEY)).toBe(
    null,
  );
  expect(setCurrentPathInSession).toBeCalledTimes(0);
});

test(`Dispatching an invalid action should return null when state is ${IN_PROGRESS_COOKIES}`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS_COOKIES,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch("INVALID_ACTION", req, APPLY_JOURNEY)).toBe(
    null,
  );
  expect(setCurrentPathInSession).toBeCalledTimes(0);
});

test(`Dispatching ${INVALIDATE_REVIEW} should set state to ${IN_PROGRESS} when state is ${IN_REVIEW}`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  stateMachine.dispatch(INVALIDATE_REVIEW, req, APPLY_JOURNEY);
  expect(getStateForApplyJourney(req)).toBe(IN_PROGRESS);
  expect(setCurrentPathInSession).toBeCalledWith(req, APPLY_JOURNEY, req.path);
});

test("setState does not log a change in state if the new state is the same as the current state", () => {
  info.mockClear();
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  stateMachine.setState(IN_PROGRESS, req, APPLY_JOURNEY);

  expect(info).toBeCalledTimes(0);
});

test("setState sets the state if the new state is different from the current state", () => {
  info.mockClear();
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    headers: {
      REQUEST_ID_HEADER: "123456",
    },
  };

  stateMachine.setState(COMPLETED, req, APPLY_JOURNEY);
  expect(getStateForApplyJourney(req)).toBe(COMPLETED);
  expect(info).toBeCalledTimes(1);
});

test(`Dispatching ${SET_NEXT_ALLOWED_PATH} sets next allowed path in session`, () => {
  const appStates = [
    { state: IN_PROGRESS, path: "/name" },
    { state: IN_PROGRESS_COOKIES, path: "/cookies-info" },
    { state: IN_REVIEW, path: "/check" },
    { state: COMPLETED, path: "/success" },
    { state: COMPLETED_COOKIES, path: "/success-cookies" },
  ];

  appStates.forEach((appState) => {
    const { state, path } = appState;
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: APPLY,
          state,
          nextAllowedPath: undefined,
        }),
      },
    };

    stateMachine.dispatch(SET_NEXT_ALLOWED_PATH, req, APPLY_JOURNEY, path);
    expect(getNextAllowedPathForApplyJourney(req)).toBe(path);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      APPLY_JOURNEY,
      undefined,
    );
  });
});

test(`Dispatching ${INCREMENT_NEXT_ALLOWED_PATH} updates next allowed path in session with the next allowed path in sequence`, () => {
  const appStates = [
    { state: IN_PROGRESS, path: "/first", expectedNextPath: "/second" },
    {
      state: IN_REVIEW,
      path: "/second",
      expectedNextPath: CHECK_ANSWERS_URL,
    },
    {
      state: COMPLETED,
      path: APPLICATION_COMPLETE_URL,
      expectedNextPath: APPLICATION_COMPLETE_URL,
    },
    { state: IN_PROGRESS_COOKIES, path: "/first", expectedNextPath: "/second" },
    {
      state: COMPLETED_COOKIES,
      path: APPLICATION_COMPLETE_URL,
      expectedNextPath: APPLICATION_COMPLETE_URL,
    },
  ];

  appStates.forEach((appState) => {
    const { state, path, expectedNextPath } = appState;
    const req = {
      path,
      session: {
        ...buildSessionForJourney({
          journeyName: APPLY,
          state,
          nextAllowedPath: undefined,
        }),
      },
    };

    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, APPLY_JOURNEY);
    expect(getNextAllowedPathForApplyJourney(req)).toBe(expectedNextPath);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      APPLY_JOURNEY,
      req.path,
    );
  });
});

test("getCurrentPath calls the function getCurrentPathFromSession", () => {
  info.mockClear();
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: APPLY,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  stateMachine.getCurrentPath(req, APPLY_JOURNEY);

  expect(getCurrentPathFromSession).toBeCalledTimes(1);
});
