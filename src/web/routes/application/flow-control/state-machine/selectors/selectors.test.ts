import expect from "expect";
import { getStepForPath, getNextInReviewPath } from "./selectors";

const CHECK_ANSWERS_URL = "/check-your-answers";

const step1 = {
  path: "/first",
};

const step2 = {
  path: "/second",
};

const step3 = {
  path: "/third",
};

test("getNextNavigablePath() gets the next step in sequence of steps", () => {
  const steps = [step1, step2, step3];
  const result = getStepForPath("/first", steps);

  expect(result).toBe(step1);
});

test(`getNextInReviewPath() should return ${CHECK_ANSWERS_URL} url`, () => {
  const req = {};
  const prefix = "/context-path";
  const result = getNextInReviewPath(req, prefix);

  expect(result).toBe(`${prefix}${CHECK_ANSWERS_URL}`);
});
