import { getNextForStep } from "./get-next-for-step";

export default {
  ...require("./selectors"),
  getNextForStep,
};
