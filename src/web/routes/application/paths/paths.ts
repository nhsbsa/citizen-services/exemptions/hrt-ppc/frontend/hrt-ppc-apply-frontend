export const CHECK_ANSWERS_URL = "/check-your-answers";
export const PROCESS_PAYMENT_RETURN_URL = "/p"; // This is set to shortest because of CPS url restriction
export const APPLICATION_COMPLETE_URL = "/application-complete";
export const PAYMENT_CANCELLED_URL = "/payment-cancelled";
export const PAYMENT_DECLINED_URL = "/payment-declined";
export const SESSION_TIMEOUT_URL = "/session-timeout";
export const DOWNLOAD_PDF_URL = "/download-pdf";
