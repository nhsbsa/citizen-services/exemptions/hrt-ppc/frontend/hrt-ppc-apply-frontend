import expect from "expect";
import { contextPath } from "./context-path";

describe("contextPath() function", () => {
  const OLD_ENV = process.env;

  beforeEach(() => {
    jest.resetModules(); // Most important - it clears the cache
    process.env = { ...OLD_ENV }; // Make a copy
  });

  afterEach(() => {
    process.env = OLD_ENV; // Restore old environment
  });

  test("adds context from environment variable if not empty", () => {
    process.env.CONTEXT_PATH = "/buy-hrt-exemption";
    expect(contextPath("/first")).toBe("/buy-hrt-exemption/first");
  });

  test("adds context from environment variable if empty", () => {
    process.env.CONTEXT_PATH = undefined;
    expect(contextPath("/first")).toBe("/first");
  });

  test('throws an error if segment does not start with "/"', () => {
    const pathResult = () => contextPath("my-journey");
    expect(pathResult).toThrowError(
      /Invalid path "my-journey". Path must be a string starting with "\/"/,
    );
  });

  test("throws an error if segment is not a string", () => {
    const pathResult = () => contextPath(true);
    expect(pathResult).toThrowError(
      /Invalid path "true". Path must be a string starting with "\/"/,
    );
  });

  test("throws an error if path is undefined", () => {
    const result = () => contextPath(undefined);
    expect(result).toThrowError(
      /Invalid path "undefined". Path must be a string starting with "\/"/,
    );
  });
});
