import expect from "expect";
import { mergeErrorMessage, mergeErrorStack, wrapError } from "./errors";

const error = {
  stack: "mock error stack",
};

const cause = {
  toString: () => "mocking Error.prototype.toString()",
  stack: "mock cause error stack",
};

const message = "Massive error!!!";

test("mergeErrorMessage()", () => {
  const result = mergeErrorMessage(message, cause);
  const expected = "Massive error!!!. mocking Error.prototype.toString()";

  expect(result).toBe(expected);
});

test("mergeErrorMessage() formats the merged message correctly when no cause is specified", () => {
  const result = mergeErrorMessage(message, undefined);
  const expected = "Massive error!!!";

  expect(result).toBe(expected);
});

test("mergeErrorStack() formats the merged stack correctly", () => {
  const result = mergeErrorStack(error, cause);
  const expected = "mock error stack\nCaused by: mock cause error stack";

  expect(result).toBe(expected);
});

test("mergeErrorStack() formats the merged stack correctly when no cause is specified", () => {
  const result = mergeErrorStack(error, undefined);
  const expected = "mock error stack";

  expect(result).toBe(expected);
});

test("wrapError() returns an error with the correct props", () => {
  const result = wrapError({ cause, message, statusCode: 500 });
  const expectedMessage =
    "Massive error!!!. mocking Error.prototype.toString()";
  const expectedCauseString = "\nCaused by: mock cause error stack";

  expect(result instanceof Error).toBe(true);
  expect(result.message).toBe(expectedMessage);
  expect(result.stack.includes(expectedCauseString)).toBe(true);
  expect(result.statusCode).toBe(500);
});

test("wrapError() returns an error with the correct props when cause is not specified", () => {
  const result = wrapError({ cause: undefined, message, statusCode: 500 });
  const expectedMessage = "Massive error!!!";
  const expectedCauseString = "\nCaused by: mock cause error stack";

  expect(result instanceof Error).toBe(true);
  expect(result.message).toBe(expectedMessage);
  expect(result.stack.includes(expectedCauseString)).toBe(false);
  expect(result.statusCode).toBe(500);
});
