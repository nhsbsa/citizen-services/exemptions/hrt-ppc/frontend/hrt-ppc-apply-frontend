import {
  isMedicineCovered,
  someMedicinesNotCovered,
  prescriptionCountryName,
  dateOfBirth,
  cookies,
  cookieDeclaration,
  cookieConfirmation,
  doYouStillWantToBuyHrtPpc,
  name,
  postcode,
  selectAddress,
  outsideEngland,
  address,
  ageEntitledToFreePrescriptions,
  existingExemptionFound,
  doYouKnowYourNhsNumber,
  medicineNotCovered,
  certificateStartDate,
  emailAddress,
} from "./steps";
import { APPLY_JOURNEY_NAME } from "../application/constants";

const CONTEXT_PATH = process.env.CONTEXT_PATH || undefined;

const APPLY = {
  name: APPLY_JOURNEY_NAME,
  endpoint: "/v4/claims",
  pathPrefix: CONTEXT_PATH,
  steps: [
    isMedicineCovered,
    someMedicinesNotCovered,
    medicineNotCovered,
    prescriptionCountryName,
    doYouStillWantToBuyHrtPpc,
    outsideEngland,
    dateOfBirth,
    ageEntitledToFreePrescriptions,
    name,
    postcode,
    selectAddress,
    address,
    existingExemptionFound,
    doYouKnowYourNhsNumber,
    certificateStartDate,
    emailAddress,
  ],
};

const COOKIE = {
  name: "cookies",
  pathPrefix: CONTEXT_PATH,
  steps: [cookies, cookieDeclaration, cookieConfirmation],
};

export const JOURNEYS = [APPLY, COOKIE];
