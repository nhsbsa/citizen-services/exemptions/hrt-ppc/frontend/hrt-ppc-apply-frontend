import express from "express";
import nocache from "nocache";

import {
  registerCheckAnswersRoutes,
  registerProcessPaymentReturnRoute,
  registerApplicationCompleteRoute,
  registerPaymentCancelled,
  registerPaymentDeclined,
  registerSessionTimeout,
  registerDownloadCertificateRoute,
} from "./steps";

import { configureGet } from "./flow-control/middleware/configure-get";
import { configurePost } from "./flow-control/middleware/configure-post";
import { configureSessionDetails } from "./flow-control/middleware/session-details";
import { checkBrowserCookie } from "./flow-control/middleware/cookies-check";
import { handlePost } from "./flow-control/middleware/handle-post";
import { handleRequestForPath } from "./flow-control/middleware/handle-path-request";
import { handlePostRedirects } from "./flow-control/middleware//handle-post-redirects";
import { injectAdditionalHeaders } from "./flow-control/middleware/inject-headers";
import { renderView } from "./flow-control/middleware/render-view";
import { sanitize } from "./flow-control/middleware/sanitize";
import { escape } from "./flow-control/middleware/unescape-specific";

const middlewareNoop = () => (req, res, next) => next();

export const handleOptionalMiddleware =
  (args) =>
  (operation, fallback = middlewareNoop) =>
    typeof operation === "undefined" ? fallback() : operation(...args);

const createRoute = (config, csrfProtection, journey, router) => (step) => {
  const { steps } = journey;
  // Make [config, journey, step] available as arguments to all optional middleware
  const optionalMiddleware = handleOptionalMiddleware([config, journey, step]);

  return router
    .route(step.path)
    .get(
      injectAdditionalHeaders,
      csrfProtection,
      configureSessionDetails(journey),
      configureGet(steps, step, journey, config),
      handleRequestForPath(journey, step),
      // Check cookies can only happen after next path check so redirect navigation works properly
      checkBrowserCookie,
      optionalMiddleware(step.behaviourForGet),
      renderView(step),
    )
    .post(
      injectAdditionalHeaders,
      csrfProtection,
      sanitize,
      escape(),
      configureSessionDetails(journey),
      configurePost(steps, step, journey),
      optionalMiddleware(step.sanitize),
      optionalMiddleware(step.validate),
      optionalMiddleware(step.behaviourForPost),
      handlePost(journey, step),
      handleRequestForPath(journey, step),
      handlePostRedirects(journey),
      renderView(step),
    );
};

export const registerJourneyRoutes =
  (config, csrfProtection, app) => (journey) => {
    const wizard = express.Router();
    journey.steps.forEach(createRoute(config, csrfProtection, journey, wizard));
    app.use(nocache());
    app.use(wizard);

    registerSessionTimeout(journey, app);
    registerCheckAnswersRoutes(journey, app, config, csrfProtection);
    registerProcessPaymentReturnRoute(journey, app, config);
    registerPaymentCancelled(journey, app);
    registerPaymentDeclined(journey, app);
    registerApplicationCompleteRoute(journey, app);
    registerDownloadCertificateRoute(journey, app);
  };
