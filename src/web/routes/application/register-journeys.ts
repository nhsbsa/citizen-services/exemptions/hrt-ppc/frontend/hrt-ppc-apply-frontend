import { doubleCsrf } from "csrf-csrf";
import { registerJourneyRoutes } from "./register-journey-routes";
import { registerSteps } from "../register-steps/register-steps";
import {
  CHECK_ANSWERS_URL,
  PROCESS_PAYMENT_RETURN_URL,
  PAYMENT_CANCELLED_URL,
  APPLICATION_COMPLETE_URL,
  PAYMENT_DECLINED_URL,
} from "./paths/paths";
import { prefixPath } from "./paths/prefix-path";

const doubleCsrfOptions = (config) => {
  const unsecureCookie = config.environment.USE_UNSECURE_COOKIE;
  const cookieSuffix = "nhsbsa.x-csrf-token";
  return doubleCsrf({
    getSecret: () => config.server.CSRF_SECRET,
    // The name of the cookie without __Host locally as can only be set on https.
    cookieName: unsecureCookie ? `${cookieSuffix}` : `__Host-${cookieSuffix}`,
    cookieOptions: {
      secure: !unsecureCookie,
    },
    getTokenFromRequest: (req) => {
      return req.body._csrf;
    },
  });
};

// contextPath has been removed from getPathsInSequence,prefixPathForStep as it is set in .env
const getPathsInSequence = (prefix, steps) => [
  ...steps.map((step) => prefixPath(prefix, step.path)),
  prefixPath(prefix, CHECK_ANSWERS_URL),
  prefixPath(prefix, PROCESS_PAYMENT_RETURN_URL),
  prefixPath(prefix, PAYMENT_CANCELLED_URL),
  prefixPath(prefix, PAYMENT_DECLINED_URL),
  prefixPath(prefix, APPLICATION_COMPLETE_URL),
];

const prefixPathForStep = (prefix) => (step) => ({
  ...step,
  path: prefixPath(prefix, step.path),
});

const registerJourney = (features) => (journey) => {
  const registeredSteps = registerSteps(features, journey.steps);
  const steps = registeredSteps.map(prefixPathForStep(journey.pathPrefix));
  const pathsInSequence = getPathsInSequence(
    journey.pathPrefix,
    registeredSteps,
  );

  return {
    ...journey,
    steps,
    pathsInSequence,
  };
};

const registerJourneys = (journeys) => (config, app) => {
  const { doubleCsrfProtection } = doubleCsrfOptions(config);

  journeys
    .map(registerJourney(config.features))
    .forEach(registerJourneyRoutes(config, doubleCsrfProtection, app));
};

export {
  prefixPath,
  getPathsInSequence,
  registerJourney,
  registerJourneys,
  doubleCsrfOptions,
};
