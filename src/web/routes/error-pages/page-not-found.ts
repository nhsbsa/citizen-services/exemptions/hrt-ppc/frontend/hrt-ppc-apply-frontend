const CONTEXT_PATH = process.env.CONTEXT_PATH || "";
import { sessionDestroy } from "../application/steps/common/session-destroy";
import { injectAdditionalHeaders } from "../application/flow-control/middleware/inject-headers";
import { CONTACT_EMAIL } from "../application/steps/common/constants";

const getPageNotFoundErrorPage = (req, res) => {
  sessionDestroy(req, res);
  injectAdditionalHeaders(req, res);

  res.render("error-pages/page-not-found", {
    title: req.t("errors:pageNotFound.title"),
    heading: req.t("errors:pageNotFound.heading"),
    paragraphOne: req.t("errors:pageNotFound.paragraphOne"),
    paragraphTwo: req.t("errors:pageNotFound.paragraphTwo"),
    paragraphThree: req.t("errors:pageNotFound.paragraphThree", {
      contactEmail: CONTACT_EMAIL,
    }),
  });
};

const registerPageNotFoundRoute = (app) => {
  app.get(`${CONTEXT_PATH}/page-not-found`, getPageNotFoundErrorPage);
  app.all("*", (req, res) => {
    res.status(404).redirect(`${CONTEXT_PATH}/page-not-found`);
  });
};

export { registerPageNotFoundRoute, getPageNotFoundErrorPage };
