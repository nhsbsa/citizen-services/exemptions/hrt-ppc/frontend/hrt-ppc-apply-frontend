import { CONTACT_EMAIL } from "../application/steps/common/constants";
const CONTEXT_PATH = process.env.CONTEXT_PATH || "";

const getProblemWithServicePage = (req, res) => {
  res.render("error-pages/problem-with-service", {
    title: req.t("errors:problemWithTheService.title"),
    heading: req.t("errors:problemWithTheService.heading"),
    paragraphOne: req.t("errors:problemWithTheService.paragraphOne"),
    paragraphTwo: req.t("errors:problemWithTheService.paragraphTwo"),
    paragraphThree: req.t("errors:problemWithTheService.paragraphThree", {
      helpEmailAddress: CONTACT_EMAIL,
    }),
  });
};

const registerProblemWithServiceError = (app) => {
  app.get(
    [`${CONTEXT_PATH}/problem-with-service`, `${CONTEXT_PATH}/timed-out`],
    getProblemWithServicePage,
  );
};

export { registerProblemWithServiceError };
