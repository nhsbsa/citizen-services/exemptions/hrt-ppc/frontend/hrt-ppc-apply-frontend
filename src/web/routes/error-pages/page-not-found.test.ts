import expect from "expect";
import {
  getPageNotFoundErrorPage,
  registerPageNotFoundRoute,
} from "./page-not-found";
import { sessionDestroy } from "../application/steps/common/session-destroy";
jest.mock("../application/steps/common/session-destroy");

import { injectAdditionalHeaders } from "../application/flow-control/middleware/inject-headers";
jest.mock("../application/flow-control/middleware/inject-headers");

const expectedPageNotFoundTranslation = {
  title: "errors:pageNotFound.title",
  heading: "errors:pageNotFound.heading",
  paragraphOne: "errors:pageNotFound.paragraphOne",
  paragraphTwo: "errors:pageNotFound.paragraphTwo",
  paragraphThree: "errors:pageNotFound.paragraphThree",
};

const PAGE_NOT_FOUND = "/test-context/page-not-found";
const STATUS_CODE = 404;

const req = {
  t: (string) => string,
};
const app = {
  ...jest.requireActual("express"),
  get: jest.fn(),
  all: jest.fn(),
};

const res = {
  redirect: jest.fn(),
  status: jest.fn().mockReturnThis(),
  render: jest.fn(),
};

test(`registerPageNotFoundRoute() should redirect to ${PAGE_NOT_FOUND} with response status as ${STATUS_CODE}`, () => {
  app.all.mockImplementation((path, callback) => {
    callback(req, res);
  });

  registerPageNotFoundRoute(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toHaveBeenNthCalledWith(1, PAGE_NOT_FOUND, expect.anything());
  expect(res.status).toBeCalledWith(STATUS_CODE);
  expect(res.redirect).toBeCalledWith(PAGE_NOT_FOUND);
});

test(`getPageNotFoundErrorPage() should call sessionDestroy function and render the page`, () => {
  getPageNotFoundErrorPage(req, res);
  expect(sessionDestroy).toHaveBeenCalledTimes(1);
  expect(injectAdditionalHeaders).toHaveBeenCalledTimes(1);
  expect(res.render).toBeCalledWith(
    "error-pages/page-not-found",
    expectedPageNotFoundTranslation,
  );
});
