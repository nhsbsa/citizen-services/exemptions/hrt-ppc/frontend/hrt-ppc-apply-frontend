const CONTEXT_PATH = process.env.CONTEXT_PATH || "";

const getNoAccessErrorPage = (req, res) => {
  res.render("error-pages/no-access", {
    title: req.t("errors:noAccess.title"),
    heading: req.t("errors:noAccess.heading"),
    paragraphOne: req.t("errors:noAccess.paragraphOne"),
    paragraphTwoLinkText: req.t("errors:noAccess.paragraphTwoLinkText"),
    paragraphTwo: req.t("errors:noAccess.paragraphTwo"),
  });
};

const registerNoAccessError = (app) => {
  app.get(`${CONTEXT_PATH}/no-access`, getNoAccessErrorPage);
};

export { registerNoAccessError };
