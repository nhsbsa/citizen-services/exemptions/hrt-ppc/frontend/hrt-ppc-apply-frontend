import { registerCookieRoutes } from "./cookies/register-cookie-routes";
import { getLanguageBase } from "./language";
import { registerHoldingRoute } from "./holding";
import { registerStartRoute } from "./start/start";
import { registerJourneys } from "./application/register-journeys";
import { JOURNEYS } from "./application/journey-definitions";
import { registerProblemWithServiceError } from "./error-pages/problem-with-service";
import { registerPageNotFoundRoute } from "./error-pages/page-not-found";
import { registerNoAccessError } from "./error-pages/no-access";
import { toPounds } from "../../common/currency";
import { config } from "../../config";

const setCommonTemplateValues = (req, res, next) => {
  res.locals.htmlLang = req.language;
  res.locals.language = getLanguageBase(req.language);
  res.locals.cookieLinkName = req.t("cookies.linkName");
  res.locals.privacyNoticeLinkName = req.t("privacyNotice.linkName");
  res.locals.footerAccessibilityLink = req.t("footer.accessibilityLink");
  res.locals.footerContactUsLink = req.t("footer.contactUsLink");
  res.locals.footerCookiesLink = req.t("footer.cookiesLink");
  res.locals.footerPrivacyLink = req.t("footer.privacyLink");
  res.locals.footerTermsLink = req.t("footer.termsLink");
  res.locals.phaseBannerHtml = req.t("phaseBanner.html");
  res.locals.back = req.t("back");
  res.locals.serviceName = req.t("header.serviceName");
  res.locals.serviceDescription = req.t("header.serviceDescription", {
    hrtPpcValue: toPounds(config.environment.HRT_PPC_VALUE),
  });
  res.locals.timeoutTitle = req.t("timeoutWarning.title");
  res.locals.timeoutTextOne = req.t("timeoutWarning.textOne");
  res.locals.timeoutTextTwo = req.t("timeoutWarning.textTwo");
  res.locals.timeoutButtonText = req.t("timeoutWarning.buttonText");
  next();
};

const registerRoutes = (config, app) => {
  app.use(setCommonTemplateValues);

  if (config.environment.MAINTENANCE_MODE) {
    registerHoldingRoute(config, app);
  } else {
    registerJourneys(JOURNEYS)(config, app);
    registerCookieRoutes(app);
    registerStartRoute(app, config);
    registerProblemWithServiceError(app);
    registerNoAccessError(app);

    // Page not found route should always be registered last as it is a catch all route
    registerPageNotFoundRoute(app);
  }
};

export { registerRoutes };
