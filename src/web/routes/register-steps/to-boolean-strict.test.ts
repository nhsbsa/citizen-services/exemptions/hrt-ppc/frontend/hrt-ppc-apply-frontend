import expect from "expect";
import { toBooleanStrict } from "./to-boolean-strict";

test("toBooleanStrict() returns correct values", () => {
  expect(toBooleanStrict("true")).toBe(true);
  expect(toBooleanStrict(true)).toBe(true);
  expect(toBooleanStrict("false")).toBe(false);
  expect(toBooleanStrict(false)).toBe(false);
  expect(toBooleanStrict(undefined)).toBe(undefined);

  expect(() => toBooleanStrict(44)).toThrowError(/Can’t coerce 44 to boolean/);
  expect(() => toBooleanStrict("this is not a boolean string")).toThrowError(
    /Can’t coerce this is not a boolean string to boolean/,
  );
  expect(() => toBooleanStrict(null)).toThrowError(
    /Can’t coerce null to boolean/,
  );
});
