import expect from "expect";
import { registerSteps } from "./register-steps";

const step1 = {
  path: "/first",
};

const step2 = {
  path: "/second",
  toggle: "STEP_2_ENABLED",
};

test("registerSteps() registers step if no toggle defined", () => {
  const steps = [step1];
  const config = {};
  const result = registerSteps(config, steps);
  const expected = [step1];
  expect(result).toEqual(expected);
});

test("registerSteps() does not register step if toggle exists for step but not in config", () => {
  const steps = [step2];
  const config = {};
  const result = registerSteps(config, steps);
  const expected = [];
  expect(result).toEqual(expected);
});

test("registerSteps() registers step if toggle exists on step and is enabled in config", () => {
  const steps = [step2];
  const config = { STEP_2_ENABLED: true };
  const result = registerSteps(config, steps);
  const expected = [step2];
  expect(result).toEqual(expected);
});

test("registerSteps() does not register step if toggle exists for step and is disabled in config", () => {
  const steps = [step2];
  const config = { STEP_2_ENABLED: false };
  const result = registerSteps(config, steps);
  const expected = [];
  expect(result).toEqual(expected);
});

test("registerSteps() throws an error if toggle is not boolean", () => {
  const steps = [step2];
  const config = { STEP_2_ENABLED: "elephant" };
  const result = () => registerSteps(config, steps);
  expect(result).toThrowError(
    /Invalid toggle config value \[STEP_2_ENABLED:elephant\] for step {"path":"\/second","toggle":"STEP_2_ENABLED"}. Error: Can’t coerce elephant to boolean/,
  );
});

test("registerSteps() throws an error if toggle key is not a string", () => {
  const invalidStep = {
    path: "/path",
    toggle: false,
  };
  const steps = [invalidStep];
  const config = { STEP_2_ENABLED: true };
  const result = () => registerSteps(config, steps);
  expect(result).toThrowError(
    /Invalid toggle for step {"path":"\/path","toggle":false}. Toggle keys must be a string/,
  );
});
