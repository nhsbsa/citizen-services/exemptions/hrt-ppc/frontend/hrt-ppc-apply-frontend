import expect from "expect";
import express from "express";
import { expectedStartPageTranslation } from "./test-constants";
import { registerStartRoute } from "./start";

const req = {
  path: "/test",
  t: (string) => string,
  session: {
    destroy: jest.fn(),
  },
};
const router = { ...jest.requireActual("express"), get: jest.fn() };
jest.spyOn(express, "Router").mockImplementationOnce(() => router);
const res = {
  redirect: jest.fn(),
  cookie: jest.fn(),
  clearCookie: jest.fn(),
  status: jest.fn().mockReturnThis(),
  render: jest.fn(),
};
const next = jest.fn();
const app = {
  use: jest.fn(),
};

beforeEach(() => {
  jest.clearAllMocks();
});

describe("Start page registerStartRoute()", () => {
  test("should destroy session and render start page content when HRT_PPC_VALUE is not set to undefined", () => {
    const config = {
      environment: {
        HRT_PPC_VALUE: 1590,
      },
    };

    router.get.mockImplementation((path, callback) => {
      callback(req, res, next);
    });

    registerStartRoute(app, config);

    expect(app.use).toBeCalledTimes(1);
    expect(router.get).toBeCalledTimes(2);
    expect(router.get).toHaveBeenNthCalledWith(
      1,
      "/test-context",
      expect.anything(),
    );
    expect(res.redirect).toBeCalledWith("/test-context/start");
    expect(router.get).toHaveBeenNthCalledWith(
      2,
      "/test-context/start",
      expect.anything(),
    );
    expect(req.session.destroy).toBeCalledWith(expect.anything());
    expect(res.clearCookie).toBeCalledWith("lang");
    expect(res.cookie).toBeCalledWith("isBrowserCookieEnabled", true, {
      httpOnly: true,
    });
    expect(res.render).toBeCalledWith("start", expectedStartPageTranslation);
  });

  test("should destroy session and throw an error when HRT_PPC_VALUE is set to undefined", () => {
    const router = { ...jest.requireActual("express"), get: jest.fn() };
    jest.spyOn(express, "Router").mockImplementationOnce(() => router);

    router.get.mockImplementation((path, callback) => {
      callback(req, res, next);
    });
    const config = {
      environment: {
        HRT_PPC_VALUE: undefined,
      },
    };

    expect(() => registerStartRoute(app, config)).toThrowError(
      "Price is not defined",
    );

    expect(app.use).toBeCalledTimes(1);
    expect(router.get).toBeCalledTimes(2);
    expect(router.get).toHaveBeenNthCalledWith(
      1,
      "/test-context",
      expect.anything(),
    );
    expect(res.redirect).toBeCalledWith("/test-context/start");
    expect(router.get).toHaveBeenNthCalledWith(
      2,
      "/test-context/start",
      expect.anything(),
    );
    expect(req.session.destroy).toBeCalledWith(expect.anything());
    expect(res.clearCookie).toBeCalledWith("lang");
    expect(res.cookie).toBeCalledWith("isBrowserCookieEnabled", true, {
      httpOnly: true,
    });
    expect(res.render).toBeCalledTimes(0);
  });
});
