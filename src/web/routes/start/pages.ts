const CONTEXT_PATH = process.env.CONTEXT_PATH || "";

module.exports.PAGES = [
  {
    title: "Hormone replacement therapy",
    path: CONTEXT_PATH + "/apply",
    template: "apply",
  },
];
