import expect from "expect";

import {
  getPageForPath,
  getNextPage,
  getPreviousPage,
  getPageMetadata,
} from "./get-page-meta-data";

const page1 = {
  title: "How it works",
  path: "/how-it-works",
};
const page2 = {
  title: "Eligibility",
  path: "/eligibility",
};
const page3 = {
  title: "What you can buy",
  path: "/what-you-can-buy",
};
const pages = [page1, page2, page3];

test("getPageForPath() should return the correct page object", () => {
  expect(getPageForPath(pages, "/how-it-works")).toEqual(page1);
  expect(getPageForPath(pages, "/eligibility")).toEqual(page2);
  expect(getPageForPath(pages, "/what-you-can-buy")).toEqual(page3);
  expect(getPageForPath(pages, "/unknown")).toBe(undefined);
});

test("getNextPage() should return the next page for the given index", () => {
  expect(getNextPage(pages, 0)).toEqual(page2);
  expect(getNextPage(pages, 1)).toEqual(page3);
  expect(getNextPage(pages, 2)).toEqual(undefined);
  expect(getNextPage(pages, 7)).toBe(undefined);
  expect(getNextPage(pages, -5)).toBe(undefined);
});

test("getPreviousPage() should return the previous page for the given index", () => {
  expect(getPreviousPage(pages, 0)).toEqual(undefined);
  expect(getPreviousPage(pages, 1)).toEqual(page1);
  expect(getPreviousPage(pages, 2)).toEqual(page2);
  expect(getPreviousPage(pages, 7)).toBe(undefined);
  expect(getPreviousPage(pages, -5)).toBe(undefined);
});

test("getPageMetadata() should return the correct metadata when has both next and previous", () => {
  const expected = {
    activePath: "/eligibility",
    previousPage: page1,
    nextPage: page3,
    title: "Eligibility",
  };
  expect(getPageMetadata(pages, "/eligibility")).toEqual(expected);
});

test("getPageMetadata() should return the correct metadata when has only next is available", () => {
  const expected = {
    activePath: "/how-it-works",
    previousPage: undefined,
    nextPage: page2,
    title: "How it works",
  };
  expect(getPageMetadata(pages, "/how-it-works")).toEqual(expected);
});

test("getPageMetadata() should return the correct metadata when has only previous", () => {
  const expected = {
    activePath: "/what-you-can-buy",
    previousPage: page2,
    nextPage: undefined,
    title: "What you can buy",
  };
  expect(getPageMetadata(pages, "/what-you-can-buy")).toEqual(expected);
});
