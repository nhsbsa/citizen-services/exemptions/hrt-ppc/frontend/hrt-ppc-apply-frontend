import * as express from "express";
import { toPounds } from "../../../common/currency";
import { logger } from "../../logger/logger";

const CONTEXT_PATH = process.env.CONTEXT_PATH || "";
const PRESCRIPTION_VALUE = Number(process.env.PRESCRIPTION_VALUE);
const START_URL = "/start";

const internationalisation = (translateFn, config) => ({
  title: translateFn("start.title"),
  heading: translateFn("start.heading"),
  calloutHeading: translateFn("start.calloutHeading"),
  calloutInnerOne: translateFn("start.calloutInnerOne"),
  calloutInnerOneLinkText: translateFn("start.calloutInnerOneLinkText"),
  calloutInnerOnePartTwo: translateFn("start.calloutInnerOnePartTwo"),
  paragraph1: translateFn("start.paragraph1"),
  paragraph2: translateFn("start.paragraph2"),
  paragraph3: translateFn("start.paragraph3", {
    ppcValue: toPounds(config.environment.HRT_PPC_VALUE),
    prescriptionValue: toPounds(PRESCRIPTION_VALUE),
  }),
  paragraph4: translateFn("start.paragraph4"),
  heading2: translateFn("start.heading2"),
  paragraph5: translateFn("start.paragraph5"),
  bulletList1: translateFn("start.bulletList1"),
  bulletList2: translateFn("start.bulletList2"),
  insetHTML: translateFn("start.insetHTML"),
  buttonText: translateFn("start.buttonText"),
  paragraph6: translateFn("start.paragraph6"),
  paragraph7: translateFn("start.paragraph7"),
});

const renderStartRoute = (app, config) => {
  const router = express.Router();
  const startUrl = CONTEXT_PATH + START_URL;
  app.use(router);
  // This will create a redirect on the route to go to the start url
  router.get(CONTEXT_PATH, (req, res) => {
    res.redirect(startUrl);
  });
  // Set-up of the start page handler
  router.get(CONTEXT_PATH + START_URL, (req, res) => {
    logger.info(`[GET] ${startUrl}`);
    req.session.destroy(sessionDestroyCallback);
    res.clearCookie("lang");

    res.cookie("isBrowserCookieEnabled", true, { httpOnly: true });

    if (config.environment.HRT_PPC_VALUE === undefined) {
      throw new Error("Price is not defined");
    } else {
      res.render("start", {
        ...internationalisation(req.t, config),
      });
    }
  });
};

const sessionDestroyCallback = () => {
  logger.debug("Session has been destroyed");
};

const registerStartRoute = (app, config) => renderStartRoute(app, config);

export { registerStartRoute };
