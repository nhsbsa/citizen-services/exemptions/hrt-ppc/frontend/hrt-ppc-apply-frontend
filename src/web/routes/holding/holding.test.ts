import expect from "expect";
import { formatDate, getServiceAvailableFromMessage } from "./holding";

const translate = (input) => {
  if (input === "holding.useServiceFrom") {
    return "You will be able to use the service from";
  } else if (input === "holding.tryAgainLater") {
    return "Please try again later.";
  }
};

test("formatDate returns undefined when no date is provided", () => {
  const result = formatDate(undefined);

  expect(result).toBe(undefined);
});

test("formatDate returns undefined when empty date is provided", () => {
  const result = formatDate("");

  expect(result).toBe(undefined);
});

test("formatDate returns undefined when invalid date is provided", () => {
  const result = formatDate("10:10 40/15/2019");

  expect(result).toBe(undefined);
});

test("formatDate returns undefined when date is not in the format ’HH:mm DD/MM/YYYY’", () => {
  const result = formatDate("03/04/2019");

  expect(result).toBe(undefined);
});

test("formatDate returns formatted date when correct date is given", () => {
  const result = formatDate("14:30 03/04/2019");

  expect(result).toBe("02:30 pm, Wednesday 03 April 2019");
});

test("formatDate returns formatted date in Welsh", () => {
  const result = formatDate("14:30 03/04/2019", "cy");

  expect(result).toBe("02:30 pm, Dydd Mercher 03 Ebrill 2019");
});

test("getServiceAvailableFromMessage returns message with date when date is given", () => {
  const result = getServiceAvailableFromMessage(
    "02:30 pm, Wednesday 03 April 2019",
    translate,
  );

  expect(result).toEqual(
    "You will be able to use the service from 02:30 pm, Wednesday 03 April 2019.",
  );
});

test("getServiceAvailableFromMessage returns try again message when no date is given", () => {
  const result = getServiceAvailableFromMessage(undefined, translate);

  expect(result).toEqual("Please try again later.");
});
