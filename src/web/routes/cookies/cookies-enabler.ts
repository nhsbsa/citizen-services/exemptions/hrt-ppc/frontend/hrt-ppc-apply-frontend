const getCookiesEnablerPage = (req, res) => {
  res.render("cookies-enabler", {
    title: req.t("errors:cookieEnabler.title"),
    heading: req.t("errors:cookieEnabler.heading"),
    paragraphOne: req.t("errors:cookieEnabler.paragraphOne"),
    paragraphTwo: req.t("errors:cookieEnabler.paragraphTwo"),
    insetHTML: req.t("errors:cookieEnabler.insetHTML"),
    paragraphThree: req.t("errors:cookieEnabler.paragraphThree"),
    linkText: req.t("errors:cookieEnabler.linkText"),
  });
};

export { getCookiesEnablerPage };
