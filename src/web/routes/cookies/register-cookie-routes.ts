import { getCookiesEnablerPage } from "./cookies-enabler";
const CONTEXT_PATH = process.env.CONTEXT_PATH || "";

const registerCookieRoutes = (app) => {
  app.get(`${CONTEXT_PATH}/enable-cookies`, getCookiesEnablerPage);
};

export { registerCookieRoutes };
