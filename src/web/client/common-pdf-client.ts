import axios, { AxiosInstance, AxiosResponse } from "axios";
import { addRequestAndLoggingInterceptors } from "./interceptors";

export class pdfClient {
  apiInstance: AxiosInstance;

  constructor(appConfig) {
    this.apiInstance = axios.create({
      proxy: false,
      timeout: parseInt(appConfig.environment.OUTBOUND_API_TIMEOUT || "30000"),
      baseURL: appConfig.environment.COMMON_PDF_API_URI,
      headers: {
        common: {
          "x-api-key": appConfig.environment.COMMON_PDF_API_KEY,
        },
      },
    });
    addRequestAndLoggingInterceptors(appConfig, this.apiInstance);
  }

  public async makeRequest(config) {
    const data: AxiosResponse<PdfResponse, any> =
      await this.apiInstance.request(config);

    return data;
  }
}
