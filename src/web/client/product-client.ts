import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { addRequestAndLoggingInterceptors } from "./interceptors";

export class productClient {
  apiInstance: AxiosInstance;

  constructor(appConfig) {
    this.apiInstance = axios.create({
      proxy: false,
      timeout: parseInt(appConfig.environment.OUTBOUND_API_TIMEOUT || "30000"),
      baseURL: appConfig.environment.PRODUCT_API_URI,
      headers: {
        common: {
          "x-api-key": appConfig.environment.PRODUCT_API_KEY,
        },
      },
    });
    addRequestAndLoggingInterceptors(appConfig, this.apiInstance);
  }

  public async makeRequest(config: AxiosRequestConfig) {
    const data = await this.apiInstance.request(config);
    return data;
  }
}
