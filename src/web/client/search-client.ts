import axios, { AxiosInstance, AxiosResponse } from "axios";
import { USERID } from "./constants";
import { addRequestAndLoggingInterceptors } from "./interceptors";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";

export class searchClient {
  apiInstance: AxiosInstance;

  constructor(appConfig) {
    this.apiInstance = axios.create({
      proxy: false,
      timeout: parseInt(appConfig.environment.OUTBOUND_API_TIMEOUT || "30000"),
      baseURL: appConfig.environment.SEARCH_API_URI,
      headers: {
        common: {
          "x-api-key": appConfig.environment.SEARCH_API_KEY,
          channel: Channel.ONLINE,
          "user-id": USERID,
        },
      },
    });
    addRequestAndLoggingInterceptors(appConfig, this.apiInstance);
  }

  public async makeRequest(config, req) {
    config.headers = {
      "correlation-id": req.session.locator,
    };

    const data: AxiosResponse<Exemptions, any> =
      await this.apiInstance.request(config);

    return data;
  }
}
