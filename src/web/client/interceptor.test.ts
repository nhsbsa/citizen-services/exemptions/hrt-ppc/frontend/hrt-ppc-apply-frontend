import expect from "expect";
import * as InterceptorLoggers from "./interceptors-loggers";
const appConfig = {
  environment: {
    LOG_REQUESTS_AND_RESPONSES: false,
  },
};

jest.mock("axios-logger", () => ({
  setGlobalConfig: jest.fn(),
}));

const spyResponseLogger = jest.spyOn(InterceptorLoggers, "responseLogger");
const spyErrorLogger = jest.spyOn(InterceptorLoggers, "errorLogger");

const instance = {
  interceptors: {
    request: {
      use: jest.fn(),
    },
    response: {
      use: jest.fn(),
    },
  },
};

import * as AxiosLogger from "axios-logger";
import { addRequestAndLoggingInterceptors } from "./interceptors";

describe("addRequestAndLoggingInterceptors()", () => {
  test("should set the interceptors to instance when LOG_REQUESTS_AND_RESPONSES set to true", () => {
    appConfig.environment.LOG_REQUESTS_AND_RESPONSES = true;

    addRequestAndLoggingInterceptors(appConfig, instance);

    expect(instance.interceptors.request.use).toBeCalledTimes(1);
    expect(instance.interceptors.request.use).toHaveBeenCalledWith(
      AxiosLogger.requestLogger,
    );
    expect(instance.interceptors.response.use).toBeCalledTimes(1);
    expect(instance.interceptors.response.use).toHaveBeenCalledWith(
      expect.any(Function),
      expect.any(Function),
    );
    expect(spyResponseLogger).toHaveBeenCalledTimes(1);
    expect(spyErrorLogger).toHaveBeenCalledTimes(1);
  });

  test("should not set the interceptors to instance when LOG_REQUESTS_AND_RESPONSES set to false", () => {
    appConfig.environment.LOG_REQUESTS_AND_RESPONSES = false;

    addRequestAndLoggingInterceptors(appConfig, instance);

    expect(instance.interceptors.request.use).toBeCalledTimes(0);
    expect(instance.interceptors.response.use).toBeCalledTimes(0);
    expect(spyResponseLogger).not.toHaveBeenCalled();
    expect(spyErrorLogger).not.toHaveBeenCalled();
  });
});
