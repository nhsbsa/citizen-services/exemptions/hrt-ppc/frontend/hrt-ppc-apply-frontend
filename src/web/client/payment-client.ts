import axios, { AxiosInstance } from "axios";
import { USERID } from "./constants";
import { logger } from "../logger/logger";
import { addRequestAndLoggingInterceptors } from "./interceptors";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";

export class paymentClient {
  apiInstance: AxiosInstance;

  constructor(appConfig) {
    this.apiInstance = axios.create({
      proxy: false,
      timeout: parseInt(appConfig.environment.OUTBOUND_API_TIMEOUT || "30000"),
      baseURL: appConfig.environment.PAYMENT_API_URI,
      headers: {
        common: {
          "x-api-key": appConfig.environment.PAYMENT_API_KEY,
          channel: Channel.ONLINE,
          "user-id": USERID,
        },
      },
    });
    addRequestAndLoggingInterceptors(appConfig, this.apiInstance);
  }

  public async makeRequest(config, req) {
    config.headers = {
      "correlation-id": req.session.locator,
    };

    const data = await this.apiInstance.request(config);

    logger.info(
      `Created payment with id: ${data.data["id"]}, transactionId: ${data.data["transactionId"]}`,
      req,
    );

    return data;
  }
}
