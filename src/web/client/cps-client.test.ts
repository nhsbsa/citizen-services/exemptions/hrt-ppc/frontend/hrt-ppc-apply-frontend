import expect from "expect";

const appConfig = {
  environment: {
    LOG_REQUESTS_AND_RESPONSES: true,
    OUTBOUND_API_TIMEOUT: 20000,
    CARD_PAYMENTS_API_URI: "baseURI",
    CARD_PAYMENTS_JWT_SECRET: "test-secret",
    CARD_PAYMENTS_SERVICE_NAME: "hrtppconline",
  },
};

const mockAddRequestAndLoggingInterceptors = jest.fn();
jest.mock("./interceptors", () => ({
  addRequestAndLoggingInterceptors: mockAddRequestAndLoggingInterceptors,
}));

import { cardPaymentsClient } from "./cps-client";
describe("cardPaymentsClient", () => {
  test("should set the correct configuration for apiInstance", () => {
    const { apiInstance } = new cardPaymentsClient(appConfig);
    expect(apiInstance.defaults.timeout).toEqual(20000);
    expect(apiInstance.defaults.proxy).toEqual(false);
    expect(apiInstance.defaults.baseURL).toEqual("baseURI/hrtppconline");
    expect(apiInstance.defaults.headers.common).toEqual({
      Accept: "application/json, text/plain, */*",
    });
    expect(mockAddRequestAndLoggingInterceptors).toHaveBeenCalledTimes(1);
    expect(mockAddRequestAndLoggingInterceptors).toHaveBeenCalledWith(
      appConfig,
      apiInstance,
    );
  });

  test("should set the correct default timeout for apiInstance", () => {
    const config = {
      environment: {},
    };
    const { apiInstance } = new cardPaymentsClient(config);
    expect(apiInstance.defaults.timeout).toEqual(30000);
  });

  test("should return the data into the response when creating payment", async () => {
    const api = new cardPaymentsClient(appConfig);
    api.apiInstance.request = jest.fn().mockResolvedValue({ data: "data" });
    const response = await api.makeRequestCreatePayment({});
    expect(response).toEqual({ data: "data" });
  });

  test("should return the data into the response when retrieving payment", async () => {
    const api = new cardPaymentsClient(appConfig);
    api.apiInstance.request = jest.fn().mockResolvedValue({ data: "data" });
    const response = await api.makeRequestRetrievePayment({});
    expect(response).toEqual({ data: "data" });
  });
});
