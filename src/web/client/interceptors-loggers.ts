import * as AxiosLogger from "axios-logger";
import { logger } from "../logger/logger";
import {
  CONTENT_TYPE_HEADER,
  CONTENT_TYPE_PDF,
  RESPONSE_TYPE_STREAM,
} from "./constants";

export const responseLogger = () => {
  return (response, config) => {
    const contentType = response.headers[CONTENT_TYPE_HEADER];
    if (
      contentType &&
      !contentType.includes(CONTENT_TYPE_PDF) &&
      response.config.responseType !== RESPONSE_TYPE_STREAM
    ) {
      return AxiosLogger.responseLogger(response, config);
    }
    logger.debug(
      "Response type is expected to be stream so not using axios logger",
    );
    return response;
  };
};

export const errorLogger = () => {
  return (error) => {
    const responseType = error.config.responseType;
    if (responseType !== RESPONSE_TYPE_STREAM) {
      return AxiosLogger.errorLogger(error);
    }
    logger.error(
      `Error expected response type stream [status: ${error.response.data.statusCode}, responseUrl: ${error.response.data.responseUrl}, error: ${error}]`,
    );

    return error;
  };
};
