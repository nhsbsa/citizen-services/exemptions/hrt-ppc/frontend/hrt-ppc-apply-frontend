// eslint-disable-next-line @typescript-eslint/no-unused-vars
type PdfResponse = {
  status: number;
  errorInfo: string;
  fieldLevelErrorMessage?: Array<FieldError>;
};

type FieldError = {
  fieldName: string;
  rejectedValue: string;
  messageError: string;
};
