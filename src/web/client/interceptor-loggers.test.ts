import expect from "expect";
import { CONTENT_TYPE_PDF, RESPONSE_TYPE_STREAM } from "./constants";

const mockResponseLogger = jest.fn();
const mockErrorLogger = jest.fn();
jest.mock("axios-logger", () => ({
  responseLogger: mockResponseLogger,
  errorLogger: mockErrorLogger,
  setGlobalConfig: jest.fn(),
}));

import { responseLogger, errorLogger } from "./interceptors-loggers";

describe("responseLogger()", () => {
  test("should call AxiosLogger when content-type is not application/pdf and resonseType is not Stream", () => {
    const response = {
      headers: {
        "content-type": "application/json",
      },
      config: {
        responseType: "json",
      },
    };
    const config = {
      baseURL: "http://test.com",
    };

    responseLogger()(response, config);

    expect(mockResponseLogger).toBeCalledTimes(1);
    expect(mockResponseLogger).toBeCalledWith(response, config);
    expect(mockErrorLogger).toBeCalledTimes(0);
  });

  test("should not call AxiosLogger when content-type is not defined", () => {
    const response = {
      headers: {
        "content-type": undefined,
      },
    };
    const config = {};

    const actual = responseLogger()(response, config);

    expect(mockResponseLogger).toBeCalledTimes(0);
    expect(actual).toEqual(response);
    expect(mockErrorLogger).toBeCalledTimes(0);
  });

  test("should not call AxiosLogger when content-type is application/pdf and responseType is stream", () => {
    const response = {
      headers: {
        "content-type": CONTENT_TYPE_PDF,
      },
      config: {
        responseType: "json",
      },
    };
    const config = {};

    const actual = responseLogger()(response, config);

    expect(mockResponseLogger).toBeCalledTimes(0);
    expect(actual).toEqual(response);
    expect(mockErrorLogger).toBeCalledTimes(0);
  });

  test("should not call AxiosLogger when content-type is not application/pdf and responseType is stream", () => {
    const response = {
      headers: {
        "content-type": "application/json",
      },
      config: {
        responseType: "stream",
      },
    };
    const config = {};

    const actual = responseLogger()(response, config);

    expect(mockResponseLogger).toBeCalledTimes(0);
    expect(actual).toEqual(response);
    expect(mockErrorLogger).toBeCalledTimes(0);
  });
});

describe("errorLogger()", () => {
  test("should call AxiosLogger when resonseType is not Stream", () => {
    const error = {
      config: {
        responseType: "json",
      },
    };

    errorLogger()(error);

    expect(mockErrorLogger).toBeCalledTimes(1);
    expect(mockErrorLogger).toBeCalledWith(error);
    expect(mockResponseLogger).toBeCalledTimes(0);
  });

  test("should call AxiosLogger when resonseType is not defined", () => {
    const error = {
      config: {
        responseType: undefined,
      },
    };

    const actual = errorLogger()(error);

    expect(mockErrorLogger).toBeCalledTimes(1);
    expect(actual).toEqual(undefined);
    expect(mockResponseLogger).toBeCalledTimes(0);
  });

  test("should not call AxiosLogger when resonseType is Stream", () => {
    const error = {
      config: {
        responseType: RESPONSE_TYPE_STREAM,
      },
      response: {
        data: {
          statusCode: 400,
          responseUrl: "/mockUrl",
        },
      },
    };

    const actual = errorLogger()(error);

    expect(mockErrorLogger).toBeCalledTimes(0);
    expect(actual).toEqual(error);
    expect(mockResponseLogger).toBeCalledTimes(0);
  });
});
