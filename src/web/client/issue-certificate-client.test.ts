import expect from "expect";
import { USERID } from "./constants";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";

const appConfig = {
  environment: {
    LOG_REQUESTS_AND_RESPONSES: true,
    OUTBOUND_API_TIMEOUT: 20000,
    ISSUE_CERTIFICATE_API_URI: "baseURI",
    ISSUE_CERTIFICATE_API_KEY: "test-key",
  },
};

const mockAddRequestAndLoggingInterceptors = jest.fn();
jest.mock("./interceptors", () => ({
  addRequestAndLoggingInterceptors: mockAddRequestAndLoggingInterceptors,
}));

import { issueCertificateClient } from "./issue-certificate-client";

describe("issueCertificateClient()", () => {
  test("should set the correct configuration for apiInstance", () => {
    const { apiInstance } = new issueCertificateClient(appConfig);
    expect(apiInstance.defaults.timeout).toEqual(20000);
    expect(apiInstance.defaults.proxy).toEqual(false);
    expect(apiInstance.defaults.baseURL).toEqual("baseURI");
    expect(apiInstance.defaults.headers.common).toEqual({
      Accept: "application/json, text/plain, */*",
      "x-api-key": "test-key",
      channel: Channel.ONLINE,
      "user-id": USERID,
    });
    expect(mockAddRequestAndLoggingInterceptors).toHaveBeenCalledTimes(1);
    expect(mockAddRequestAndLoggingInterceptors).toHaveBeenCalledWith(
      appConfig,
      apiInstance,
    );
  });

  test("should set the correct default timeout for apiInstance", () => {
    const appConfig = {
      environment: {},
    };
    const { apiInstance } = new issueCertificateClient(appConfig);
    expect(apiInstance.defaults.timeout).toEqual(30000);
  });

  test("should return the data into the response", async () => {
    const api = new issueCertificateClient(appConfig);
    const config = {};
    const returnedData = {
      certificate: {
        id: "testId",
        citizenId: "testCitizenId",
        reference: "testReference",
      },
    };
    api.apiInstance.request = jest
      .fn()
      .mockResolvedValue({ data: returnedData });
    const response = await api.makeRequest(config, {
      session: { locator: "test" },
    });
    expect(config).toEqual({ headers: { "correlation-id": "test" } });
    expect(response).toEqual({ data: returnedData });
  });
});
