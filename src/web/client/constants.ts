export const USERID: string = "ONLINE";
export const CONTENT_TYPE_PDF: string = "application/pdf";
export const CONTENT_TYPE_HEADER: string = "content-type";
export const RESPONSE_TYPE_STREAM: string = "stream";
