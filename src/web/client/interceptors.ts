import * as AxiosLogger from "axios-logger";
import { logger } from "../logger/logger";
import { errorLogger, responseLogger } from "./interceptors-loggers";

export const addRequestAndLoggingInterceptors = (appConfig, apiInstance) => {
  if (appConfig.environment.LOG_REQUESTS_AND_RESPONSES === true) {
    logger.warn("Logging request and response interceptors enabled");
    AxiosLogger.setGlobalConfig({
      params: true,
      logger: logger.debug.bind(this),
    });

    apiInstance.interceptors.request.use(AxiosLogger.requestLogger);
    apiInstance.interceptors.response.use(responseLogger(), errorLogger());
  }
};
