import axios, { AxiosInstance, AxiosResponse } from "axios";
import {
  CreatePaymentResponse,
  RetrievePaymentResponse,
} from "./cps-payment-types";
import { addRequestAndLoggingInterceptors } from "./interceptors";

export class cardPaymentsClient {
  apiInstance: AxiosInstance;

  constructor(appConfig) {
    this.apiInstance = axios.create({
      proxy: false,
      timeout: parseInt(appConfig.environment.OUTBOUND_API_TIMEOUT || "30000"),
      baseURL:
        appConfig.environment.CARD_PAYMENTS_API_URI +
        "/" +
        appConfig.environment.CARD_PAYMENTS_SERVICE_NAME,
    });
    addRequestAndLoggingInterceptors(appConfig, this.apiInstance);
  }

  public async makeRequestCreatePayment(config) {
    const data: AxiosResponse<CreatePaymentResponse, any> =
      await this.apiInstance.request(config);

    return data;
  }

  public async makeRequestRetrievePayment(config) {
    const data: AxiosResponse<RetrievePaymentResponse, any> =
      await this.apiInstance.request(config);

    return data;
  }
}
