import expect from "expect";

const appConfig = {
  environment: {
    LOG_REQUESTS_AND_RESPONSES: true,
    OUTBOUND_API_TIMEOUT: 20000,
    COMMON_PDF_API_URI: "baseURI",
    COMMON_PDF_API_KEY: "test-key",
  },
};

const mockAddRequestAndLoggingInterceptors = jest.fn();
jest.mock("./interceptors", () => ({
  addRequestAndLoggingInterceptors: mockAddRequestAndLoggingInterceptors,
}));

import { pdfClient } from "./common-pdf-client";

describe("pdfClient()", () => {
  test("should set the correct configuration for apiInstance", () => {
    const { apiInstance } = new pdfClient(appConfig);
    expect(apiInstance.defaults.timeout).toEqual(20000);
    expect(apiInstance.defaults.proxy).toEqual(false);
    expect(apiInstance.defaults.baseURL).toEqual("baseURI");
    expect(apiInstance.defaults.headers.common).toEqual({
      Accept: "application/json, text/plain, */*",
      "x-api-key": "test-key",
    });
    expect(mockAddRequestAndLoggingInterceptors).toHaveBeenCalledTimes(1);
    expect(mockAddRequestAndLoggingInterceptors).toHaveBeenCalledWith(
      appConfig,
      apiInstance,
    );
  });

  test("should set the correct default timeout for apiInstance", () => {
    const appConfig = {
      environment: {},
    };
    const { apiInstance } = new pdfClient(appConfig);
    expect(apiInstance.defaults.timeout).toEqual(30000);
  });

  test("should return the data into the response", async () => {
    const api = new pdfClient(appConfig);
    const config = {};
    const returnedData = {
      data: "data",
    };
    api.apiInstance.request = jest
      .fn()
      .mockResolvedValue({ data: returnedData });
    const response = await api.makeRequest(config);
    expect(response).toEqual({ data: returnedData });
  });
});
