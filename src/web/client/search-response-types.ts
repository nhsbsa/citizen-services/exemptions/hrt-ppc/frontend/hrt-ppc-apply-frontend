// eslint-disable-next-line @typescript-eslint/no-unused-vars
type Exemptions = {
  exemptions: {
    "hrt-ppcs": Array<Exemption>;
    others: Array<Exemption>;
  };
};

type Exemption = {
  citizens: Array<Citizen>;
};

type Address = {
  id: string;
  addressLine1: string;
  addressLine2?: string;
  townOrCity: string;
  country?: Date;
  postcode: Date;
  type: Date;
};

type Citizen = {
  id?: string;
  firstName: string;
  lastName: string;
  dateOfBirth: Date;
  addresses?: Array<Address>;
  certificates: Array<Certificate>;
};

type Certificate = {
  id?: string;
  reference: string;
  type: string;
  status: string;
  startDate: Date;
  endDate: Date;
};
