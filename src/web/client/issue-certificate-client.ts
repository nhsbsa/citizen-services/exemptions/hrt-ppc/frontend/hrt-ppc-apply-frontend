import axios, { AxiosInstance } from "axios";
import { logger } from "../logger/logger";
import { USERID } from "./constants";
import { addRequestAndLoggingInterceptors } from "./interceptors";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";

export class issueCertificateClient {
  apiInstance: AxiosInstance;

  constructor(appConfig) {
    this.apiInstance = axios.create({
      proxy: false,
      timeout: parseInt(appConfig.environment.OUTBOUND_API_TIMEOUT || "30000"),
      baseURL: appConfig.environment.ISSUE_CERTIFICATE_API_URI,
      headers: {
        common: {
          "x-api-key": appConfig.environment.ISSUE_CERTIFICATE_API_KEY,
          channel: Channel.ONLINE,
          "user-id": USERID,
        },
      },
    });
    addRequestAndLoggingInterceptors(appConfig, this.apiInstance);
  }

  public async makeRequest(config, req) {
    config.headers = {
      "correlation-id": req.session.locator,
    };

    const data = await this.apiInstance.request(config);

    logger.info(
      `Created citizen with citizenId: ${data.data.certificate["citizenId"]}`,
      req,
    );
    logger.info(
      `Created certificate with certificateId: ${data.data.certificate["id"]}, certificateReference: ${data.data.certificate["reference"]}`,
      req,
    );

    return data;
  }
}
