import { CpsStatus } from "./cps-payment-status";

export type CreatePaymentResponse = {
  nextUrl: string;
};

export type RetrievePaymentResponse = {
  reference: string;
  transactionId: string;
  status: CpsStatus;
  amount: string;
  donePageURL: string;
};
