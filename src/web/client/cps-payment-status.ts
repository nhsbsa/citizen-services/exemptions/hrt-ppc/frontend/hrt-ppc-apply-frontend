// eslint-disable-next-line @typescript-eslint/no-unused-vars
export enum CpsStatus {
  SUCCESS = "SUCCESS",
  CANCELLED = "CANCELLED", // Cancelled by the Payment Provider
  FAILED_PAYMENT_METHOD_REJECTED = "FAILED_PAYMENT_METHOD_REJECTED",
  FAILED_PAYMENT_CANCELLED_BY_USER = "FAILED_PAYMENT_CANCELLED_BY_USER",
  FAILED_PAYMENT_EXPIRED = "FAILED_PAYMENT_EXPIRED", // Expires after no action from the user after 90 minutes
  ERROR = "ERROR", // Error on GovPays end
  ERROR_PAYMENT_NOT_FOUND = "ERROR_PAYMENT_NOT_FOUND", // This shouldnt happen, holding payment details that GovUkPay doesnt recognise
  ERROR_PAYMENT_NOT_FINISHED = "ERROR_PAYMENT_NOT_FINISHED", // This shouldn’t happen, but payment is older than 90 minutes but GovUkPay still has it in started status
  UNKNOWN = "UNKNOWN", // Payment provider responded with an unexpected status
}
