import expect from "expect";
import { logFormatter } from "./format";

test("logFormatter() formats log correctly if IDs are not available", () => {
  const req = {};
  const level = "info";
  const message = "log message";
  const timestamp = "2019-02-14";

  const result = logFormatter({ level, message, timestamp, req });
  const expected = "2019-02-14 INFO [][] log message";

  expect(result).toBe(expected);
});

test("logFormatter() formats log correctly if IDs are available", () => {
  const req = {
    headers: {
      "x-request-id": "1234",
    },
    session: {
      locator: "4567",
    },
  };
  const level = "info";
  const message = "log message";
  const timestamp = "2019-02-14";

  const result = logFormatter({ level, message, timestamp, req });
  const expected = "2019-02-14 INFO [4567][1234] log message";

  expect(result).toBe(expected);
});
