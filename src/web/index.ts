// This line must come before importing any instrumented module.
import { config } from "../config";
import * as ddtrace from "dd-trace";
if (config.environment.DATADOG_APM_ENABLED === true) {
  ddtrace.default.init({});
}
import express from "express";
import server from "./server";

const app = express();

server.initialise(config, app);
