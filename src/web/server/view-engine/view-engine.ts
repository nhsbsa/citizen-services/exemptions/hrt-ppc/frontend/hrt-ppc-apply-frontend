import * as nunjucks from "nunjucks";

import { camelToKebabCase, toErrorList, getErrorForField } from "./filters";

const setViewEngine = (config, app) => {
  const env = nunjucks.configure(
    [
      "src/web/views",
      "node_modules/nhsuk-frontend/",
      "node_modules/govuk-frontend/",
    ],
    {
      autoescape: true,
      express: app,
      noCache: config.server.NO_CACHE_VIEW_TEMPLATES,
    },
  );

  env.addFilter("camelToKebabCase", camelToKebabCase);
  env.addFilter("toErrorList", toErrorList);
  env.addFilter("getErrorForField", getErrorForField);

  env.addGlobal("gaTrackingId", config.environment.GA_TRACKING_ID);
  env.addGlobal("contextPath", config.server.CONTEXT_PATH);
  env.addGlobal("appVersion", config.server.APP_VERSION);

  const minutesBeforeTimeoutModal =
    config.environment.SESSION_TIMEOUT_MINUTES -
    config.environment.TIMEOUT_WARNING_MINUTES;

  env.addGlobal("minutesBeforeTimeoutModal", minutesBeforeTimeoutModal);
  env.addGlobal(
    "timeoutWarningDuration",
    config.environment.TIMEOUT_WARNING_MINUTES,
  );
  env.addGlobal("appBaseUrl", config.environment.APP_BASE_URL);

  app.set("view engine", "njk");
};

export { setViewEngine };
