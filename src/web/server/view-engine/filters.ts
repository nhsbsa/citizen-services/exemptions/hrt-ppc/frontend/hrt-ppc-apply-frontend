import { compose, map, values, isNil } from "ramda";
import { ErrorObject } from "../../routes/application/steps/common/types";

const camelToKebabCase = (string) =>
  string.replace(/([a-z0-9])([A-Z0-9])/g, "$1-$2").toLowerCase();

const customHashToError = (param) => {
  const customHashError = {
    dateOfBirth: "date-of-birth-day",
    prescriptionCountryName: "where-you-collect-your-prescriptions-1",
    doYouStillWantToBuyCountry: "do-you-still-want-to-buy-country-1",
    selectedAddress: "address-results",
    isMedicineCovered: "medicine-covered-1",
    doYouStillWantToBuySomeMedicineNotCovered:
      "do-you-still-want-to-buy-some-medicine-not-covered-1",
    doYouKnowYourNhsNumber: "do-you-know-your-nhs-number-1",
    doYouWantEmail: "do-you-want-hrt-ppc-by-email-1",
    prescriptionStartFromToday: "when-do-you-want-your-hrt-ppc-to-start-1",
    certificateStartDate: "certificate-start-date-day",
    certificateBackDate: "certificate-back-date-day",
  };
  for (const key in customHashError) {
    if (key === param) {
      return customHashError[key];
    }
  }
  return param;
};

const toError = (error): ErrorObject => ({
  text: error.msg,
  href: `#${camelToKebabCase(customHashToError(error.path))}`,
  attributes: {
    id: `error-link-${camelToKebabCase(error.path)}`,
  },
});

const toErrorList = compose(values, map(toError));

const getErrorByPath = (errors, field) =>
  isNil(errors) ? null : errors.find((error) => error.path === field);

const getErrorForField = (errors, field) => {
  const error = getErrorByPath(errors, field);

  return error
    ? {
        text: error.msg,
      }
    : null;
};
export { toErrorList, getErrorForField, camelToKebabCase };
