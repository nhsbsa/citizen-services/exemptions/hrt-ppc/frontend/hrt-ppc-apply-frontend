import { camelToKebabCase, getErrorForField, toErrorList } from "./filters";
import { setViewEngine } from "./view-engine";

const config = {
  server: {
    NO_CACHE_VIEW_TEMPLATES: false,
    CONTEXT_PATH: "test-context",
    APP_VERSION: "test",
  },
  environment: {
    GA_TRACKING_ID: "GAid",
    SESSION_TIMEOUT_MINUTES: 30,
    TIMEOUT_WARNING_MINUTES: 5,
    APP_BASE_URL: "test-url",
  },
};
const app = { set: jest.fn() };
const mockFilter = jest.fn();
const mockGlobal = jest.fn();

const mockConfigure = {
  addFilter: mockFilter,
  addGlobal: mockGlobal,
};

jest.mock("nunjucks", () => ({
  configure: () => mockConfigure,
}));

afterEach(() => {
  jest.resetAllMocks();
});
describe("setViewEngine() variable unit tests", () => {
  test("should set correct filters", () => {
    setViewEngine(config, app);

    expect(mockFilter).toBeCalledTimes(3);
    expect(mockFilter).nthCalledWith(1, "camelToKebabCase", camelToKebabCase);
    expect(mockFilter).nthCalledWith(2, "toErrorList", toErrorList);
    expect(mockFilter).nthCalledWith(3, "getErrorForField", getErrorForField);
  });

  test("should set correct global variables", () => {
    setViewEngine(config, app);

    expect(mockGlobal).toBeCalledTimes(6);
    expect(mockGlobal).nthCalledWith(
      1,
      "gaTrackingId",
      config.environment.GA_TRACKING_ID,
    );
    expect(mockGlobal).nthCalledWith(
      2,
      "contextPath",
      config.server.CONTEXT_PATH,
    );
    expect(mockGlobal).nthCalledWith(
      3,
      "appVersion",
      config.server.APP_VERSION,
    );
    const minutesBeforeTimeoutModal =
      config.environment.SESSION_TIMEOUT_MINUTES -
      config.environment.TIMEOUT_WARNING_MINUTES;
    expect(mockGlobal).nthCalledWith(
      4,
      "minutesBeforeTimeoutModal",
      minutesBeforeTimeoutModal,
    );
    expect(mockGlobal).nthCalledWith(
      5,
      "timeoutWarningDuration",
      config.environment.TIMEOUT_WARNING_MINUTES,
    );
    expect(mockGlobal).nthCalledWith(
      6,
      "appBaseUrl",
      config.environment.APP_BASE_URL,
    );
  });

  test("should set view engine to nunjucks - njk", () => {
    setViewEngine(config, app);

    expect(app.set).toBeCalledTimes(1);
    expect(app.set).toBeCalledWith("view engine", "njk");
  });
});
