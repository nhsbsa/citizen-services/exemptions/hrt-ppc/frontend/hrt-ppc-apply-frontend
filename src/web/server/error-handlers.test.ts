import expect from "expect";
import * as handler from "./error-handlers";

test("Error handler should set the status to 500", () => {
  const status = jest.fn();
  const redirect = jest.fn();
  const req = {
    t: () => {
      /* explicit empty function */
    },
  };

  const res = {
    status,
    redirect,
  };

  const next = jest.fn();

  handler.errorHandler({}, req, res, next);
  expect(status).toHaveBeenCalledWith(500);
  expect(redirect).toHaveBeenCalledWith("/test-context/problem-with-service");
  expect(next).toBeCalledTimes(1);
});

test("Error handler should set the status to 408", () => {
  const status = jest.fn();
  const redirect = jest.fn();

  const req = {
    t: () => {
      /* explicit empty function */
    },
  };

  const res = {
    status,
    redirect,
  };

  const err = {
    statusCode: 408,
  };

  const next = jest.fn();

  handler.errorHandler(err, req, res, next);
  expect(status).toHaveBeenCalledWith(408);
  expect(redirect).toHaveBeenCalledWith("/test-context/timed-out");
  expect(next).toBeCalledTimes(1);
});

test("Error handler should set the status to 403", () => {
  const status = jest.fn();
  const redirect = jest.fn();

  const req = {
    t: () => {
      /* explicit empty function */
    },
  };

  const res = {
    status,
    redirect,
  };

  const err = {
    statusCode: 403,
  };

  const next = jest.fn();

  handler.errorHandler(err, req, res, next);
  expect(status).toHaveBeenCalledWith(403);
  expect(redirect).toHaveBeenCalledWith("/test-context/no-access");
  expect(next).toBeCalledTimes(1);
});

test("registerErrorHandlers() should configure app to use error handler and logging", () => {
  const app = {
    use: jest.fn(),
    listen: jest.fn(),
  };

  handler.registerErrorHandlers(app);
  expect(app.use).toBeCalledTimes(2);
});
