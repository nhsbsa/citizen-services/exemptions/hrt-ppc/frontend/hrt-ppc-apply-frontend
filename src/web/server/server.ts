import compression from "compression";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as path from "path";
import cookieParser from "cookie-parser";
import { logger } from "../logger/logger";
import { configureSecurity } from "./configure-security";
import { registerRoutes } from "../routes";
import { initialiseSession } from "./session";
import { registerErrorHandlers } from "./error-handlers";
import { setViewEngine } from "./view-engine";
import { internationalisation } from "./internationalisation";
import { requestID } from "./headers/request-id";
import schedule from "node-schedule";
import { productPriceProvider } from "./product-price-provider";
import { v4 } from "uuid";

const { PRODUCT_API_CRON_FREQUENCY } = process.env;

const configureStaticPaths = (app) => {
  /**
   * 1. NHS UK local CSS compiled from SASS in node_modules/nhsuk-frontend
   * 2. NHS UK images and icons
   * 3. NHS UK Javascript
   */
  app.use(
    process.env.CONTEXT_PATH + "/assets",
    express.static(path.resolve("node_modules/nhsuk-frontend/packages/assets")),
  ); /* 1 */
  app.use(
    process.env.CONTEXT_PATH + "/assets",
    express.static(path.resolve("node_modules/nhsuk-frontend/dist")),
  ); /* 2 */
  app.use(
    process.env.CONTEXT_PATH + "/assets",
    express.static(path.resolve("src/web/public")),
  ); /* 3 */
};

const listen = async (config, app) => {
  await productPriceProvider(config, "server-start");

  app.listen(config.server.PORT, () => {
    logger.info(`App listening on port ${config.server.PORT}`);
    logger.info(`App context path is ${config.server.CONTEXT_PATH}`);
    logger.info(`App version is ${config.server.APP_VERSION}`);
    logger.info(`Logging level set to ${config.environment.LOG_LEVEL}`);
    logger.info(
      `Datadog APM enabled is ${config.environment.DATADOG_APM_ENABLED}`,
    );
    logger.info(`Datadog host url is ${config.environment.DATADOG_HOST_URL}`);
    logger.info(`Product service url is ${config.environment.PRODUCT_API_URI}`);
    logger.info(
      `Common PDF service url is ${config.environment.COMMON_PDF_API_URI}`,
    );
    logger.info(
      `Hrt search service url is ${config.environment.SEARCH_API_URI}`,
    );
    logger.info(
      `Card payment service url is ${config.environment.CARD_PAYMENTS_API_URI}`,
    );
    logger.info(`GA url is ${config.environment.GOOGLE_ANALYTICS_URI}`);
    logger.info(`OS places url is ${config.environment.OS_PLACES_URI}`);
    logger.info(
      `Issue certificate url is ${config.environment.ISSUE_CERTIFICATE_API_URI}`,
    );
    logger.info(`Contact telephone is ${config.environment.CONTACT_TELEPHONE}`);
    logger.info(`Contact email is ${config.environment.CONTACT_EMAIL}`);
    logger.info(`12 month value is ${config.environment.HRT_PPC_VALUE}`);
    logger.info(
      `Prescription value is ${config.environment.PRESCRIPTION_VALUE}`,
    );
    logger.info(`GA tracking id is ${config.environment.GA_TRACKING_ID}`);
    logger.info(
      `Search expiry grace period is ${config.environment.EXPIRING_EXEMPTION_GRACE_PERIOD} month`,
    );
    logger.info(
      `Card payment service name is ${config.environment.CARD_PAYMENTS_SERVICE_NAME}`,
    );
    logger.info(
      `Card payment mock enabled is ${config.environment.CARD_PAYMENTS_USE_MOCK}`,
    );
    logger.info(
      `Log request and responses is ${config.environment.LOG_REQUESTS_AND_RESPONSES}`,
    );
    logger.info(
      `Session timeout is ${config.environment.SESSION_TIMEOUT_MINUTES} minutes`,
    );
    logger.info(
      `Warning timeout is ${config.environment.TIMEOUT_WARNING_MINUTES} minutes`,
    );
  });
};

const start = (config, app) => () => {
  app.use(requestID(v4()));
  app.use(compression());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());

  configureSecurity(app);
  setViewEngine(config, app);
  registerRoutes(config, app);
  registerErrorHandlers(app);
  listen(config, app);
  schedule.scheduleJob(PRODUCT_API_CRON_FREQUENCY, async function () {
    await productPriceProvider(config);
  });
};

const initialise = (config, app) => {
  // Configure static paths before registering any middleware to ensure
  // assets are available to all middleware
  configureStaticPaths(app);

  // Apply internationalisation before initialising session to ensure
  // translation function is available to session middleware
  internationalisation(config, app);
  initialiseSession(start(config, app), config, app);
};

export { initialise };
