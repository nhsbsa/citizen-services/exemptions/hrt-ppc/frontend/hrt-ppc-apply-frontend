import { config } from "../../../config";

// canonical workaround to typescript error
const TIMEOUT_MINUTES = +config.environment.SESSION_TIMEOUT_MINUTES;
const COOKIE_EXPIRES_MILLISECONDS = TIMEOUT_MINUTES * 60 * 1000;

export { COOKIE_EXPIRES_MILLISECONDS };
