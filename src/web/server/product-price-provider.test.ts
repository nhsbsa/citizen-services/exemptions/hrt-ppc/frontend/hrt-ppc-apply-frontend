import expect from "expect";
import { productPriceProvider } from "./product-price-provider";
import { productClient } from "../client/product-client";
jest.mock("../client/product-client");

const config = {
  environment: {
    OUTBOUND_API_TIMEOUT: 20000,
    PRODUCT_API_URI: "baseURI",
    PRODUCT_API_KEY: "test-key",
    HRT_PPC_VALUE: undefined,
    HRT_PPC_VALUE_UPDATE_TIME: undefined,
  },
};

describe("productPriceProvider()", () => {
  test("should set HRT_PPC_VALUE and HRT_PPC_VALUE_UPDATE_TIME in config", async () => {
    const makeRequestMock = jest.fn();
    productClient.prototype.makeRequest = makeRequestMock;
    makeRequestMock.mockReturnValue(Promise.resolve({ data: { price: 1570 } }));
    const mockDate = new Date();
    jest.spyOn(global, "Date").mockImplementation(() => mockDate);
    const mockedDate = new Date();

    await productPriceProvider(config);

    expect(config.environment.HRT_PPC_VALUE).toBe(1570);
    expect(config.environment.HRT_PPC_VALUE_UPDATE_TIME).toBe(mockedDate);
  });

  test("should set HRT_PPC_VALUE to undefined and update HRT_PPC_VALUE_UPDATE_TIME in config when error is thrown and instance called from scheduler", async () => {
    const makeRequestMock = jest
      .fn()
      .mockRejectedValue("Internal Server Error");
    productClient.prototype.makeRequest = makeRequestMock;
    const mockDate = new Date();
    jest.spyOn(global, "Date").mockImplementation(() => mockDate);
    const mockedDate = new Date();

    await productPriceProvider(config);

    expect(config.environment.HRT_PPC_VALUE).toBe(undefined);
    expect(config.environment.HRT_PPC_VALUE_UPDATE_TIME).toBe(mockedDate);
  });

  test("should set HRT_PPC_VALUE to undefined and update HRT_PPC_VALUE_UPDATE_TIME in config when error is thrown and instance called from server", async () => {
    const makeRequestMock = jest
      .fn()
      .mockRejectedValue("Internal Server Error");
    productClient.prototype.makeRequest = makeRequestMock;
    const mockDate = new Date();
    jest.spyOn(global, "Date").mockImplementation(() => mockDate);
    const mockedDate = new Date();

    await expect(() =>
      productPriceProvider(config, "server-start"),
    ).rejects.toThrowError("Price is not defined");
    expect(config.environment.HRT_PPC_VALUE).toBe(undefined);
    expect(config.environment.HRT_PPC_VALUE_UPDATE_TIME).toBe(mockedDate);
  });
});
