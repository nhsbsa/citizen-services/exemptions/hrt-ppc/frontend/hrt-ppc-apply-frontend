import i18next from "i18next";
import * as middleware from "i18next-http-middleware";
import Backend from "i18next-fs-backend";
import * as path from "path";
import * as R from "ramda";

import moment from "moment";
import { COOKIE_EXPIRES_MILLISECONDS } from "./session/cookie-settings";

const detection = (config) => ({
  order: ["querystring", "cookie", "header"],
  lookupQuerystring: "lang",
  lookupCookie: "lang",
  cookieSecure: !config.environment.USE_UNSECURE_COOKIE,
  caches: ["cookie"],
});

const refreshCookieExpirationDate = (req, res, next) => {
  if (R.path(["i18n", "options", "detection"], req)) {
    req.i18n.options.detection.cookieExpirationDate = moment()
      .add(COOKIE_EXPIRES_MILLISECONDS, "milliseconds")
      .toDate();
  }
  next();
};

const internationalisation = (config, app) => {
  i18next
    .use(Backend)
    .use(middleware.LanguageDetector)
    .init({
      preload: ["en"],
      ns: ["common", "validation", "buttons", "errors"],
      defaultNS: "common",
      detection: detection(config),
      backend: {
        loadPath: path.join(__dirname, "/locales/{{lng}}/{{ns}}.json"),
      },
      fallbackLng: "en",
      returnObjects: true,
    });

  app.use(middleware.handle(i18next));
  app.use(refreshCookieExpirationDate);
};

export { internationalisation };
