function TimeoutWarning($module) {
  this.$module = $module;
  this.$lastFocusedEl = null;
  this.$cancelButton = $module.querySelector('.js-dialog-cancel');
  this.overLayClass = 'hrt-timeout-warning-overlay';
  this.timers = [];
  // UI countdown timer specific markup
  this.$countdown = $module.querySelector('.timer');
  this.$accessibleCountdown = $module.querySelector('.at-timer');
  // UI countdown specific settings
  this.timeout = $module.getAttribute('data-minutes-show-after') || 25;
  this.timeOutRedirectUrl =
    $module.getAttribute('data-url-redirect') || '/session-timeout';
  this.staySignedInUrl = $module.getAttribute('data-url-stay-signed-in') || '';

  this.minutesTimeOutModalVisible =
    $module.getAttribute('data-minutes-modal-visible') || 5;
  this.text =
    $module.getAttribute('data-text') ||
    'We will reset your application if you do not respond in ';

  this.extraText =
    $module.getAttribute('data-extra-text') ||
    '. We do this to keep your information secure.';

  this.$staySignedIn = $module.querySelector('#stay-signed-in-button');
  this.$closeModal = $module.querySelector('#close-modal-button');

  var tokenTag = document.querySelector('[name="csrf-token"]');

  if (tokenTag) {
    this.csrfToken = tokenTag.getAttribute('content');
  }
  this.timeUserLastInteractedWithPage = '';
}

// Initialise component
TimeoutWarning.prototype.init = function () {
  // Check for module
  if (!this.$module) {
    return;
  }

  // Check that dialog element has native or polyfill support
  if (!this.dialogSupported()) {
    return;
  }

  this.showModalAfterTimeout();

  if (this.$staySignedIn) {
    this.$staySignedIn.onclick = this.staySignedIn.bind(this);
  }
  if (this.$closeModal) {
    this.$closeModal.onclick = function () {
      this.closeDialog(true);
    }.bind(this);
  }

  // DN TODO: Extend session if escape key is pressed
  // this.$escClose = this.escClose.bind(this);
  this.$module.addEventListener('cancel', function (e) {
    e.preventDefault();
  });

  this.escClose = this.escClose.bind(this);
  this.checkIfShouldHaveTimedOut = this.checkIfShouldHaveTimedOut.bind(this);

  // We set in localstorage the time the user lands on the page - this time is
  // available across browser tabs so other tabs can read and set this value
  this.setPageInteraction();
};

TimeoutWarning.prototype.setPageInteraction = function () {
  if (window.localStorage) {
    window.localStorage.setItem('timeUserLastInteractedWithPage', new Date());
  }
};

TimeoutWarning.prototype.checkIfShouldHaveTimedOut = function () {
  if (window.localStorage) {
    var sessionLength =
      (parseFloat(this.timeout, 10) +
        parseFloat(this.minutesTimeOutModalVisible, 10)) *
      60000;

    var timeUserLastInteractedWithPage = new Date(
      window.localStorage.getItem('timeUserLastInteractedWithPage')
    );
    return timeUserLastInteractedWithPage < new Date() - sessionLength;
  }

  // If localstorage isn't available we have to timeout
  return true;
};

// Check if browser supports native dialog element or can use polyfill
TimeoutWarning.prototype.dialogSupported = function () {
  if (typeof HTMLDialogElement === 'function') {
    // Native dialog is supported by browser
    return true;
  } else {
    // Native dialog is not supported by browser so use polyfill
    try {
      window.dialogPolyfill.registerDialog(this.$module);
      return true;
    } catch (error) {
      return false;
    }
  }
};

TimeoutWarning.prototype.staySignedIn = function (e) {
  if (!this.csrfToken) {
    return;
  }

  if (window.fetch) {
    fetch(this.staySignedInUrl, {
      headers: {
        'csrf-token': this.csrfToken,
      },
      method: 'POST',
    })
      .then(this.closeDialog.bind(this))
      .catch(function (e) {
        // We redirect to the timeout page if the status is not successful
        this.redirect();
      });
  } else {
    var self = this;
    var xhr = new XMLHttpRequest();
    xhr.open('POST', this.staySignedInUrl, true);
    xhr.responseType = 'json';

    xhr.setRequestHeader('csrf-token', this.csrfToken);

    xhr.addEventListener('load', function (e) {
      if (xhr.status === 200) {
        self.closeDialog();
      } else {
        // We redirect to the timeout page if the status is not successful
        self.redirect();
      }
    });

    // We redirect to the timeout page if there is an error
    xhr.addEventListener('error', function () {
      self.redirect();
    });

    xhr.send(null);
  }
};

TimeoutWarning.prototype.showModalAfterTimeout = function () {
  var milliseconds = this.timeout * 60000;

  setTimeout(this.openDialog.bind(this), milliseconds);
};

TimeoutWarning.prototype.openDialog = function () {
  document.addEventListener('keydown', this.escClose);

  // TO DO - Step B of client/server interaction
  // GET last interactive time from server before showing warning
  // User could be interacting with site in 2nd tab
  // Update time left accordingly
  if (!this.isDialogOpen()) {
    document.body.classList.add(this.overLayClass);
    this.saveLastFocusedEl();
    this.makePageContentInert();
    this.$module.showModal();

    this.startUiCountdown();

    // if (window.history.pushState) {
    //   window.history.pushState('', '') // This updates the History API to enable state to be "popped" to detect browser navigation for disableBackButtonWhenOpen
    // }
  }
};

// Starts a UI countdown timer. If timer is not cancelled before 0
// reached + 4 seconds grace period, user is redirected.
TimeoutWarning.prototype.startUiCountdown = function () {
  this.clearTimers(); // Clear any other modal timers that might have been running
  var $module = this;
  var $countdown = this.$countdown;
  var $accessibleCountdown = this.$accessibleCountdown;
  var minutes = this.minutesTimeOutModalVisible;
  var timerRunOnce = false;
  var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  var timers = this.timers;

  var seconds = 60 * minutes;

  $countdown.innerHTML = minutes + ' minute' + (minutes > 1 ? 's' : '');

  (function runTimer() {
    var minutesLeft = parseInt(seconds / 60, 10);
    var secondsLeft = parseInt(seconds % 60, 10);
    var timerExpired = minutesLeft < 1 && secondsLeft < 1;

    var minutesText =
      minutesLeft > 0
        ? '<span class="tabular-numbers">' +
          minutesLeft +
          '</span> minute' +
          (minutesLeft > 1 ? 's' : '') +
          ''
        : ' ';
    var secondsText =
      secondsLeft >= 1
        ? ' <span class="tabular-numbers">' +
          secondsLeft +
          '</span> second' +
          (secondsLeft > 1 ? 's' : '') +
          ''
        : '';
    var atMinutesNumberAsText = '';
    var atSecondsNumberAsText = '';

    try {
      atMinutesNumberAsText = this.numberToWords(minutesLeft); // Attempt to convert numerics into text as iOS VoiceOver ccassionally stalled when encountering numbers
      atSecondsNumberAsText = this.numberToWords(secondsLeft);
    } catch (e) {
      atMinutesNumberAsText = minutesLeft;
      atSecondsNumberAsText = secondsLeft;
    }

    var atMinutesText =
      minutesLeft > 0
        ? atMinutesNumberAsText + ' minute' + (minutesLeft > 1 ? 's' : '') + ''
        : '';
    var atSecondsText =
      secondsLeft >= 1
        ? ' ' +
          atSecondsNumberAsText +
          ' second' +
          (secondsLeft > 1 ? 's' : '') +
          ''
        : '';

    // Below string will get read out by screen readers every time the timeout refreshes (every 15 secs. See below).
    // Please add additional information in the modal body content or in below extraText which will get announced to AT the first time the time out opens
    var text =
      $module.text +
      '<span class="nhsuk-u-font-weight-bold">' +
      minutesText +
      secondsText +
      '</span>';
    var atText = $module.text + atMinutesText;

    if (atSecondsText) {
      if (minutesLeft > 0) {
        atText += ' and';
      }
      atText += atSecondsText + '';
    } else {
      atText += '';
    }

    if (timerExpired) {
      if ($module.checkIfShouldHaveTimedOut() || $module.mfaTimeOut) {
        $countdown.innerHTML = 'You are about to be redirected.';
        $accessibleCountdown.innerHTML = 'You are about to be redirected.';

        setTimeout($module.redirect.bind($module), 2000);
      } else {
        $module.closeDialog();
      }
    } else {
      seconds--;

      $countdown.innerHTML = text + $module.extraText;

      if (minutesLeft < 1 && secondsLeft < 20) {
        $accessibleCountdown.setAttribute('aria-live', 'assertive');
      }

      if (!timerRunOnce) {
        // Read out the extra content only once. Don't read out on iOS VoiceOver which stalls on the longer text

        if (iOS) {
          $accessibleCountdown.innerHTML = atText;
        } else {
          $accessibleCountdown.innerHTML = atText + $module.extraText;
        }
        timerRunOnce = true;
      } else if (secondsLeft % 15 === 0) {
        // Update screen reader friendly content every 15 secs
        $accessibleCountdown.innerHTML = atText + $module.extraText;
      }

      // TO DO - client/server interaction
      // GET last interactive time from server while the warning is being displayed.
      // If user interacts with site in second tab, warning should be dismissed.
      // Compare what server returned to what is stored in client
      // If needed, call this.closeDialog()

      // JS doesn't allow resetting timers globally so timers need to be retained for resetting.
      timers.push(setTimeout(runTimer, 1000));
    }
  })();
};

TimeoutWarning.prototype.saveLastFocusedEl = function () {
  this.$lastFocusedEl = document.activeElement;
  if (!this.$lastFocusedEl || this.$lastFocusedEl === document.body) {
    this.$lastFocusedEl = null;
  } else if (document.querySelector) {
    this.$lastFocusedEl = document.querySelector(':focus');
  }
};

// Set focus back on last focused el when modal closed
TimeoutWarning.prototype.setFocusOnLastFocusedEl = function () {
  if (this.$lastFocusedEl) {
    setTimeout(
      function () {
        this.$lastFocusedEl.focus();
      }.bind(this),
      0
    );
  }
};

// Set page content to inert to indicate to screenreaders it's inactive
// NB: This will look for #maincontent for toggling inert state
TimeoutWarning.prototype.makePageContentInert = function () {
  if (document.querySelector('#maincontent')) {
    document.querySelector('#maincontent').inert = true;
    document.querySelector('#maincontent').setAttribute('aria-hidden', 'true');
  }
};

// Make page content active when modal is not open
// NB: This will look for #maincontent for toggling inert state
TimeoutWarning.prototype.removeInertFromPageContent = function () {
  if (document.querySelector('#maincontent')) {
    document.querySelector('#maincontent').inert = false;
    document.querySelector('#maincontent').setAttribute('aria-hidden', 'false');
  }
};

TimeoutWarning.prototype.isDialogOpen = function () {
  return this.$module.open;
};

TimeoutWarning.prototype.closeDialog = function (keepTimers) {
  keepTimers = typeof keepTimers !== 'undefined' ? keepTimers : false;

  if (this.isDialogOpen()) {
    document.querySelector('body').classList.remove(this.overLayClass);
    this.$module.close();
    this.setFocusOnLastFocusedEl();
    this.removeInertFromPageContent();
    document.removeEventListener('keydown', this.escClose);

    if (!keepTimers) {
      this.clearTimers();
      this.showModalAfterTimeout();
    }
    this.setPageInteraction();
  }
};

// Clears modal timer
TimeoutWarning.prototype.clearTimers = function () {
  for (var i = 0; i < this.timers.length; i++) {
    clearTimeout(this.timers[i]);
  }
};

TimeoutWarning.prototype.disableBackButtonWhenOpen = function () {
  window.addEventListener('popstate', function () {
    if (this.isDialogOpen()) {
      this.closeDialog();
    } else {
      window.history.go(-1);
    }
  });
};

// Close modal when ESC pressed
TimeoutWarning.prototype.escClose = function (event) {
  // get the target element
  if (this.isDialogOpen() && event.keyCode === 27) {
    this.staySignedIn();
  }
};

TimeoutWarning.prototype.redirect = function () {
  window.location.replace(this.timeOutRedirectUrl);
};

TimeoutWarning.prototype.numberToWords = function () {
  var string = n.toString();
  var units;
  var tens;
  var scales;
  var start;
  var end;
  var chunks;
  var chunksLen;
  var chunk;
  var ints;
  var i;
  var word;
  var words = 'and';

  if (parseInt(string) === 0) {
    return 'zero';
  }

  /* Array of units as words */
  units = [
    '',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
    'twelve',
    'thirteen',
    'fourteen',
    'fifteen',
    'sixteen',
    'seventeen',
    'eighteen',
    'nineteen',
  ];

  /* Array of tens as words */
  tens = [
    '',
    '',
    'twenty',
    'thirty',
    'forty',
    'fifty',
    'sixty',
    'seventy',
    'eighty',
    'ninety',
  ];

  /* Array of scales as words */
  scales = [
    '',
    'thousand',
    'million',
    'billion',
    'trillion',
    'quadrillion',
    'quintillion',
    'sextillion',
    'septillion',
    'octillion',
    'nonillion',
    'decillion',
    'undecillion',
    'duodecillion',
    'tredecillion',
    'quatttuor-decillion',
    'quindecillion',
    'sexdecillion',
    'septen-decillion',
    'octodecillion',
    'novemdecillion',
    'vigintillion',
    'centillion',
  ];

  /* Split user arguemnt into 3 digit chunks from right to left */
  start = string.length;
  chunks = [];
  while (start > 0) {
    end = start;
    chunks.push(string.slice((start = Math.max(0, start - 3)), end));
  }

  /* Check if function has enough scale words to be able to stringify the user argument */
  chunksLen = chunks.length;
  if (chunksLen > scales.length) {
    return '';
  }

  /* Stringify each integer in each chunk */
  words = [];
  for (i = 0; i < chunksLen; i++) {
    chunk = parseInt(chunks[i]);

    if (chunk) {
      /* Split chunk into array of individual integers */
      ints = chunks[i].split('').reverse().map(parseFloat);

      /* If tens integer is 1, i.e. 10, then add 10 to units integer */
      if (ints[1] === 1) {
        ints[0] += 10;
      }

      /* Add scale word if chunk is not zero and array item exists */
      if ((word = scales[i])) {
        words.push(word);
      }

      /* Add unit word if array item exists */
      if ((word = units[ints[0]])) {
        words.push(word);
      }

      /* Add tens word if array item exists */
      if ((word = tens[ints[1]])) {
        words.push(word);
      }

      /* Add hundreds word if array item exists */
      if ((word = units[ints[2]])) {
        words.push(word + ' hundred');
      }
    }
  }
  return words.reverse().join(' ');
};
