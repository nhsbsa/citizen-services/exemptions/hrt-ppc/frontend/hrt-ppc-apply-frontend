window.addEventListener('load', function (e) {
  initListener()
}, false)

/*
 * Disable the submit button after first click to avoid duplicate submission
 */
function initListener () {
  const form = document.getElementById('form')
  if (form) {
    form.addEventListener('submit', function (event) {
      document.getElementById('submit-button').disabled = true
    })
  }
  if (document.getElementById('nhsuk-print-certificate')) {
    document.getElementById('nhsuk-print-certificate').onclick = function() {
      javascript:window.print();
    };
  }

  var $timeoutWarnings = document.querySelectorAll(
    '[data-module="hrt-timeout-warning"]'
  );

  for (var i = 0; i < $timeoutWarnings.length; i++) {
    var $timeoutWarning = $timeoutWarnings[i];

    new TimeoutWarning($timeoutWarning).init();
  }
}

/*
 * If browser back button was used, flush cache
 * This ensures that user will always see an accurate, up-to-date view based on their state
 * https://stackoverflow.com/questions/8788802/prevent-safari-loading-from-cache-when-back-button-is-clicked
 */
(function () {
  window.onpageshow = function (event) {
    if (event.persisted) {
      window.location.reload()
    }
  }
})()
