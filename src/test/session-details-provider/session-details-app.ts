import * as express from "express";
import * as session from "express-session";
import * as redis from "redis";
import { config } from "../../config";
import { logger } from "../../web/logger/logger";
import RedisStore from "connect-redis";
const app = express();
import { COOKIE_EXPIRES_MILLISECONDS } from "../../web/server/session/cookie-settings";
import { CONFIRMATION_CODE_SESSION_PROPERTY } from "../../web/routes/application/steps/common/constants";
import { SESSION_DETAILS_PORT, SESSION_DETAILS_PATH } from "./constants";

const client = redis.createClient(config.redis);
const store = new RedisStore({ client });
const sessionConfig = {
  store,
  secret: config.server.SESSION_SECRET,
  saveUninitialized: false,
  resave: false,
  name: config.server.SESSION_ID_NAME,
  cookie: {
    secure: true,
    maxAge: COOKIE_EXPIRES_MILLISECONDS,
  },
};
app.set("trust proxy", 1); // trust first proxy
app.use(session(sessionConfig));

logger.info("config:" + JSON.stringify(config));

const handleConfirmationCode = (req, res) => {
  const confirmationCode = req.session[CONFIRMATION_CODE_SESSION_PROPERTY];
  logger.info(
    `Confirmation code for request with cookies '${JSON.stringify(
      req.headers.cookie,
    )}' is ${confirmationCode}`,
  );
  res.send(confirmationCode);
};

const server = app.listen(SESSION_DETAILS_PORT, () => {
  logger.info(`Session Details App listening on port ${SESSION_DETAILS_PORT}`);
  app.get(SESSION_DETAILS_PATH, handleConfirmationCode);
});

process.on("SIGTERM", () => {
  server.close(() => {
    logger.info("Session Details App closed.");
    process.exit(0);
  });
});

process.on("SIGINT", () => {
  server.close(() => {
    logger.info("Session Details App closed.");
    process.exit(0);
  });
});
