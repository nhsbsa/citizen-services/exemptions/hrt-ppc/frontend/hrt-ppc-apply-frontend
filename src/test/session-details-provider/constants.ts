"use strict";

import * as dotEnv from "dotenv";

dotEnv.config();

export const SESSION_DETAILS_PORT: string | undefined =
  process.env.SESSION_DETAILS_PORT || process.env.PORT;
const SESSION_DETAILS_BASE_URL: string =
  process.env.SESSION_DETAILS_BASE_URL ||
  `http://localhost:${SESSION_DETAILS_PORT}`;
export const SESSION_DETAILS_PATH: string =
  "/session-details/confirmation-code";

export const SESSION_CONFIRMATION_CODE_URL: string = `${SESSION_DETAILS_BASE_URL}${SESSION_DETAILS_PATH}`;
