process.env.CONTEXT_PATH = "/test-context";
process.env.APP_VERSION = "example-version";
process.env.HRT_PPC_VALUE = "1870";
process.env.CARD_PAYMENTS_JWT_SECRET = "test-secret";
process.env.CONTACT_EMAIL = "nhsbsa.hrtppc-support@nhs.net";
process.env.GA_TRACKING_ID = "G-WDL00001T";
