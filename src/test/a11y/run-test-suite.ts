/* no-process-exit */
"use strict";

import {
  setupSuccessfulNoSearchMatchMapping,
  setupSuccessfulActiveExemptionFoundSearchMatchMapping,
} from "./wiremock/hrt-search/match";
import { setupPostcodeLookupWithResults } from "./wiremock/postcode-lookup/postcode-lookup";
import { setupSuccessfulIssueCertificateMapping } from "./wiremock/hrt-issue-certificate/issue-certificate";
import { setupSuccessfulNextUrlMapping } from "./wiremock/card-payment-service/create-payment";
import {
  setupPaymentWithStatusDeclinedMapping,
  setupSuccessfulPaymentWithStatusSuccessMapping,
  setupPaymentWithStatusCancelledMapping,
} from "./wiremock/card-payment-service/get-payment";
import { setupSuccessfulPaymentMapping } from "./wiremock/payment/payment";
import { deleteAllWiremockMappings } from "./wiremock/wiremock";

import { runAllTests } from "./test-suite";
import { TEST_SUITE } from "./tests";
/*
  Runs the accessibility test suite, having set up wiremock to mock the backend services.
 */
const runTestSuite = async (testSuite) => {
  try {
    const makeArrEligible: any[] = [];
    makeArrEligible.push(testSuite[0]);

    const makeArrMedicineNotCovered: any[] = [];
    makeArrMedicineNotCovered.push(testSuite[1]);

    const makeArrNotEligibleOutsideEng: any[] = [];
    makeArrNotEligibleOutsideEng.push(testSuite[2]);

    const makeArrExistingExemptionFound: any[] = [];
    makeArrExistingExemptionFound.push(testSuite[3]);

    const makeArrNotEligibleEntitledFreePresc: any[] = [];
    makeArrNotEligibleEntitledFreePresc.push(testSuite[4]);

    const makeArrUpdateCookies: any[] = [];
    makeArrUpdateCookies.push(testSuite[5]);

    const makeArrNoCookies: any[] = [];
    makeArrNoCookies.push(testSuite[6]);

    const makeArrPageNotFound: any[] = [];
    makeArrPageNotFound.push(testSuite[7]);

    const makeArrProblemWithService: any[] = [];
    makeArrProblemWithService.push(testSuite[8]);

    const makeArrDailyLimitReached: any[] = [];
    makeArrDailyLimitReached.push(testSuite[9]);

    const makeArrPaymentDeclined: any[] = [];
    makeArrPaymentDeclined.push(testSuite[10]);

    const makeArrPaymentCancelled: any[] = [];
    makeArrPaymentCancelled.push(testSuite[11]);

    const makeArrSessionTimeout: any[] = [];
    makeArrSessionTimeout.push(testSuite[12]);

    await setupSuccessfulNoSearchMatchMapping();
    await setupPostcodeLookupWithResults();
    await setupSuccessfulIssueCertificateMapping();
    await setupSuccessfulPaymentMapping();
    await setupSuccessfulNextUrlMapping();
    await setupSuccessfulPaymentWithStatusSuccessMapping();
    // stubs wiremocks and run test for happy path where eligible
    await runAllTests(makeArrEligible);
    await runAllTests(makeArrUpdateCookies);
    // run test for kickout pages
    await runAllTests(makeArrMedicineNotCovered);
    await runAllTests(makeArrNotEligibleOutsideEng);
    await runAllTests(makeArrNotEligibleEntitledFreePresc);
    // run test for page not found
    await runAllTests(makeArrPageNotFound);
    // run test for cookies not enabled
    await runAllTests(makeArrNoCookies);
    // run test for error pages
    await runAllTests(makeArrProblemWithService);
    await runAllTests(makeArrDailyLimitReached);
    await runAllTests(makeArrPageNotFound);
    await setupPaymentWithStatusDeclinedMapping();
    await runAllTests(makeArrPaymentDeclined);
    await setupPaymentWithStatusCancelledMapping();
    await runAllTests(makeArrPaymentCancelled);
    // stubs wiremocks and run test for where exemption found
    await setupSuccessfulActiveExemptionFoundSearchMatchMapping();
    await runAllTests(makeArrExistingExemptionFound);
  } catch (error) {
    console.log(error);
    process.exit(1);
  } finally {
    await deleteAllWiremockMappings();
  }
};

runTestSuite(TEST_SUITE);
