import {
  CertificateStatus,
  CertificateType,
} from "@nhsbsa/health-charge-exemption-common-frontend";

export default {
  status: 200,
  jsonBody: {
    exemptions: {
      "hrt-ppcs": [
        {
          citizens: [
            {
              id: "c0a80005-859c-1146-8185-b08e84980012",
              firstName: "HRT",
              lastName: "ACTIVE",
              dateOfBirth: "1997-03-24",
              certificates: [
                {
                  reference: "HRT7DDB232E",
                  type: CertificateType.HRT_PPC,
                  status: CertificateStatus.ACTIVE,
                  startDate: "2022-03-31",
                  endDate: "2099-03-31",
                },
              ],
            },
          ],
        },
      ],
      others: [],
    },
  },
  headers: {
    "Content-Type": "application/json",
  },
};
