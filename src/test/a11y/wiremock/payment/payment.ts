import { postJsonData } from "../../request";
import { createPaymentResultsMapping } from "./mappings";
import { WIREMOCK_PAYMENT_MAPPING_URL } from "../paths";

async function setupSuccessfulPaymentMapping() {
  await postJsonData(
    WIREMOCK_PAYMENT_MAPPING_URL,
    createPaymentResultsMapping(),
  );
}

export { setupSuccessfulPaymentMapping };
