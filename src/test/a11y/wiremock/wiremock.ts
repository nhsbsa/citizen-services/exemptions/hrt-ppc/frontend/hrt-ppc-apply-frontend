import { postJsonData, performDelete } from "../request";
import {
  WIREMOCK_SEARCH_MAPPING_URL,
  WIREMOCK_SEARCH_FIND_REQUESTS_URL,
  WIREMOCK_OS_PLACES_MAPPING_URL,
  WIREMOCK_ISSUE_CERTIFICATE_MAPPING_URL,
  WIREMOCK_PAYMENT_MAPPING_URL,
} from "./paths";

async function deleteAllWiremockMappings() {
  await performDelete(WIREMOCK_SEARCH_MAPPING_URL);
  await performDelete(WIREMOCK_OS_PLACES_MAPPING_URL);
  await performDelete(WIREMOCK_ISSUE_CERTIFICATE_MAPPING_URL);
  await performDelete(WIREMOCK_PAYMENT_MAPPING_URL);
}

async function getOutboundRequestsToSearchService(url) {
  const body = JSON.stringify({
    method: "POST",
    url,
  });
  const response: any = await postJsonData(
    WIREMOCK_SEARCH_FIND_REQUESTS_URL,
    body,
  );
  return JSON.parse(response).requests;
}

export { deleteAllWiremockMappings, getOutboundRequestsToSearchService };
