module.exports = {
  ...require("./postcode-lookup"),
  ...require("./hrt-search"),
  ...require("./hrt-issue-certificate"),
  ...require("./payment"),
  ...require("./access-control"),
  ...require("./card-payment-service"),
  ...require("./wiremock"),
};
