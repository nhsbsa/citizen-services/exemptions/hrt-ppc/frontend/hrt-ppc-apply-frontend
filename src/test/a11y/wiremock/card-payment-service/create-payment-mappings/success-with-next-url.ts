export default {
  status: 200,
  jsonBody: {
    nextUrl: "{{{jsonPath request.body '$.donePageURL'}}}",
  },
  transformers: ["response-template"],
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
  },
};
