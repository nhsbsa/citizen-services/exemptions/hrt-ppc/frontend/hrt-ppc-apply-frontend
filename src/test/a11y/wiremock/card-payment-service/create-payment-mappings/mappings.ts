const SUCCESS = "SUCCESS";
import successWithNextUrl from "./success-with-next-url";

const RESPONSE_MAP = {
  [SUCCESS]: successWithNextUrl,
};

const createSuccessWithNextUrlMapping = (scenario = SUCCESS) =>
  JSON.stringify({
    request: {
      method: "POST",
      url: `/card-payments-services/hrtppconline/payments`,
    },
    response: RESPONSE_MAP[scenario],
  });

export { createSuccessWithNextUrlMapping, SUCCESS };
