import { postJsonData } from "../../request";
import { WIREMOCK_CPS_MAPPING_URL } from "../paths";
import {
  createSuccessStatusPaymentMapping,
  SUCCESS,
  CANCELLED,
  DECLINED,
} from "./get-payment-mappings/mappings";

async function setupSuccessfulPaymentWithStatusSuccessMapping() {
  await postJsonData(
    WIREMOCK_CPS_MAPPING_URL,
    createSuccessStatusPaymentMapping(SUCCESS),
  );
}

async function setupPaymentWithStatusDeclinedMapping() {
  await postJsonData(
    WIREMOCK_CPS_MAPPING_URL,
    createSuccessStatusPaymentMapping(DECLINED),
  );
}

async function setupPaymentWithStatusCancelledMapping() {
  await postJsonData(
    WIREMOCK_CPS_MAPPING_URL,
    createSuccessStatusPaymentMapping(CANCELLED),
  );
}

export {
  setupSuccessfulPaymentWithStatusSuccessMapping,
  setupPaymentWithStatusDeclinedMapping,
  setupPaymentWithStatusCancelledMapping,
};
