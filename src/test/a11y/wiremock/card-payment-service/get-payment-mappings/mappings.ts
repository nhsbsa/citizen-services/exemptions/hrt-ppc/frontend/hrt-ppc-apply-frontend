const SUCCESS = "SUCCESS";
const CANCELLED = "CANCELLED";
const DECLINED = "ERROR";
import successStatusPayment from "./success-payment";
import paymentDeclinedStatusPayment from "./payment-declined";
import paymentCancelledStatusPayment from "./payment-cancelled";

const RESPONSE_MAP = {
  [SUCCESS]: successStatusPayment,
  [DECLINED]: paymentDeclinedStatusPayment,
  [CANCELLED]: paymentCancelledStatusPayment,
};

const createSuccessStatusPaymentMapping = (scenario = SUCCESS) =>
  JSON.stringify({
    request: {
      method: "GET",
      url: `/card-payments-services/hrtppconline/payments/906944761072A2088F71`,
    },
    response: RESPONSE_MAP[scenario],
  });

export { createSuccessStatusPaymentMapping, SUCCESS, DECLINED, CANCELLED };
