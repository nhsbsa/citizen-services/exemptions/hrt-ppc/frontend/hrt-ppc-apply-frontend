export default {
  status: 200,
  jsonBody: {
    amount: 1870,
    donePageUrl: "http://done-url-not-used-locally.com",
    reference: "NOTUSED",
    status: "SUCCESS",
    transactionId: "TRANSID123",
  },
  headers: {
    "Content-Type": "application/json",
  },
};
