import { postJsonData } from "../../request";
import { WIREMOCK_CPS_MAPPING_URL } from "../paths";
import {
  createSuccessWithNextUrlMapping,
  SUCCESS,
} from "./create-payment-mappings/mappings";

async function setupSuccessfulNextUrlMapping() {
  await postJsonData(
    WIREMOCK_CPS_MAPPING_URL,
    createSuccessWithNextUrlMapping(SUCCESS),
  );
}

export { setupSuccessfulNextUrlMapping };
