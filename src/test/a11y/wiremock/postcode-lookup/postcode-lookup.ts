import { postJsonData } from "../../request";
import {
  WIREMOCK_OS_PLACES_MAPPING_URL,
  WIREMOCK_GOOGLE_ANALYTICS_MAPPING_URL,
} from "../paths";

import {
  createPostcodeLookupWithResultsMapping,
  createGoogleAnalyticsMapping,
} from "./mappings";

async function setupPostcodeLookupWithResults() {
  await postJsonData(
    WIREMOCK_GOOGLE_ANALYTICS_MAPPING_URL,
    createGoogleAnalyticsMapping(),
  );
  await postJsonData(
    WIREMOCK_OS_PLACES_MAPPING_URL,
    createPostcodeLookupWithResultsMapping(),
  );
}

export { setupPostcodeLookupWithResults };
