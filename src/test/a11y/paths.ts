import * as dotEnv from "dotenv";
dotEnv.config();

const BASE_URL =
  (process.env.APP_BASE_URL || "http://localhost:8080") +
    process.env.CONTEXT_PATH || `http://localhost:${process.env.PORT}`;

const URLS = {
  START: `${BASE_URL}/start`,
  IS_MEDICINE_COVERED: `${BASE_URL}/is-your-medicine-covered`,
  MEDICINE_NOT_COVERED: `${BASE_URL}/medicine-not-covered`,
  SOME_MEDICINE_COVERED: `${BASE_URL}/some-medicine-covered`,
  WHERE_YOU_COLLECT: `${BASE_URL}/where-you-collect`,
  DO_YOU_WANT_TO_CONTINUE: `${BASE_URL}/do-you-want-to-continue`,
  OUTSIDE_ENGLAND: `${BASE_URL}/outside-england`,
  ENTER_DOB: `${BASE_URL}/dob`,
  AGE_ENTITLED_TO_FREE_PRESCRIPTIONS: `${BASE_URL}/age-entitled-to-free-prescriptions`,
  ENTER_NAME: `${BASE_URL}/name`,
  ENTER_POSTCODE: `${BASE_URL}/postcode`,
  SELECT_ADDRESS: `${BASE_URL}/what-is-your-address`,
  ENTER_ADDRESS: `${BASE_URL}/manual-address`,
  EXISTING_EXEMPTION_FOUND: `${BASE_URL}/existing-exemption-found`,
  NHS_NUMBER: `${BASE_URL}/nhs-number`,
  CHECK_ANSWERS: `${BASE_URL}/check-your-answers`,
  PROCESS_PAYMENT: `${BASE_URL}/p`,
  ENABLE_COOKIES: `${BASE_URL}/enable-cookies`,
  PAGE_NOT_FOUND: `${BASE_URL}/page-not-found`,
  PROBLEM_WITH_SERVICE: `${BASE_URL}/problem-with-service`,
  COOKIES: `${BASE_URL}/cookie-information`,
  COOKIES_DECLARATION: `${BASE_URL}/choose-cookies`,
  COOKIES_CONFIRMATION: `${BASE_URL}/cookies-saved`,
  APPLICATION_COMPLETE_URL: `${BASE_URL}/application-complete`,
  CERTIFICATE_START_DATE: `${BASE_URL}/your-start-date`,
  PAYMENT_CANCELLED: `${BASE_URL}/payment-cancelled`,
  PAYMENT_DECLINED: `${BASE_URL}/payment-declined`,
  EMAIL_ADDRESS: `${BASE_URL}/do-you-want-email`,
  SESSION_TIMEOUT: `${BASE_URL}/session-timeout`,
};

export { BASE_URL, URLS };
