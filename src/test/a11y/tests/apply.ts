import { POSTCODE } from "../constants";
import { URLS } from "../paths";
import { currentDate } from "./dates";

const APPLICATION_SUCCESSFUL_TITLE = "Confirmation of PPC - HRT PPC";
const PAYMENT_DECLINED_TITLE = "Your payment has been declined - HRT PPC";
const PAYMENT_CANCELLED_TITLE = "Your payment has been cancelled - HRT PPC";

const START_DATE_TODAY = {
  url: URLS.CERTIFICATE_START_DATE,
  formData: () => {
    const { day, month, year, iso } = currentDate();
    return {
      prescriptionStartFromToday: "yes",
      "certificateStartDate-day": `${day}`,
      "certificateStartDate-month": `${month}`,
      "certificateStartDate-year": `${year}`,
      certificateStartDate: iso,
    };
  },
};

const applyEligible = [
  {
    url: URLS.IS_MEDICINE_COVERED,
    formData: () => {
      return {
        isMedicineCovered: "yes",
      };
    },
  },
  {
    url: URLS.WHERE_YOU_COLLECT,
    formData: () => {
      return {
        prescriptionCountryName: "wales",
      };
    },
  },
  {
    url: URLS.DO_YOU_WANT_TO_CONTINUE,
    formData: () => {
      return {
        doYouStillWantToBuyCountry: "yes",
      };
    },
  },
  {
    url: URLS.ENTER_DOB,
    formData: () => ({
      "dateOfBirth-day": "1",
      "dateOfBirth-month": "10",
      "dateOfBirth-year": "1980",
      dateOfBirth: "1980-10-01",
    }),
  },
  {
    url: URLS.ENTER_NAME,
    formData: () => {
      return {
        firstName: "Lisa",
        lastName: "Smith",
      };
    },
  },
  {
    url: URLS.ENTER_POSTCODE,
    formData: () => {
      return {
        postcode: POSTCODE,
      };
    },
  },
  {
    url: URLS.SELECT_ADDRESS,
    formData: () => {
      return {};
    },
  },
  {
    url: URLS.ENTER_ADDRESS,
    formData: () => {
      return {
        addressLine1: "Flat b",
        addressLine2: "123 Fake Street",
        townOrCity: "Springfield",
        postcode: "AA1 1AA",
      };
    },
  },
  {
    url: URLS.NHS_NUMBER,
    formData: () => {
      return {
        doYouKnowYourNhsNumber: "yes",
        nhsNumber: 9992158107,
      };
    },
  },
  START_DATE_TODAY,
  {
    url: URLS.EMAIL_ADDRESS,
    formData: () => {
      return {
        doYouWantEmail: "yes",
        emailAddress: "test@domain.com",
      };
    },
  },
  {
    url: URLS.CHECK_ANSWERS,
    formData: () => ({}),
  },
  {
    url: URLS.PROCESS_PAYMENT,
    callPreviousRedirect: () => true,
  },
  {
    url: URLS.APPLICATION_COMPLETE_URL,
    formData: () => ({}),
    issueChecks: [
      (url, result) =>
        result.documentTitle !== APPLICATION_SUCCESSFUL_TITLE
          ? `Expected title to be ${APPLICATION_SUCCESSFUL_TITLE}, instead got ${result.documentTitle}`
          : null,
    ],
  },
];

const applyNotEligibleOutsideEngland = [
  {
    url: URLS.IS_MEDICINE_COVERED,
    formData: () => {
      return {
        isMedicineCovered: "yes",
      };
    },
  },
  {
    url: URLS.WHERE_YOU_COLLECT,
    formData: () => {
      return {
        prescriptionCountryName: "wales",
      };
    },
  },
  {
    url: URLS.DO_YOU_WANT_TO_CONTINUE,
    formData: () => {
      return {
        doYouStillWantToBuyCountry: "no",
      };
    },
  },
  {
    url: URLS.OUTSIDE_ENGLAND,
    formData: () => ({}),
  },
];

const applyAgeEntitledToFreePrescription = [
  {
    url: URLS.IS_MEDICINE_COVERED,
    formData: () => {
      return {
        isMedicineCovered: "yes",
      };
    },
  },
  {
    url: URLS.WHERE_YOU_COLLECT,
    formData: () => {
      return {
        prescriptionCountryName: "england",
      };
    },
  },
  {
    url: URLS.ENTER_DOB,
    formData: () => ({
      "dateOfBirth-day": "1",
      "dateOfBirth-month": "10",
      "dateOfBirth-year": "1955",
      dateOfBirth: "1955-10-01",
    }),
  },
  {
    url: URLS.AGE_ENTITLED_TO_FREE_PRESCRIPTIONS,
    formData: () => ({}),
  },
];

const applyYourExistingExemptionFound = [
  {
    url: URLS.IS_MEDICINE_COVERED,
    formData: () => {
      return {
        isMedicineCovered: "yes",
      };
    },
  },
  {
    url: URLS.WHERE_YOU_COLLECT,
    formData: () => {
      return {
        prescriptionCountryName: "wales",
      };
    },
  },
  {
    url: URLS.DO_YOU_WANT_TO_CONTINUE,
    formData: () => {
      return {
        doYouStillWantToBuyCountry: "yes",
      };
    },
  },
  {
    url: URLS.ENTER_DOB,
    formData: () => ({
      "dateOfBirth-day": "1",
      "dateOfBirth-month": "10",
      "dateOfBirth-year": "1980",
      dateOfBirth: "1980-10-01",
    }),
  },
  {
    url: URLS.ENTER_NAME,
    formData: () => {
      return {
        firstName: "Lisa",
        lastName: "Smith",
      };
    },
  },
  {
    url: URLS.ENTER_POSTCODE,
    formData: () => {
      return {
        postcode: POSTCODE,
      };
    },
  },
  {
    url: URLS.EXISTING_EXEMPTION_FOUND,
    formData: () => ({}),
  },
];

const applyMedicineNotCovered = [
  {
    url: URLS.IS_MEDICINE_COVERED,
    formData: () => {
      return {
        isMedicineCovered: "no",
      };
    },
  },
  {
    url: URLS.MEDICINE_NOT_COVERED,
    formData: () => ({}),
  },
];

const applySomeMedicineAreNotCovered = [
  {
    url: URLS.IS_MEDICINE_COVERED,
    formData: () => {
      return {
        isMedicineCovered: "some",
      };
    },
  },
  {
    url: URLS.SOME_MEDICINE_COVERED,
    formData: () => ({}),
  },
];

const applyPaymentDeclined = [
  {
    url: URLS.IS_MEDICINE_COVERED,
    formData: () => {
      return {
        isMedicineCovered: "yes",
      };
    },
  },
  {
    url: URLS.WHERE_YOU_COLLECT,
    formData: () => {
      return {
        prescriptionCountryName: "wales",
      };
    },
  },
  {
    url: URLS.DO_YOU_WANT_TO_CONTINUE,
    formData: () => {
      return {
        doYouStillWantToBuyCountry: "yes",
      };
    },
  },
  {
    url: URLS.ENTER_DOB,
    formData: () => ({
      "dateOfBirth-day": "1",
      "dateOfBirth-month": "10",
      "dateOfBirth-year": "1980",
      dateOfBirth: "1980-10-01",
    }),
  },
  {
    url: URLS.ENTER_NAME,
    formData: () => {
      return {
        firstName: "ERROR",
        lastName: "Smith",
      };
    },
  },
  {
    url: URLS.ENTER_POSTCODE,
    formData: () => {
      return {
        postcode: POSTCODE,
      };
    },
  },
  {
    url: URLS.SELECT_ADDRESS,
    formData: () => {
      return {};
    },
  },
  {
    url: URLS.ENTER_ADDRESS,
    formData: () => {
      return {
        addressLine1: "Flat b",
        addressLine2: "123 Fake Street",
        townOrCity: "Springfield",
        postcode: "AA1 1AA",
      };
    },
  },
  {
    url: URLS.NHS_NUMBER,
    formData: () => {
      return {
        doYouKnowYourNhsNumber: "yes",
        nhsNumber: 9992158107,
      };
    },
  },
  START_DATE_TODAY,
  {
    url: URLS.EMAIL_ADDRESS,
    formData: () => {
      return {
        doYouWantEmail: "yes",
        emailAddress: "test@domain.com",
      };
    },
  },
  {
    url: URLS.CHECK_ANSWERS,
    formData: () => ({}),
  },
  {
    url: URLS.PROCESS_PAYMENT,
    callPreviousRedirect: () => true,
  },
  {
    url: URLS.PAYMENT_DECLINED,
    formData: () => ({}),
    issueChecks: [
      (url, result) =>
        result.documentTitle !== PAYMENT_DECLINED_TITLE
          ? `Expected title to be ${PAYMENT_DECLINED_TITLE}, instead got ${result.documentTitle}`
          : null,
    ],
  },
];

const applyPaymentCancelled = [
  {
    url: URLS.IS_MEDICINE_COVERED,
    formData: () => {
      return {
        isMedicineCovered: "yes",
      };
    },
  },
  {
    url: URLS.WHERE_YOU_COLLECT,
    formData: () => {
      return {
        prescriptionCountryName: "wales",
      };
    },
  },
  {
    url: URLS.DO_YOU_WANT_TO_CONTINUE,
    formData: () => {
      return {
        doYouStillWantToBuyCountry: "yes",
      };
    },
  },
  {
    url: URLS.ENTER_DOB,
    formData: () => ({
      "dateOfBirth-day": "1",
      "dateOfBirth-month": "10",
      "dateOfBirth-year": "1980",
      dateOfBirth: "1980-10-01",
    }),
  },
  {
    url: URLS.ENTER_NAME,
    formData: () => {
      return {
        firstName: "CANCELLED",
        lastName: "Smith",
      };
    },
  },
  {
    url: URLS.ENTER_POSTCODE,
    formData: () => {
      return {
        postcode: POSTCODE,
      };
    },
  },
  {
    url: URLS.SELECT_ADDRESS,
    formData: () => {
      return {};
    },
  },
  {
    url: URLS.ENTER_ADDRESS,
    formData: () => {
      return {
        addressLine1: "Flat b",
        addressLine2: "123 Fake Street",
        townOrCity: "Springfield",
        postcode: "AA1 1AA",
      };
    },
  },
  {
    url: URLS.NHS_NUMBER,
    formData: () => {
      return {
        doYouKnowYourNhsNumber: "yes",
        nhsNumber: 9992158107,
      };
    },
  },
  START_DATE_TODAY,
  {
    url: URLS.EMAIL_ADDRESS,
    formData: () => {
      return {
        doYouWantEmail: "yes",
        emailAddress: "test@domain.com",
      };
    },
  },
  {
    url: URLS.CHECK_ANSWERS,
    formData: () => ({}),
  },
  {
    url: URLS.PROCESS_PAYMENT,
    callPreviousRedirect: () => true,
  },
  {
    url: URLS.PAYMENT_CANCELLED,
    formData: () => ({}),
    issueChecks: [
      (url, result) =>
        result.documentTitle !== PAYMENT_CANCELLED_TITLE
          ? `Expected title to be ${PAYMENT_CANCELLED_TITLE}, instead got ${result.documentTitle}`
          : null,
    ],
  },
];

const applySessionTimeout = [
  {
    url: URLS.IS_MEDICINE_COVERED,
    formData: () => {
      return {
        isMedicineCovered: "yes",
      };
    },
  },
  {
    url: URLS.WHERE_YOU_COLLECT,
    formData: () => {
      return {
        prescriptionCountryName: "wales",
      };
    },
  },
  {
    url: URLS.SESSION_TIMEOUT,
    formData: () => ({}),
  },
];

export {
  applyEligible,
  applyNotEligibleOutsideEngland,
  applyAgeEntitledToFreePrescription,
  applyYourExistingExemptionFound,
  applyMedicineNotCovered,
  applyPaymentCancelled,
  applyPaymentDeclined,
  applySomeMedicineAreNotCovered,
  applySessionTimeout,
};
