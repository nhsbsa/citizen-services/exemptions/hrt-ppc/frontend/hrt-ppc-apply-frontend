import { isStepEnabled } from "../../../web/routes/register-steps/register-steps";
import { features } from "../../../config/features";
import { URLS } from "../paths";

const checkPageUrl = (url, result) => {
  if (url === URLS.PROCESS_PAYMENT) {
    return null;
  }
  return result.pageUrl !== url
    ? `Expected to GET ${url}, but was ${result.pageUrl}`
    : null;
};

// Add a default page URL check to every test
const addCheckUrl = (test) => ({
  ...test,
  issueChecks: !test.issueChecks
    ? [checkPageUrl]
    : [...test.issueChecks, checkPageUrl],
});

const configureTestSuite = (testSuite) =>
  testSuite.map(addCheckUrl).filter(isStepEnabled(features));

export { configureTestSuite };
