import {
  applyEligible,
  applyNotEligibleOutsideEngland,
  applyAgeEntitledToFreePrescription,
  applyMedicineNotCovered,
  applyYourExistingExemptionFound,
  applyPaymentCancelled,
  applyPaymentDeclined,
  applySessionTimeout,
} from "./apply";
import { applyPageNotFound, applyProblemWithService } from "./apply-errors";
import { applyCookiesNotEnabled, applyCookiesUpdate } from "./apply-cookies";
import { configureTestSuite } from "./configure-test-suite";

const ALL_TESTS = [
  applyEligible,
  applyMedicineNotCovered,
  applyNotEligibleOutsideEngland,
  applyYourExistingExemptionFound,
  applyAgeEntitledToFreePrescription,
  applyCookiesUpdate,
  applyCookiesNotEnabled,
  applyPageNotFound,
  applyProblemWithService,
  applyPaymentDeclined,
  applyPaymentCancelled,
  applySessionTimeout,
];

const TEST_SUITE = ALL_TESTS.map(configureTestSuite);

export { TEST_SUITE };
