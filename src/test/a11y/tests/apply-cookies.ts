import { YES } from "../../../web/routes/application/steps/common/constants";
import { URLS } from "../paths";

const applyCookiesNotEnabled = [
  {
    url: URLS.ENABLE_COOKIES,
    cookieDisabled: true,
    formData: () => ({}),
  },
];

const applyCookiesUpdate = [
  {
    url: URLS.COOKIES,
    formData: () => ({}),
  },
  {
    url: URLS.COOKIES_DECLARATION,
    formData: () => ({
      measureStatistics: YES,
    }),
  },
  {
    url: URLS.COOKIES_CONFIRMATION,
    formData: () => ({}),
  },
];

export { applyCookiesNotEnabled, applyCookiesUpdate };
