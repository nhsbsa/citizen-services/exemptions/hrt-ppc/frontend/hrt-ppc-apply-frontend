import { URLS } from "../paths";

const applyPageNotFound = [
  {
    url: URLS.PAGE_NOT_FOUND,
    formData: () => ({}),
  },
];
const applyProblemWithService = [
  {
    url: URLS.IS_MEDICINE_COVERED,
    formData: () => {
      return {
        isMedicineCovered: "yes",
      };
    },
  },
  {
    url: URLS.WHERE_YOU_COLLECT,
    formData: () => {
      return {
        prescriptionCountryName: "england",
      };
    },
  },
  {
    url: URLS.PROBLEM_WITH_SERVICE,
    formData: () => ({}),
  },
];

export { applyPageNotFound, applyProblemWithService };
