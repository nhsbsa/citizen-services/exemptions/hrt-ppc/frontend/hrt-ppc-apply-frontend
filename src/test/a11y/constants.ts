const POSTCODE = "BS14TB";
const PHONE_NUMBER = "07123456789";
const EMAIL_ADDRESS = "test@email.com";
const TEXT = "text";

export { POSTCODE, PHONE_NUMBER, EMAIL_ADDRESS, TEXT };
