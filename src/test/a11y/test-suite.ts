/* no-process-exit */
"use strict";

import pa11y from "pa11y";
import { Promise } from "bluebird";
import { handleTestResults } from "./results";
import { IGNORE_RULES } from "./ignore-rules";
import { getSIDCookieAndCSRFToken, postFormData } from "./request";
import { URLS, BASE_URL } from "./paths";
import { notIsNil } from "../../common/predicates";

const FIRST_APPLY_PAGE_URL = URLS.IS_MEDICINE_COVERED;

/*
  Runs though the application, evaluating each page and performing post requests to populate the necessary
  data in the database.
 */
const runPa11yTests = async (pages) => {
  try {
    const { sessionCookie, csrfCookie, csrfToken } =
      await getSIDCookieAndCSRFToken(FIRST_APPLY_PAGE_URL);
    let headers = {};
    const formData = { _csrf: csrfToken };
    const results: any = [];
    let previousPageRedirect;

    await Promise.each(pages, async (page) => {
      console.log("Testing", page.url);

      if (page.cookieDisabled === false || page.cookieDisabled === undefined) {
        headers = {
          Cookie: `${sessionCookie};isBrowserCookieEnabled=true;${csrfCookie};`,
        };
      }

      let url = page.url;
      if (typeof page.callPreviousRedirect !== "undefined") {
        url = previousPageRedirect;
      }
      const result = await pa11y(url, {
        ignore: IGNORE_RULES,
        headers,
        timeout: 45000,
        chromeLaunchConfig: {
          args: ["--no-sandbox"],
        },
      });

      const customIssues = page.issueChecks.map((checkFn) =>
        checkFn(page.url, result),
      );
      result.issues = result.issues.concat(customIssues.filter(notIsNil));

      results.push(result);

      // POST form data to allow next a11y page test
      if (typeof page.formData !== "undefined") {
        const pageFormData = await page.formData(sessionCookie, csrfCookie);
        previousPageRedirect = await postFormData(
          page.url,
          { ...formData, ...pageFormData },
          sessionCookie,
          csrfCookie,
        );

        if (previousPageRedirect !== undefined) {
          previousPageRedirect = previousPageRedirect.replace("&#x3D;", "=");
        }
      }
    });

    return results;
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const runTestsAndAggregateResults = async (acc, pages) => {
  const testResults = await runPa11yTests(pages);
  return [...acc, ...testResults];
};

const runAllTests = async (testSuite) => {
  try {
    console.log(`Running accessibility tests against ${BASE_URL}`);

    const results = await Promise.reduce(
      testSuite,
      runTestsAndAggregateResults,
      [],
    );
    handleTestResults(results);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

export { runAllTests };
