#!/bin/bash

os_places_port=${OS_PLACES_PORT:-8300}
product_service_port=${PRODUCT_SERVICE_PORT:-8120}
hrt_search_service_port=${HRT_SEARCH_SERVICE_PORT:-8400}
issue_certificate_service_port=${ISSUE_CERTIFICATE_SERVICE_PORT:-8500}
payment_service_port=${PAYMENT_SERVICE_PORT:-8110}
access_control_service_port=${ACCESS_CONTROL_SERVICE_PORT:-8303}
cps_service_port=${CARD_PAYMENT_SERVICE_PORT:-8302}

curl -d "{}" http://localhost:${hrt_search_service_port}/__admin/shutdown

curl -d "{}" http://localhost:${os_places_port}/__admin/shutdown

curl -d "{}" http://localhost:${product_service_port}/__admin/shutdown

curl -d "{}" http://localhost:${issue_certificate_service_port}/__admin/shutdown

curl -d "{}" http://localhost:${payment_service_port}/__admin/shutdown

curl -d "{}" http://localhost:${access_control_service_port}/__admin/shutdown

curl -d "{}" http://localhost:${cps_service_port}/__admin/shutdown
