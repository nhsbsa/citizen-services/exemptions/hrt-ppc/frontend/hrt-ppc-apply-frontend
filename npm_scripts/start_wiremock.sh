#!/bin/bash

bin_dir=${BIN_DIR:-./bin}
os_places_port=${OS_PLACES_PORT:-8300}
product_service_port=${PRODUCT_SERVICE_PORT:-8120}
hrt_search_service_port=${HRT_SEARCH_SERVICE_PORT:-8400}
issue_certificate_service_port=${ISSUE_CERTIFICATE_SERVICE_PORT:-8500}
payment_service_port=${PAYMENT_SERVICE_PORT:-8110}
access_control_service_port=${ACCESS_CONTROL_SERVICE_PORT:-8303}
cps_service_port=${CARD_PAYMENT_SERVICE_PORT:-8302}

SCRIPT_DIR=$(dirname "$0")
source ${SCRIPT_DIR}/wait_for_url.sh

mkdir -p ${bin_dir}
if [ ! -f ${bin_dir}/wiremock.jar ]; then
  wget https://repo1.maven.org/maven2/com/github/tomakehurst/wiremock-standalone/2.27.2/wiremock-standalone-2.27.2.jar -O ${bin_dir}/wiremock.jar
fi

echo "Starting Wiremock on port ${os_places_port} for OS places"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${os_places_port} &

wait_for_url http://localhost:${os_places_port}/__admin

echo "Starting Wiremock on port ${product_service_port} for product service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${product_service_port} --root-dir npm_scripts/mappings_product-api &

wait_for_url http://localhost:${product_service_port}/__admin

echo "Starting Wiremock on port ${hrt_search_service_port} for hrt search service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${hrt_search_service_port} &

wait_for_url http://localhost:${hrt_search_service_port}/__admin

echo "Starting Wiremock on port ${issue_certificate_service_port} for hrt issue certificate service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${issue_certificate_service_port} &

wait_for_url http://localhost:${issue_certificate_service_port}/__admin

echo "Starting Wiremock on port ${payment_service_port} for hrt payment service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${payment_service_port} &

wait_for_url http://localhost:${payment_service_port}/__admin

echo "Starting Wiremock on port ${access_control_service_port} for access control service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${access_control_service_port} &

wait_for_url http://localhost:${access_control_service_port}/__admin

echo "Starting Wiremock on port ${cps_service_port} for card payment service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --local-response-templating --port ${cps_service_port} &

wait_for_url http://localhost:${cps_service_port}/__admin

echo "Wiremock started"
