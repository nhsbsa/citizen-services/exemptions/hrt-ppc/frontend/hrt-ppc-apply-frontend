# HRT (Hormone replacement therapy) apply frontend

A developer's guide for working with the **Hormone replacement therapy** applicant web UI is available in the [project wiki](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/).

## [Getting started](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Getting-started)

* [System requirements](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Getting-started#system-requirements)
* [Installing dependencies](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Getting-started#installing-dependencies)
* [Setting environment variables](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Getting-started#setting-environment-variables)
* [Running the application](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Getting-started#running-the-application)

## [Application structure](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Application-structure)

* [Defining a step](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Defining-a-step)
* [Defining a journey](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Defining-a-journey)
* [Journey flow control](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Journey-flow-control)
* [Feature toggle](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Feature-toggle)
* [Error handling](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Error-handling)
* [Internationalisation](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Internationalisation)

## [Testing the application](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Testing-the-application)

* [Linting](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Testing-the-application#linting)
* [Unit tests](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Testing-the-application#unit-tests)
* [Acceptance and compatibility tests](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Testing-the-application#acceptance-and-compatibility-tests)
* [Performance tests](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Testing-the-application#performance-tests)
* [A11y tests](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Testing-the-application#a11y-tests)
* [Session details provider](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/Session-details-provider)

## [User interface](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/User-interface)

* [Design patterns](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/User-interface#design-patterns)
* [Templates](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/User-interface#templates)
* [Styles](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/hrt-ppc-apply-frontend/-/wikis/User-interface#styles)